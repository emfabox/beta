DROP TABLE IF EXISTS mwrblclients;
CREATE TABLE mwrblclients (
  clientip text,
  hour smallint(6)  NOT NULL default '0',
  count int(11)  NOT NULL default '0',
  ham int(11) NOT NULL default '0',
  spam int(11) NOT NULL default '0'
) TYPE=MyISAM;

DROP TABLE IF EXISTS mwrblconfig;
CREATE TABLE mwrblconfig (
  setting text,
  value text
) TYPE=MyISAM;

DROP TABLE IF EXISTS mwrblblock;
CREATE TABLE mwrblblock (
  clientip text,
  expires datetime NOT NULL default '0000-00-00 00:00:00',
  errormsg text
) TYPE=MyISAM;

