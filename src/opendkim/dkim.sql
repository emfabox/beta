
-- ----------------------------
-- Table structure for `dkim`
-- ----------------------------
DROP TABLE IF EXISTS `dkim`;
CREATE TABLE `dkim` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(255) NOT NULL,
  `selector` varchar(63) NOT NULL,
  `private_key` text,
  `public_key` text,
  `dns_text` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
