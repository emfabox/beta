
Please scroll down for the German translation.
Die deutsche Uebersetzung finden Sie im Anschluss an den englischen Text.

---- ENGLISH -----------

From: $from
To: $to
Subject: Unrequested mail rejected
X-%org-name%-EMFABox: generated

You have sent a mail message to us which was not requested and has been
rejected. Please do not send any more mail to us at this address:
  To: $to
  Subject: $subject 
  Date: $date 

If you have any questions about this, or you believe you have received
this message in error, please contact the site system administrators.

-- 
EMFABox
Email Security
%org-long-name%
%web-site%

---- DEUTSCH ----------- 

Von: $from
An: $to

Betreff: Unrequested mail rejected
X-%org-name%-EMFABox: generated

Sie haben uns eine E-Mail geschickt, welche nicht verlangt wurde und sind 
zurückgewiesen worden. Bitte schicken Sie uns kein Mail mehr an diese Adresse:
  An: $to 
  Betreff:  $subject
  Datum: $date 
  
Wenn Sie Fragen haben oder glauben, diese Nachricht zu Unrecht erhalten zu
haben, kontaktieren Sie bitte die Systemadministratoren.

-- 
EMFABox
Email Security
%org-long-name%
%web-site%

