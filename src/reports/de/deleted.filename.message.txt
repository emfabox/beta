Please scroll down for the German translation.
Die deutsche Uebersetzung finden Sie im Anschluss an den englischen Text.
---- ENGLISH -----------
This is a message from the EMFABox E-Mail Virus Protection Service
----------------------------------------------------------------------
The original e-mail attachment $filename is on the list of unacceptable attachments for this site and has been replaced by this warning message.
Due to limitations placed on us by the Regulation of Investigatory Powers Act 2000, we were unable to keep a copy of the original attachment.
$report ID: $id
--
EMFABox Email Security 
%org-long-name%
%web-site%
--- DEUTSCH -----------
Dies ist eine EMFABox E-Mail Virus Protection Service Nachricht
-------------------------------------------------------------------
Der ursprngliche E-Mail-Anhang $filename ist auf der Liste der unerwnschten Dateianhnge fr diese Domain und wurde durch diese Warnmeldung ersetzt. Aus datenschutzrechtlichen Grnden halten wir keine Kopie des Originalanhangs vor.
$report ID: $id
--
EMFABox Email Security
%org-long-name%
%web-site%
