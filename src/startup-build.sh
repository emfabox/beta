#!/bin/bash
########################################################################
# EMFABOX BETA Install                                                 #
# WORKS WITH                                                           #
# CentOS 6.5 NETINSTALL 64 BIT ONLY                                    #
########################################################################
# Copyright (C) 2014  http://www.cycomptec.com                         #
########################################################################
#
#
SOURCE_DIR=/usr/src/EMFA
VAR_TRACK=/opt/emfa/id
INSTALL_LOG_DIR=/opt/emfa/logs
CreateDate="`date +%Y%m%d-%s`"
InstallDate="`date +%Y-%m-%d`"
INSTALLER_SOURCE="https://bitbucket.org/emfabox/beta/raw/39e31fc13f96003539d3fccd338d526872b400f4/src/installer-beta.sh"

### START ###
#
clear
#
#
echo ""
echo -----testing host: "www.google.com" via ping-----
ping -c1 -W1 "www.google.com" | grep 'time=' > /dev/null
#echo $?
if [ $? -eq 0 ]
  then
  echo OK: "www.google.com" is up
  echo ""
  else
  echo "Internet is down or google.com is not accessible so we can not download ... Aborting."; echo;
  exit 192
fi



# OS
MACHINE_TYPE=`uname -m`

# CENTOS VERS

CENTOS=`cat /etc/centos-release`

# MEMORY
MY_VAR_MEM=$(awk -F":" '$1~/MemTotal/{print $2}' /proc/meminfo )

# PWD
BACK=`pwd`

# Check OS

if [ ${MACHINE_TYPE} == 'x86_64' ]; then

    # 64-bit
    printf "\n"
    printf "64-bit OS detected ...\n"
    printf "\n"
    
else

echo "32-bit OS detected ... Aborting."; echo;  
 
  exit 192
 
fi

sleep 3s
   

if [ "${CENTOS}" == 'CentOS release 6.5 (Final)' ]
  then
  printf "\n"
  printf "Good ... you are running CentOS 6.5 x64\n"
  printf "\n"
else

  echo "Unsupported system ... Aborting."; echo; 
  
  exit 192
fi

sleep 3s

clear
echo "***********************************************"
echo ""
echo ""
echo "           !!! W A R N I N G !!!"
echo ""
echo ""
echo "***********************************************"
echo ""
echo ""
echo "EMFABox Beta install for Centos 6.5 minimal x64"
echo ""
echo "Only for testing - now .... :/"
echo ""
echo ""
echo "***********************************************"
echo ""
echo ""
echo -n "Are you sure you want to continue? (y/N):"
read YN
flag=1
while [ $flag != "0" ]
    do 
      if [[ "$YN" == "Y" || "$YN" == "y" ]]; then 
        flag=0
      elif [[ "$YN" == "" || "$YN" == "N" || "$YN" == "n" ]]; then 
		echo "Aborting this setup"
        exit 1192
      else
          echo -n "Are you sure you want to continue? (y/N):"
          read YN
      fi
done   

# create track dir
if [ ! -d ${VAR_TRACK} ]; then
 
 mkdir -p ${VAR_TRACK}
 
fi

# create log dir
if [ ! -d ${INSTALL_LOG_DIR} ]; then
 
mkdir -p ${INSTALL_LOG_DIR}
 
fi

touch $VAR_TRACK/startup

 # logging starts here
( 

# make sure uuidgen,wget is installed
yum install -y util-linux-ng wget ntp

sleep 3s


#echo "NETWORKING=yes" > /etc/sysconfig/network
#echo "HOSTNAME=emfabox.local" >> /etc/sysconfig/network

echo "# Do not remove the following line, or various programs" > /etc/hosts
echo "# that require network functionality will fail." >> /etc/hosts
echo "127.0.0.1         localhost.localdomain localhost" >> /etc/hosts
#echo "127.0.0.1         emfabox.local emfabox " >> /etc/hosts



echo "driftfile /var/lib/ntp/drift" > /etc/ntp.conf
echo "server 0.pool.ntp.org" >> /etc/ntp.conf
echo "server 1.pool.ntp.org" >> /etc/ntp.conf
echo "server 2.pool.ntp.org" >> /etc/ntp.conf
echo "server 127.127.1.0" >> /etc/ntp.conf
echo "fudge 127.127.1.0 stratum 10" >> /etc/ntp.conf

sleep 1s

## http://usgcb.nist.gov/usgcb/content/configuration/workstation-ks.cfg

chkconfig rsyslog on
chkconfig rsyslog --levels 345 on

service rsyslog restart


sleep 1s
clear
sleep 5s

# SOURCE_DIR

if [ ! -d ${SOURCE_DIR} ]; then
 
mkdir -p ${SOURCE_DIR}
 
fi

# fetch the install script

/usr/bin/wget --no-check-certificate -O ${SOURCE_DIR}/install.sh ${INSTALLER_SOURCE}

cd ${SOURCE_DIR}

chmod 700 install.sh

if [ -f  ${SOURCE_DIR}/install.sh ]; then 

echo ""
echo -n "Do you want to start the build script? (y/N):"
read YN
flag=1
while [ $flag != "0" ]
    do 
      if [[ "$YN" == "Y" || "$YN" == "y" ]]; then
	    ${SOURCE_DIR}/install.sh
        flag=0
      elif [[ "$YN" == "" || "$YN" == "N" || "$YN" == "n" ]]; then 
		echo ""
		echo "Please don't forget to run the install script ...!"
        exit 1
      else
          echo -n "Do you want to start the install script? (y/N):"
          read YN
      fi
  done

else 
    echo "[Error] Script not found ... Aborting."; echo;
  
  exit 192
fi


 )2>&1 | tee -a ${INSTALL_LOG_DIR}/startup.log