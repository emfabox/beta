#!/bin/bash

SERVICE=sendmailanalyzer
DATE=`date`
OUTPUT=$(ps aux | grep -v grep | grep -v sendmailanalyzer | grep sendmailanalyzer)
#echo $OUTPUT
if [ "${#OUTPUT}" -gt 0 ] ;
then exit 0
else /etc/rc.d/init.d/sendmailanalyzer restart > /dev/null 2>&1
fi
