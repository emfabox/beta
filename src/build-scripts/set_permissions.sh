#!/bin/sh



# set permissions
#


#Postfix Dir
chown root:mtagroup /etc/postfix/main.cf
chmod 0664 /etc/postfix/main.cf
chown root:apache /etc/postfix/sasl_passwd
chmod 0664 /etc/postfix/sasl_passwd


chown root:mtagroup /etc/python-policyd-spf/policyd-spf.conf
chmod 0664 /etc/python-policyd-spf/policyd-spf.conf


chown root:mtagroup /etc/my.cnf
chmod 0664 /etc/my.cnf
chown root:mtagroup /etc/sysconfig/iptables
chmod 0664 /etc/sysconfig/iptables
chown root:mtagroup /etc/httpd/conf/httpd.conf
chmod 0664 /etc/httpd/conf/httpd.conf


chown -R root:mtagroup /etc/MailScanner/reports/
chown postfix:mtagroup /var/spool/MailScanner
chown postfix:mtagroup /var/spool/MailScanner/incoming
chown postfix:mtagroup /var/spool/MailScanner/quarantine
chown postfix.mtagroup /var/spool/MailScanner/spamassassin
chown postfix:mtagroup /var/spool/postfix
chown postfix:mtagroup /var/spool/postfix/incoming
chown -R root:mtagroup /var/www/html
find /var/www/html -type d -exec chmod 0655 {} \;
find /var/www/html -type f -exec chmod 0644 {} \;

#GEOIP
chmod 755 /var/www/html/master/lib/temp

find /etc/MailScanner/reports/ -type d -exec chmod 0655 {} \;
find /etc/MailScanner/reports/ -type f -exec chmod 0664 {} \;
chmod g+w /var/spool/MailScanner/*
touch /var/spool/MailScanner/incoming/SpamAssassin.cache.db
chown postfix:mtagroup /var/spool/MailScanner/incoming/SpamAssassin.cache.db
touch /var/spool/MailScanner/incoming/Processing.db
chown postfix:mtagroup /var/spool/MailScanner/incoming/Processing.db


mkdir -p /var/spool/postfix/hold
chown postfix:mtagroup /var/spool/postfix/hold
chmod 0775 /var/spool/postfix/hold 