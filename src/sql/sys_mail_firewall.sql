
-- ----------------------------
-- Table structure for `sys_mail_firewall`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mail_firewall`;
CREATE TABLE `sys_mail_firewall` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_perm_user` varchar(5) DEFAULT NULL,
  `sys_perm_group` varchar(5) DEFAULT NULL,
  `sys_perm_other` varchar(5) DEFAULT NULL,
  `server_id` int(11) unsigned NOT NULL DEFAULT '0',
  `block_ip` varchar(255) NOT NULL,
  `block_ip_mask` varchar(255) NOT NULL,
  `tcp_port` varchar(255) DEFAULT NULL,
  `udp_port` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `active` enum('n','y') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
