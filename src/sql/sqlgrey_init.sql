-- ----------------------------
-- Table structure for `config`
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `parameter` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`parameter`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `connect`
-- ----------------------------
DROP TABLE IF EXISTS `connect`;
CREATE TABLE `connect` (
  `sender_name` varchar(64) NOT NULL,
  `sender_domain` varchar(255) NOT NULL,
  `src` varchar(39) NOT NULL,
  `rcpt` varchar(255) NOT NULL,
  `first_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `connect_idx` (`src`,`sender_domain`,`sender_name`) USING BTREE,
  KEY `connect_fseen` (`first_seen`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `domain_awl`
-- ----------------------------
DROP TABLE IF EXISTS `domain_awl`;
CREATE TABLE `domain_awl` (
  `sender_domain` varchar(255) NOT NULL,
  `src` varchar(39) NOT NULL,
  `first_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_seen` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`src`,`sender_domain`),
  KEY `domain_awl_lseen` (`last_seen`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `from_awl`
-- ----------------------------
DROP TABLE IF EXISTS `from_awl`;
CREATE TABLE `from_awl` (
  `sender_name` varchar(64) NOT NULL,
  `sender_domain` varchar(255) NOT NULL,
  `src` varchar(39) NOT NULL,
  `first_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_seen` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`src`,`sender_domain`,`sender_name`),
  KEY `from_awl_lseen` (`last_seen`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `optin_domain`
-- ----------------------------
DROP TABLE IF EXISTS `optin_domain`;
CREATE TABLE `optin_domain` (
  `domain` varchar(255) NOT NULL,
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `optin_email`
-- ----------------------------
DROP TABLE IF EXISTS `optin_email`;
CREATE TABLE `optin_email` (
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `optout_domain`
-- ----------------------------
DROP TABLE IF EXISTS `optout_domain`;
CREATE TABLE `optout_domain` (
  `domain` varchar(255) NOT NULL,
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- ----------------------------
-- Table structure for `optout_email`
-- ----------------------------
DROP TABLE IF EXISTS `optout_email`;
CREATE TABLE `optout_email` (
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
exit


