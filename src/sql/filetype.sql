/*
Date: 2015-03-25 16:56:47
*/

-- ----------------------------
-- Table structure for `filetype`
-- ----------------------------
DROP TABLE IF EXISTS `filetype`;
CREATE TABLE `filetype` (
  `mid` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `rule` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of filetype
-- ----------------------------
INSERT INTO `filetype` VALUES ('1', 'QuickTime', 'qt', '.qt', 'Apple QuickTime', 'Apple QuickTime movies and audio.', '1');
INSERT INTO `filetype` VALUES ('2', 'AVI', 'avi', '.avi', 'AVI Movies or Audio', 'Movies or audio files.', '1');
INSERT INTO `filetype` VALUES ('3', 'ELF', 'elf', '.elf', 'Executable Linkable Format', 'SUN Systems executable.', '0');
INSERT INTO `filetype` VALUES ('4', 'executable', 'Executable', 'many', 'Executables', 'All executable file types from any OS in any language. ', '0');
INSERT INTO `filetype` VALUES ('5', 'MNG', 'mng', '.mng', 'MNG or PNG Movies', 'Movies or audio files.', '1');
INSERT INTO `filetype` VALUES ('6', 'MPEG', 'mpeg', '.mpg .mpeg .mp3 .mp4', 'MPEG Movies or Audio', 'Movies or audio files. This includes .mp3\'s.', '1');
INSERT INTO `filetype` VALUES ('7', 'self-extract', 'self-extract', '.exe', 'Self-extracting Archives', 'Self extracting archives are typically zipped archives with a \".exe\" extension.', '0');
INSERT INTO `filetype` VALUES ('8', 'ASF', 'asf', '.asf .qt', 'Windows Media Files', 'Movies or audio files.', '1');
INSERT INTO `filetype` VALUES ('9', 'metafont', 'mf', '.mf', 'Windows Metafont', 'Windows Metafont Drawings.', '1');
