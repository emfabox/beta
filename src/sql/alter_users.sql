  ALTER TABLE `users`
  ADD `email` varchar(256) NOT NULL,
  ADD `confirmation` char(40) NOT NULL DEFAULT '',
  ADD `regdate` int(11) unsigned NOT NULL,
  ADD  `last_login` datetime NOT NULL,
  ADD `num_logins` int(11) NOT NULL,
  ADD`groupid` int(2) unsigned NOT NULL DEFAULT '1',
  ADD `memo` text NOT NULL,
  ADD`enabled` int(1) NOT NULL DEFAULT '1',
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`)