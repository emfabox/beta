
-- ----------------------------
-- Table structure for `ms_config_tmp`
-- ----------------------------
DROP TABLE IF EXISTS `ms_config_tmp`;
CREATE TABLE `ms_config_tmp` (
  `variable_name` varchar(45) NOT NULL,
  `value` text,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ms_config_tmp
-- ----------------------------
