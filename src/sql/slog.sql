-- ----------------------------
-- Table structure for `slog`
-- ----------------------------
DROP TABLE IF EXISTS `slog`;
CREATE TABLE `slog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stype` text,
  `hour` text,
  `sid` text,
  `sender` text,
  `size` text,
  `nrcpts` text,
  `relay` text,
  `rule` text,
  `arg1` text,
  `status` text,
  `sourceid` text,
  `rcpt` text,
  `from_address` text,
  `to_address` text,
  `spam` text,
  `action` text,
  `file` text,
  `virus` text,
  `error` text,
  `mech` text,
  `type` text,
  `score` text,
  `sender_ip` text,
  `cache` text,
  `autolearn` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1105 DEFAULT CHARSET=utf8;
