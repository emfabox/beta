-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.1.73-log - Source distribution
-- Server OS:                    redhat-linux-gnu
-- HeidiSQL Version:             9.2.0.4950
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for mailscanner
DROP DATABASE IF EXISTS `mailscanner`;
CREATE DATABASE IF NOT EXISTS `mailscanner` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `mailscanner`;


-- Dumping structure for table mailscanner.alias
DROP TABLE IF EXISTS `alias`;
CREATE TABLE IF NOT EXISTS `alias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `goto` text COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ldap_account_suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.allowed_hosts
DROP TABLE IF EXISTS `allowed_hosts`;
CREATE TABLE IF NOT EXISTS `allowed_hosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IP` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `host_name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Client` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Comments` text COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.api_servers
DROP TABLE IF EXISTS `api_servers`;
CREATE TABLE IF NOT EXISTS `api_servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `ipaddress` varchar(255) NOT NULL,
  `password_hash` text NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ipaddress` (`ipaddress`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.api_tasks
DROP TABLE IF EXISTS `api_tasks`;
CREATE TABLE IF NOT EXISTS `api_tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task` text NOT NULL,
  `sender_name` text NOT NULL,
  `sender_ip` text NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `var1` text NOT NULL,
  `var2` text NOT NULL,
  `var3` text NOT NULL,
  `var4` text NOT NULL,
  `var5` text NOT NULL,
  `var6` text NOT NULL,
  `var7` text NOT NULL,
  `var8` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.api_users
DROP TABLE IF EXISTS `api_users`;
CREATE TABLE IF NOT EXISTS `api_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` text NOT NULL,
  `api_key` varchar(32) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.audit_log
DROP TABLE IF EXISTS `audit_log`;
CREATE TABLE IF NOT EXISTS `audit_log` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `action` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.blacklist
DROP TABLE IF EXISTS `blacklist`;
CREATE TABLE IF NOT EXISTS `blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_address` text COLLATE utf8_unicode_ci,
  `to_domain` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blacklist_uniq` (`to_address`(100),`from_address`(100))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.captcha
DROP TABLE IF EXISTS `captcha`;
CREATE TABLE IF NOT EXISTS `captcha` (
  `captcha_id` int(11) NOT NULL AUTO_INCREMENT,
  `captcha_phpsessid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `captcha_time` int(11) NOT NULL,
  `captcha_captcha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`captcha_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dkim
DROP TABLE IF EXISTS `dkim`;
CREATE TABLE IF NOT EXISTS `dkim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selector` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `private_key` text COLLATE utf8_unicode_ci,
  `public_key` text COLLATE utf8_unicode_ci,
  `dns_text` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dkim_config
DROP TABLE IF EXISTS `dkim_config`;
CREATE TABLE IF NOT EXISTS `dkim_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selector` varchar(63) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `private_key` text COLLATE utf8_unicode_ci,
  `public_key` text COLLATE utf8_unicode_ci,
  `SigningTable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KeyTable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dkim_trustedhosts
DROP TABLE IF EXISTS `dkim_trustedhosts`;
CREATE TABLE IF NOT EXISTS `dkim_trustedhosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dnsbl_domains
DROP TABLE IF EXISTS `dnsbl_domains`;
CREATE TABLE IF NOT EXISTS `dnsbl_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.dobrblconfig
DROP TABLE IF EXISTS `dobrblconfig`;
CREATE TABLE IF NOT EXISTS `dobrblconfig` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.domain
DROP TABLE IF EXISTS `domain`;
CREATE TABLE IF NOT EXISTS `domain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `aliases` int(10) NOT NULL DEFAULT '0',
  `mailboxes` int(10) NOT NULL DEFAULT '0',
  `maxquota` bigint(20) NOT NULL DEFAULT '0',
  `quota` bigint(20) NOT NULL DEFAULT '0',
  `transport` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'dovecot',
  `desthost` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `backupmx` tinyint(1) NOT NULL DEFAULT '0',
  `avscan` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `opscan` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `antispoofing` varchar(55) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'REJECT',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.domain_admins
DROP TABLE IF EXISTS `domain_admins`;
CREATE TABLE IF NOT EXISTS `domain_admins` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.filename
DROP TABLE IF EXISTS `filename`;
CREATE TABLE IF NOT EXISTS `filename` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.filename: 70 rows
DELETE FROM `filename`;
/*!40000 ALTER TABLE `filename` DISABLE KEYS */;
INSERT INTO `filename` (`id`, `rule`, `type`, `ext`, `name`, `description`, `status`) VALUES
	('1', '\\.bak$', 'bak', '.bak', 'Backup files', 'Backup files.', '0'),
	('2', '\\.bz2$', 'bz2', '.bz2', 'Bzip Compressed File', 'Typically harmless.', '1'),
	('3', '\\{[a-hA-H0-9-]{25,}\\}', 'CLSID', '{[a-hA-H0-9-]{25,}}', 'CLSID', 'Files containing CLSID\'s are trying to hide their real type.', '0'),
	('4', '\\.Z$', 'z', '.z', 'Compressed File', 'Typically harmless.', '1'),
	('5', '\\s{10,}', 'whitespace', '> 10', 'Contiguous White Space', 'Filename contains a lot of white space. A long gap in a name is often used to hide part of it.', '0'),
	('6', '\\.fdf$', 'fdf', '.fdf', 'Dangerous Adobe Acrobat', 'Opening this file can cause auto-loading of any file from the internet.', '0'),
	('7', '\\.(mon|tue|wed|thu|fri|sat|sun)\\.[a-z0-9]{3}$', 'days', '.mon .tue .wed .thu .fri .sat .sun', 'Days of Week', 'This will allow file names with days of the week embedded in the file name. Example: report.mon.doc. ', '1'),
	('8', '\\.x\\d+\\.rel$', 'doc', '.doc, .docx', 'Document', 'Microsoft Word, etc.', '1'),
	('9', '\\.shb$', 'shb', '.shb', 'Document Shortcut', 'Possible Document Shortcut attack.', '0'),
	('10', '\\.vcf$', 'vcf', '.vcf', 'E-Business Card', 'Typically harmless.', '1'),
	('11', '\\.lnk$', 'lnk', '.lnk', 'Eudora Links', 'Attack that targets Eudora mail client.', '0'),
	('12', '\\.mhtml$', 'mhtml', '.mhtml', 'Eudora Meta Refresh', 'Possible Eudora meta-refresh attack. Eudora vulnerability.', '0'),
	('13', '\\.xnk$', 'xnk', '.xnk', 'Exchange Shortcut', 'Possible Microsoft Exchange Shortcut attack.', '0'),
	('14', '\\.gif$', 'gif', '.gif', 'GIF Image', 'Image file. Typically harmless.', '1'),
	('15', '\\.t?gz$', 'targz', '.tar.gz', 'Gzip & Tar Compressed File', 'Typically harmless.', '1'),
	('16', '\\.[a-z][a-z0-9]{2,3}\\s*\\.[a-z0-9]{3}$', 'double-ext', 'doc.exe, doc.zip, etc', 'Hidden Extensions', 'This stops users trying to hide file types by adding a different extension. By default doc.doc or zip.zip (or similar) is allowed. This will stop items like this: exe.doc, doc.zip, exe.txt, etc. ', '1'),
	('17', '\\.ins$', 'ins', '.ins', 'Internet Communications Setting', 'Possible Microsoft Internet Communications settings attack string.', '0'),
	('18', '\\.its$', 'its', '.its', 'Internet Document Set', 'Dangerous Internet Document Set (according to Microsoft)', '0'),
	('19', '\\.jpg$', 'jpg', '.jpg', 'JPEG Image', 'Image file. Typically harmless.', '1'),
	('20', '.{150,}', 'long', '> 150', 'Long File Name', 'Very long filenames are good signs of attacks against Microsoft e-mail packages.', '0'),
	('21', '\\.hqx$', 'hqx', '.qx', 'Macintosh Archive', 'Typically harmless.', '1'),
	('22', '\\.sea$', 'sea', '.sea', 'Macintosh Archive', 'Typically harmless.', '1'),
	('23', '\\.sit.bin$', 'sitbin', '.sit.bin', 'Macintosh Archive', 'Typically harmless.', '1'),
	('24', '\\.md[az]$', 'md', '.md[az]', 'Microsoft', 'Dangerous attachment type (according to Microsoft)', '0'),
	('25', '\\.vs[stw]$', 'vs', '.vs[stw]', 'Microsoft', 'Dangerous attachment type', '0'),
	('26', '\\.mau$', 'mau', '.mau', 'Microsoft', 'Dangerous attachment type (according to Microsoft)', '0'),
	('27', '\\.ma[dfgmqrsvw]$', 'ma', '.ma[dfgmqrsvw]', 'Microsoft Access', 'Microsoft Access shortcut attack.', '0'),
	('28', '\\.cab$', 'cab', '.cab', 'Microsoft Cabinet File', 'Cabinet files may hide viruses. Should not be emailed. ', '0'),
	('29', '\\.xml\\d*\\.rel$', 'xls', '.xml, .xls', 'Microsoft Excel', ' Microsoft Office Spread Sheet.', '1'),
	('30', '\\.hta$', 'hta', '.hta', 'Microsoft HTML Archive', 'Possible HTML archive attack.', '0'),
	('31', '\\.jse?$', 'jse', '.jse', 'Microsoft JScript', 'Possible Microsoft JScript attack.', '0'),
	('32', '\\.pst$', 'pst', '.pst', 'Microsoft Office', 'Dangerous Office Data File (according to Microsoft)', '0'),
	('33', '\\.prf$', 'prf', '.prf', 'Microsoft Outlook', 'Dangerous Outlook Profile Settings (according to Microsoft)', '0'),
	('34', '\\.pif$', 'pif', '.pif', 'Microsoft Shortcut', 'Probable Microsoft MS-Dos type shortcut attack.', '0'),
	('35', '\\.job$', 'job', '.job', 'Microsoft Task scheduler', 'Possible Microsoft Task scheduler attack.', '0'),
	('36', '\\.tmp$', 'tmp', '.tmp', 'Microsoft Temp File', 'Dangerous Temporary File', '0'),
	('37', '\\.vsmacros$', 'vsmacros', '. vsmacros', 'Microsoft Visual Studio', 'Dangerous Visual Studio Macros', '0'),
	('38', '\\.(jan|feb|mar|apr|may|jun|june|jul|july|aug|sep|sept|oct|nov|dec)\\.[a-z0-9]{3}$', 'months', '.jan .feb .mar .apr .may etc', 'Months Extension', 'This will allow documents with months in the file names. Example: report.dec.doc. ', '1'),
	('39', '\\.gpg$', 'gpg', '.gpg', 'PGP & GPG Signatures', 'Digital signatures attached as a file.', '1'),
	('40', '\\.pgp$', 'pgp', '.pgp', 'PGP & GPG Signatures', 'Typically harmless.', '1'),
	('41', '\\.asc$', 'pgpasc', '.asc', 'PGP & GPG Signatures', 'application/pgp-encrypted, application/pgp.', '1'),
	('42', '\\.sig$', 'sig', '.sig', 'PGP Signature', 'PKI signature via PGP or GPG. ', '1'),
	('43', '\\.rpm$', 'rpm', '.rpm', 'Redhat RPM Files', 'Typically harmless.', '1'),
	('44', '(\\.[a-z0-9]{3})\\1$', 'repeat-ext', 'zip.zip, doc.doc, etc', 'Repeat Extensions', 'This will allow files with repeat extensions. Examples: file.zip.zip or report.doc.doc. ', '1'),
	('45', '\\.rtf$', 'rtf', '.rtf', 'Rich Text', 'Rich text document. ', '1'),
	('46', '\\.cer$', 'cer', '.cer', 'Security Certificate', 'Security Certificate, which should never be mailed. ', '0'),
	('47', '\\.shs$', 'shs', '.shs', 'Shell Scrap', 'Possible Shell Scrap Object attack.', '0'),
	('48', '\\.cnf$', 'cnf', '.cnf', 'Speed Dial', 'Possible Speed Dial attack.', '0'),
	('49', '\\.sit$', 'sit', '.sit', 'StuffIt Archive', 'Typically harmless.', '1'),
	('50', '\\.tex$', 'tex', '.tex', 'Tex and Latex', 'Tex and Latex', '1'),
	('51', '\\.txt$', 'txt', '.txt', 'Text File', 'Typically harmless.', '1'),
	('52', '\\.vb[es]$', 'vb', '.vb[es]', 'Visual Basic', 'Possible Microsoft Visual Basic script attack.', '0'),
	('53', '\\.url$', 'url', '.url', 'Web Link', 'Typically harmless.', '1'),
	('54', '\\.cmd$', 'cmd', '.cmd', 'Windows Batch File', 'Possible malicious batch file script.', '0'),
	('55', '\\.cpl$', 'cpl', '.cpl', 'Windows Control Panel', 'Windows control panel item.', '0'),
	('56', '\\.cur$', 'cur', '.cur', 'Windows Cursor', 'Windows cursor file security vulnerability. Possible buffer overflow attempt.', '0'),
	('57', '\\.scf$', 'scf', '.scf', 'Windows Explorer', 'Possible Windows Explorer Command attack.', '0'),
	('58', '\\.chm$', 'chm', '.chm', 'Windows Help File', 'Possible compiled Help file based virus.', '0'),
	('59', '\\.hlp$', 'hlp', '.hlp', 'Windows Help File', 'Windows Help file security vulnerability. Possible buffer overflow attempt.', '0'),
	('60', '\\.ani$', 'ani', '.ani', 'Windows icon file', 'Possible buffer overflow in Windows', '0'),
	('61', '\\.ico$', 'ico', '.ico', 'Windows Icons', 'Windows animated cursor file security vulnerability. Possible buffer overflow attempt.', '0'),
	('62', '\\.reg$', 'reg', '.reg', 'Windows Registry', 'Possible Windows Registry attack.', '0'),
	('63', '\\.scr$', 'scr', '.scr', 'Windows Screensaver', 'Possible virus hidden in a screensaver.', '0'),
	('64', '\\.ws$', 'ws2', '.ws', 'Windows Script', 'Dangerous Windows Script ', '0'),
	('65', '\\.sct$', 'sct', '.sct', 'Windows Script Component', 'Possible Microsoft Windows Script Component attack.', '0'),
	('66', '\\.ws[cfh]$', 'ws', '.ws[cfh]', 'Windows Script Host', 'Possible Microsoft Windows Script Host attack.', '0'),
	('67', '\\.com$', 'com', '.com', 'Windows/DOS Executable', 'Windows/DOS Executable', '0'),
	('68', '\\.exe$', 'exe', '.exe', 'Windows/DOS Executable', 'Windows/DOS Executable', '0'),
	('69', '\\.bat$', 'bat', '.bat', 'Windows/MS-Dos Batch File', 'Possible malicious batch file script.', '0'),
	('70', '\\.zip$', 'zip', '.zip', 'Zip Compressed File', 'Typically harmless.', '1');
/*!40000 ALTER TABLE `filename` ENABLE KEYS */;


-- Dumping structure for table mailscanner.filetype
DROP TABLE IF EXISTS `filetype`;
CREATE TABLE IF NOT EXISTS `filetype` (
  `mid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ext` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.filetype: 9 rows
DELETE FROM `filetype`;
/*!40000 ALTER TABLE `filetype` DISABLE KEYS */;
INSERT INTO `filetype` (`mid`, `rule`, `type`, `ext`, `name`, `description`, `status`) VALUES
	('1', 'QuickTime', 'qt', '.qt', 'Apple QuickTime', 'Apple QuickTime movies and audio.', '1'),
	('2', 'AVI', 'avi', '.avi', 'AVI Movies or Audio', 'Movies or audio files.', '1'),
	('3', 'ELF', 'elf', '.elf', 'Executable Linkable Format', 'SUN Systems executable.', '0'),
	('4', 'executable', 'Executable', 'many', 'Executables', 'All executable file types from any OS in any language. ', '0'),
	('5', 'MNG', 'mng', '.mng', 'MNG or PNG Movies', 'Movies or audio files.', '1'),
	('6', 'MPEG', 'mpeg', '.mpg .mpeg .mp3 .mp4', 'MPEG Movies or Audio', 'Movies or audio files. This includes .mp3\'s.', '1'),
	('7', 'self-extract', 'self-extract', '.exe', 'Self-extracting Archives', 'Self extracting archives are typically zipped archives with a ".exe" extension.', '0'),
	('8', 'ASF', 'asf', '.asf .qt', 'Windows Media Files', 'Movies or audio files.', '1'),
	('9', 'metafont', 'mf', '.mf', 'Windows Metafont', 'Windows Metafont Drawings.', '1');
/*!40000 ALTER TABLE `filetype` ENABLE KEYS */;


-- Dumping structure for table mailscanner.geoip_country
DROP TABLE IF EXISTS `geoip_country`;
CREATE TABLE IF NOT EXISTS `geoip_country` (
  `begin_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `begin_num` bigint(20) DEFAULT NULL,
  `end_num` bigint(20) DEFAULT NULL,
  `iso_country_code` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` text COLLATE utf8_unicode_ci,
  KEY `geoip_country_begin` (`begin_num`),
  KEY `geoip_country_end` (`end_num`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.imp_mailuser
DROP TABLE IF EXISTS `imp_mailuser`;
CREATE TABLE IF NOT EXISTS `imp_mailuser` (
  `imp_userid` int(11) NOT NULL AUTO_INCREMENT,
  `sys_userid` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `sys_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_perm_user` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_group` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_other` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ad_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ad_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ad_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ad_username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ad_functions` text COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`imp_userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.inq
DROP TABLE IF EXISTS `inq`;
CREATE TABLE IF NOT EXISTS `inq` (
  `id` mediumtext COLLATE utf8_unicode_ci,
  `cdate` date DEFAULT NULL,
  `ctime` time DEFAULT NULL,
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `message` mediumtext COLLATE utf8_unicode_ci,
  `size` mediumtext COLLATE utf8_unicode_ci,
  `priority` mediumtext COLLATE utf8_unicode_ci,
  `attempts` mediumtext COLLATE utf8_unicode_ci,
  `lastattempt` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  KEY `inq_hostname` (`hostname`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.loginAttempts
DROP TABLE IF EXISTS `loginAttempts`;
CREATE TABLE IF NOT EXISTS `loginAttempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(128) NOT NULL DEFAULT '',
  `count` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.lusers
DROP TABLE IF EXISTS `lusers`;
CREATE TABLE IF NOT EXISTS `lusers` (
  `lusername` text COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lusername`(255))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mailbox
DROP TABLE IF EXISTS `mailbox`;
CREATE TABLE IF NOT EXISTS `mailbox` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `storagebasedirectory` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `maildir` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `quota` bigint(20) NOT NULL DEFAULT '0',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rank` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'normal',
  `employeeid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enablesmtp` tinyint(1) NOT NULL DEFAULT '1',
  `enablepop3` tinyint(1) NOT NULL DEFAULT '1',
  `enableimap` tinyint(1) NOT NULL DEFAULT '1',
  `enabledeliver` tinyint(1) NOT NULL DEFAULT '1',
  `enablemanagesieve` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.maillog
DROP TABLE IF EXISTS `maillog`;
CREATE TABLE IF NOT EXISTS `maillog` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` mediumtext COLLATE utf8_unicode_ci,
  `size` bigint(20) DEFAULT '0',
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `from_domain` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `to_domain` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `clientip` mediumtext COLLATE utf8_unicode_ci,
  `archive` mediumtext COLLATE utf8_unicode_ci,
  `isspam` tinyint(1) DEFAULT '0',
  `ishighspam` tinyint(1) DEFAULT '0',
  `issaspam` tinyint(1) DEFAULT '0',
  `isrblspam` tinyint(1) DEFAULT '0',
  `isfp` tinyint(1) DEFAULT '0',
  `isfn` tinyint(1) DEFAULT '0',
  `spamwhitelisted` tinyint(1) DEFAULT '0',
  `spamblacklisted` tinyint(1) DEFAULT '0',
  `sascore` decimal(7,2) DEFAULT '0.00',
  `spamreport` mediumtext COLLATE utf8_unicode_ci,
  `virusinfected` tinyint(1) DEFAULT '0',
  `nameinfected` tinyint(1) DEFAULT '0',
  `otherinfected` tinyint(1) DEFAULT '0',
  `report` mediumtext COLLATE utf8_unicode_ci,
  `ismcp` tinyint(1) DEFAULT '0',
  `ishighmcp` tinyint(1) DEFAULT '0',
  `issamcp` tinyint(1) DEFAULT '0',
  `mcpwhitelisted` tinyint(1) DEFAULT '0',
  `mcpblacklisted` tinyint(1) DEFAULT '0',
  `mcpsascore` decimal(7,2) DEFAULT '0.00',
  `mcpreport` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `headers` mediumtext COLLATE utf8_unicode_ci,
  `quarantined` tinyint(1) DEFAULT '0',
  KEY `maillog_datetime_idx` (`date`,`time`),
  KEY `maillog_id_idx` (`id`(20)),
  KEY `maillog_clientip_idx` (`clientip`(20)),
  KEY `maillog_from_idx` (`from_address`(200)),
  KEY `maillog_to_idx` (`to_address`(200)),
  KEY `maillog_host` (`hostname`(30)),
  KEY `from_domain_idx` (`from_domain`(50)),
  KEY `to_domain_idx` (`to_domain`(50)),
  KEY `maillog_quarantined` (`quarantined`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mail_smtp_spam_blacklists
DROP TABLE IF EXISTS `mail_smtp_spam_blacklists`;
CREATE TABLE IF NOT EXISTS `mail_smtp_spam_blacklists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mcp_rules
DROP TABLE IF EXISTS `mcp_rules`;
CREATE TABLE IF NOT EXISTS `mcp_rules` (
  `rule` char(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule_desc` char(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`rule`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_archive_fname_rules
DROP TABLE IF EXISTS `ms_archive_fname_rules`;
CREATE TABLE IF NOT EXISTS `ms_archive_fname_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_archive_ftype_rules
DROP TABLE IF EXISTS `ms_archive_ftype_rules`;
CREATE TABLE IF NOT EXISTS `ms_archive_ftype_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_config
DROP TABLE IF EXISTS `ms_config`;
CREATE TABLE IF NOT EXISTS `ms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `external` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `options` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_config_tmp
DROP TABLE IF EXISTS `ms_config_tmp`;
CREATE TABLE IF NOT EXISTS `ms_config_tmp` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_filename_rules
DROP TABLE IF EXISTS `ms_filename_rules`;
CREATE TABLE IF NOT EXISTS `ms_filename_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_filetype_rules
DROP TABLE IF EXISTS `ms_filetype_rules`;
CREATE TABLE IF NOT EXISTS `ms_filetype_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.ms_rulesets
DROP TABLE IF EXISTS `ms_rulesets`;
CREATE TABLE IF NOT EXISTS `ms_rulesets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `num` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rule` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mtalog
DROP TABLE IF EXISTS `mtalog`;
CREATE TABLE IF NOT EXISTS `mtalog` (
  `timestamp` datetime DEFAULT NULL,
  `host` mediumtext COLLATE utf8_unicode_ci,
  `type` mediumtext COLLATE utf8_unicode_ci,
  `msg_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relay` mediumtext COLLATE utf8_unicode_ci,
  `dsn` mediumtext COLLATE utf8_unicode_ci,
  `status` mediumtext COLLATE utf8_unicode_ci,
  `delay` time DEFAULT NULL,
  UNIQUE KEY `mtalog_uniq` (`timestamp`,`host`(10),`type`(10),`msg_id`,`relay`(20)),
  KEY `mtalog_timestamp` (`timestamp`),
  KEY `mtalog_type` (`type`(10)),
  KEY `msg_id` (`msg_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mtalog_ids
DROP TABLE IF EXISTS `mtalog_ids`;
CREATE TABLE IF NOT EXISTS `mtalog_ids` (
  `smtpd_id` varchar(20) CHARACTER SET ascii DEFAULT NULL,
  `smtp_id` varchar(20) CHARACTER SET ascii DEFAULT NULL,
  UNIQUE KEY `mtalog_ids_idx` (`smtpd_id`,`smtp_id`),
  KEY `smtpd_id` (`smtpd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mwrblblock
DROP TABLE IF EXISTS `mwrblblock`;
CREATE TABLE IF NOT EXISTS `mwrblblock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientip` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `errormsg` text COLLATE utf8_unicode_ci NOT NULL,
  `created_record` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dob` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mwrblclients
DROP TABLE IF EXISTS `mwrblclients`;
CREATE TABLE IF NOT EXISTS `mwrblclients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientip` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hour` smallint(6) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `ham` int(11) NOT NULL DEFAULT '0',
  `spam` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mwrblconfig
DROP TABLE IF EXISTS `mwrblconfig`;
CREATE TABLE IF NOT EXISTS `mwrblconfig` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.mwrblwlist
DROP TABLE IF EXISTS `mwrblwlist`;
CREATE TABLE IF NOT EXISTS `mwrblwlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `clientip` text COLLATE utf8_unicode_ci NOT NULL,
  `hostname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.outq
DROP TABLE IF EXISTS `outq`;
CREATE TABLE IF NOT EXISTS `outq` (
  `id` mediumtext COLLATE utf8_unicode_ci,
  `cdate` date DEFAULT NULL,
  `ctime` time DEFAULT NULL,
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `message` mediumtext COLLATE utf8_unicode_ci,
  `size` mediumtext COLLATE utf8_unicode_ci,
  `priority` mediumtext COLLATE utf8_unicode_ci,
  `attempts` mediumtext COLLATE utf8_unicode_ci,
  `lastattempt` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  KEY `outq_hostname` (`hostname`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.recipient_bcc_domain
DROP TABLE IF EXISTS `recipient_bcc_domain`;
CREATE TABLE IF NOT EXISTS `recipient_bcc_domain` (
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.recipient_bcc_user
DROP TABLE IF EXISTS `recipient_bcc_user`;
CREATE TABLE IF NOT EXISTS `recipient_bcc_user` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.saved_filters
DROP TABLE IF EXISTS `saved_filters`;
CREATE TABLE IF NOT EXISTS `saved_filters` (
  `name` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `col` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `operator` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `username` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_filters` (`name`(20),`col`(20),`operator`(20),`value`(20),`username`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sa_rules
DROP TABLE IF EXISTS `sa_rules`;
CREATE TABLE IF NOT EXISTS `sa_rules` (
  `rule` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule_desc` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`rule`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.sa_rules: 3,412 rows
DELETE FROM `sa_rules`;
/*!40000 ALTER TABLE `sa_rules` DISABLE KEYS */;
INSERT INTO `sa_rules` (`rule`, `rule_desc`) VALUES
	('AC_BR_BONANZA', 'Too many newlines in a row... spammy template'),
	('ACCESSDB', 'Message would have been caught by accessdb'),
	('ACCT_PHISHING', 'Possible phishing for account information'),
	('AC_DIV_BONANZA', 'Too many divs in a row... spammy template'),
	('AC_HTML_NONSENSE_TAGS', 'Many consecutive multi-letter HTML tags, likely nonsense/spam'),
	('AC_SPAMMY_URI_PATTERNS10', 'link combos match highly spammy template'),
	('AC_SPAMMY_URI_PATTERNS11', 'link combos match highly spammy template'),
	('AC_SPAMMY_URI_PATTERNS12', 'link combos match highly spammy template'),
	('AC_SPAMMY_URI_PATTERNS1', 'link combos match highly spammy template'),
	('AC_SPAMMY_URI_PATTERNS2', 'link combos match highly spammy template'),
	('AC_SPAMMY_URI_PATTERNS3', 'link combos match highly spammy template'),
	('AC_SPAMMY_URI_PATTERNS4', 'link combos match highly spammy template'),
	('AC_SPAMMY_URI_PATTERNS8', 'link combos match highly spammy template'),
	('AC_SPAMMY_URI_PATTERNS9', 'link combos match highly spammy template'),
	('ACT_NOW_CAPS', 'Talks about \'acting now\' with capitals'),
	('ADMAIL', '"admail" and variants'),
	('ADMITS_SPAM', 'Admits this is an ad'),
	('AD_PREFS', 'Advertising preferences'),
	('ADVANCE_FEE_2', 'Appears to be advance fee fraud (Nigerian 419)'),
	('ADVANCE_FEE_2_NEW_FORM', 'Advance Fee fraud and a form'),
	('ADVANCE_FEE_2_NEW_FRM_MNY', 'Advance Fee fraud form and lots of money'),
	('ADVANCE_FEE_2_NEW_MONEY', 'Advance Fee fraud and lots of money'),
	('ADVANCE_FEE_3', 'Appears to be advance fee fraud (Nigerian 419)'),
	('ADVANCE_FEE_3_NEW', 'Appears to be advance fee fraud (Nigerian 419)'),
	('ADVANCE_FEE_3_NEW_FORM', 'Advance Fee fraud and a form'),
	('ADVANCE_FEE_3_NEW_FRM_MNY', 'Advance Fee fraud form and lots of money'),
	('ADVANCE_FEE_3_NEW_MONEY', 'Advance Fee fraud and lots of money'),
	('ADVANCE_FEE_4', 'Appears to be advance fee fraud (Nigerian 419)'),
	('ADVANCE_FEE_4_NEW', 'Appears to be advance fee fraud (Nigerian 419)'),
	('ADVANCE_FEE_4_NEW_FRM_MNY', 'Advance Fee fraud form and lots of money'),
	('ADVANCE_FEE_4_NEW_MONEY', 'Advance Fee fraud and lots of money'),
	('ADVANCE_FEE_5_NEW', 'Appears to be advance fee fraud (Nigerian 419)'),
	('ADVANCE_FEE_5_NEW_FORM', 'Advance Fee fraud and a form'),
	('ADVANCE_FEE_5_NEW_FRM_MNY', 'Advance Fee fraud form and lots of money'),
	('ADVANCE_FEE_5_NEW_MONEY', 'Advance Fee fraud and lots of money'),
	('ALL_TRUSTED', 'Passed through trusted hosts only via SMTP'),
	('ANY_BOUNCE_MESSAGE', 'Message is some kind of bounce message'),
	('APOSTROPHE_FROM', 'From address contains an apostrophe'),
	('AWL', 'From: address is in the auto white-list'),
	('AXB_HELO_HOME_UN', 'HELO from home  - untrusted'),
	('AXB_X_FF_SEZ_S', 'Forefront sez this is spam'),
	('AXB_XMAILER_MIMEOLE_OL_024C2', 'Yet another X header trait'),
	('AXB_XMAILER_MIMEOLE_OL_1ECD5', 'Yet another X header trait##} AXB_XMAILER_MIMEOLE_OL_1ECD5'),
	('AXB_XM_FORGED_OL2600', 'Forged OE v. 6.2600'),
	('AXB_XMID_1212', 'Barbera Fingerprint'),
	('AXB_XMID_1510', 'Brunello Fingerprint'),
	('AXB_XMID_OEGOESNULL', 'Amarone Fingerprint'),
	('AXB_XM_SENDMAIL_NOT', 'Nebbiolo fingerprint'),
	('BAD_CREDIT', 'Eliminate Bad Credit'),
	('BAD_ENC_HEADER', 'Message has bad MIME encoding in the header'),
	('BANG_GUAR', 'Something is emphatically guaranteed'),
	('BANG_OPRAH', 'Talks about Oprah with an exclamation!'),
	('BANKING_LAWS', 'Talks about banking laws'),
	('BASE64_LENGTH_79_INF', 'base64 encoded email part uses line length of 78 or 79 characters'),
	('BAYES_00', 'Bayes spam probability is 0 to 1%'),
	('BAYES_05', 'Bayes spam probability is 1 to 5%'),
	('BAYES_20', 'Bayes spam probability is 5 to 20%'),
	('BAYES_40', 'Bayes spam probability is 20 to 40%'),
	('BAYES_50', 'Bayes spam probability is 40 to 60%'),
	('BAYES_60', 'Bayes spam probability is 60 to 80%'),
	('BAYES_80', 'Bayes spam probability is 80 to 95%'),
	('BAYES_95', 'Bayes spam probability is 95 to 99%'),
	('BAYES_999', 'Bayes spam probability is 99.9 to 100%'),
	('BAYES_99', 'Bayes spam probability is 99 to 100%'),
	('BIGNUM_EMAILS', 'Lots of email addresses/leads'),
	('BILLION_DOLLARS', 'Talks about lots of money'),
	('BLANK_LINES_80_90', 'Message body has 80-90% blank lines'),
	('BODY_8BITS', 'Body includes 8 consecutive 8-bit characters'),
	('BODY_ENHANCEMENT2', 'Information on getting larger body parts'),
	('BODY_ENHANCEMENT', 'Information on growing body parts'),
	('BODY_URI_ONLY', 'Message body is only a URI in one line of text or for an image'),
	('BOUNCE_MESSAGE', 'MTA bounce message'),
	('CANT_SEE_AD', 'You really want to see our spam.'),
	('CHALLENGE_RESPONSE', 'Challenge-Response message for mail you sent'),
	('CHARSET_FARAWAY', 'Character set indicates a foreign language'),
	('CHARSET_FARAWAY_HEADER', 'A foreign language charset used in headers'),
	('CK_HELO_DYNAMIC_SPLIT_IP', 'Relay HELO\'d using suspicious hostname (Split IP)'),
	('CK_HELO_GENERIC', 'Relay used name indicative of a Dynamic Pool or Generic rPTR'),
	('CN_B2B_SPAMMER', 'Chinese company introducing itself'),
	('COMMENT_GIBBERISH', 'Nonsense in long HTML comment'),
	('COMPENSATION', '"Compensation"'),
	('CONFIRMED_FORGED', 'Received headers are forged'),
	('CORRUPT_FROM_LINE_IN_HDRS', 'Informational: message is corrupt, with a From line in its headers'),
	('CRBOUNCE_MESSAGE', 'Challenge-Response bounce message'),
	('CTYPE_8SPACE_GIF', 'Stock spam image part \'Content-Type\' found (8 spc)'),
	('CUM_SHOT', 'Possible porn - Cum Shot'),
	('DATE_DOTS', 'Periods in date header'),
	('DATE_IN_FUTURE_03_06', 'Date: is 3 to 6 hours after Received: date'),
	('DATE_IN_FUTURE_06_12', 'Date: is 6 to 12 hours after Received: date'),
	('DATE_IN_FUTURE_12_24', 'Date: is 12 to 24 hours after Received: date'),
	('DATE_IN_FUTURE_24_48', 'Date: is 24 to 48 hours after Received: date'),
	('DATE_IN_FUTURE_48_96', 'Date: is 48 to 96 hours after Received: date'),
	('DATE_IN_FUTURE_96_Q', 'Date: is 4 days to 4 months after Received: date'),
	('DATE_IN_FUTURE_96_XX', 'Date: is 96 hours or more after Received: date'),
	('DATE_IN_FUTURE_Q_PLUS', 'Date: is over 4 months after Received: date'),
	('DATE_IN_PAST_03_06', 'Date: is 3 to 6 hours before Received: date'),
	('DATE_IN_PAST_06_12', 'Date: is 6 to 12 hours before Received: date'),
	('DATE_IN_PAST_12_24', 'Date: is 12 to 24 hours before Received: date'),
	('DATE_IN_PAST_24_48', 'Date: is 24 to 48 hours before Received: date'),
	('DATE_IN_PAST_96_XX', 'Date: is 96 hours or more before Received: date'),
	('DATE_SPAMWARE_Y2K', 'Date header uses unusual Y2K formatting'),
	('DCC_CHECK', 'Listed in DCC (http://rhyolite.com/anti-spam/dcc/)'),
	('DCC_REPUT_00_12', 'DCC reputation between 0 and 12 %  (mostly ham)'),
	('DCC_REPUT_70_89', 'DCC reputation between 70 and 89 %'),
	('DCC_REPUT_90_94', 'DCC reputation between 90 and 94 %'),
	('DCC_REPUT_95_98', 'DCC reputation between 95 and 98 %  (mostly spam)'),
	('DCC_REPUT_99_100', 'DCC reputation between 99 % or higher (spam)'),
	('__DC_GIF_MULTI_LARGO', 'Message has 2+ inline gif covering lots of area'),
	('DC_GIF_UNO_LARGO', 'Message contains a single large inline gif'),
	('DC_IMAGE_SPAM_HTML', 'Possible Image-only spam'),
	('DC_IMAGE_SPAM_TEXT', 'Possible Image-only spam with little text'),
	('__DC_IMG_HTML_RATIO', 'Low rawbody to pixel area ratio'),
	('__DC_IMG_TEXT_RATIO', 'Low body to pixel area ratio'),
	('__DC_PNG_MULTI_LARGO', 'Message has 2+ inline png covering lots of area'),
	('DC_PNG_UNO_LARGO', 'Message contains a single large inline gif'),
	('DEAR_BENEFICIARY', 'Dear Beneficiary:'),
	('DEAR_EMAIL', 'Message contains Dear email address'),
	('DEAR_EMAIL_USER', 'Dear Email User:'),
	('DEAR_FRIEND', 'Dear Friend? That\'s not very dear!'),
	('DEAR_SOMETHING', 'Contains \'Dear (something)\''),
	('DEAR_WINNER', 'Spam with generic salutation of "dear winner"'),
	('DIET_1', 'Lose Weight Spam'),
	('DIGEST_MULTIPLE', 'Message hits more than one network digest check'),
	('DKIM_ADSP_ALL', 'No valid author signature, domain signs all mail'),
	('DKIM_ADSP_CUSTOM_HIGH', 'No valid author signature, adsp_override is CUSTOM_HIGH'),
	('DKIM_ADSP_CUSTOM_LOW', 'No valid author signature, adsp_override is CUSTOM_LOW'),
	('DKIM_ADSP_CUSTOM_MED', 'No valid author signature, adsp_override is CUSTOM_MED'),
	('DKIM_ADSP_DISCARD', 'No valid author signature, domain signs all mail and suggests discarding the rest'),
	('DKIM_ADSP_NXDOMAIN', 'No valid author signature and domain not in DNS'),
	('__DKIM_DEPENDABLE', 'A validation failure not attributable to truncation'),
	('__DKIMDOMAIN_IN_DWL_ANY', 'Any TXT response received from a Spamhaus DWL'),
	('DKIMDOMAIN_IN_DWL', 'Signing domain listed in Spamhaus DWL'),
	('DKIMDOMAIN_IN_DWL_UNKNOWN', 'Unrecognized response from Spamhaus DWL'),
	('DKIM_SIGNED', 'Message has a DKIM or DK signature, not necessarily valid'),
	('DKIM_VALID_AU', 'Message has a valid DKIM or DK signature from author\'s domain'),
	('DKIM_VALID', 'Message has at least one valid DKIM or DK signature'),
	('DNS_FROM_AHBL_RHSBL', 'Envelope sender listed in dnsbl.ahbl.org'),
	('DNS_FROM_RFC_BOGUSMX', 'Envelope sender in bogusmx.rfc-ignorant.org'),
	('DNS_FROM_RFC_DSN', 'Envelope sender in dsn.rfc-ignorant.org'),
	('DOS_ANAL_SPAM_MAILER', 'X-mailer pattern common to anal porn site spam'),
	('DOS_FIX_MY_URI', 'Looks like a "fix my obfu\'d URI please" spam'),
	('DOS_HIGH_BAT_TO_MX', 'The Bat! Direct to MX with High Bits'),
	('DOS_HIGHBIT_HDRS_BODY', 'Headers need encoding and body is highbit'),
	('DOS_LET_GO_JOB', 'Let go from their job and now makes lots of dough!'),
	('DOS_OE_TO_MX', 'Delivered direct to MX with OE headers'),
	('DOS_OE_TO_MX_IMAGE', 'Direct to MX with OE headers and an image'),
	('DOS_OUTLOOK_TO_MX', 'Delivered direct to MX with Outlook headers'),
	('DOS_OUTLOOK_TO_MX_IMAGE', 'Direct to MX with Outlook headers and an image'),
	('DOS_RCVD_IP_TWICE_C', 'Received from the same IP twice in a row (only one external relay; empty or IP helo)'),
	('DOS_STOCK_BAT', 'Probable pump and dump stock spam'),
	('DOS_URI_ASTERISK', 'Found an asterisk in a URI'),
	('DOS_YOUR_PLACE', 'Russian dating spam'),
	('DRUG_DOSAGE', 'Talks about price per dose'),
	('DRUG_ED_CAPS', 'Mentions an E.D. drug'),
	('DRUG_ED_GENERIC', 'Mentions Generic Viagra'),
	('DRUG_ED_ONLINE', 'Fast Viagra Delivery'),
	('DRUG_ED_SILD', 'Talks about an E.D. drug using its chemical name'),
	('DRUGS_ANXIETY_EREC', 'Refers to both an erectile and an anxiety drug'),
	('DRUGS_ANXIETY_OBFU', 'Obfuscated reference to an anxiety control drug'),
	('DRUGS_ANXIETY', 'Refers to an anxiety control drug'),
	('DRUGS_DIET_OBFU', 'Obfuscated reference to a diet drug'),
	('DRUGS_DIET', 'Refers to a diet drug'),
	('DRUGS_ERECTILE_OBFU', 'Obfuscated reference to an erectile drug'),
	('DRUGS_ERECTILE', 'Refers to an erectile drug'),
	('DRUGS_HDIA', 'Subject mentions "hoodia"'),
	('DRUGS_MANYKINDS', 'Refers to at least four kinds of drugs'),
	('DRUGS_MUSCLE', 'Refers to a muscle relaxant'),
	('DRUGS_SLEEP_EREC', 'Refers to both an erectile and a sleep aid drug'),
	('DRUGS_SMEAR1', 'Two or more drugs crammed together into one word'),
	('DRUGS_STOCK_MIMEOLE', 'Stock-spam forged headers found (5510)'),
	('DSN_NO_MIMEVERSION', 'Return-Path <> and no MIME-Version: header'),
	('DX_TEXT_02', '"change your message stat"'),
	('DX_TEXT_03', '"XXX Media Group"'),
	('DX_TEXT_05', 'HTML snobbery'),
	('DYN_RDNS_AND_INLINE_IMAGE', 'Contains image, and was sent by dynamic rDNS'),
	('DYN_RDNS_SHORT_HELO_HTML', 'Sent by dynamic rDNS, short HELO, and HTML'),
	('DYN_RDNS_SHORT_HELO_IMAGE', 'Short HELO string, dynamic rDNS, inline image'),
	('EMAIL_ROT13', 'Body contains a ROT13-encoded email address'),
	('EMPTY_MESSAGE', 'Message appears to have no textual parts and no Subject: text'),
	('EM_ROLEX', 'Message puts emphasis on the watch manufacturer'),
	('ENGLISH_UCE_SUBJECT', 'Subject contains an English UCE tag'),
	('ENV_AND_HDR_SPF_MATCH', 'Env and Hdr From used in default SPF WL Match'),
	('EXCUSE_24', 'Claims you wanted this ad'),
	('EXCUSE_4', 'Claims you can be removed from the list'),
	('EXCUSE_REMOVE', 'Talks about how to be removed from mailings'),
	('EXTRA_MPART_TYPE', 'Header has extraneous Content-type:...type= entry'),
	('FAKE_HELO_MAIL_COM_DOM', 'Relay HELO\'d with suspicious hostname (mail.com)'),
	('FAKE_OUTBLAZE_RCVD', 'Received header contains faked \'mr.outblaze.com\''),
	('FB_ADD_INCHES', 'Add / Gain inches'),
	('FB_ALMOST_SEX', 'It\'s almost sex, but not!'),
	('FB_ANA_TRIM', 'Broken AnaTrim phrase.'),
	('FB_ANUI', 'Phrase: A_U_N_I'),
	('FB_BILLI0N', 'Phrase: [BM]Illi0n'),
	('FB_C0MPANY', 'Phrase: C0mpany'),
	('FB_CAN_LONGER', 'Phrase: can last longer'),
	('FB_CIALIS_LEO3', 'Uses a mis-spelled version of cialis.'),
	('FB_DOUBLE_0WORDS', 'Looks like double 0 words'),
	('FB_EMAIL_HIER', 'Phrase: email hier'),
	('FB_EXTRA_INCHES', 'Phrase: extra inches'),
	('FB_FAKE_NUMBERS', 'Looks like numbers with O\'s insted of 0\'s'),
	('FB_FAKE_NUMS4', 'Looks like fake numbers (4)'),
	('FB_FHARMACY', 'Phrase: Farmacy'),
	('FB_FORWARD_LOOK', 'Phrase: forward look with 0\'s'),
	('FB_GAPPY_ADDRESS', 'Too much spacing in Address'),
	('FB_GET_MEDS', 'Looks like trying to sell meds'),
	('FB_GVR', 'Looks like generic viagra'),
	('FB_HEY_BRO_COMMA', 'Phrase hey bro,'),
	('FB_HG_H_CAP', 'Phrase: HGH'),
	('FB_HOMELOAN', 'Phrase $x home loan'),
	('FBI_MONEY', 'The FBI wants to give you lots of money?'),
	('FB_IMPRESS_GIRL', 'Phrase: impress ... girl'),
	('FB_INCREASE_YOUR', 'Phrase: Increase your energy'),
	('FB_INDEPEND_RWD', 'Phrase: independent reward'),
	('FBI_SPOOF', 'Claims to be FBI, but not from FBI domain'),
	('FB_L0AN', 'Phrase: L0an'),
	('FB_LETTERS_21B', 'Special people leave special signs!'),
	('FB_LOSE_WEIGHT_CAP', 'Phrase: LOSE WEIGHT'),
	('FB_LOWER_PAYM', 'Phrase: lower your monthly payments'),
	('FB_MORE_SIZE', 'Phrase: more size'),
	('FB_NO_SCRIP_NEEDED', 'Phrase: no prescription needed.'),
	('FB_NOT_PHONE_NUM1', 'Looks like a fake phone number (1)'),
	('FB_NOT_PHONE_NUM3', 'Looks like a fake phone number (3)'),
	('FB_NOT_SCHOOL', 'Looks like school but it\'s not!'),
	('FB_NUMYO2', 'Speaks of 20+ year old.'),
	('FB_NUMYO', 'Speaks of teenager.'),
	('FB_ODD_SPACED_MONEY', 'Looks like money but has odd spacing.'),
	('FB_ONIINE', 'Mis-spelled online'),
	('FB_P1LL', 'Phrase: p1ll'),
	('FB_PENIS_GROWTH', 'Phrase: penis growth'),
	('FB_PIPEDOLLAR', 'Phrase: Dollar, with pipes or 0\'s.'),
	('FB_PIPE_ILLION', 'Looks like illion, but it\'s not'),
	('FB_PROLONGED_HARD', 'Talks about prolonged hardness'),
	('FB_QUALITY_REPLICA', 'Phrase: quality replica'),
	('FB_REF_CODE_SPACE', 'Refcode with spacing'),
	('FB_RE_FI', 'Looks like refi.'),
	('FB_REPLICA_ROLEX', 'Phrase: Replica Rolex'),
	('FB_REPLIC_CAP', 'Phrase: REPLICA'),
	('FB_ROLLER_IS_T', 'Phrase: Roller is th'),
	('FB_ROLX', 'Phrase: rolx'),
	('FB_SAVE_PERSC', 'Phrase: save ... prescription.'),
	('FB_SOFTTABS', 'Phrase: Softabs'),
	('FB_SPACED_FREE', 'Phrase: F R E E'),
	('FB_SPACED_PHN_3B', 'Phone number with -- spacing. (B)'),
	('FB_SPACEY_ZIP', 'Looks like a  s p a c e d zipcode.'),
	('FB_SPUR_M', 'Phrase: SPUR-M'),
	('FB_SSEX', 'Phrase: ssex'),
	('FB_STOCK_EXPLODE', 'Looks like stocks exploding.'),
	('FB_SYMBLO', 'Mis-spelled symbol.'),
	('FB_THIS_ADVERT', 'Phrase: this advertiser'),
	('FB_THOUS_PERSONAL', 'Phrase: thousand personal'),
	('FB_TO_STOP_DISTRO', 'Phrase: to stop further distribution'),
	('FB_ULTRA_ALLURE', 'Phrase: Ultra Allure'),
	('FB_UNLOCK_YOUR_G', 'Phrase: lock to your girlfriend'),
	('FB_UNRESOLV_PROV', 'Pattern Replacement PROV_D'),
	('FB_YOUR_REFI', 'Phrase: Your refi'),
	('FB_YOURSELF_MASTER', 'Phrase: yourself master'),
	('FH_BAD_OEV1441', 'Bad X-Mailer version'),
	('FH_DATE_IS_19XX', 'The date is not 19xx.'),
	('FH_FAKE_RCVD_LINE_B', 'RCVD line looks faked (B)'),
	('FH_FAKE_RCVD_LINE', 'RCVD line looks faked (A)'),
	('FH_FROM_CASH', 'From name has "cash"'),
	('FH_FROMEML_NOTLD', 'E-mail address doesn\'t have TLD (.com, etc.)'),
	('FH_FROM_GET_NAME', 'From name says Get'),
	('FH_FROM_GIVEAWAY', 'From name is giveaway.'),
	('FH_FROM_HOODIA', 'From has Hoodia!!?'),
	('FH_HAS_XAIMC', 'Has X-AIMC-AUTH header'),
	('FH_HAS_XID', 'Has X-ID'),
	('FH_HELO_ALMOST_IP', 'Helo is almost an IP addr.'),
	('FH_HELO_ENDS_DOT', 'Helo ends with a dot.'),
	('FH_HELO_EQ_610HEX', 'Helo is 6-10 hex chr\'s.'),
	('FH_HELO_EQ_CHARTER', 'Helo is d-d-d-d charter.com'),
	('FH_HELO_EQ_D_D_D_D', 'Helo is d-d-d-d'),
	('FH_HELO_GMAILSMTP', 'Faked helo of gmail-smtp-in'),
	('FH_HOST_EQ_DYNAMICIP', 'Host is dynamicip'),
	('FH_HOST_EQ_PACBELL_D', 'Host is pacbell.net dsl'),
	('FH_HOST_EQ_VERIZON_P', 'Host is pool-.+verizon.net'),
	('FH_HOST_IN_ADDRARPA', 'HOST dns says "in-addr.arpa"'),
	('FH_MSGID_000000', 'Special MSGID'),
	('FH_MSGID_01C67', 'Special MSGID'),
	('FH_MSGID_01C70XXX', 'MESSAGE ID seen often!!!'),
	('FH_MSGID_REPLACE', 'Broken Replace Template'),
	('FH_MSGID_XXBLAH', 'Common sign in msg-id\'s 12/21/2006'),
	('FH_MSGID_XXX', 'Message-Id = @xxx'),
	('FH_RE_NEW_DDD', 'Subject is Re: new \\d\\d\\d'),
	('FH_XMAIL_REPLACE', 'Broken Replace Template'),
	('FIN_FREE', 'Freedom of a financial nature'),
	('FM_DOESNT_SAY_STOCK', 'It\'s a stock spam but doesn\'t say stock'),
	('FM_FAKE_53COM_SPOOF', 'Spoof mail from 53.com?'),
	('FM_FAKE_HELO_HOTMAIL', 'Looks like a fake hotmail.com helo.'),
	('FM_FAKE_HELO_VERIZON', 'Looks like a fake verizon.net helo.'),
	('FM_FRM_RN_L_BRACK', 'From name has > but not <'),
	('FM_IS_IT_OUR_ACCOUNT', 'Is it our account?'),
	('FM_LIKE_STOCKS', 'It looks like a duck, it\'s a duck!'),
	('FM_LOTTO_YOU_WON', 'Talks about lotto and you won!'),
	('FM_LUX_GIFTS_REDUCED', 'Luxury Gifts with dd%'),
	('FM_MANY_DRUG_WORDS', 'Lot\'s of almost drug words'),
	('FM_MORTGAGE5PLUS', 'Looks like a mortgage spam (5+)'),
	('FM_MORTGAGE6PLUS', 'Looks like a mortgage spam (6+)'),
	('FM_MULTI_LUX_GIFTS', 'Talks about variety of luxury gifts'),
	('FM_PHN_NODNS', 'Phone spacing + no dns'),
	('FM_RATSIGN_1106', 'Fingerprint seen in lots of spam. 11/2006'),
	('FM_RE_HELLO_SPAM', 'Re: Hello / hi'),
	('FM_ROLEX_ADS', 'Looks like Rolex spams.'),
	('FM_SCHOOL_DIPLOMA', 'Meta for Schooling + Diploma.'),
	('FM_SCHOOLING', 'Meta Combo Phrase for Schooling (2)'),
	('FM_SCHOOL_TYPES', 'Meta Combo Phrase for Schooling'),
	('FM_SEX_HELODDDD', 'Sex words + helo = dddd'),
	('FM_SUBJ_APPROVE', 'Subject has Approve and !'),
	('FM_TRUE_LOV_ALL_N', 'True Love all Night!'),
	('FM_VEGAS_CASINO', 'Looks like vega casino spam'),
	('FM_XMAIL_F_OUT', 'Looks like Fake Outlook?'),
	('FORGED_HOTMAIL_RCVD2', 'hotmail.com \'From\' address, but no \'Received:\''),
	('FORGED_IMS_HTML', 'IMS can\'t send HTML message only'),
	('FORGED_IMS_TAGS', 'IMS mailers can\'t send HTML in this format'),
	('FORGED_MSGID_AOL', 'Message-ID is forged, (aol.com)'),
	('FORGED_MSGID_EXCITE', 'Message-ID is forged, (excite.com)'),
	('FORGED_MSGID_HOTMAIL', 'Message-ID is forged, (hotmail.com)'),
	('FORGED_MSGID_MSN', 'Message-ID is forged, (msn.com)'),
	('FORGED_MSGID_YAHOO', 'Message-ID is forged, (yahoo.com)'),
	('FORGED_MUA_EUDORA', 'Forged mail pretending to be from Eudora'),
	('FORGED_MUA_IMS', 'Forged mail pretending to be from IMS'),
	('FORGED_MUA_MOZILLA', 'Forged mail pretending to be from Mozilla'),
	('FORGED_MUA_OIMO', 'Forged mail pretending to be from MS Outlook IMO'),
	('FORGED_MUA_OUTLOOK', 'Forged mail pretending to be from MS Outlook'),
	('FORGED_MUA_THEBAT_BOUN', 'Mail pretending to be from The Bat! (boundary)'),
	('FORGED_MUA_THEBAT_CS', 'Mail pretending to be from The Bat! (charset)'),
	('FORGED_OUTLOOK_HTML', 'Outlook can\'t send HTML message only'),
	('FORGED_OUTLOOK_TAGS', 'Outlook can\'t send HTML in this format'),
	('FORGED_QUALCOMM_TAGS', 'QUALCOMM mailers can\'t send HTML in this format'),
	('FORGED_TBIRD_IMG_ARROW', 'Likely forged Thunderbird image spam'),
	('__FORGED_TBIRD_IMG', 'Possibly forged Thunderbird image spam'),
	('FORGED_TBIRD_IMG_TO_MX', 'Likely forged Thunderbird image spam'),
	('FORGED_TELESP_RCVD', 'Contains forged hostname for a DSL IP in Brazil'),
	('FORGED_THEBAT_HTML', 'The Bat! can\'t send HTML message only'),
	('FORGED_YAHOO_RCVD', '\'From\' yahoo.com does not match \'Received\' headers'),
	('FORM_FRAUD_3', 'Fill a form and several fraud phrases'),
	('FORM_FRAUD_5', 'Fill a form and many fraud phrases'),
	('FORM_FRAUD', 'Fill a form and a fraud phrase'),
	('FORM_LOW_CONTRAST', 'Fill in a form with hidden text'),
	('FORWARD_LOOKING', 'Stock Disclaimer Statement'),
	('FOUND_YOU', 'I found you...'),
	('FR_3TAG_3TAG', 'Looks like 3 <e> small tags.'),
	('FRAGMENTED_MESSAGE', 'Partial message'),
	('FR_ALMOST_VIAG2', 'Almost looks like viagra.'),
	('FR_CANTSEETEXT', 'Phrase class=cantseetext'),
	('FREEMAIL_ENVFROM_END_DIGIT', 'Envelope-from freemail username ends in digit'),
	('FREEMAIL_FORGED_REPLYTO', 'Freemail in Reply-To, but not From'),
	('FREEMAIL_FROM', 'Sender email is freemail'),
	('FREEMAIL_REPLY', 'From and body contain different freemails'),
	('FREEMAIL_REPLYTO_END_DIGIT', 'Reply-To freemail username ends in digit'),
	('FREEMAIL_REPLYTO', 'Reply-To/From or Reply-To/body contain different freemails'),
	('FREE_PORN', 'Possible porn - Free Porn'),
	('FREE_QUOTE_INSTANT', 'Free express or no-obligation quote'),
	('FR_MIDER', 'Sign often seen in spams'),
	('FROM_BLANK_NAME', 'From: contains empty name'),
	('FROM_DOMAIN_NOVOWEL', 'From: domain has series of non-vowel letters'),
	('FROM_EXCESS_BASE64', 'From: base64 encoded unnecessarily'),
	('FROM_ILLEGAL_CHARS', 'From: has too many raw illegal characters'),
	('FROM_IN_TO_AND_SUBJ', 'From address is in To and Subject'),
	('FROM_LOCAL_DIGITS', 'From: localpart has long digit sequence'),
	('FROM_LOCAL_HEX', 'From: localpart has long hexadecimal sequence'),
	('FROM_LOCAL_NOVOWEL', 'From: localpart has series of non-vowel letters'),
	('FROM_MISSPACED', 'From: missing whitespace'),
	('FROM_MISSP_DYNIP', 'From misspaced + dynamic rDNS'),
	('FROM_MISSP_EH_MATCH', 'From misspaced, matches envelope'),
	('FROM_MISSP_MSFT', 'From misspaced + supposed Microsoft tool'),
	('FROM_MISSP_PHISH', 'Malformed, claims to be from financial organization - possible phish'),
	('FROM_MISSP_REPLYTO', 'From misspaced, has Reply-To'),
	('FROM_MISSP_TO_UNDISC', 'From misspaced, To undisclosed'),
	('FROM_MISSP_USER', 'From misspaced, from "User"'),
	('FROM_MISSP_XPRIO', 'Misspaced FROM + X-Priority'),
	('FROM_NO_USER', 'From: has no local-part before @ sign'),
	('FROM_OFFERS', 'From address is "at something-offers"'),
	('FROM_STARTS_WITH_NUMS', 'From: starts with several numbers'),
	('FRT_ADOBE2', 'ReplaceTags: Adobe'),
	('FRT_APPROV', 'ReplaceTags: Approve'),
	('FRT_BIGGERMEM1', 'ReplaceTags: Bigger / Larger, Penis / Member'),
	('FRT_DIPLOMA', 'ReplaceTags: Diploma'),
	('FRT_DISCOUNT', 'ReplaceTags: Discount'),
	('FRT_DOLLAR', 'ReplaceTags: Dollar'),
	('FRT_ESTABLISH2', 'ReplaceTags: Establish (2)'),
	('FRT_FUCK2', 'ReplaceTags: Fuck (2)'),
	('FRT_GUARANTEE1', 'ReplaceTags: Guarantee (1)'),
	('FRT_INVESTOR', 'ReplaceTags: Investor'),
	('FR_TITLE_CONS_6', 'Title Attribute has 6 non-vowels.'),
	('FR_TITLE_NUMS', 'HTML Title is only numbers'),
	('FRT_LEVITRA', 'ReplaceTags: Levitra'),
	('FRT_MEETING', 'ReplaceTags: Meeting'),
	('FRT_OFFER2', 'ReplaceTags: Offer (2)'),
	('FRT_OPPORTUN2', 'ReplaceTags: Oppertun (2)'),
	('FRT_PENIS1', 'ReplaceTags: Penis'),
	('FRT_PRICE', 'ReplaceTags: Price'),
	('FRT_REFINANCE1', 'ReplaceTags: Refinance (1)'),
	('FRT_ROLEX', 'ReplaceTags: Rolex'),
	('FRT_SEXUAL', 'ReplaceTags: Sexual'),
	('FRT_SOMA2', 'ReplaceTags: Soma (2)'),
	('FRT_SOMA', 'ReplaceTags: Soma'),
	('FRT_STRONG1', 'ReplaceTags: Strong (1)'),
	('FRT_STRONG2', 'ReplaceTags: Strong (2)'),
	('FRT_SYMBOL', 'ReplaceTags: Symbol'),
	('FRT_TODAY2', 'ReplaceTags: Today (2)'),
	('FRT_VALIUM1', 'ReplaceTags: Valium'),
	('FRT_VALIUM2', 'ReplaceTags: Valium (2)'),
	('FRT_WEIGHT2', 'ReplaceTags: Weight (2)'),
	('FRT_XANAX1', 'ReplaceTags: Xanax (1)'),
	('FRT_XANAX2', 'ReplaceTags: Xanax (2)'),
	('FS_ABIGGER', 'Subject has "a bigger"'),
	('FS_APPROVE_YOU', 'Subject says approve you'),
	('FS_AT_NO_COST', 'Subject says "At No Cost"'),
	('FS_CHEAP_CAP', 'Phrase: Cheap in Caps in Subject.'),
	('FS_DOLLAR_BONUS', 'Subject talks about money bonus!'),
	('FS_EJACULA', 'Phrase: ejaculation in subject.'),
	('FS_ERECTION', 'Phrase: erection in subject.'),
	('FS_HUGECOCK', 'Phrase: Huge Cock'),
	('FS_LARGE_PERCENT2', 'Larger than 100% in subj.'),
	('FSL_BOTSPAM_1', 'Two-line spam with URI pointing to .ru domain'),
	('FSL_CTYPE_WIN1251', 'Content-Type only seen in 419 spam'),
	('FSL_MID_419', 'Spam signature in Message-ID'),
	('FSL_NEW_HELO_USER', 'Spam\'s using Helo and User'),
	('FS_LOW_RATES', 'Subject says low rates'),
	('FS_MORE_CONFIDENT', 'Subject says More Confident'),
	('FS_NEW_SOFT_UPLOAD', 'Subj starts with New software uploaded'),
	('FS_NEW_XXX', 'Subject looks like Fharmacy spams.'),
	('FS_NO_SCRIP', 'Subject almost says No prescription'),
	('FS_NUDE', 'Subject says Nude'),
	('FS_OBFU_PRMCY', 'what could this word be?'),
	('FS_PERSCRIPTION', 'Subject mis-spelled prescription'),
	('FS_PHARMASUB2', 'Looks like Phramacy subject.'),
	('FS_RAMROD', 'Subject says Ramrod'),
	('FS_RE_APPROV', 'Phrase: re approved'),
	('FS_REPLICA', 'Subject says "replica"'),
	('FS_REPLICAWATCH', 'Subject says Replica watch'),
	('FS_START_DOYOU2', 'Subject starts with Do you dream,have,want,love, etc.'),
	('FS_START_LOSE', 'Subject starts with Lose'),
	('FS_TEEN_BAD', 'Subject says something bad about teens'),
	('FS_TIP_DDD', 'Phrase: subject = tip ddd'),
	('FS_WEIGHT_LOSS', 'Subject says Weight Loss'),
	('FS_WILL_HELP', 'Subject says will help'),
	('FS_WITH_SMALL', 'Subject says With ... small'),
	('FU_COMMON_SUBS2', 'Sub-dir seen often in spam (2).'),
	('FU_END_ET', 'ET Phone Home?'),
	('FU_ENDS_NUMS_DOTS_CLK', 'Ends with clk/d+.d+.d+'),
	('FU_HOODIA', 'URL has hoodia in it.'),
	('FU_LONG_QUERY3', 'URL has a long file name with .aspx extension.'),
	('FU_MIDER', 'URL has /gal/'),
	('FU_UKGEOCITIES', 'URL with [a-z]{2}.geocities.com'),
	('FU_URI_TRACKER_T', 'URI style tracker (T)'),
	('FUZZY_AFFORDABLE', 'Attempt to obfuscate words in spam'),
	('FUZZY_AMBIEN', 'Attempt to obfuscate words in spam'),
	('FUZZY_BILLION', 'Attempt to obfuscate words in spam'),
	('FUZZY_CPILL', 'Attempt to obfuscate words in spam'),
	('FUZZY_CREDIT', 'Attempt to obfuscate words in spam'),
	('FUZZY_ERECT', 'Attempt to obfuscate words in spam'),
	('FUZZY_GUARANTEE', 'Attempt to obfuscate words in spam'),
	('FUZZY_MEDICATION', 'Attempt to obfuscate words in spam'),
	('FUZZY_MERIDIA', 'Obfuscation of the word "meridia"'),
	('FUZZY_MILLION', 'Attempt to obfuscate words in spam'),
	('FUZZY_MONEY', 'Attempt to obfuscate words in spam'),
	('FUZZY_MORTGAGE', 'Attempt to obfuscate words in spam'),
	('FUZZY_OBLIGATION', 'Attempt to obfuscate words in spam'),
	('FUZZY_OFFERS', 'Attempt to obfuscate words in spam'),
	('FUZZY_PHARMACY', 'Attempt to obfuscate words in spam'),
	('FUZZY_PHENT', 'Attempt to obfuscate words in spam'),
	('FUZZY_PRESCRIPT', 'Attempt to obfuscate words in spam'),
	('FUZZY_PRICES', 'Attempt to obfuscate words in spam'),
	('FUZZY_REFINANCE', 'Attempt to obfuscate words in spam'),
	('FUZZY_REMOVE', 'Attempt to obfuscate words in spam'),
	('FUZZY_ROLEX', 'Attempt to obfuscate words in spam'),
	('FUZZY_SOFTWARE', 'Attempt to obfuscate words in spam'),
	('FUZZY_THOUSANDS', 'Attempt to obfuscate words in spam'),
	('FUZZY_VIOXX', 'Attempt to obfuscate words in spam'),
	('FUZZY_VLIUM', 'Attempt to obfuscate words in spam'),
	('FUZZY_VPILL', 'Attempt to obfuscate words in spam'),
	('FUZZY_XPILL', 'Attempt to obfuscate words in spam'),
	('GAPPY_SUBJECT', 'Subject: contains G.a.p.p.y-T.e.x.t'),
	('GMD_AUTHOR_COLET', 'PDF author was \'colet\''),
	('GMD_AUTHOR_HPADMIN', 'PDF author was \'HP_Administrator\''),
	('GMD_AUTHOR_MOBILE', 'PDF author was \'mobile\''),
	('GMD_AUTHOR_OOO', 'PDF author was \'openofficeuser\''),
	('GMD_PDF_EMPTY_BODY', 'Attached PDF with empty message body'),
	('GMD_PDF_ENCRYPTED', 'Attached PDF is encrypted'),
	('GMD_PDF_FUZZY1_T1', 'Fuzzy MD5 Match 57EBC1FFB1A24CC14AE23E1E227C3484'),
	('GMD_PDF_FUZZY2_T10', 'Fuzzy tags Match C47FC4E71CEFDEA4F334AEC8E26F647B'),
	('GMD_PDF_FUZZY2_T11', 'Fuzzy tags Match 5A4CB7600371063164BB7AFA6EDE7FE9'),
	('GMD_PDF_FUZZY2_T12', 'Fuzzy tags Match BB7030054D848151C6C4C0D905592BAD'),
	('GMD_PDF_FUZZY2_T1', 'Fuzzy MD5 Match 653C8AA9FDFD03D382523488058360A2'),
	('GMD_PDF_FUZZY2_T2', 'Fuzzy MD5 Match DDFFCC40889F44C0C1D218163A39689D'),
	('GMD_PDF_FUZZY2_T3', 'Fuzzy MD5 Match 3D4E25DE4A05695681D694716D579474'),
	('GMD_PDF_FUZZY2_T4', 'Fuzzy tags Match 4349559AF03A18ABDAE50A54DCCDA527'),
	('GMD_PDF_FUZZY2_T5', 'Fuzzy tags Match 1AF87ABAF88F3C2A80577BE2E3A5886E'),
	('GMD_PDF_FUZZY2_T6', 'Fuzzy tags Match 1AF87ABAF88F3C2A80577BE2E3A5886E'),
	('GMD_PDF_FUZZY2_T7', 'Fuzzy tags Match 73A3A5E8D8048EC599EDE1D045345A87'),
	('GMD_PDF_FUZZY2_T8', 'Fuzzy tags Match 24EE98188F3BE8C6D3C7FE6190985ED8'),
	('GMD_PDF_FUZZY2_T9', 'Fuzzy tags Match 875C8F0810E6524EF0C3A7C4221A4C28'),
	('GMD_PDF_HORIZ', 'Contains pdf 100-240 (high) x 450-800 (wide)'),
	('GMD_PDF_SQUARE', 'Contains pdf 180-360 (high) x 180-360 (wide)'),
	('GMD_PDF_STOX_M1', 'PDF Stox spam'),
	('GMD_PDF_STOX_M2', 'PDF Stox spam'),
	('GMD_PDF_STOX_M3', 'PDF Stox spam'),
	('GMD_PDF_STOX_M4', 'PDF Stox spam'),
	('GMD_PDF_STOX_M5', 'PDF Stox Spam'),
	('GMD_PDF_VERT', 'Contains pdf 450-800 (high) x 100-240 (wide)'),
	('GMD_PRODUCER_EASYPDF', 'PDF producer was BCL easyPDF'),
	('GMD_PRODUCER_GPL', 'PDF producer was GPL Ghostscript'),
	('GMD_PRODUCER_POWERPDF', 'PDF producer was PowerPDF'),
	('GOOGLE_DOCS_PHISH_MANY', 'Phishing via a Google Docs form'),
	('GOOGLE_DOCS_PHISH', 'Possible phishing via a Google Docs form'),
	('GOOG_MALWARE_DNLD', 'File download via Google - Malware?'),
	('GOOG_REDIR_SHORT', 'Google redirect to obscure spamvertised website + short message'),
	('GTUBE', 'Generic Test for Unsolicited Bulk Email'),
	('GUARANTEED_100_PERCENT', 'One hundred percent guaranteed'),
	('HASHCASH_20', 'Contains valid Hashcash token (20 bits)'),
	('HASHCASH_21', 'Contains valid Hashcash token (21 bits)'),
	('HASHCASH_22', 'Contains valid Hashcash token (22 bits)'),
	('HASHCASH_23', 'Contains valid Hashcash token (23 bits)'),
	('HASHCASH_24', 'Contains valid Hashcash token (24 bits)'),
	('HASHCASH_25', 'Contains valid Hashcash token (25 bits)'),
	('HASHCASH_2SPEND', 'Hashcash token already spent in another mail'),
	('HASHCASH_HIGH', 'Contains valid Hashcash token (>25 bits)'),
	('HAS_SHORT_URL', 'Message contains one or more shortened URLs'),
	('HDR_ORDER_FTSDMCXX_001C', 'Header order similar to spam (FTSDMCXX/MID variant)'),
	('HDR_ORDER_FTSDMCXX_BAT', 'Header order similar to spam (FTSDMCXX/boundary variant)'),
	('HEADER_COUNT_CTYPE', 'Multiple Content-Type headers found'),
	('HEADER_COUNT_SUBJECT', 'Multiple Subject headers found'),
	('HEADER_HOST_IN_BLACKLIST', 'Blacklisted header host or domain'),
	('HEADER_HOST_IN_WHITELIST', 'Whitelisted header host or domain'),
	('HEADER_SPAM', 'Bulk email fingerprint (header-based) found'),
	('HEAD_ILLEGAL_CHARS', 'Headers have too many raw illegal characters'),
	('HEAD_LONG', 'Message headers are very long'),
	('HELO_DYNAMIC_CHELLO_NL', 'Relay HELO\'d using suspicious hostname (Chello.nl)'),
	('HELO_DYNAMIC_DHCP', 'Relay HELO\'d using suspicious hostname (DHCP)'),
	('HELO_DYNAMIC_DIALIN', 'Relay HELO\'d using suspicious hostname (T-Dialin)'),
	('HELO_DYNAMIC_HCC', 'Relay HELO\'d using suspicious hostname (HCC)'),
	('HELO_DYNAMIC_HEXIP', 'Relay HELO\'d using suspicious hostname (Hex IP)'),
	('HELO_DYNAMIC_HOME_NL', 'Relay HELO\'d using suspicious hostname (Home.nl)'),
	('HELO_DYNAMIC_IPADDR2', 'Relay HELO\'d using suspicious hostname (IP addr 2)'),
	('HELO_DYNAMIC_IPADDR', 'Relay HELO\'d using suspicious hostname (IP addr 1)'),
	('HELO_DYNAMIC_ROGERS', 'Relay HELO\'d using suspicious hostname (Rogers)'),
	('HELO_DYNAMIC_SPLIT_IP', 'Relay HELO\'d using suspicious hostname (Split IP)'),
	('HELO_NO_DOMAIN', 'Relay reports its domain incorrectly'),
	('HELO_STATIC_HOST', 'Relay HELO\'d using static hostname'),
	('HEXHASH_WORD', 'Multiple instances of word + hexadecimal hash'),
	('HIDE_WIN_STATUS', 'Javascript to hide URLs in browser'),
	('HK_FAKENAME_EBAY', 'From name mentions eBay, but not relayed from there'),
	('HK_FAKENAME_MICROSOFT', 'From name mentions Microsoft, but not relayed from there'),
	('HK_NAME_DRUGS', 'From name contains drugs'),
	('HK_NAME_FREE', 'From name mentions free stuff'),
	('HK_RANDOM_ENVFROM', 'Envelope sender username looks random'),
	('HK_RANDOM_FROM', 'From username looks random'),
	('HK_RANDOM_REPLYTO', 'Reply-To username looks random'),
	('HS_BOBAX_MID_2', 'Bobax? Message-Id: <0IX000EJXVWDA000@example.com>'),
	('HS_BODY_1000', 'Heinlein Support Spamschutz Body-1000 Phishing'),
	('HS_BODY_1001', 'Heinlein Support Spamschutz Body-1001 Phishing'),
	('HS_BODY_1002', 'Heinlein Support Spamschutz Body-1002'),
	('HS_BODY_1003', 'Heinlein Support Spamschutz Body-1003'),
	('HS_BODY_1004', 'Heinlein Support Spamschutz Body-1004'),
	('HS_BODY_1005', 'Heinlein Support Spamschutz Body-1005 Spam'),
	('HS_BODY_1006', 'Heinlein Support Spamschutz Body-1006 Phishing'),
	('HS_BODY_1007', 'Heinlein Support Spamschutz Body-1007 Phishing'),
	('HS_BODY_1008', 'Heinlein Support Spamschutz Body-1008 Phishing'),
	('HS_BODY_1009', 'Heinlein Support Spamschutz Body-1009 Ebay Phishing'),
	('HS_BODY_100', 'Heinlein Support Spamschutz Body-100'),
	('HS_BODY_1010', 'Heinlein Support Spamschutz Body-1010'),
	('HS_BODY_1011', 'Heinlein Support Spamschutz Body-1011'),
	('HS_BODY_1012', 'Heinlein Support Spamschutz Body-1012'),
	('HS_BODY_1013', 'Heinlein Support Spamschutz Body-1013'),
	('HS_BODY_1014', 'Heinlein Support Spamschutz Body-1014'),
	('HS_BODY_1015', 'Heinlein Support Spamschutz Body-1015'),
	('HS_BODY_1016', 'Heinlein Support Spamschutz Body-1016'),
	('HS_BODY_1017', 'Heinlein Support Spamschutz Body-1017 Spam'),
	('HS_BODY_1018', 'Heinlein Support Spamschutz Body-1018'),
	('HS_BODY_1019', 'Heinlein Support Spamschutz Body-1019'),
	('HS_BODY_101', 'Heinlein Support Spamschutz Body-101'),
	('HS_BODY_1020', 'Heinlein Support Spamschutz Body-1020 Spam'),
	('HS_BODY_1021', 'Heinlein Support Spamschutz Body-1021'),
	('HS_BODY_1022', 'Heinlein Support Spamschutz Body-1022'),
	('HS_BODY_1023', 'Heinlein Support Spamschutz Body-1023'),
	('HS_BODY_1024', 'Heinlein Support Spamschutz Body-1024 Spam'),
	('HS_BODY_1025', 'Heinlein Support Spamschutz Body-1025'),
	('HS_BODY_1026', 'Heinlein Support Spamschutz Body-1026'),
	('HS_BODY_1027', 'Heinlein Support Spamschutz Body-1027'),
	('HS_BODY_1028', 'Heinlein Support Spamschutz Body-1028'),
	('HS_BODY_1029', 'Heinlein Support Spamschutz Body-1029'),
	('HS_BODY_1030', 'Heinlein Support Spamschutz Body-1030'),
	('HS_BODY_1031', 'Heinlein Support Spamschutz Body-1031'),
	('HS_BODY_1032', 'Heinlein Support Spamschutz Body-1032'),
	('HS_BODY_1033', 'Heinlein Support Spamschutz Body-1033 Spam'),
	('HS_BODY_1034', 'Heinlein Support Spamschutz Body-1034 Spam'),
	('HS_BODY_1035', 'Heinlein Support Spamschutz Body-1035'),
	('HS_BODY_1036', 'Heinlein Support Spamschutz Body-1036 Spam'),
	('HS_BODY_1037', 'Heinlein Support Spamschutz Body-1037 Spam'),
	('HS_BODY_1038', 'Heinlein Support Spamschutz Body-1038 Spam'),
	('HS_BODY_1039', 'Heinlein Support Spamschutz Body-1039 Spam'),
	('HS_BODY_1040', 'Heinlein Support Spamschutz Body-1040 Spam'),
	('HS_BODY_1041', 'Heinlein Support Spamschutz Body-1041 Spam'),
	('HS_BODY_1042', 'Heinlein Support Spamschutz Body-1042 Spam'),
	('HS_BODY_1043', 'Heinlein Support Spamschutz Body-1043'),
	('HS_BODY_1046', 'Heinlein Support Spamschutz Body-1046 Spam'),
	('HS_BODY_1047', 'Heinlein Support Spamschutz Body-1047'),
	('HS_BODY_1048', 'Heinlein Support Spamschutz Body-1048 Spam'),
	('HS_BODY_1049', 'Heinlein Support Spamschutz Body-1049 Spam'),
	('HS_BODY_1050', 'Heinlein Support Spamschutz Body-1050 Spam'),
	('HS_BODY_1051', 'Heinlein Support Spamschutz Body-1051 Spam'),
	('HS_BODY_1052', 'Heinlein Support Spamschutz Body-1052 Spam'),
	('HS_BODY_1053', 'Heinlein Support Spamschutz Body-1053 Spam'),
	('HS_BODY_1054', 'Heinlein Support Spamschutz Body-1054 Spam'),
	('HS_BODY_1055', 'Heinlein Support Spamschutz Body-1055 Spam'),
	('HS_BODY_1056', 'Heinlein Support Spamschutz Body-1056 Spam'),
	('HS_BODY_1057', 'Heinlein Support Spamschutz Body-1057 Spam'),
	('HS_BODY_1058', 'Heinlein Support Spamschutz Body-1058'),
	('HS_BODY_1059', 'Heinlein Support Spamschutz Body-1059'),
	('HS_BODY_1060', 'Heinlein Support Spamschutz Body-1060'),
	('HS_BODY_1061', 'Heinlein Support Spamschutz Body-1061 Phishing'),
	('HS_BODY_1062', 'Heinlein Support Spamschutz Body-1062 Spam'),
	('HS_BODY_1063', 'Heinlein Support Spamschutz Body-1063'),
	('HS_BODY_1064', 'Heinlein Support Spamschutz Body-1064'),
	('HS_BODY_1065', 'Heinlein Support Spamschutz Body-1065'),
	('HS_BODY_1066', 'Heinlein Support Spamschutz Body-1066'),
	('HS_BODY_1067', 'Heinlein Support Spamschutz Body-1067'),
	('HS_BODY_1068', 'Heinlein Support Spamschutz Body-1068'),
	('HS_BODY_1069', 'Heinlein Support Spamschutz Body-1069'),
	('HS_BODY_106', 'Heinlein Support Spamschutz Body-106'),
	('HS_BODY_1070', 'Heinlein Support Spamschutz Body-1070 Spam'),
	('HS_BODY_1072', 'Heinlein Support Spamschutz Body-1072 You are a backscatter'),
	('HS_BODY_1073', 'Heinlein Support Spamschutz Body-1073 You are a backscatter'),
	('HS_BODY_1074', 'Heinlein Support Spamschutz Body-1074 You are a backscatter'),
	('HS_BODY_1075', 'Heinlein Support Spamschutz Body-1075 You are a backscatter'),
	('HS_BODY_1076', 'Heinlein Support Spamschutz Body-1076 You are a backscatter'),
	('HS_BODY_1077', 'Heinlein Support Spamschutz Body-1077'),
	('HS_BODY_1078', 'Heinlein Support Spamschutz Body-1078 Phishing'),
	('HS_BODY_1079', 'Heinlein Support Spamschutz Body-1079'),
	('HS_BODY_107', 'Heinlein Support Spamschutz Body-107'),
	('HS_BODY_1080', 'Heinlein Support Spamschutz Body-1080 Spam'),
	('HS_BODY_1081', 'Heinlein Support Spamschutz Body-1081 Phishing'),
	('HS_BODY_1082', 'Heinlein Support Spamschutz Body-1082 Spam'),
	('HS_BODY_1083', 'Heinlein Support Spamschutz Body-1083 Phishing'),
	('HS_BODY_1085', 'Heinlein Support Spamschutz Body-1085 Phishing'),
	('HS_BODY_1087', 'Heinlein Support Spamschutz Body-1087'),
	('HS_BODY_1088', 'Heinlein Support Spamschutz Body-1088 SPAM'),
	('HS_BODY_1089', 'Heinlein Support Spamschutz Body-1089 SPAM'),
	('HS_BODY_108', 'Heinlein Support Spamschutz Body-108'),
	('HS_BODY_1090', 'Heinlein Support Spamschutz Body-1090 Spam'),
	('HS_BODY_1091', 'Heinlein Support Spamschutz Body-1091 Spam'),
	('HS_BODY_1092', 'Heinlein Support Spamschutz Body-1092 Spam'),
	('HS_BODY_1093', 'Heinlein Support Spamschutz Body-1093 Spam'),
	('HS_BODY_1094', 'Heinlein Support Spamschutz Body-1094 Spam'),
	('HS_BODY_1095', 'Heinlein Support Spamschutz Body-1095 Phishing'),
	('HS_BODY_1096', 'Heinlein Support Spamschutz Body-1096 Spam'),
	('HS_BODY_1097', 'Heinlein Support Spamschutz Body-1097 Phishing'),
	('HS_BODY_1098', 'Heinlein Support Spamschutz Body-1098 Phishing'),
	('HS_BODY_1099', 'Heinlein Support Spamschutz Body-1099 Phishing'),
	('HS_BODY_109', 'Heinlein Support Spamschutz Body-109'),
	('HS_BODY_10', 'Heinlein Support Spamschutz Body-10'),
	('HS_BODY_1101', 'Heinlein Support Spamschutz Body-1101 Malware'),
	('HS_BODY_1102', 'Heinlein Support Spamschutz Body-1102 Malware'),
	('HS_BODY_1103', 'Heinlein Support Spamschutz Body-1103 Phishing'),
	('HS_BODY_1104', 'Heinlein Support Spamschutz Body-1104 Phishing'),
	('HS_BODY_1105', 'Heinlein Support Spamschutz Body-1105 Spam'),
	('HS_BODY_1106', 'Heinlein Support Spamschutz Body-1106'),
	('HS_BODY_1107', 'Heinlein Support Spamschutz Body-1107 Phishing'),
	('HS_BODY_1108', 'Heinlein Support Spamschutz Body-1108 Phishing'),
	('HS_BODY_1109', 'Heinlein Support Spamschutz Body-1109 Phishing'),
	('HS_BODY_110', 'Heinlein Support Spamschutz Body-110'),
	('HS_BODY_1110', 'Heinlein Support Spamschutz Body-1110 Malware'),
	('HS_BODY_1111', 'Heinlein Support Spamschutz Body-1111 Malware'),
	('HS_BODY_1112', 'Heinlein Support Spamschutz Body-1112 Phishing'),
	('HS_BODY_1113', 'Heinlein Support Spamschutz Body-1113 Phishing'),
	('HS_BODY_1114', 'Heinlein Support Spamschutz Body-1114 Phishing'),
	('HS_BODY_1115', 'Heinlein Support Spamschutz Body-1115 Spam'),
	('HS_BODY_1116', 'Heinlein Support Spamschutz Body-1116 Phishing'),
	('HS_BODY_1117', 'Heinlein Support Spamschutz Body-1117 Malware'),
	('HS_BODY_1118', 'Heinlein Support Spamschutz Body-1118 Phishing'),
	('HS_BODY_1119', 'Heinlein Support Spamschutz Body-1119 Phishing'),
	('HS_BODY_111', 'Heinlein Support Spamschutz Body-111'),
	('HS_BODY_1120', 'Heinlein Support Spamschutz Body-1120 Spam'),
	('HS_BODY_1121', 'Heinlein Support Spamschutz Body-1121 Spam'),
	('HS_BODY_1122', 'Heinlein Support Spamschutz Body-1122 Spam'),
	('HS_BODY_1123', 'Heinlein Support Spamschutz Body-1123 Spam'),
	('HS_BODY_1124', 'Heinlein Support Spamschutz Body-1124 Phishing'),
	('HS_BODY_1125', 'Heinlein Support Spamschutz Body-1125 Bankphishing'),
	('HS_BODY_1127', 'Heinlein Support Spamschutz Body-1127 Malware'),
	('HS_BODY_1128', 'Heinlein Support Spamschutz Body-1128 Phishing'),
	('HS_BODY_1129', 'Heinlein Support Spamschutz Body-1129 Spam'),
	('HS_BODY_112', 'Heinlein Support Spamschutz Body-112'),
	('HS_BODY_1130', 'Heinlein Support Spamschutz Body-1130 Spam'),
	('HS_BODY_1131', 'Heinlein Support Spamschutz Body-1131 Spam'),
	('HS_BODY_1132', 'Heinlein Support Spamschutz Body-1132 Phishing'),
	('HS_BODY_1133', 'Heinlein Support Spamschutz Body-1133 Phishing'),
	('HS_BODY_1135', 'Heinlein Support Spamschutz Body-1135 Phishingverdacht'),
	('HS_BODY_1136', 'Heinlein Support Spamschutz Body-1136 Phishingverdacht'),
	('HS_BODY_1137', 'Heinlein Support Spamschutz Body-1137 Phishingverdacht'),
	('HS_BODY_1138', 'Heinlein Support Spamschutz Body-1138 Phishingverdacht'),
	('HS_BODY_1139', 'Heinlein Support Spamschutz Body-1139 Phishingverdacht'),
	('HS_BODY_113', 'Heinlein Support Spamschutz Body-113'),
	('HS_BODY_1140', 'Heinlein Support Spamschutz Body-1140'),
	('HS_BODY_1141', 'Heinlein Support Spamschutz Body-1141 Phishing'),
	('HS_BODY_1142', 'Heinlein Support Spamschutz Body-1142 Spam'),
	('HS_BODY_1143', 'Heinlein Support Spamschutz Body-1143 Phishing'),
	('HS_BODY_1144', 'Heinlein Support Spamschutz Body-1144 Phishing'),
	('HS_BODY_1145', 'Heinlein Support Spamschutz Body-1145 Spam'),
	('HS_BODY_1146', 'Heinlein Support Spamschutz Body-1146 Spam'),
	('HS_BODY_1147', 'Heinlein Support Spamschutz Body-1147 Spam'),
	('HS_BODY_1148', 'Heinlein Support Spamschutz Body-1148'),
	('HS_BODY_1149', 'Heinlein Support Spamschutz Body-1149 Spam'),
	('HS_BODY_114', 'Heinlein Support Spamschutz Body-114'),
	('HS_BODY_1150', 'Heinlein Support Spamschutz Body-1150 Spam'),
	('HS_BODY_1151', 'Heinlein Support Spamschutz Body-1151 Spam'),
	('HS_BODY_1152', 'Heinlein Support Spamschutz Body-1152 Spam'),
	('HS_BODY_1153', 'Heinlein Support Spamschutz Body-1153 Malware'),
	('HS_BODY_1154', 'Heinlein Support Spamschutz Body-1154 Malware'),
	('HS_BODY_1155', 'Heinlein Support Spamschutz Body-1155 Spam'),
	('HS_BODY_1156', 'Heinlein Support Spamschutz Body-1156 Spam'),
	('HS_BODY_1157', 'Heinlein Support Spamschutz Body-1157 Phishing'),
	('HS_BODY_1158', 'Heinlein Support Spamschutz Body-1158 Spam'),
	('HS_BODY_1159', 'Heinlein Support Spamschutz Body-1159 Phishing'),
	('HS_BODY_115', 'Heinlein Support Spamschutz Body-115'),
	('HS_BODY_1160', 'Heinlein Support Spamschutz Body-1160 Spam'),
	('HS_BODY_1161', 'Heinlein Support Spamschutz Body-1161 Spam'),
	('HS_BODY_1162', 'Heinlein Support Spamschutz Body-1162 Spam'),
	('HS_BODY_1163', 'Heinlein Support Spamschutz Body-1163 Spam'),
	('HS_BODY_1165', 'Heinlein Support Spamschutz Body-1165 Phishing'),
	('HS_BODY_1166', 'Heinlein Support Spamschutz Body-1166 Phishing'),
	('HS_BODY_1167', 'Heinlein Support Spamschutz Body-1167'),
	('HS_BODY_1168', 'Heinlein Support Spamschutz Body-1168'),
	('HS_BODY_1169', 'Heinlein Support Spamschutz Body-1169'),
	('HS_BODY_116', 'Heinlein Support Spamschutz Body-116'),
	('HS_BODY_1170', 'Heinlein Support Spamschutz Body-1170'),
	('HS_BODY_1171', 'Heinlein Support Spamschutz Body-1171 Spam'),
	('HS_BODY_1172', 'Heinlein Support Spamschutz Body-1172 Spam'),
	('HS_BODY_1173', 'Heinlein Support Spamschutz Body-1173 Phishing'),
	('HS_BODY_1174', 'Heinlein Support Spamschutz Body-1174 Phishing'),
	('HS_BODY_1176', 'Heinlein Support Spamschutz Body-1176 Phishing'),
	('HS_BODY_1177', 'Heinlein Support Spamschutz Body-1177 Phishing'),
	('HS_BODY_1178', 'Heinlein Support Spamschutz Body-1178 Phishing'),
	('HS_BODY_1179', 'Heinlein Support Spamschutz Body-1179 Spam'),
	('HS_BODY_117', 'Heinlein Support Spamschutz Body-117'),
	('HS_BODY_1180', 'Heinlein Support Spamschutz Body-1180 Spam'),
	('HS_BODY_1181', 'Heinlein Support Spamschutz Body-1181 Spam'),
	('HS_BODY_1182', 'Heinlein Support Spamschutz Body-1182 Spam'),
	('HS_BODY_1183', 'Heinlein Support Spamschutz Body-1183 Spam'),
	('HS_BODY_1184', 'Heinlein Support Spamschutz Body-1184 Spam'),
	('HS_BODY_1185', 'Heinlein Support Spamschutz Body-1185 SPAM'),
	('HS_BODY_1186', 'Heinlein Support Spamschutz Body-1186 Spam'),
	('HS_BODY_1187', 'Heinlein Support Spamschutz Body-1187 Spam'),
	('HS_BODY_1188', 'Heinlein Support Spamschutz Body-1188 Phishing'),
	('HS_BODY_1189', 'Heinlein Support Spamschutz Body-1189 Phishing'),
	('HS_BODY_118', 'Heinlein Support Spamschutz Body-118'),
	('HS_BODY_1190', 'Heinlein Support Spamschutz Body-1190 Phishing'),
	('HS_BODY_1191', 'Heinlein Support Spamschutz Body-1191 Phishing'),
	('HS_BODY_1192', 'Heinlein Support Spamschutz Body-1192 Phishing'),
	('HS_BODY_1193', 'Heinlein Support Spamschutz Body-1193 Spam'),
	('HS_BODY_1194', 'Heinlein Support Spamschutz Body-1194 Spam'),
	('HS_BODY_1195', 'Heinlein Support Spamschutz Body-1195 Spam'),
	('HS_BODY_1196', 'Heinlein Support Spamschutz Body-1196 Phishing'),
	('HS_BODY_1197', 'Heinlein Support Spamschutz Body-1197 Spam'),
	('HS_BODY_1198', 'Heinlein Support Spamschutz Body-1198 Phishing'),
	('HS_BODY_1199', 'Heinlein Support Spamschutz Body-1199 Spam'),
	('HS_BODY_119', 'Heinlein Support Spamschutz Body-119'),
	('HS_BODY_11', 'Heinlein Support Spamschutz Body-11'),
	('HS_BODY_1200', 'Heinlein Support Spamschutz Body-1200 Phishing'),
	('HS_BODY_1201', 'Heinlein Support Spamschutz Body-1201 Spam'),
	('HS_BODY_1202', 'Heinlein Support Spamschutz Body-1202 Spam'),
	('HS_BODY_1203', 'Heinlein Support Spamschutz Body-1203 Spam'),
	('HS_BODY_1204', 'Heinlein Support Spamschutz Body-1204 Spam'),
	('HS_BODY_1205', 'Heinlein Support Spamschutz Body-1205 Spam'),
	('HS_BODY_1206', 'Heinlein Support Spamschutz Body-1206 Spam'),
	('HS_BODY_1207', 'Heinlein Support Spamschutz Body-1207 Spam'),
	('HS_BODY_1208', 'Heinlein Support Spamschutz Body-1208 Phishing'),
	('HS_BODY_1209', 'Heinlein Support Spamschutz Body-1209 Spam'),
	('HS_BODY_120', 'Heinlein Support Spamschutz Body-120'),
	('HS_BODY_1210', 'Heinlein Support Spamschutz Body-1210 Spam'),
	('HS_BODY_1211', 'Heinlein Support Spamschutz Body-1211 Spam'),
	('HS_BODY_1212', 'Heinlein Support Spamschutz Body-1212 Spam'),
	('HS_BODY_1213', 'Heinlein Support Spamschutz Body-1213 Spam'),
	('HS_BODY_1214', 'Heinlein Support Spamschutz Body-1214 Spam'),
	('HS_BODY_1215', 'Heinlein Support Spamschutz Body-1215 Spam'),
	('HS_BODY_1216', 'Heinlein Support Spamschutz Body-1216 Spam'),
	('HS_BODY_1217', 'Heinlein Support Spamschutz Body-1217 Spam'),
	('HS_BODY_1219', 'Heinlein Support Spamschutz Body-1219 Phishing'),
	('HS_BODY_121', 'Heinlein Support Spamschutz Body-121'),
	('HS_BODY_1221', 'Heinlein Support Spamschutz Body-1221 Spam'),
	('HS_BODY_1222', 'Heinlein Support Spamschutz Body-1222 Malware'),
	('HS_BODY_1223', 'Heinlein Support Spamschutz Body-1223 Spam'),
	('HS_BODY_1224', 'Heinlein Support Spamschutz Body-1224 Spam'),
	('HS_BODY_1225', 'Heinlein Support Spamschutz Body-1225 Phishing'),
	('HS_BODY_1226', 'Heinlein Support Spamschutz Body-1226 Spam'),
	('HS_BODY_1227', 'Heinlein Support Spamschutz Body-1227 Phishing'),
	('HS_BODY_1228', 'Heinlein Support Spamschutz Body-1228 Spam'),
	('HS_BODY_1229', 'Heinlein Support Spamschutz Body-1229 Spam'),
	('HS_BODY_122', 'Heinlein Support Spamschutz Body-122'),
	('HS_BODY_1231', 'Heinlein Support Spamschutz Body-1231'),
	('HS_BODY_1232', 'Heinlein Support Spamschutz Body-1232 Spam'),
	('HS_BODY_1233', 'Heinlein Support Spamschutz Body-1233 Spam'),
	('HS_BODY_1234', 'Heinlein Support Spamschutz Body-1234 Spam'),
	('HS_BODY_1235', 'Heinlein Support Spamschutz Body-1235 Spam'),
	('HS_BODY_123', 'Heinlein Support Spamschutz Body-123'),
	('HS_BODY_124', 'Heinlein Support Spamschutz Body-124'),
	('HS_BODY_126', 'Heinlein Support Spamschutz Body-126'),
	('HS_BODY_127', 'Heinlein Support Spamschutz Body-127'),
	('HS_BODY_128', 'Heinlein Support Spamschutz Body-128'),
	('HS_BODY_129', 'Heinlein Support Spamschutz Body-129'),
	('HS_BODY_12', 'Heinlein Support Spamschutz Body-12'),
	('HS_BODY_131', 'Heinlein Support Spamschutz Body-131'),
	('HS_BODY_132', 'Heinlein Support Spamschutz Body-132'),
	('HS_BODY_133', 'Heinlein Support Spamschutz Body-133'),
	('HS_BODY_134', 'Heinlein Support Spamschutz Body-134'),
	('HS_BODY_135', 'Heinlein Support Spamschutz Body-135'),
	('HS_BODY_136', 'Heinlein Support Spamschutz Body-136'),
	('HS_BODY_137', 'Heinlein Support Spamschutz Body-137'),
	('HS_BODY_138', 'Heinlein Support Spamschutz Body-138'),
	('HS_BODY_139', 'Heinlein Support Spamschutz Body-139'),
	('HS_BODY_13', 'Heinlein Support Spamschutz Body-13'),
	('HS_BODY_140', 'Heinlein Support Spamschutz Body-140'),
	('HS_BODY_141', 'Heinlein Support Spamschutz Body-141'),
	('HS_BODY_142', 'Heinlein Support Spamschutz Body-142'),
	('HS_BODY_144', 'Heinlein Support Spamschutz Body-144'),
	('HS_BODY_145', 'Heinlein Support Spamschutz Body-145'),
	('HS_BODY_146', 'Heinlein Support Spamschutz Body-146'),
	('HS_BODY_147', 'Heinlein Support Spamschutz Body-147'),
	('HS_BODY_148', 'Heinlein Support Spamschutz Body-148'),
	('HS_BODY_149', 'Heinlein Support Spamschutz Body-149'),
	('HS_BODY_14', 'Heinlein Support Spamschutz Body-14'),
	('HS_BODY_150', 'Heinlein Support Spamschutz Body-150'),
	('HS_BODY_151', 'Heinlein Support Spamschutz Body-151'),
	('HS_BODY_152', 'Heinlein Support Spamschutz Body-152'),
	('HS_BODY_153', 'Heinlein Support Spamschutz Body-153'),
	('HS_BODY_154', 'Heinlein Support Spamschutz Body-154'),
	('HS_BODY_155', 'Heinlein Support Spamschutz Body-155'),
	('HS_BODY_156', 'Heinlein Support Spamschutz Body-156'),
	('HS_BODY_157', 'Heinlein Support Spamschutz Body-157'),
	('HS_BODY_158', 'Heinlein Support Spamschutz Body-158'),
	('HS_BODY_159', 'Heinlein Support Spamschutz Body-159'),
	('HS_BODY_15', 'Heinlein Support Spamschutz Body-15'),
	('HS_BODY_160', 'Heinlein Support Spamschutz Body-160'),
	('HS_BODY_161', 'Heinlein Support Spamschutz Body-161'),
	('HS_BODY_162', 'Heinlein Support Spamschutz Body-162'),
	('HS_BODY_163', 'Heinlein Support Spamschutz Body-163'),
	('HS_BODY_164', 'Heinlein Support Spamschutz Body-164'),
	('HS_BODY_165', 'Heinlein Support Spamschutz Body-165'),
	('HS_BODY_166', 'Heinlein Support Spamschutz Body-166'),
	('HS_BODY_167', 'Heinlein Support Spamschutz Body-167'),
	('HS_BODY_168', 'Heinlein Support Spamschutz Body-168'),
	('HS_BODY_169', 'Heinlein Support Spamschutz Body-169'),
	('HS_BODY_16', 'Heinlein Support Spamschutz Body-16'),
	('HS_BODY_170', 'Heinlein Support Spamschutz Body-170'),
	('HS_BODY_171', 'Heinlein Support Spamschutz Body-171'),
	('HS_BODY_172', 'Heinlein Support Spamschutz Body-172'),
	('HS_BODY_173', 'Heinlein Support Spamschutz Body-173'),
	('HS_BODY_174', 'Heinlein Support Spamschutz Body-174'),
	('HS_BODY_175', 'Heinlein Support Spamschutz Body-175'),
	('HS_BODY_176', 'Heinlein Support Spamschutz Body-176'),
	('HS_BODY_177', 'Heinlein Support Spamschutz Body-177'),
	('HS_BODY_178', 'Heinlein Support Spamschutz Body-178'),
	('HS_BODY_179', 'Heinlein Support Spamschutz Body-179'),
	('HS_BODY_17', 'Heinlein Support Spamschutz Body-17'),
	('HS_BODY_180', 'Heinlein Support Spamschutz Body-180'),
	('HS_BODY_181', 'Heinlein Support Spamschutz Body-181'),
	('HS_BODY_182', 'Heinlein Support Spamschutz Body-182'),
	('HS_BODY_183', 'Heinlein Support Spamschutz Body-183'),
	('HS_BODY_184', 'Heinlein Support Spamschutz Body-184'),
	('HS_BODY_185', 'Heinlein Support Spamschutz Body-185'),
	('HS_BODY_186', 'Heinlein Support Spamschutz Body-186'),
	('HS_BODY_187', 'Heinlein Support Spamschutz Body-187'),
	('HS_BODY_188', 'Heinlein Support Spamschutz Body-188'),
	('HS_BODY_189', 'Heinlein Support Spamschutz Body-189'),
	('HS_BODY_18', 'Heinlein Support Spamschutz Body-18'),
	('HS_BODY_190', 'Heinlein Support Spamschutz Body-190'),
	('HS_BODY_191', 'Heinlein Support Spamschutz Body-191'),
	('HS_BODY_192', 'Heinlein Support Spamschutz Body-192'),
	('HS_BODY_193', 'Heinlein Support Spamschutz Body-193'),
	('HS_BODY_194', 'Heinlein Support Spamschutz Body-194'),
	('HS_BODY_195', 'Heinlein Support Spamschutz Body-195'),
	('HS_BODY_196', 'Heinlein Support Spamschutz Body-196'),
	('HS_BODY_197', 'Heinlein Support Spamschutz Body-197'),
	('HS_BODY_198', 'Heinlein Support Spamschutz Body-198'),
	('HS_BODY_199', 'Heinlein Support Spamschutz Body-199'),
	('HS_BODY_19', 'Heinlein Support Spamschutz Body-19'),
	('HS_BODY_200', 'Heinlein Support Spamschutz Body-200'),
	('HS_BODY_201', 'Heinlein Support Spamschutz Body-201'),
	('HS_BODY_202', 'Heinlein Support Spamschutz Body-202'),
	('HS_BODY_203', 'Heinlein Support Spamschutz Body-203'),
	('HS_BODY_204', 'Heinlein Support Spamschutz Body-204'),
	('HS_BODY_205', 'Heinlein Support Spamschutz Body-205'),
	('HS_BODY_206', 'Heinlein Support Spamschutz Body-206'),
	('HS_BODY_207', 'Heinlein Support Spamschutz Body-207'),
	('HS_BODY_208', 'Heinlein Support Spamschutz Body-208'),
	('HS_BODY_209', 'Heinlein Support Spamschutz Body-209'),
	('HS_BODY_20', 'Heinlein Support Spamschutz Body-20'),
	('HS_BODY_210', 'Heinlein Support Spamschutz Body-210'),
	('HS_BODY_211', 'Heinlein Support Spamschutz Body-211'),
	('HS_BODY_212', 'Heinlein Support Spamschutz Body-212'),
	('HS_BODY_213', 'Heinlein Support Spamschutz Body-213'),
	('HS_BODY_214', 'Heinlein Support Spamschutz Body-214'),
	('HS_BODY_216', 'Heinlein Support Spamschutz Body-216'),
	('HS_BODY_217', 'Heinlein Support Spamschutz Body-217'),
	('HS_BODY_218', 'Heinlein Support Spamschutz Body-218'),
	('HS_BODY_219', 'Heinlein Support Spamschutz Body-219'),
	('HS_BODY_21', 'Heinlein Support Spamschutz Body-21'),
	('HS_BODY_220', 'Heinlein Support Spamschutz Body-220'),
	('HS_BODY_221', 'Heinlein Support Spamschutz Body-221'),
	('HS_BODY_222', 'Heinlein Support Spamschutz Body-222'),
	('HS_BODY_223', 'Heinlein Support Spamschutz Body-223'),
	('HS_BODY_224', 'Heinlein Support Spamschutz Body-224'),
	('HS_BODY_225', 'Heinlein Support Spamschutz Body-225'),
	('HS_BODY_226', 'Heinlein Support Spamschutz Body-226'),
	('HS_BODY_227', 'Heinlein Support Spamschutz Body-227'),
	('HS_BODY_228', 'Heinlein Support Spamschutz Body-228'),
	('HS_BODY_229', 'Heinlein Support Spamschutz Body-229'),
	('HS_BODY_22', 'Heinlein Support Spamschutz Body-22'),
	('HS_BODY_230', 'Heinlein Support Spamschutz Body-230'),
	('HS_BODY_231', 'Heinlein Support Spamschutz Body-231'),
	('HS_BODY_232', 'Heinlein Support Spamschutz Body-232'),
	('HS_BODY_233', 'Heinlein Support Spamschutz Body-233'),
	('HS_BODY_234', 'Heinlein Support Spamschutz Body-234'),
	('HS_BODY_235', 'Heinlein Support Spamschutz Body-235'),
	('HS_BODY_236', 'Heinlein Support Spamschutz Body-236'),
	('HS_BODY_237', 'Heinlein Support Spamschutz Body-237'),
	('HS_BODY_238', 'Heinlein Support Spamschutz Body-238'),
	('HS_BODY_239', 'Heinlein Support Spamschutz Body-239'),
	('HS_BODY_23', 'Heinlein Support Spamschutz Body-23'),
	('HS_BODY_240', 'Heinlein Support Spamschutz Body-240'),
	('HS_BODY_241', 'Heinlein Support Spamschutz Body-241'),
	('HS_BODY_242', 'Heinlein Support Spamschutz Body-242'),
	('HS_BODY_243', 'Heinlein Support Spamschutz Body-243'),
	('HS_BODY_244', 'Heinlein Support Spamschutz Body-244'),
	('HS_BODY_245', 'Heinlein Support Spamschutz Body-245'),
	('HS_BODY_246', 'Heinlein Support Spamschutz Body-246'),
	('HS_BODY_247', 'Heinlein Support Spamschutz Body-247'),
	('HS_BODY_24', 'Heinlein Support Spamschutz Body-24'),
	('HS_BODY_250', 'Heinlein Support Spamschutz Body-250'),
	('HS_BODY_251', 'Heinlein Support Spamschutz Body-251'),
	('HS_BODY_252', 'Heinlein Support Spamschutz Body-252'),
	('HS_BODY_253', 'Heinlein Support Spamschutz Body-253'),
	('HS_BODY_254', 'Heinlein Support Spamschutz Body-254'),
	('HS_BODY_255', 'Heinlein Support Spamschutz Body-255'),
	('HS_BODY_256', 'Heinlein Support Spamschutz Body-256'),
	('HS_BODY_257', 'Heinlein Support Spamschutz Body-257'),
	('HS_BODY_258', 'Heinlein Support Spamschutz Body-258'),
	('HS_BODY_259', 'Heinlein Support Spamschutz Body-259'),
	('HS_BODY_25', 'Heinlein Support Spamschutz Body-25'),
	('HS_BODY_260', 'Heinlein Support Spamschutz Body-260'),
	('HS_BODY_261', 'Heinlein Support Spamschutz Body-261'),
	('HS_BODY_262', 'Heinlein Support Spamschutz Body-262'),
	('HS_BODY_263', 'Heinlein Support Spamschutz Body-263'),
	('HS_BODY_264', 'Heinlein Support Spamschutz Body-264'),
	('HS_BODY_265', 'Heinlein Support Spamschutz Body-265'),
	('HS_BODY_266', 'Heinlein Support Spamschutz Body-266'),
	('HS_BODY_267', 'Heinlein Support Spamschutz Body-267'),
	('HS_BODY_268', 'Heinlein Support Spamschutz Body-268'),
	('HS_BODY_269', 'Heinlein Support Spamschutz Body-269'),
	('HS_BODY_26', 'Heinlein Support Spamschutz Body-26'),
	('HS_BODY_270', 'Heinlein Support Spamschutz Body-270'),
	('HS_BODY_271', 'Heinlein Support Spamschutz Body-271'),
	('HS_BODY_272', 'Heinlein Support Spamschutz Body-272'),
	('HS_BODY_273', 'Heinlein Support Spamschutz Body-273'),
	('HS_BODY_274', 'Heinlein Support Spamschutz Body-274'),
	('HS_BODY_275', 'Heinlein Support Spamschutz Body-275'),
	('HS_BODY_276', 'Heinlein Support Spamschutz Body-276'),
	('HS_BODY_277', 'Heinlein Support Spamschutz Body-277'),
	('HS_BODY_278', 'Heinlein Support Spamschutz Body-278'),
	('HS_BODY_279', 'Heinlein Support Spamschutz Body-279'),
	('HS_BODY_27', 'Heinlein Support Spamschutz Body-27'),
	('HS_BODY_280', 'Heinlein Support Spamschutz Body-280'),
	('HS_BODY_281', 'Heinlein Support Spamschutz Body-281'),
	('HS_BODY_282', 'Heinlein Support Spamschutz Body-282'),
	('HS_BODY_283', 'Heinlein Support Spamschutz Body-283'),
	('HS_BODY_284', 'Heinlein Support Spamschutz Body-284'),
	('HS_BODY_285', 'Heinlein Support Spamschutz Body-285'),
	('HS_BODY_286', 'Heinlein Support Spamschutz Body-286'),
	('HS_BODY_287', 'Heinlein Support Spamschutz Body-287'),
	('HS_BODY_288', 'Heinlein Support Spamschutz Body-288'),
	('HS_BODY_289', 'Heinlein Support Spamschutz Body-289'),
	('HS_BODY_28', 'Heinlein Support Spamschutz Body-28'),
	('HS_BODY_290', 'Heinlein Support Spamschutz Body-290'),
	('HS_BODY_291', 'Heinlein Support Spamschutz Body-291'),
	('HS_BODY_292', 'Heinlein Support Spamschutz Body-292'),
	('HS_BODY_293', 'Heinlein Support Spamschutz Body-293'),
	('HS_BODY_294', 'Heinlein Support Spamschutz Body-294'),
	('HS_BODY_295', 'Heinlein Support Spamschutz Body-295'),
	('HS_BODY_296', 'Heinlein Support Spamschutz Body-296'),
	('HS_BODY_297', 'Heinlein Support Spamschutz Body-297'),
	('HS_BODY_298', 'Heinlein Support Spamschutz Body-298'),
	('HS_BODY_299', 'Heinlein Support Spamschutz Body-299'),
	('HS_BODY_29', 'Heinlein Support Spamschutz Body-29'),
	('HS_BODY_2', 'Heinlein Support Spamschutz Body-2'),
	('HS_BODY_300', 'Heinlein Support Spamschutz Body-300'),
	('HS_BODY_301', 'Heinlein Support Spamschutz Body-301'),
	('HS_BODY_302', 'Heinlein Support Spamschutz Body-302'),
	('HS_BODY_303', 'Heinlein Support Spamschutz Body-303'),
	('HS_BODY_304', 'Heinlein Support Spamschutz Body-304'),
	('HS_BODY_305', 'Heinlein Support Spamschutz Body-305'),
	('HS_BODY_306', 'Heinlein Support Spamschutz Body-306'),
	('HS_BODY_307', 'Heinlein Support Spamschutz Body-307'),
	('HS_BODY_308', 'Heinlein Support Spamschutz Body-308'),
	('HS_BODY_309', 'Heinlein Support Spamschutz Body-309'),
	('HS_BODY_30', 'Heinlein Support Spamschutz Body-30'),
	('HS_BODY_310', 'Heinlein Support Spamschutz Body-310'),
	('HS_BODY_311', 'Heinlein Support Spamschutz Body-311'),
	('HS_BODY_312', 'Heinlein Support Spamschutz Body-312'),
	('HS_BODY_313', 'Heinlein Support Spamschutz Body-313'),
	('HS_BODY_314', 'Heinlein Support Spamschutz Body-314'),
	('HS_BODY_315', 'Heinlein Support Spamschutz Body-315'),
	('HS_BODY_316', 'Heinlein Support Spamschutz Body-316'),
	('HS_BODY_317', 'Heinlein Support Spamschutz Body-317'),
	('HS_BODY_318', 'Heinlein Support Spamschutz Body-318'),
	('HS_BODY_319', 'Heinlein Support Spamschutz Body-319'),
	('HS_BODY_31', 'Heinlein Support Spamschutz Body-31'),
	('HS_BODY_320', 'Heinlein Support Spamschutz Body-320'),
	('HS_BODY_321', 'Heinlein Support Spamschutz Body-321'),
	('HS_BODY_322', 'Heinlein Support Spamschutz Body-322'),
	('HS_BODY_323', 'Heinlein Support Spamschutz Body-323'),
	('HS_BODY_324', 'Heinlein Support Spamschutz Body-324'),
	('HS_BODY_325', 'Heinlein Support Spamschutz Body-325'),
	('HS_BODY_326', 'Heinlein Support Spamschutz Body-326'),
	('HS_BODY_327', 'Heinlein Support Spamschutz Body-327'),
	('HS_BODY_328', 'Heinlein Support Spamschutz Body-328'),
	('HS_BODY_329', 'Heinlein Support Spamschutz Body-329'),
	('HS_BODY_32', 'Heinlein Support Spamschutz Body-32'),
	('HS_BODY_330', 'Heinlein Support Spamschutz Body-330'),
	('HS_BODY_331', 'Heinlein Support Spamschutz Body-331'),
	('HS_BODY_332', 'Heinlein Support Spamschutz Body-332'),
	('HS_BODY_333', 'Heinlein Support Spamschutz Body-333'),
	('HS_BODY_334', 'Heinlein Support Spamschutz Body-334'),
	('HS_BODY_335', 'Heinlein Support Spamschutz Body-335'),
	('HS_BODY_336', 'Heinlein Support Spamschutz Body-336'),
	('HS_BODY_337', 'Heinlein Support Spamschutz Body-337'),
	('HS_BODY_338', 'Heinlein Support Spamschutz Body-338'),
	('HS_BODY_339', 'Heinlein Support Spamschutz Body-339'),
	('HS_BODY_33', 'Heinlein Support Spamschutz Body-33'),
	('HS_BODY_340', 'Heinlein Support Spamschutz Body-340'),
	('HS_BODY_341', 'Heinlein Support Spamschutz Body-341'),
	('HS_BODY_342', 'Heinlein Support Spamschutz Body-342'),
	('HS_BODY_343', 'Heinlein Support Spamschutz Body-343'),
	('HS_BODY_344', 'Heinlein Support Spamschutz Body-344'),
	('HS_BODY_345', 'Heinlein Support Spamschutz Body-345'),
	('HS_BODY_346', 'Heinlein Support Spamschutz Body-346'),
	('HS_BODY_347', 'Heinlein Support Spamschutz Body-347'),
	('HS_BODY_348', 'Heinlein Support Spamschutz Body-348'),
	('HS_BODY_349', 'Heinlein Support Spamschutz Body-349'),
	('HS_BODY_34', 'Heinlein Support Spamschutz Body-34'),
	('HS_BODY_350', 'Heinlein Support Spamschutz Body-350'),
	('HS_BODY_351', 'Heinlein Support Spamschutz Body-351'),
	('HS_BODY_352', 'Heinlein Support Spamschutz Body-352'),
	('HS_BODY_353', 'Heinlein Support Spamschutz Body-353'),
	('HS_BODY_354', 'Heinlein Support Spamschutz Body-354'),
	('HS_BODY_355', 'Heinlein Support Spamschutz Body-355'),
	('HS_BODY_356', 'Heinlein Support Spamschutz Body-356'),
	('HS_BODY_357', 'Heinlein Support Spamschutz Body-357'),
	('HS_BODY_358', 'Heinlein Support Spamschutz Body-358'),
	('HS_BODY_359', 'Heinlein Support Spamschutz Body-359'),
	('HS_BODY_35', 'Heinlein Support Spamschutz Body-35'),
	('HS_BODY_360', 'Heinlein Support Spamschutz Body-360'),
	('HS_BODY_361', 'Heinlein Support Spamschutz Body-361'),
	('HS_BODY_362', 'Heinlein Support Spamschutz Body-362'),
	('HS_BODY_363', 'Heinlein Support Spamschutz Body-363'),
	('HS_BODY_364', 'Heinlein Support Spamschutz Body-364'),
	('HS_BODY_365', 'Heinlein Support Spamschutz Body-365'),
	('HS_BODY_366', 'Heinlein Support Spamschutz Body-366'),
	('HS_BODY_367', 'Heinlein Support Spamschutz Body-367'),
	('HS_BODY_368', 'Heinlein Support Spamschutz Body-368'),
	('HS_BODY_369', 'Heinlein Support Spamschutz Body-369'),
	('HS_BODY_36', 'Heinlein Support Spamschutz Body-36'),
	('HS_BODY_370', 'Heinlein Support Spamschutz Body-370'),
	('HS_BODY_371', 'Heinlein Support Spamschutz Body-371'),
	('HS_BODY_372', 'Heinlein Support Spamschutz Body-372'),
	('HS_BODY_373', 'Heinlein Support Spamschutz Body-373'),
	('HS_BODY_374', 'Heinlein Support Spamschutz Body-374'),
	('HS_BODY_375', 'Heinlein Support Spamschutz Body-375'),
	('HS_BODY_376', 'Heinlein Support Spamschutz Body-376'),
	('HS_BODY_377', 'Heinlein Support Spamschutz Body-377'),
	('HS_BODY_378', 'Heinlein Support Spamschutz Body-378'),
	('HS_BODY_379', 'Heinlein Support Spamschutz Body-379'),
	('HS_BODY_37', 'Heinlein Support Spamschutz Body-37'),
	('HS_BODY_380', 'Heinlein Support Spamschutz Body-380'),
	('HS_BODY_381', 'Heinlein Support Spamschutz Body-381'),
	('HS_BODY_382', 'Heinlein Support Spamschutz Body-382'),
	('HS_BODY_383', 'Heinlein Support Spamschutz Body-383'),
	('HS_BODY_384', 'Heinlein Support Spamschutz Body-384'),
	('HS_BODY_385', 'Heinlein Support Spamschutz Body-385'),
	('HS_BODY_386', 'Heinlein Support Spamschutz Body-386'),
	('HS_BODY_387', 'Heinlein Support Spamschutz Body-387'),
	('HS_BODY_388', 'Heinlein Support Spamschutz Body-388'),
	('HS_BODY_389', 'Heinlein Support Spamschutz Body-389'),
	('HS_BODY_38', 'Heinlein Support Spamschutz Body-38'),
	('HS_BODY_390', 'Heinlein Support Spamschutz Body-390'),
	('HS_BODY_391', 'Heinlein Support Spamschutz Body-391'),
	('HS_BODY_392', 'Heinlein Support Spamschutz Body-392'),
	('HS_BODY_393', 'Heinlein Support Spamschutz Body-393'),
	('HS_BODY_394', 'Heinlein Support Spamschutz Body-394'),
	('HS_BODY_395', 'Heinlein Support Spamschutz Body-395'),
	('HS_BODY_396', 'Heinlein Support Spamschutz Body-396'),
	('HS_BODY_397', 'Heinlein Support Spamschutz Body-397'),
	('HS_BODY_398', 'Heinlein Support Spamschutz Body-398'),
	('HS_BODY_399', 'Heinlein Support Spamschutz Body-399'),
	('HS_BODY_39', 'Heinlein Support Spamschutz Body-39'),
	('HS_BODY_400', 'Heinlein Support Spamschutz Body-400'),
	('HS_BODY_401', 'Heinlein Support Spamschutz Body-401'),
	('HS_BODY_402', 'Heinlein Support Spamschutz Body-402'),
	('HS_BODY_403', 'Heinlein Support Spamschutz Body-403'),
	('HS_BODY_404', 'Heinlein Support Spamschutz Body-404'),
	('HS_BODY_405', 'Heinlein Support Spamschutz Body-405'),
	('HS_BODY_406', 'Heinlein Support Spamschutz Body-406'),
	('HS_BODY_407', 'Heinlein Support Spamschutz Body-407'),
	('HS_BODY_408', 'Heinlein Support Spamschutz Body-408'),
	('HS_BODY_409', 'Heinlein Support Spamschutz Body-409'),
	('HS_BODY_40', 'Heinlein Support Spamschutz Body-40'),
	('HS_BODY_410', 'Heinlein Support Spamschutz Body-410'),
	('HS_BODY_411', 'Heinlein Support Spamschutz Body-411'),
	('HS_BODY_412', 'Heinlein Support Spamschutz Body-412'),
	('HS_BODY_413', 'Heinlein Support Spamschutz Body-413'),
	('HS_BODY_414', 'Heinlein Support Spamschutz Body-414'),
	('HS_BODY_415', 'Heinlein Support Spamschutz Body-415'),
	('HS_BODY_416', 'Heinlein Support Spamschutz Body-416'),
	('HS_BODY_417', 'Heinlein Support Spamschutz Body-417'),
	('HS_BODY_418', 'Heinlein Support Spamschutz Body-418'),
	('HS_BODY_419', 'Heinlein Support Spamschutz Body-419'),
	('HS_BODY_41', 'Heinlein Support Spamschutz Body-41'),
	('HS_BODY_420', 'Heinlein Support Spamschutz Body-420'),
	('HS_BODY_421', 'Heinlein Support Spamschutz Body-421'),
	('HS_BODY_422', 'Heinlein Support Spamschutz Body-422'),
	('HS_BODY_423', 'Heinlein Support Spamschutz Body-423'),
	('HS_BODY_424', 'Heinlein Support Spamschutz Body-424'),
	('HS_BODY_425', 'Heinlein Support Spamschutz Body-425'),
	('HS_BODY_426', 'Heinlein Support Spamschutz Body-426'),
	('HS_BODY_427', 'Heinlein Support Spamschutz Body-427'),
	('HS_BODY_428', 'Heinlein Support Spamschutz Body-428'),
	('HS_BODY_429', 'Heinlein Support Spamschutz Body-429'),
	('HS_BODY_42', 'Heinlein Support Spamschutz Body-42'),
	('HS_BODY_430', 'Heinlein Support Spamschutz Body-430'),
	('HS_BODY_431', 'Heinlein Support Spamschutz Body-431'),
	('HS_BODY_432', 'Heinlein Support Spamschutz Body-432'),
	('HS_BODY_433', 'Heinlein Support Spamschutz Body-433'),
	('HS_BODY_434', 'Heinlein Support Spamschutz Body-434'),
	('HS_BODY_435', 'Heinlein Support Spamschutz Body-435'),
	('HS_BODY_436', 'Heinlein Support Spamschutz Body-436'),
	('HS_BODY_437', 'Heinlein Support Spamschutz Body-437'),
	('HS_BODY_438', 'Heinlein Support Spamschutz Body-438'),
	('HS_BODY_439', 'Heinlein Support Spamschutz Body-439'),
	('HS_BODY_43', 'Heinlein Support Spamschutz Body-43'),
	('HS_BODY_440', 'Heinlein Support Spamschutz Body-440'),
	('HS_BODY_441', 'Heinlein Support Spamschutz Body-441'),
	('HS_BODY_442', 'Heinlein Support Spamschutz Body-442'),
	('HS_BODY_443', 'Heinlein Support Spamschutz Body-443'),
	('HS_BODY_444', 'Heinlein Support Spamschutz Body-444'),
	('HS_BODY_445', 'Heinlein Support Spamschutz Body-445'),
	('HS_BODY_446', 'Heinlein Support Spamschutz Body-446'),
	('HS_BODY_447', 'Heinlein Support Spamschutz Body-447'),
	('HS_BODY_448', 'Heinlein Support Spamschutz Body-448'),
	('HS_BODY_449', 'Heinlein Support Spamschutz Body-449'),
	('HS_BODY_44', 'Heinlein Support Spamschutz Body-44'),
	('HS_BODY_450', 'Heinlein Support Spamschutz Body-450'),
	('HS_BODY_451', 'Heinlein Support Spamschutz Body-451'),
	('HS_BODY_452', 'Heinlein Support Spamschutz Body-452'),
	('HS_BODY_453', 'Heinlein Support Spamschutz Body-453'),
	('HS_BODY_454', 'Heinlein Support Spamschutz Body-454'),
	('HS_BODY_455', 'Heinlein Support Spamschutz Body-455'),
	('HS_BODY_456', 'Heinlein Support Spamschutz Body-456'),
	('HS_BODY_457', 'Heinlein Support Spamschutz Body-457'),
	('HS_BODY_458', 'Heinlein Support Spamschutz Body-458'),
	('HS_BODY_459', 'Heinlein Support Spamschutz Body-459'),
	('HS_BODY_45', 'Heinlein Support Spamschutz Body-45'),
	('HS_BODY_460', 'Heinlein Support Spamschutz Body-460'),
	('HS_BODY_461', 'Heinlein Support Spamschutz Body-461'),
	('HS_BODY_462', 'Heinlein Support Spamschutz Body-462'),
	('HS_BODY_463', 'Heinlein Support Spamschutz Body-463'),
	('HS_BODY_464', 'Heinlein Support Spamschutz Body-464'),
	('HS_BODY_465', 'Heinlein Support Spamschutz Body-465'),
	('HS_BODY_466', 'Heinlein Support Spamschutz Body-466'),
	('HS_BODY_467', 'Heinlein Support Spamschutz Body-467'),
	('HS_BODY_468', 'Heinlein Support Spamschutz Body-468'),
	('HS_BODY_469', 'Heinlein Support Spamschutz Body-469'),
	('HS_BODY_46', 'Heinlein Support Spamschutz Body-46'),
	('HS_BODY_470', 'Heinlein Support Spamschutz Body-470'),
	('HS_BODY_471', 'Heinlein Support Spamschutz Body-471'),
	('HS_BODY_472', 'Heinlein Support Spamschutz Body-472'),
	('HS_BODY_474', 'Heinlein Support Spamschutz Body-474'),
	('HS_BODY_475', 'Heinlein Support Spamschutz Body-475'),
	('HS_BODY_476', 'Heinlein Support Spamschutz Body-476'),
	('HS_BODY_477', 'Heinlein Support Spamschutz Body-477'),
	('HS_BODY_478', 'Heinlein Support Spamschutz Body-478'),
	('HS_BODY_479', 'Heinlein Support Spamschutz Body-479'),
	('HS_BODY_47', 'Heinlein Support Spamschutz Body-47'),
	('HS_BODY_480', 'Heinlein Support Spamschutz Body-480'),
	('HS_BODY_481', 'Heinlein Support Spamschutz Body-481'),
	('HS_BODY_482', 'Heinlein Support Spamschutz Body-482'),
	('HS_BODY_483', 'Heinlein Support Spamschutz Body-483'),
	('HS_BODY_484', 'Heinlein Support Spamschutz Body-484'),
	('HS_BODY_485', 'Heinlein Support Spamschutz Body-485'),
	('HS_BODY_486', 'Heinlein Support Spamschutz Body-486'),
	('HS_BODY_487', 'Heinlein Support Spamschutz Body-487'),
	('HS_BODY_488', 'Heinlein Support Spamschutz Body-488'),
	('HS_BODY_489', 'Heinlein Support Spamschutz Body-489'),
	('HS_BODY_48', 'Heinlein Support Spamschutz Body-48'),
	('HS_BODY_490', 'Heinlein Support Spamschutz Body-490'),
	('HS_BODY_491', 'Heinlein Support Spamschutz Body-491'),
	('HS_BODY_492', 'Heinlein Support Spamschutz Body-492'),
	('HS_BODY_493', 'Heinlein Support Spamschutz Body-493'),
	('HS_BODY_494', 'Heinlein Support Spamschutz Body-494'),
	('HS_BODY_495', 'Heinlein Support Spamschutz Body-495'),
	('HS_BODY_496', 'Heinlein Support Spamschutz Body-496'),
	('HS_BODY_497', 'Heinlein Support Spamschutz Body-497'),
	('HS_BODY_498', 'Heinlein Support Spamschutz Body-498'),
	('HS_BODY_499', 'Heinlein Support Spamschutz Body-499'),
	('HS_BODY_49', 'Heinlein Support Spamschutz Body-49'),
	('HS_BODY_4', 'Heinlein Support Spamschutz Body-4'),
	('HS_BODY_500', 'Heinlein Support Spamschutz Body-500'),
	('HS_BODY_501', 'Heinlein Support Spamschutz Body-501'),
	('HS_BODY_502', 'Heinlein Support Spamschutz Body-502'),
	('HS_BODY_503', 'Heinlein Support Spamschutz Body-503'),
	('HS_BODY_504', 'Heinlein Support Spamschutz Body-504'),
	('HS_BODY_505', 'Heinlein Support Spamschutz Body-505'),
	('HS_BODY_506', 'Heinlein Support Spamschutz Body-506'),
	('HS_BODY_507', 'Heinlein Support Spamschutz Body-507'),
	('HS_BODY_508', 'Heinlein Support Spamschutz Body-508'),
	('HS_BODY_509', 'Heinlein Support Spamschutz Body-509'),
	('HS_BODY_50', 'Heinlein Support Spamschutz Body-50'),
	('HS_BODY_510', 'Heinlein Support Spamschutz Body-510'),
	('HS_BODY_511', 'Heinlein Support Spamschutz Body-511'),
	('HS_BODY_512', 'Heinlein Support Spamschutz Body-512'),
	('HS_BODY_513', 'Heinlein Support Spamschutz Body-513'),
	('HS_BODY_514', 'Heinlein Support Spamschutz Body-514'),
	('HS_BODY_515', 'Heinlein Support Spamschutz Body-515'),
	('HS_BODY_516', 'Heinlein Support Spamschutz Body-516'),
	('HS_BODY_517', 'Heinlein Support Spamschutz Body-517'),
	('HS_BODY_518', 'Heinlein Support Spamschutz Body-518'),
	('HS_BODY_519', 'Heinlein Support Spamschutz Body-519'),
	('HS_BODY_51', 'Heinlein Support Spamschutz Body-51'),
	('HS_BODY_520', 'Heinlein Support Spamschutz Body-520'),
	('HS_BODY_521', 'Heinlein Support Spamschutz Body-521'),
	('HS_BODY_522', 'Heinlein Support Spamschutz Body-522'),
	('HS_BODY_523', 'Heinlein Support Spamschutz Body-523'),
	('HS_BODY_524', 'Heinlein Support Spamschutz Body-524'),
	('HS_BODY_525', 'Heinlein Support Spamschutz Body-525'),
	('HS_BODY_526', 'Heinlein Support Spamschutz Body-526'),
	('HS_BODY_527', 'Heinlein Support Spamschutz Body-527'),
	('HS_BODY_528', 'Heinlein Support Spamschutz Body-528'),
	('HS_BODY_529', 'Heinlein Support Spamschutz Body-529'),
	('HS_BODY_52', 'Heinlein Support Spamschutz Body-52'),
	('HS_BODY_530', 'Heinlein Support Spamschutz Body-530'),
	('HS_BODY_531', 'Heinlein Support Spamschutz Body-531'),
	('HS_BODY_532', 'Heinlein Support Spamschutz Body-532'),
	('HS_BODY_533', 'Heinlein Support Spamschutz Body-533'),
	('HS_BODY_534', 'Heinlein Support Spamschutz Body-534'),
	('HS_BODY_535', 'Heinlein Support Spamschutz Body-535'),
	('HS_BODY_536', 'Heinlein Support Spamschutz Body-536'),
	('HS_BODY_537', 'Heinlein Support Spamschutz Body-537'),
	('HS_BODY_538', 'Heinlein Support Spamschutz Body-538'),
	('HS_BODY_539', 'Heinlein Support Spamschutz Body-539'),
	('HS_BODY_53', 'Heinlein Support Spamschutz Body-53'),
	('HS_BODY_540', 'Heinlein Support Spamschutz Body-540'),
	('HS_BODY_541', 'Heinlein Support Spamschutz Body-541'),
	('HS_BODY_542', 'Heinlein Support Spamschutz Body-542'),
	('HS_BODY_543', 'Heinlein Support Spamschutz Body-543'),
	('HS_BODY_544', 'Heinlein Support Spamschutz Body-544'),
	('HS_BODY_545', 'Heinlein Support Spamschutz Body-545'),
	('HS_BODY_546', 'Heinlein Support Spamschutz Body-546'),
	('HS_BODY_547', 'Heinlein Support Spamschutz Body-547'),
	('HS_BODY_548', 'Heinlein Support Spamschutz Body-548'),
	('HS_BODY_549', 'Heinlein Support Spamschutz Body-549'),
	('HS_BODY_54', 'Heinlein Support Spamschutz Body-54'),
	('HS_BODY_550', 'Heinlein Support Spamschutz Body-550'),
	('HS_BODY_551', 'Heinlein Support Spamschutz Body-551'),
	('HS_BODY_552', 'Heinlein Support Spamschutz Body-552'),
	('HS_BODY_553', 'Heinlein Support Spamschutz Body-553'),
	('HS_BODY_554', 'Heinlein Support Spamschutz Body-554'),
	('HS_BODY_555', 'Heinlein Support Spamschutz Body-555'),
	('HS_BODY_556', 'Heinlein Support Spamschutz Body-556'),
	('HS_BODY_557', 'Heinlein Support Spamschutz Body-557'),
	('HS_BODY_558', 'Heinlein Support Spamschutz Body-558'),
	('HS_BODY_559', 'Heinlein Support Spamschutz Body-559'),
	('HS_BODY_560', 'Heinlein Support Spamschutz Body-560'),
	('HS_BODY_561', 'Heinlein Support Spamschutz Body-561'),
	('HS_BODY_562', 'Heinlein Support Spamschutz Body-562'),
	('HS_BODY_563', 'Heinlein Support Spamschutz Body-563'),
	('HS_BODY_564', 'Heinlein Support Spamschutz Body-564'),
	('HS_BODY_565', 'Heinlein Support Spamschutz Body-565'),
	('HS_BODY_566', 'Heinlein Support Spamschutz Body-566'),
	('HS_BODY_567', 'Heinlein Support Spamschutz Body-567'),
	('HS_BODY_568', 'Heinlein Support Spamschutz Body-568'),
	('HS_BODY_569', 'Heinlein Support Spamschutz Body-569'),
	('HS_BODY_56', 'Heinlein Support Spamschutz Body-56'),
	('HS_BODY_570', 'Heinlein Support Spamschutz Body-570'),
	('HS_BODY_571', 'Heinlein Support Spamschutz Body-571'),
	('HS_BODY_572', 'Heinlein Support Spamschutz Body-572'),
	('HS_BODY_573', 'Heinlein Support Spamschutz Body-573'),
	('HS_BODY_574', 'Heinlein Support Spamschutz Body-574'),
	('HS_BODY_575', 'Heinlein Support Spamschutz Body-575'),
	('HS_BODY_576', 'Heinlein Support Spamschutz Body-576'),
	('HS_BODY_577', 'Heinlein Support Spamschutz Body-577'),
	('HS_BODY_578', 'Heinlein Support Spamschutz Body-578'),
	('HS_BODY_579', 'Heinlein Support Spamschutz Body-579'),
	('HS_BODY_57', 'Heinlein Support Spamschutz Body-57'),
	('HS_BODY_580', 'Heinlein Support Spamschutz Body-580'),
	('HS_BODY_581', 'Heinlein Support Spamschutz Body-581'),
	('HS_BODY_582', 'Heinlein Support Spamschutz Body-582'),
	('HS_BODY_583', 'Heinlein Support Spamschutz Body-583'),
	('HS_BODY_584', 'Heinlein Support Spamschutz Body-584'),
	('HS_BODY_585', 'Heinlein Support Spamschutz Body-585'),
	('HS_BODY_586', 'Heinlein Support Spamschutz Body-586'),
	('HS_BODY_587', 'Heinlein Support Spamschutz Body-587'),
	('HS_BODY_588', 'Heinlein Support Spamschutz Body-588'),
	('HS_BODY_589', 'Heinlein Support Spamschutz Body-589'),
	('HS_BODY_590', 'Heinlein Support Spamschutz Body-590'),
	('HS_BODY_591', 'Heinlein Support Spamschutz Body-591'),
	('HS_BODY_592', 'Heinlein Support Spamschutz Body-592'),
	('HS_BODY_593', 'Heinlein Support Spamschutz Body-593'),
	('HS_BODY_594', 'Heinlein Support Spamschutz Body-594'),
	('HS_BODY_596', 'Heinlein Support Spamschutz Body-596'),
	('HS_BODY_597', 'Heinlein Support Spamschutz Body-597'),
	('HS_BODY_598', 'Heinlein Support Spamschutz Body-598'),
	('HS_BODY_599', 'Heinlein Support Spamschutz Body-599'),
	('HS_BODY_59', 'Heinlein Support Spamschutz Body-59'),
	('HS_BODY_5', 'Heinlein Support Spamschutz Body-5'),
	('HS_BODY_607', 'Heinlein Support Spamschutz Body-607'),
	('HS_BODY_608', 'Heinlein Support Spamschutz Body-608'),
	('HS_BODY_609', 'Heinlein Support Spamschutz Body-609'),
	('HS_BODY_60', 'Heinlein Support Spamschutz Body-60'),
	('HS_BODY_611', 'Heinlein Support Spamschutz Body-611'),
	('HS_BODY_612', 'Heinlein Support Spamschutz Body-612'),
	('HS_BODY_613', 'Heinlein Support Spamschutz Body-613'),
	('HS_BODY_614', 'Heinlein Support Spamschutz Body-614'),
	('HS_BODY_615', 'Heinlein Support Spamschutz Body-615'),
	('HS_BODY_616', 'Heinlein Support Spamschutz Body-616'),
	('HS_BODY_617', 'Heinlein Support Spamschutz Body-617'),
	('HS_BODY_618', 'Heinlein Support Spamschutz Body-618'),
	('HS_BODY_619', 'Heinlein Support Spamschutz Body-619'),
	('HS_BODY_61', 'Heinlein Support Spamschutz Body-61'),
	('HS_BODY_625', 'Heinlein Support Spamschutz Body-625'),
	('HS_BODY_626', 'Heinlein Support Spamschutz Body-626 Phishing'),
	('HS_BODY_627', 'Heinlein Support Spamschutz Body-627'),
	('HS_BODY_628', 'Heinlein Support Spamschutz Body-628'),
	('HS_BODY_629', 'Heinlein Support Spamschutz Body-629'),
	('HS_BODY_62', 'Heinlein Support Spamschutz Body-62'),
	('HS_BODY_630', 'Heinlein Support Spamschutz Body-630'),
	('HS_BODY_631', 'Heinlein Support Spamschutz Body-631'),
	('HS_BODY_632', 'Heinlein Support Spamschutz Body-632'),
	('HS_BODY_633', 'Heinlein Support Spamschutz Body-633'),
	('HS_BODY_634', 'Heinlein Support Spamschutz Body-634'),
	('HS_BODY_635', 'Heinlein Support Spamschutz Body-635 Phishing'),
	('HS_BODY_636', 'Heinlein Support Spamschutz Body-636 Phishing'),
	('HS_BODY_637', 'Heinlein Support Spamschutz Body-637 phishing'),
	('HS_BODY_638', 'Heinlein Support Spamschutz Body-638 phishing'),
	('HS_BODY_639', 'Heinlein Support Spamschutz Body-639 phishing'),
	('HS_BODY_63', 'Heinlein Support Spamschutz Body-63'),
	('HS_BODY_640', 'Heinlein Support Spamschutz Body-640 phishing'),
	('HS_BODY_641', 'Heinlein Support Spamschutz Body-641 phishing'),
	('HS_BODY_642', 'Heinlein Support Spamschutz Body-642 phishing'),
	('HS_BODY_643', 'Heinlein Support Spamschutz Body-643'),
	('HS_BODY_644', 'Heinlein Support Spamschutz Body-644 SPAM'),
	('HS_BODY_645', 'Heinlein Support Spamschutz Body-645'),
	('HS_BODY_646', 'Heinlein Support Spamschutz Body-646 Phishing'),
	('HS_BODY_647', 'Heinlein Support Spamschutz Body-647 Phishing'),
	('HS_BODY_648', 'Heinlein Support Spamschutz Body-648'),
	('HS_BODY_649', 'Heinlein Support Spamschutz Body-649 MALWARE'),
	('HS_BODY_64', 'Heinlein Support Spamschutz Body-64'),
	('HS_BODY_650', 'Heinlein Support Spamschutz Body-650 Phishing'),
	('HS_BODY_651', 'Heinlein Support Spamschutz Body-651 Phishing'),
	('HS_BODY_652', 'Heinlein Support Spamschutz Body-652'),
	('HS_BODY_653', 'Heinlein Support Spamschutz Body-653'),
	('HS_BODY_654', 'Heinlein Support Spamschutz Body-654'),
	('HS_BODY_655', 'Heinlein Support Spamschutz Body-655'),
	('HS_BODY_658', 'Heinlein Support Spamschutz Body-658'),
	('HS_BODY_65', 'Heinlein Support Spamschutz Body-65'),
	('HS_BODY_660', 'Heinlein Support Spamschutz Body-660'),
	('HS_BODY_661', 'Heinlein Support Spamschutz Body-661'),
	('HS_BODY_662', 'Heinlein Support Spamschutz Body-662'),
	('HS_BODY_663', 'Heinlein Support Spamschutz Body-663'),
	('HS_BODY_664', 'Heinlein Support Spamschutz Body-664'),
	('HS_BODY_665', 'Heinlein Support Spamschutz Body-665'),
	('HS_BODY_666', 'Heinlein Support Spamschutz Body-666 MALWARE'),
	('HS_BODY_667', 'Heinlein Support Spamschutz Body-667 Malware'),
	('HS_BODY_668', 'Heinlein Support Spamschutz Body-668'),
	('HS_BODY_669', 'Heinlein Support Spamschutz Body-669'),
	('HS_BODY_66', 'Heinlein Support Spamschutz Body-66'),
	('HS_BODY_670', 'Heinlein Support Spamschutz Body-670'),
	('HS_BODY_671', 'Heinlein Support Spamschutz Body-671'),
	('HS_BODY_672', 'Heinlein Support Spamschutz Body-672'),
	('HS_BODY_673', 'Heinlein Support Spamschutz Body-673'),
	('HS_BODY_674', 'Heinlein Support Spamschutz Body-674'),
	('HS_BODY_675', 'Heinlein Support Spamschutz Body-675'),
	('HS_BODY_676', 'Heinlein Support Spamschutz Body-676'),
	('HS_BODY_677', 'Heinlein Support Spamschutz Body-677'),
	('HS_BODY_678', 'Heinlein Support Spamschutz Body-678 PHISHING'),
	('HS_BODY_679', 'Heinlein Support Spamschutz Body-679 PHISHING'),
	('HS_BODY_67', 'Heinlein Support Spamschutz Body-67'),
	('HS_BODY_680', 'Heinlein Support Spamschutz Body-680 PHISHING'),
	('HS_BODY_681', 'Heinlein Support Spamschutz Body-681 Spam'),
	('HS_BODY_682', 'Heinlein Support Spamschutz Body-682 Spam'),
	('HS_BODY_683', 'Heinlein Support Spamschutz Body-683 Spam'),
	('HS_BODY_684', 'Heinlein Support Spamschutz Body-684 Spam'),
	('HS_BODY_685', 'Heinlein Support Spamschutz Body-685 Spam'),
	('HS_BODY_686', 'Heinlein Support Spamschutz Body-686 Phishing'),
	('HS_BODY_687', 'Heinlein Support Spamschutz Body-687 Phishing'),
	('HS_BODY_688', 'Heinlein Support Spamschutz Body-688 Phishing'),
	('HS_BODY_689', 'Heinlein Support Spamschutz Body-689 Phishing'),
	('HS_BODY_68', 'Heinlein Support Spamschutz Body-68'),
	('HS_BODY_690', 'Heinlein Support Spamschutz Body-690 Phishing'),
	('HS_BODY_691', 'Heinlein Support Spamschutz Body-691'),
	('HS_BODY_692', 'Heinlein Support Spamschutz Body-692'),
	('HS_BODY_693', 'Heinlein Support Spamschutz Body-693 SPAM'),
	('HS_BODY_694', 'Heinlein Support Spamschutz Body-694 phishing'),
	('HS_BODY_695', 'Heinlein Support Spamschutz Body-695 Phishing'),
	('HS_BODY_696', 'Heinlein Support Spamschutz Body-696 Malware'),
	('HS_BODY_697', 'Heinlein Support Spamschutz Body-697 Malware'),
	('HS_BODY_698', 'Heinlein Support Spamschutz Body-698 Malware'),
	('HS_BODY_699', 'Heinlein Support Spamschutz Body-699 SCAM'),
	('HS_BODY_69', 'Heinlein Support Spamschutz Body-69'),
	('HS_BODY_6', 'Heinlein Support Spamschutz Body-6'),
	('HS_BODY_702', 'Heinlein Support Spamschutz Body-702 PHISHING'),
	('HS_BODY_703', 'Heinlein Support Spamschutz Body-703 PHISHING'),
	('HS_BODY_704', 'Heinlein Support Spamschutz Body-704 Phishing'),
	('HS_BODY_705', 'Heinlein Support Spamschutz Body-705 Phisihing'),
	('HS_BODY_706', 'Heinlein Support Spamschutz Body-706 Spam'),
	('HS_BODY_707', 'Heinlein Support Spamschutz Body-707 Spam'),
	('HS_BODY_708', 'Heinlein Support Spamschutz Body-708 Spam'),
	('HS_BODY_709', 'Heinlein Support Spamschutz Body-709 Spam'),
	('HS_BODY_70', 'Heinlein Support Spamschutz Body-70'),
	('HS_BODY_710', 'Heinlein Support Spamschutz Body-710 Spam'),
	('HS_BODY_711', 'Heinlein Support Spamschutz Body-711 Spam'),
	('HS_BODY_712', 'Heinlein Support Spamschutz Body-712 Spam'),
	('HS_BODY_713', 'Heinlein Support Spamschutz Body-713 Spam'),
	('HS_BODY_715', 'Heinlein Support Spamschutz Body-715 SPAM'),
	('HS_BODY_716', 'Heinlein Support Spamschutz Body-716 Spam'),
	('HS_BODY_717', 'Heinlein Support Spamschutz Body-717 Spam'),
	('HS_BODY_718', 'Heinlein Support Spamschutz Body-718 SEO-SPAM'),
	('HS_BODY_719', 'Heinlein Support Spamschutz Body-719 SEO-SPAM'),
	('HS_BODY_71', 'Heinlein Support Spamschutz Body-71'),
	('HS_BODY_720', 'Heinlein Support Spamschutz Body-720 SEO-SPAM'),
	('HS_BODY_721', 'Heinlein Support Spamschutz Body-721 Phishing'),
	('HS_BODY_722', 'Heinlein Support Spamschutz Body-722 Phishing'),
	('HS_BODY_723', 'Heinlein Support Spamschutz Body-723 Phishing'),
	('HS_BODY_724', 'Heinlein Support Spamschutz Body-724 SEO-SPAM'),
	('HS_BODY_725', 'Heinlein Support Spamschutz Body-725 SEO-SPAM'),
	('HS_BODY_726', 'Heinlein Support Spamschutz Body-726 SEO-SPAM'),
	('HS_BODY_727', 'Heinlein Support Spamschutz Body-727 Spam'),
	('HS_BODY_728', 'Heinlein Support Spamschutz Body-728 Spam'),
	('HS_BODY_729', 'Heinlein Support Spamschutz Body-729 Spam'),
	('HS_BODY_72', 'Heinlein Support Spamschutz Body-72'),
	('HS_BODY_730', 'Heinlein Support Spamschutz Body-730 SPAM'),
	('HS_BODY_731', 'Heinlein Support Spamschutz Body-731 SPAM'),
	('HS_BODY_735', 'Heinlein Support Spamschutz Body-735 SPAM'),
	('HS_BODY_736', 'Heinlein Support Spamschutz Body-736 Phishing'),
	('HS_BODY_737', 'Heinlein Support Spamschutz Body-737 Phishing'),
	('HS_BODY_738', 'Heinlein Support Spamschutz Body-738 SPAM'),
	('HS_BODY_739', 'Heinlein Support Spamschutz Body-739 Spam'),
	('HS_BODY_73', 'Heinlein Support Spamschutz Body-73'),
	('HS_BODY_740', 'Heinlein Support Spamschutz Body-740 SPAM'),
	('HS_BODY_741', 'Heinlein Support Spamschutz Body-741 Spam'),
	('HS_BODY_743', 'Heinlein Support Spamschutz Body-743 Spam'),
	('HS_BODY_744', 'Heinlein Support Spamschutz Body-744 Spam'),
	('HS_BODY_745', 'Heinlein Support Spamschutz Body-745'),
	('HS_BODY_746', 'Heinlein Support Spamschutz Body-746'),
	('HS_BODY_747', 'Heinlein Support Spamschutz Body-747'),
	('HS_BODY_748', 'Heinlein Support Spamschutz Body-748'),
	('HS_BODY_749', 'Heinlein Support Spamschutz Body-749'),
	('HS_BODY_74', 'Heinlein Support Spamschutz Body-74'),
	('HS_BODY_750', 'Heinlein Support Spamschutz Body-750'),
	('HS_BODY_751', 'Heinlein Support Spamschutz Body-751 Phishing'),
	('HS_BODY_752', 'Heinlein Support Spamschutz Body-752 Phishing'),
	('HS_BODY_753', 'Heinlein Support Spamschutz Body-753 Phishing'),
	('HS_BODY_754', 'Heinlein Support Spamschutz Body-754'),
	('HS_BODY_755', 'Heinlein Support Spamschutz Body-755'),
	('HS_BODY_756', 'Heinlein Support Spamschutz Body-756'),
	('HS_BODY_759', 'Heinlein Support Spamschutz Body-759'),
	('HS_BODY_75', 'Heinlein Support Spamschutz Body-75'),
	('HS_BODY_760', 'Heinlein Support Spamschutz Body-760'),
	('HS_BODY_761', 'Heinlein Support Spamschutz Body-761'),
	('HS_BODY_762', 'Heinlein Support Spamschutz Body-762 PHISHING'),
	('HS_BODY_763', 'Heinlein Support Spamschutz Body-763 Phishing'),
	('HS_BODY_766', 'Heinlein Support Spamschutz Body-766 Phishing'),
	('HS_BODY_767', 'Heinlein Support Spamschutz Body-767 Phishing'),
	('HS_BODY_768', 'Heinlein Support Spamschutz Body-768'),
	('HS_BODY_769', 'Heinlein Support Spamschutz Body-769 Phishing'),
	('HS_BODY_76', 'Heinlein Support Spamschutz Body-76'),
	('HS_BODY_770', 'Heinlein Support Spamschutz Body-770 Phishing'),
	('HS_BODY_771', 'Heinlein Support Spamschutz Body-771 Phishing'),
	('HS_BODY_772', 'Heinlein Support Spamschutz Body-772 Phishing'),
	('HS_BODY_773', 'Heinlein Support Spamschutz Body-773 Phishing'),
	('HS_BODY_774', 'Heinlein Support Spamschutz Body-774'),
	('HS_BODY_775', 'Heinlein Support Spamschutz Body-775'),
	('HS_BODY_776', 'Heinlein Support Spamschutz Body-776'),
	('HS_BODY_777', 'Heinlein Support Spamschutz Body-777'),
	('HS_BODY_778', 'Heinlein Support Spamschutz Body-778'),
	('HS_BODY_779', 'Heinlein Support Spamschutz Body-779'),
	('HS_BODY_77', 'Heinlein Support Spamschutz Body-77'),
	('HS_BODY_780', 'Heinlein Support Spamschutz Body-780'),
	('HS_BODY_781', 'Heinlein Support Spamschutz Body-781'),
	('HS_BODY_782', 'Heinlein Support Spamschutz Body-782'),
	('HS_BODY_783', 'Heinlein Support Spamschutz Body-783'),
	('HS_BODY_784', 'Heinlein Support Spamschutz Body-784'),
	('HS_BODY_785', 'Heinlein Support Spamschutz Body-785'),
	('HS_BODY_786', 'Heinlein Support Spamschutz Body-786'),
	('HS_BODY_787', 'Heinlein Support Spamschutz Body-787'),
	('HS_BODY_788', 'Heinlein Support Spamschutz Body-788'),
	('HS_BODY_789', 'Heinlein Support Spamschutz Body-789'),
	('HS_BODY_78', 'Heinlein Support Spamschutz Body-78'),
	('HS_BODY_790', 'Heinlein Support Spamschutz Body-790'),
	('HS_BODY_791', 'Heinlein Support Spamschutz Body-791'),
	('HS_BODY_792', 'Heinlein Support Spamschutz Body-792'),
	('HS_BODY_793', 'Heinlein Support Spamschutz Body-793 SCAM'),
	('HS_BODY_794', 'Heinlein Support Spamschutz Body-794 SCAM'),
	('HS_BODY_795', 'Heinlein Support Spamschutz Body-795'),
	('HS_BODY_796', 'Heinlein Support Spamschutz Body-796'),
	('HS_BODY_798', 'Heinlein Support Spamschutz Body-798'),
	('HS_BODY_799', 'Heinlein Support Spamschutz Body-799'),
	('HS_BODY_79', 'Heinlein Support Spamschutz Body-79'),
	('HS_BODY_7', 'Heinlein Support Spamschutz Body-7'),
	('HS_BODY_800', 'Heinlein Support Spamschutz Body-800 SCAM'),
	('HS_BODY_801', 'Heinlein Support Spamschutz Body-801 SCAM'),
	('HS_BODY_802', 'Heinlein Support Spamschutz Body-802 SCAM'),
	('HS_BODY_803', 'Heinlein Support Spamschutz Body-803 SCAM'),
	('HS_BODY_804', 'Heinlein Support Spamschutz Body-804'),
	('HS_BODY_805', 'Heinlein Support Spamschutz Body-805'),
	('HS_BODY_806', 'Heinlein Support Spamschutz Body-806'),
	('HS_BODY_807', 'Heinlein Support Spamschutz Body-807'),
	('HS_BODY_808', 'Heinlein Support Spamschutz Body-808'),
	('HS_BODY_809', 'Heinlein Support Spamschutz Body-809'),
	('HS_BODY_80', 'Heinlein Support Spamschutz Body-80'),
	('HS_BODY_813', 'Heinlein Support Spamschutz Body-813'),
	('HS_BODY_814', 'Heinlein Support Spamschutz Body-814'),
	('HS_BODY_815', 'Heinlein Support Spamschutz Body-815'),
	('HS_BODY_816', 'Heinlein Support Spamschutz Body-816'),
	('HS_BODY_817', 'Heinlein Support Spamschutz Body-817'),
	('HS_BODY_818', 'Heinlein Support Spamschutz Body-818 PHISHING'),
	('HS_BODY_819', 'Heinlein Support Spamschutz Body-819 PHISHING'),
	('HS_BODY_81', 'Heinlein Support Spamschutz Body-81'),
	('HS_BODY_820', 'Heinlein Support Spamschutz Body-820 PHISHING'),
	('HS_BODY_821', 'Heinlein Support Spamschutz Body-821'),
	('HS_BODY_822', 'Heinlein Support Spamschutz Body-822'),
	('HS_BODY_823', 'Heinlein Support Spamschutz Body-823'),
	('HS_BODY_824', 'Heinlein Support Spamschutz Body-824'),
	('HS_BODY_82', 'Heinlein Support Spamschutz Body-82'),
	('HS_BODY_834', 'Heinlein Support Spamschutz Body-834'),
	('HS_BODY_835', 'Heinlein Support Spamschutz Body-835 Downloadlinks zu WORM.WIN32.CRIDEX.POV'),
	('HS_BODY_836', 'Heinlein Support Spamschutz Body-836'),
	('HS_BODY_837', 'Heinlein Support Spamschutz Body-837'),
	('HS_BODY_838', 'Heinlein Support Spamschutz Body-838'),
	('HS_BODY_839', 'Heinlein Support Spamschutz Body-839 Downloadlinks zu WORM.WIN32.CRIDEX.POV'),
	('HS_BODY_83', 'Heinlein Support Spamschutz Body-83'),
	('HS_BODY_840', 'Heinlein Support Spamschutz Body-840'),
	('HS_BODY_841', 'Heinlein Support Spamschutz Body-841'),
	('HS_BODY_842', 'Heinlein Support Spamschutz Body-842'),
	('HS_BODY_843', 'Heinlein Support Spamschutz Body-843'),
	('HS_BODY_844', 'Heinlein Support Spamschutz Body-844'),
	('HS_BODY_846', 'Heinlein Support Spamschutz Body-846'),
	('HS_BODY_847', 'Heinlein Support Spamschutz Body-847'),
	('HS_BODY_848', 'Heinlein Support Spamschutz Body-848'),
	('HS_BODY_849', 'Heinlein Support Spamschutz Body-849'),
	('HS_BODY_84', 'Heinlein Support Spamschutz Body-84'),
	('HS_BODY_850', 'Heinlein Support Spamschutz Body-850'),
	('HS_BODY_851', 'Heinlein Support Spamschutz Body-851'),
	('HS_BODY_852', 'Heinlein Support Spamschutz Body-852'),
	('HS_BODY_853', 'Heinlein Support Spamschutz Body-853'),
	('HS_BODY_854', 'Heinlein Support Spamschutz Body-854'),
	('HS_BODY_856', 'Heinlein Support Spamschutz Body-856'),
	('HS_BODY_858', 'Heinlein Support Spamschutz Body-858'),
	('HS_BODY_859', 'Heinlein Support Spamschutz Body-859'),
	('HS_BODY_85', 'Heinlein Support Spamschutz Body-85'),
	('HS_BODY_860', 'Heinlein Support Spamschutz Body-860 Account-Phishing'),
	('HS_BODY_861', 'Heinlein Support Spamschutz Body-861'),
	('HS_BODY_862', 'Heinlein Support Spamschutz Body-862'),
	('HS_BODY_863', 'Heinlein Support Spamschutz Body-863'),
	('HS_BODY_864', 'Heinlein Support Spamschutz Body-864'),
	('HS_BODY_865', 'Heinlein Support Spamschutz Body-865'),
	('HS_BODY_867', 'Heinlein Support Spamschutz Body-867'),
	('HS_BODY_868', 'Heinlein Support Spamschutz Body-868'),
	('HS_BODY_869', 'Heinlein Support Spamschutz Body-869 Phishing'),
	('HS_BODY_86', 'Heinlein Support Spamschutz Body-86'),
	('HS_BODY_870', 'Heinlein Support Spamschutz Body-870 Phishing'),
	('HS_BODY_871', 'Heinlein Support Spamschutz Body-871'),
	('HS_BODY_872', 'Heinlein Support Spamschutz Body-872'),
	('HS_BODY_873', 'Heinlein Support Spamschutz Body-873'),
	('HS_BODY_874', 'Heinlein Support Spamschutz Body-874'),
	('HS_BODY_875', 'Heinlein Support Spamschutz Body-875'),
	('HS_BODY_876', 'Heinlein Support Spamschutz Body-876'),
	('HS_BODY_877', 'Heinlein Support Spamschutz Body-877'),
	('HS_BODY_878', 'Heinlein Support Spamschutz Body-878'),
	('HS_BODY_879', 'Heinlein Support Spamschutz Body-879'),
	('HS_BODY_87', 'Heinlein Support Spamschutz Body-87'),
	('HS_BODY_880', 'Heinlein Support Spamschutz Body-880'),
	('HS_BODY_881', 'Heinlein Support Spamschutz Body-881 Phishing'),
	('HS_BODY_882', 'Heinlein Support Spamschutz Body-882 Phishing'),
	('HS_BODY_883', 'Heinlein Support Spamschutz Body-883'),
	('HS_BODY_884', 'Heinlein Support Spamschutz Body-884'),
	('HS_BODY_885', 'Heinlein Support Spamschutz Body-885'),
	('HS_BODY_886', 'Heinlein Support Spamschutz Body-886'),
	('HS_BODY_887', 'Heinlein Support Spamschutz Body-887'),
	('HS_BODY_888', 'Heinlein Support Spamschutz Body-888'),
	('HS_BODY_889', 'Heinlein Support Spamschutz Body-889 Phishing'),
	('HS_BODY_88', 'Heinlein Support Spamschutz Body-88'),
	('HS_BODY_890', 'Heinlein Support Spamschutz Body-890'),
	('HS_BODY_891', 'Heinlein Support Spamschutz Body-891'),
	('HS_BODY_892', 'Heinlein Support Spamschutz Body-892 Phishing'),
	('HS_BODY_893', 'Heinlein Support Spamschutz Body-893 Phishing'),
	('HS_BODY_894', 'Heinlein Support Spamschutz Body-894 Phishing'),
	('HS_BODY_895', 'Heinlein Support Spamschutz Body-895 Phishing'),
	('HS_BODY_896', 'Heinlein Support Spamschutz Body-896 Phishing'),
	('HS_BODY_897', 'Heinlein Support Spamschutz Body-897 Phishing'),
	('HS_BODY_898', 'Heinlein Support Spamschutz Body-898'),
	('HS_BODY_899', 'Heinlein Support Spamschutz Body-899 Phishing'),
	('HS_BODY_89', 'Heinlein Support Spamschutz Body-89'),
	('HS_BODY_8', 'Heinlein Support Spamschutz Body-8'),
	('HS_BODY_900', 'Heinlein Support Spamschutz Body-900'),
	('HS_BODY_901', 'Heinlein Support Spamschutz Body-901'),
	('HS_BODY_902', 'Heinlein Support Spamschutz Body-902 Phishing'),
	('HS_BODY_903', 'Heinlein Support Spamschutz Body-903 Phishing'),
	('HS_BODY_904', 'Heinlein Support Spamschutz Body-904 Phishing'),
	('HS_BODY_905', 'Heinlein Support Spamschutz Body-905 Phishing'),
	('HS_BODY_907', 'Heinlein Support Spamschutz Body-907 Phishing'),
	('HS_BODY_908', 'Heinlein Support Spamschutz Body-908 Phishing'),
	('HS_BODY_909', 'Heinlein Support Spamschutz Body-909 Phishing'),
	('HS_BODY_90', 'Heinlein Support Spamschutz Body-90'),
	('HS_BODY_910', 'Heinlein Support Spamschutz Body-910 Spam'),
	('HS_BODY_911', 'Heinlein Support Spamschutz Body-911 Spqm'),
	('HS_BODY_912', 'Heinlein Support Spamschutz Body-912 Spam'),
	('HS_BODY_913', 'Heinlein Support Spamschutz Body-913 Phishing'),
	('HS_BODY_914', 'Heinlein Support Spamschutz Body-914 Phishing'),
	('HS_BODY_915', 'Heinlein Support Spamschutz Body-915 Phishing'),
	('HS_BODY_916', 'Heinlein Support Spamschutz Body-916 Phishing'),
	('HS_BODY_917', 'Heinlein Support Spamschutz Body-917 Phishing'),
	('HS_BODY_918', 'Heinlein Support Spamschutz Body-918 Phishing'),
	('HS_BODY_919', 'Heinlein Support Spamschutz Body-919 Phishing'),
	('HS_BODY_91', 'Heinlein Support Spamschutz Body-91'),
	('HS_BODY_920', 'Heinlein Support Spamschutz Body-920 Phishing'),
	('HS_BODY_921', 'Heinlein Support Spamschutz Body-921'),
	('HS_BODY_922', 'Heinlein Support Spamschutz Body-922 Phishing'),
	('HS_BODY_923', 'Heinlein Support Spamschutz Body-923'),
	('HS_BODY_924', 'Heinlein Support Spamschutz Body-924'),
	('HS_BODY_925', 'Heinlein Support Spamschutz Body-925 Spam'),
	('HS_BODY_926', 'Heinlein Support Spamschutz Body-926'),
	('HS_BODY_927', 'Heinlein Support Spamschutz Body-927 Spam'),
	('HS_BODY_928', 'Heinlein Support Spamschutz Body-928 Spam'),
	('HS_BODY_929', 'Heinlein Support Spamschutz Body-929 Spam'),
	('HS_BODY_92', 'Heinlein Support Spamschutz Body-92'),
	('HS_BODY_930', 'Heinlein Support Spamschutz Body-930 Spam'),
	('HS_BODY_931', 'Heinlein Support Spamschutz Body-931 Spam'),
	('HS_BODY_932', 'Heinlein Support Spamschutz Body-932 Spam'),
	('HS_BODY_933', 'Heinlein Support Spamschutz Body-933 Spam'),
	('HS_BODY_934', 'Heinlein Support Spamschutz Body-934'),
	('HS_BODY_935', 'Heinlein Support Spamschutz Body-935'),
	('HS_BODY_936', 'Heinlein Support Spamschutz Body-936'),
	('HS_BODY_939', 'Heinlein Support Spamschutz Body-939 Pishing'),
	('HS_BODY_93', 'Heinlein Support Spamschutz Body-93'),
	('HS_BODY_941', 'Heinlein Support Spamschutz Body-941'),
	('HS_BODY_942', 'Heinlein Support Spamschutz Body-942 Phishing'),
	('HS_BODY_944', 'Heinlein Support Spamschutz Body-944'),
	('HS_BODY_945', 'Heinlein Support Spamschutz Body-945'),
	('HS_BODY_946', 'Heinlein Support Spamschutz Body-946 Phishing'),
	('HS_BODY_947', 'Heinlein Support Spamschutz Body-947 Phishing'),
	('HS_BODY_948', 'Heinlein Support Spamschutz Body-948 Phishing'),
	('HS_BODY_949', 'Heinlein Support Spamschutz Body-949 Phishing'),
	('HS_BODY_94', 'Heinlein Support Spamschutz Body-94'),
	('HS_BODY_950', 'Heinlein Support Spamschutz Body-950'),
	('HS_BODY_951', 'Heinlein Support Spamschutz Body-951'),
	('HS_BODY_952', 'Heinlein Support Spamschutz Body-952'),
	('HS_BODY_953', 'Heinlein Support Spamschutz Body-953'),
	('HS_BODY_954', 'Heinlein Support Spamschutz Body-954'),
	('HS_BODY_955', 'Heinlein Support Spamschutz Body-955'),
	('HS_BODY_956', 'Heinlein Support Spamschutz Body-956'),
	('HS_BODY_957', 'Heinlein Support Spamschutz Body-957'),
	('HS_BODY_958', 'Heinlein Support Spamschutz Body-958'),
	('HS_BODY_959', 'Heinlein Support Spamschutz Body-959 Spam'),
	('HS_BODY_95', 'Heinlein Support Spamschutz Body-95'),
	('HS_BODY_960', 'Heinlein Support Spamschutz Body-960'),
	('HS_BODY_961', 'Heinlein Support Spamschutz Body-961 Spam'),
	('HS_BODY_962', 'Heinlein Support Spamschutz Body-962 Spam'),
	('HS_BODY_963', 'Heinlein Support Spamschutz Body-963'),
	('HS_BODY_964', 'Heinlein Support Spamschutz Body-964 Spam'),
	('HS_BODY_966', 'Heinlein Support Spamschutz Body-966'),
	('HS_BODY_96', 'Heinlein Support Spamschutz Body-96'),
	('HS_BODY_970', 'Heinlein Support Spamschutz Body-970'),
	('HS_BODY_972', 'Heinlein Support Spamschutz Body-972 Spam'),
	('HS_BODY_973', 'Heinlein Support Spamschutz Body-973'),
	('HS_BODY_974', 'Heinlein Support Spamschutz Body-974'),
	('HS_BODY_975', 'Heinlein Support Spamschutz Body-975 Spam'),
	('HS_BODY_976', 'Heinlein Support Spamschutz Body-976'),
	('HS_BODY_977', 'Heinlein Support Spamschutz Body-977'),
	('HS_BODY_978', 'Heinlein Support Spamschutz Body-978'),
	('HS_BODY_979', 'Heinlein Support Spamschutz Body-979'),
	('HS_BODY_97', 'Heinlein Support Spamschutz Body-97'),
	('HS_BODY_980', 'Heinlein Support Spamschutz Body-980 Spam'),
	('HS_BODY_981', 'Heinlein Support Spamschutz Body-981'),
	('HS_BODY_982', 'Heinlein Support Spamschutz Body-982'),
	('HS_BODY_983', 'Heinlein Support Spamschutz Body-983'),
	('HS_BODY_984', 'Heinlein Support Spamschutz Body-984'),
	('HS_BODY_985', 'Heinlein Support Spamschutz Body-985'),
	('HS_BODY_986', 'Heinlein Support Spamschutz Body-986 SPAM'),
	('HS_BODY_987', 'Heinlein Support Spamschutz Body-987 Spam'),
	('HS_BODY_988', 'Heinlein Support Spamschutz Body-988 Phishing'),
	('HS_BODY_989', 'Heinlein Support Spamschutz Body-989 Spam'),
	('HS_BODY_98', 'Heinlein Support Spamschutz Body-98'),
	('HS_BODY_990', 'Heinlein Support Spamschutz Body-990'),
	('HS_BODY_991', 'Heinlein Support Spamschutz Body-991'),
	('HS_BODY_992', 'Heinlein Support Spamschutz Body-992'),
	('HS_BODY_993', 'Heinlein Support Spamschutz Body-993 Spam'),
	('HS_BODY_994', 'Heinlein Support Spamschutz Body-994 Spam'),
	('HS_BODY_995', 'Heinlein Support Spamschutz Body-995 Phishing'),
	('HS_BODY_996', 'Heinlein Support Spamschutz Body-996'),
	('HS_BODY_997', 'Heinlein Support Spamschutz Body-997'),
	('HS_BODY_998', 'Heinlein Support Spamschutz Body-998'),
	('HS_BODY_999', 'Heinlein Support Spamschutz Body-999 Phishing'),
	('HS_BODY_99', 'Heinlein Support Spamschutz Body-99'),
	('HS_BODY_9', 'Heinlein Support Spamschutz Body-9'),
	('HS_BODY_UPLOADED_SOFTWARE', 'Somebody has uploaded some new software for you'),
	('HS_DRUG_DOLLAR_1', 'Contains a drug and price-like pattern.'),
	('HS_DRUG_DOLLAR_2', 'Contains a drug and price-like pattern.'),
	('HS_DRUG_DOLLAR_3', 'Contains a drug and price-like pattern.'),
	('HS_DRUG_DOLLAR_MANY', 'Contains several drug and dollar-like patterns.'),
	('HS_FORGED_OE_FW', 'Outlook does not prefix forwards with "FW:"'),
	('HS_GETMEOFF', 'Links to common unsubscribe script: \'getmeoff.php\''),
	('HS_HEADER_0', 'Heinlein Support Spamschutz Header-0 Header-Spamschutzregel 1031'),
	('HS_HEADER_100', 'Heinlein Support Spamschutz Header-100  JPBERLIN-22'),
	('HS_HEADER_101', 'Heinlein Support Spamschutz Header-101  JPBERLIN-23'),
	('HS_HEADER_102', 'Heinlein Support Spamschutz Header-102  JPBERLIN-24'),
	('HS_HEADER_103', 'Heinlein Support Spamschutz Header-103  JPBERLIN-24'),
	('HS_HEADER_104', 'Heinlein Support Spamschutz Header-104  JPBERLIN-25'),
	('HS_HEADER_105', 'Heinlein Support Spamschutz Header-105  JPBERLIN-26'),
	('HS_HEADER_106', 'Heinlein Support Spamschutz Header-106  JPBERLIN-27'),
	('HS_HEADER_107', 'Heinlein Support Spamschutz Header-107  JPBERLIN-28'),
	('HS_HEADER_108', 'Heinlein Support Spamschutz Header-108  JPBERLIN-29'),
	('HS_HEADER_109', 'Heinlein Support Spamschutz Header-109  JPBERLIN-30'),
	('HS_HEADER_10', 'Heinlein Support Spamschutz Header-10 Header-Spamschutzregel 1169'),
	('HS_HEADER_110', 'Heinlein Support Spamschutz Header-110  JPBERLIN-31'),
	('HS_HEADER_111', 'Heinlein Support Spamschutz Header-111  JPBERLIN-32'),
	('HS_HEADER_112', 'Heinlein Support Spamschutz Header-112  JPBERLIN-33'),
	('HS_HEADER_113', 'Heinlein Support Spamschutz Header-113  JPBERLIN-34'),
	('HS_HEADER_114', 'Heinlein Support Spamschutz Header-114  JPBERLIN-35'),
	('HS_HEADER_115', 'Heinlein Support Spamschutz Header-115  JPBERLIN-36'),
	('HS_HEADER_116', 'Heinlein Support Spamschutz Header-116  JPBERLIN-37'),
	('HS_HEADER_117', 'Heinlein Support Spamschutz Header-117  JPBERLIN-38'),
	('HS_HEADER_118', 'Heinlein Support Spamschutz Header-118  JPBERLIN-39'),
	('HS_HEADER_119', 'Heinlein Support Spamschutz Header-119  JPBERLIN-40'),
	('HS_HEADER_11', 'Heinlein Support Spamschutz Header-11 Header-Spamschutzregel 1168'),
	('HS_HEADER_120', 'Heinlein Support Spamschutz Header-120  JPBERLIN-41'),
	('HS_HEADER_121', 'Heinlein Support Spamschutz Header-121  JPBERLIN-50'),
	('HS_HEADER_122', 'Heinlein Support Spamschutz Header-122  JPBERLIN-52'),
	('HS_HEADER_123', 'Heinlein Support Spamschutz Header-123  JPBERLIN-53'),
	('HS_HEADER_124', 'Heinlein Support Spamschutz Header-124  JPBERLIN-54'),
	('HS_HEADER_125', 'Heinlein Support Spamschutz Header-125  JPBERLIN-55'),
	('HS_HEADER_126', 'Heinlein Support Spamschutz Header-126  JPBERLIN-56'),
	('HS_HEADER_127', 'Heinlein Support Spamschutz Header-127  JPBERLIN-57'),
	('HS_HEADER_128', 'Heinlein Support Spamschutz Header-128  JPBERLIN-60'),
	('HS_HEADER_12', 'Heinlein Support Spamschutz Header-12 Header-Spamschutzregel 1167'),
	('HS_HEADER_130', 'Heinlein Support Spamschutz Header-130  JPBERLIN-61'),
	('HS_HEADER_132', 'Heinlein Support Spamschutz Header-132  JPBERLIN-64'),
	('HS_HEADER_133', 'Heinlein Support Spamschutz Header-133  JPBERLIN-65'),
	('HS_HEADER_134', 'Heinlein Support Spamschutz Header-134  JPBERLIN-66'),
	('HS_HEADER_135', 'Heinlein Support Spamschutz Header-135  JPBERLIN-12'),
	('HS_HEADER_136', 'Heinlein Support Spamschutz Header-136  JPBERLIN-13'),
	('HS_HEADER_137', 'Heinlein Support Spamschutz Header-137 Header-Spamschutzregel 22'),
	('HS_HEADER_138', 'Heinlein Support Spamschutz Header-138 Header-Spamschutzregel 27'),
	('HS_HEADER_139', 'Heinlein Support Spamschutz Header-139 Header-Spamschutzregel 29'),
	('HS_HEADER_13', 'Heinlein Support Spamschutz Header-13 Header-Spamschutzregel 1166'),
	('HS_HEADER_140', 'Heinlein Support Spamschutz Header-140 Header-Spamschutzregel 31'),
	('HS_HEADER_141', 'Heinlein Support Spamschutz Header-141 Header-Spamschutzregel 32'),
	('HS_HEADER_142', 'Heinlein Support Spamschutz Header-142 Header-Spamschutzregel 33'),
	('HS_HEADER_143', 'Heinlein Support Spamschutz Header-143 Header-Spamschutzregel 34'),
	('HS_HEADER_144', 'Heinlein Support Spamschutz Header-144 Header-Spamschutzregel 35'),
	('HS_HEADER_145', 'Heinlein Support Spamschutz Header-145 Header-Spamschutzregel 36'),
	('HS_HEADER_146', 'Heinlein Support Spamschutz Header-146 Header-Spamschutzregel 37'),
	('HS_HEADER_147', 'Heinlein Support Spamschutz Header-147 Header-Spamschutzregel 38'),
	('HS_HEADER_148', 'Heinlein Support Spamschutz Header-148 Header-Spamschutzregel 28'),
	('HS_HEADER_149', 'Heinlein Support Spamschutz Header-149 Header-Spamschutzregel 39'),
	('HS_HEADER_14', 'Heinlein Support Spamschutz Header-14 Header-Spamschutzregel 1165'),
	('HS_HEADER_150', 'Heinlein Support Spamschutz Header-150 Header-Spamschutzregel 40'),
	('HS_HEADER_151', 'Heinlein Support Spamschutz Header-151 Header-Spamschutzregel 41'),
	('HS_HEADER_152', 'Heinlein Support Spamschutz Header-152 Header-Spamschutzregel 42'),
	('HS_HEADER_154', 'Heinlein Support Spamschutz Header-154 Header-Spamschutzregel 51'),
	('HS_HEADER_155', 'Heinlein Support Spamschutz Header-155 Header-Spamschutzregel 58'),
	('HS_HEADER_156', 'Heinlein Support Spamschutz Header-156 Header-Spamschutzregel 60'),
	('HS_HEADER_157', 'Heinlein Support Spamschutz Header-157 Header-Spamschutzregel 62'),
	('HS_HEADER_158', 'Heinlein Support Spamschutz Header-158 Header-Spamschutzregel 63'),
	('HS_HEADER_159', 'Heinlein Support Spamschutz Header-159 Header-Spamschutzregel 64'),
	('HS_HEADER_15', 'Heinlein Support Spamschutz Header-15 Header-Spamschutzregel 1164'),
	('HS_HEADER_160', 'Heinlein Support Spamschutz Header-160 Header-Spamschutzregel 65'),
	('HS_HEADER_161', 'Heinlein Support Spamschutz Header-161 Header-Spamschutzregel 66'),
	('HS_HEADER_162', 'Heinlein Support Spamschutz Header-162 Header-Spamschutzregel 67'),
	('HS_HEADER_163', 'Heinlein Support Spamschutz Header-163 Header-Spamschutzregel 68'),
	('HS_HEADER_164', 'Heinlein Support Spamschutz Header-164 Header-Spamschutzregel 69'),
	('HS_HEADER_165', 'Heinlein Support Spamschutz Header-165 Header-Spamschutzregel 70'),
	('HS_HEADER_166', 'Heinlein Support Spamschutz Header-166 Header-Spamschutzregel 71'),
	('HS_HEADER_167', 'Heinlein Support Spamschutz Header-167 Header-Spamschutzregel 72'),
	('HS_HEADER_168', 'Heinlein Support Spamschutz Header-168 Header-Spamschutzregel 73'),
	('HS_HEADER_169', 'Heinlein Support Spamschutz Header-169 Header-Spamschutzregel 74'),
	('HS_HEADER_16', 'Heinlein Support Spamschutz Header-16 Header-Spamschutzregel 1163'),
	('HS_HEADER_170', 'Heinlein Support Spamschutz Header-170 Header-Spamschutzregel 75'),
	('HS_HEADER_171', 'Heinlein Support Spamschutz Header-171 Header-Spamschutzregel 76'),
	('HS_HEADER_172', 'Heinlein Support Spamschutz Header-172 Header-Spamschutzregel 77'),
	('HS_HEADER_173', 'Heinlein Support Spamschutz Header-173 Header-Spamschutzregel 78'),
	('HS_HEADER_174', 'Heinlein Support Spamschutz Header-174 Header-Spamschutzregel 79'),
	('HS_HEADER_175', 'Heinlein Support Spamschutz Header-175 Header-Spamschutzregel 80'),
	('HS_HEADER_176', 'Heinlein Support Spamschutz Header-176 Header-Spamschutzregel 81'),
	('HS_HEADER_177', 'Heinlein Support Spamschutz Header-177 Header-Spamschutzregel 82'),
	('HS_HEADER_178', 'Heinlein Support Spamschutz Header-178 Header-Spamschutzregel 83'),
	('HS_HEADER_179', 'Heinlein Support Spamschutz Header-179 Header-Spamschutzregel 84'),
	('HS_HEADER_17', 'Heinlein Support Spamschutz Header-17 Header-Spamschutzregel 1162'),
	('HS_HEADER_180', 'Heinlein Support Spamschutz Header-180 Header-Spamschutzregel 85'),
	('HS_HEADER_181', 'Heinlein Support Spamschutz Header-181 Header-Spamschutzregel 87'),
	('HS_HEADER_182', 'Heinlein Support Spamschutz Header-182 Header-Spamschutzregel 88'),
	('HS_HEADER_183', 'Heinlein Support Spamschutz Header-183 Header-Spamschutzregel 89'),
	('HS_HEADER_184', 'Heinlein Support Spamschutz Header-184 Header-Spamschutzregel 90'),
	('HS_HEADER_185', 'Heinlein Support Spamschutz Header-185 Header-Spamschutzregel 92'),
	('HS_HEADER_186', 'Heinlein Support Spamschutz Header-186 Header-Spamschutzregel 93'),
	('HS_HEADER_187', 'Heinlein Support Spamschutz Header-187 Header-Spamschutzregel 94'),
	('HS_HEADER_188', 'Heinlein Support Spamschutz Header-188 Header-Spamschutzregel 95'),
	('HS_HEADER_189', 'Heinlein Support Spamschutz Header-189 Header-Spamschutzregel 96'),
	('HS_HEADER_18', 'Heinlein Support Spamschutz Header-18 Header-Spamschutzregel 1161'),
	('HS_HEADER_190', 'Heinlein Support Spamschutz Header-190 Header-Spamschutzregel 97'),
	('HS_HEADER_191', 'Heinlein Support Spamschutz Header-191 Header-Spamschutzregel 98'),
	('HS_HEADER_192', 'Heinlein Support Spamschutz Header-192 Header-Spamschutzregel 99'),
	('HS_HEADER_193', 'Heinlein Support Spamschutz Header-193 Header-Spamschutzregel 100'),
	('HS_HEADER_194', 'Heinlein Support Spamschutz Header-194 Header-Spamschutzregel 101'),
	('HS_HEADER_195', 'Heinlein Support Spamschutz Header-195 Header-Spamschutzregel 102'),
	('HS_HEADER_196', 'Heinlein Support Spamschutz Header-196 Header-Spamschutzregel 103'),
	('HS_HEADER_197', 'Heinlein Support Spamschutz Header-197 Header-Spamschutzregel 104'),
	('HS_HEADER_198', 'Heinlein Support Spamschutz Header-198 Header-Spamschutzregel 105'),
	('HS_HEADER_199', 'Heinlein Support Spamschutz Header-199 Header-Spamschutzregel 106'),
	('HS_HEADER_19', 'Heinlein Support Spamschutz Header-19 Header-Spamschutzregel 1160'),
	('HS_HEADER_1', 'Heinlein Support Spamschutz Header-1'),
	('HS_HEADER_200', 'Heinlein Support Spamschutz Header-200 Header-Spamschutzregel 107'),
	('HS_HEADER_201', 'Heinlein Support Spamschutz Header-201 Header-Spamschutzregel 108'),
	('HS_HEADER_202', 'Heinlein Support Spamschutz Header-202 Header-Spamschutzregel 109'),
	('HS_HEADER_203', 'Heinlein Support Spamschutz Header-203 Header-Spamschutzregel 112'),
	('HS_HEADER_204', 'Heinlein Support Spamschutz Header-204 Header-Spamschutzregel 113'),
	('HS_HEADER_205', 'Heinlein Support Spamschutz Header-205 Header-Spamschutzregel 114'),
	('HS_HEADER_206', 'Heinlein Support Spamschutz Header-206 Header-Spamschutzregel 115'),
	('HS_HEADER_207', 'Heinlein Support Spamschutz Header-207 Header-Spamschutzregel 116'),
	('HS_HEADER_208', 'Heinlein Support Spamschutz Header-208 Header-Spamschutzregel 117'),
	('HS_HEADER_209', 'Heinlein Support Spamschutz Header-209 Header-Spamschutzregel 119'),
	('HS_HEADER_20', 'Heinlein Support Spamschutz Header-20 Header-Spamschutzregel 1159'),
	('HS_HEADER_210', 'Heinlein Support Spamschutz Header-210 Header-Spamschutzregel 120'),
	('HS_HEADER_211', 'Heinlein Support Spamschutz Header-211 Header-Spamschutzregel 122'),
	('HS_HEADER_212', 'Heinlein Support Spamschutz Header-212 Header-Spamschutzregel 124'),
	('HS_HEADER_213', 'Heinlein Support Spamschutz Header-213 Header-Spamschutzregel 126'),
	('HS_HEADER_214', 'Heinlein Support Spamschutz Header-214 Header-Spamschutzregel 127'),
	('HS_HEADER_215', 'Heinlein Support Spamschutz Header-215 Header-Spamschutzregel 131'),
	('HS_HEADER_216', 'Heinlein Support Spamschutz Header-216 Header-Spamschutzregel 153'),
	('HS_HEADER_217', 'Heinlein Support Spamschutz Header-217 Header-Spamschutzregel 155'),
	('HS_HEADER_218', 'Heinlein Support Spamschutz Header-218 Header-Spamschutzregel 156'),
	('HS_HEADER_219', 'Heinlein Support Spamschutz Header-219 Header-Spamschutzregel 162'),
	('HS_HEADER_21', 'Heinlein Support Spamschutz Header-21 Header-Spamschutzregel 1158'),
	('HS_HEADER_221', 'Heinlein Support Spamschutz Header-221 Header-Spamschutzregel 180'),
	('HS_HEADER_222', 'Heinlein Support Spamschutz Header-222 Header-Spamschutzregel 201'),
	('HS_HEADER_223', 'Heinlein Support Spamschutz Header-223 Header-Spamschutzregel 202'),
	('HS_HEADER_224', 'Heinlein Support Spamschutz Header-224 Header-Spamschutzregel 203'),
	('HS_HEADER_225', 'Heinlein Support Spamschutz Header-225 Header-Spamschutzregel 204'),
	('HS_HEADER_226', 'Heinlein Support Spamschutz Header-226 Header-Spamschutzregel 205'),
	('HS_HEADER_227', 'Heinlein Support Spamschutz Header-227 Header-Spamschutzregel 206'),
	('HS_HEADER_228', 'Heinlein Support Spamschutz Header-228 Header-Spamschutzregel 207'),
	('HS_HEADER_229', 'Heinlein Support Spamschutz Header-229 Header-Spamschutzregel 208'),
	('HS_HEADER_22', 'Heinlein Support Spamschutz Header-22 Header-Spamschutzregel 1157'),
	('HS_HEADER_230', 'Heinlein Support Spamschutz Header-230 Header-Spamschutzregel 210'),
	('HS_HEADER_231', 'Heinlein Support Spamschutz Header-231 Header-Spamschutzregel 211'),
	('HS_HEADER_232', 'Heinlein Support Spamschutz Header-232 Header-Spamschutzregel 53'),
	('HS_HEADER_233', 'Heinlein Support Spamschutz Header-233 Header-Spamschutzregel 54'),
	('HS_HEADER_234', 'Heinlein Support Spamschutz Header-234 Header-Spamschutzregel 57'),
	('HS_HEADER_236', 'Heinlein Support Spamschutz Header-236 Header-Spamschutzregel 110'),
	('HS_HEADER_237', 'Heinlein Support Spamschutz Header-237 Header-Spamschutzregel 111'),
	('HS_HEADER_238', 'Heinlein Support Spamschutz Header-238 Header-Spamschutzregel 118'),
	('HS_HEADER_23', 'Heinlein Support Spamschutz Header-23 Header-Spamschutzregel 1156'),
	('HS_HEADER_248', 'Heinlein Support Spamschutz Header-248 Header-Spamschutzregel 142'),
	('HS_HEADER_249', 'Heinlein Support Spamschutz Header-249'),
	('HS_HEADER_24', 'Heinlein Support Spamschutz Header-24 Header-Spamschutzregel 1155'),
	('HS_HEADER_250', 'Heinlein Support Spamschutz Header-250'),
	('HS_HEADER_251', 'Heinlein Support Spamschutz Header-251'),
	('HS_HEADER_252', 'Heinlein Support Spamschutz Header-252'),
	('HS_HEADER_253', 'Heinlein Support Spamschutz Header-253'),
	('HS_HEADER_254', 'Heinlein Support Spamschutz Header-254'),
	('HS_HEADER_255', 'Heinlein Support Spamschutz Header-255'),
	('HS_HEADER_257', 'Heinlein Support Spamschutz Header-257'),
	('HS_HEADER_259', 'Heinlein Support Spamschutz Header-259'),
	('HS_HEADER_25', 'Heinlein Support Spamschutz Header-25 Header-Spamschutzregel 1154'),
	('HS_HEADER_260', 'Heinlein Support Spamschutz Header-260'),
	('HS_HEADER_261', 'Heinlein Support Spamschutz Header-261'),
	('HS_HEADER_262', 'Heinlein Support Spamschutz Header-262'),
	('HS_HEADER_264', 'Heinlein Support Spamschutz Header-264'),
	('HS_HEADER_265', 'Heinlein Support Spamschutz Header-265'),
	('HS_HEADER_266', 'Heinlein Support Spamschutz Header-266'),
	('HS_HEADER_26', 'Heinlein Support Spamschutz Header-26 Header-Spamschutzregel 1153'),
	('HS_HEADER_270', 'Heinlein Support Spamschutz Header-270'),
	('HS_HEADER_274', 'Heinlein Support Spamschutz Header-274'),
	('HS_HEADER_275', 'Heinlein Support Spamschutz Header-275'),
	('HS_HEADER_279', 'Heinlein Support Spamschutz Header-279'),
	('HS_HEADER_27', 'Heinlein Support Spamschutz Header-27 Header-Spamschutzregel 1052'),
	('HS_HEADER_280', 'Heinlein Support Spamschutz Header-280'),
	('HS_HEADER_282', 'Heinlein Support Spamschutz Header-282'),
	('HS_HEADER_283', 'Heinlein Support Spamschutz Header-283'),
	('HS_HEADER_284', 'Heinlein Support Spamschutz Header-284'),
	('HS_HEADER_285', 'Heinlein Support Spamschutz Header-285'),
	('HS_HEADER_286', 'Heinlein Support Spamschutz Header-286'),
	('HS_HEADER_287', 'Heinlein Support Spamschutz Header-287'),
	('HS_HEADER_289', 'Heinlein Support Spamschutz Header-289 SPAM'),
	('HS_HEADER_28', 'Heinlein Support Spamschutz Header-28 Header-Spamschutzregel 1051'),
	('HS_HEADER_290', 'Heinlein Support Spamschutz Header-290 phishing'),
	('HS_HEADER_293', 'Heinlein Support Spamschutz Header-293 Phishing'),
	('HS_HEADER_295', 'Heinlein Support Spamschutz Header-295 SPAM'),
	('HS_HEADER_296', 'Heinlein Support Spamschutz Header-296'),
	('HS_HEADER_298', 'Heinlein Support Spamschutz Header-298'),
	('HS_HEADER_299', 'Heinlein Support Spamschutz Header-299 Spam'),
	('HS_HEADER_29', 'Heinlein Support Spamschutz Header-29 Header-Spamschutzregel 1050'),
	('HS_HEADER_2', 'Heinlein Support Spamschutz Header-2 Header-Spamschutzregel 1177'),
	('HS_HEADER_300', 'Heinlein Support Spamschutz Header-300 Spam'),
	('HS_HEADER_301', 'Heinlein Support Spamschutz Header-301'),
	('HS_HEADER_304', 'Heinlein Support Spamschutz Header-304'),
	('HS_HEADER_305', 'Heinlein Support Spamschutz Header-305 SCAM'),
	('HS_HEADER_307', 'Heinlein Support Spamschutz Header-307 Spam'),
	('HS_HEADER_308', 'Heinlein Support Spamschutz Header-308 Phishing'),
	('HS_HEADER_309', 'Heinlein Support Spamschutz Header-309 Phishing'),
	('HS_HEADER_30', 'Heinlein Support Spamschutz Header-30 Header-Spamschutzregel 1049'),
	('HS_HEADER_310', 'Heinlein Support Spamschutz Header-310 SEO Spam'),
	('HS_HEADER_311', 'Heinlein Support Spamschutz Header-311 SPAM'),
	('HS_HEADER_312', 'Heinlein Support Spamschutz Header-312 SPAM'),
	('HS_HEADER_314', 'Heinlein Support Spamschutz Header-314 Spam'),
	('HS_HEADER_315', 'Heinlein Support Spamschutz Header-315 SPAM'),
	('HS_HEADER_319', 'Heinlein Support Spamschutz Header-319 Phishing'),
	('HS_HEADER_31', 'Heinlein Support Spamschutz Header-31 Header-Spamschutzregel 1048'),
	('HS_HEADER_322', 'Heinlein Support Spamschutz Header-322 Phishing'),
	('HS_HEADER_323', 'Heinlein Support Spamschutz Header-323'),
	('HS_HEADER_324', 'Heinlein Support Spamschutz Header-324'),
	('HS_HEADER_325', 'Heinlein Support Spamschutz Header-325'),
	('HS_HEADER_326', 'Heinlein Support Spamschutz Header-326'),
	('HS_HEADER_327', 'Heinlein Support Spamschutz Header-327'),
	('HS_HEADER_328', 'Heinlein Support Spamschutz Header-328'),
	('HS_HEADER_32', 'Heinlein Support Spamschutz Header-32'),
	('HS_HEADER_330', 'Heinlein Support Spamschutz Header-330'),
	('HS_HEADER_331', 'Heinlein Support Spamschutz Header-331'),
	('HS_HEADER_332', 'Heinlein Support Spamschutz Header-332 Bulk-Mailer'),
	('HS_HEADER_333', 'Heinlein Support Spamschutz Header-333'),
	('HS_HEADER_334', 'Heinlein Support Spamschutz Header-334'),
	('HS_HEADER_335', 'Heinlein Support Spamschutz Header-335'),
	('HS_HEADER_337', 'Heinlein Support Spamschutz Header-337'),
	('HS_HEADER_339', 'Heinlein Support Spamschutz Header-339'),
	('HS_HEADER_33', 'Heinlein Support Spamschutz Header-33'),
	('HS_HEADER_340', 'Heinlein Support Spamschutz Header-340'),
	('HS_HEADER_341', 'Heinlein Support Spamschutz Header-341'),
	('HS_HEADER_343', 'Heinlein Support Spamschutz Header-343'),
	('HS_HEADER_344', 'Heinlein Support Spamschutz Header-344'),
	('HS_HEADER_345', 'Heinlein Support Spamschutz Header-345'),
	('HS_HEADER_346', 'Heinlein Support Spamschutz Header-346'),
	('HS_HEADER_347', 'Heinlein Support Spamschutz Header-347'),
	('HS_HEADER_348', 'Heinlein Support Spamschutz Header-348'),
	('HS_HEADER_349', 'Heinlein Support Spamschutz Header-349'),
	('HS_HEADER_34', 'Heinlein Support Spamschutz Header-34'),
	('HS_HEADER_350', 'Heinlein Support Spamschutz Header-350'),
	('HS_HEADER_351', 'Heinlein Support Spamschutz Header-351 SPAM'),
	('HS_HEADER_352', 'Heinlein Support Spamschutz Header-352 SPAM'),
	('HS_HEADER_353', 'Heinlein Support Spamschutz Header-353'),
	('HS_HEADER_354', 'Heinlein Support Spamschutz Header-354 Phishing'),
	('HS_HEADER_355', 'Heinlein Support Spamschutz Header-355 Phishing'),
	('HS_HEADER_356', 'Heinlein Support Spamschutz Header-356'),
	('HS_HEADER_357', 'Heinlein Support Spamschutz Header-357 Phishing'),
	('HS_HEADER_358', 'Heinlein Support Spamschutz Header-358 Phishing'),
	('HS_HEADER_359', 'Heinlein Support Spamschutz Header-359 Spam'),
	('HS_HEADER_35', 'Heinlein Support Spamschutz Header-35'),
	('HS_HEADER_360', 'Heinlein Support Spamschutz Header-360'),
	('HS_HEADER_361', 'Heinlein Support Spamschutz Header-361'),
	('HS_HEADER_362', 'Heinlein Support Spamschutz Header-362'),
	('HS_HEADER_363', 'Heinlein Support Spamschutz Header-363 Phishing'),
	('HS_HEADER_364', 'Heinlein Support Spamschutz Header-364'),
	('HS_HEADER_365', 'Heinlein Support Spamschutz Header-365 Spam'),
	('HS_HEADER_366', 'Heinlein Support Spamschutz Header-366'),
	('HS_HEADER_367', 'Heinlein Support Spamschutz Header-367'),
	('HS_HEADER_368', 'Heinlein Support Spamschutz Header-368'),
	('HS_HEADER_36', 'Heinlein Support Spamschutz Header-36'),
	('HS_HEADER_372', 'Heinlein Support Spamschutz Header-372'),
	('HS_HEADER_373', 'Heinlein Support Spamschutz Header-373'),
	('HS_HEADER_374', 'Heinlein Support Spamschutz Header-374 Phishing'),
	('HS_HEADER_377', 'Heinlein Support Spamschutz Header-377'),
	('HS_HEADER_378', 'Heinlein Support Spamschutz Header-378'),
	('HS_HEADER_379', 'Heinlein Support Spamschutz Header-379'),
	('HS_HEADER_37', 'Heinlein Support Spamschutz Header-37'),
	('HS_HEADER_381', 'Heinlein Support Spamschutz Header-381'),
	('HS_HEADER_383', 'Heinlein Support Spamschutz Header-383 Spam'),
	('HS_HEADER_384', 'Heinlein Support Spamschutz Header-384'),
	('HS_HEADER_38', 'Heinlein Support Spamschutz Header-38 Header-Spamschutzregel 1141'),
	('HS_HEADER_390', 'Heinlein Support Spamschutz Header-390'),
	('HS_HEADER_391', 'Heinlein Support Spamschutz Header-391'),
	('HS_HEADER_393', 'Heinlein Support Spamschutz Header-393'),
	('HS_HEADER_394', 'Heinlein Support Spamschutz Header-394'),
	('HS_HEADER_398', 'Heinlein Support Spamschutz Header-398'),
	('HS_HEADER_399', 'Heinlein Support Spamschutz Header-399 Ebay-Phishing'),
	('HS_HEADER_39', 'Heinlein Support Spamschutz Header-39 Header-Spamschutzregel 1140'),
	('HS_HEADER_3', 'Heinlein Support Spamschutz Header-3 Header-Spamschutzregel 1176'),
	('HS_HEADER_400', 'Heinlein Support Spamschutz Header-400'),
	('HS_HEADER_401', 'Heinlein Support Spamschutz Header-401'),
	('HS_HEADER_402', 'Heinlein Support Spamschutz Header-402 Spam'),
	('HS_HEADER_403', 'Heinlein Support Spamschutz Header-403 Spam'),
	('HS_HEADER_404', 'Heinlein Support Spamschutz Header-404 Spam'),
	('HS_HEADER_405', 'Heinlein Support Spamschutz Header-405'),
	('HS_HEADER_406', 'Heinlein Support Spamschutz Header-406'),
	('HS_HEADER_407', 'Heinlein Support Spamschutz Header-407'),
	('HS_HEADER_408', 'Heinlein Support Spamschutz Header-408 Spam'),
	('HS_HEADER_409', 'Heinlein Support Spamschutz Header-409 Spam'),
	('HS_HEADER_40', 'Heinlein Support Spamschutz Header-40 Header-Spamschutzregel 1139'),
	('HS_HEADER_410', 'Heinlein Support Spamschutz Header-410'),
	('HS_HEADER_411', 'Heinlein Support Spamschutz Header-411'),
	('HS_HEADER_412', 'Heinlein Support Spamschutz Header-412'),
	('HS_HEADER_413', 'Heinlein Support Spamschutz Header-413'),
	('HS_HEADER_414', 'Heinlein Support Spamschutz Header-414'),
	('HS_HEADER_415', 'Heinlein Support Spamschutz Header-415'),
	('HS_HEADER_416', 'Heinlein Support Spamschutz Header-416'),
	('HS_HEADER_417', 'Heinlein Support Spamschutz Header-417 Phishing'),
	('HS_HEADER_418', 'Heinlein Support Spamschutz Header-418'),
	('HS_HEADER_41', 'Heinlein Support Spamschutz Header-41 Header-Spamschutzregel 1038'),
	('HS_HEADER_420', 'Heinlein Support Spamschutz Header-420'),
	('HS_HEADER_421', 'Heinlein Support Spamschutz Header-421'),
	('HS_HEADER_422', 'Heinlein Support Spamschutz Header-422 Spam'),
	('HS_HEADER_423', 'Heinlein Support Spamschutz Header-423 Spam'),
	('HS_HEADER_424', 'Heinlein Support Spamschutz Header-424 Spam'),
	('HS_HEADER_425', 'Heinlein Support Spamschutz Header-425 Phishing'),
	('HS_HEADER_426', 'Heinlein Support Spamschutz Header-426'),
	('HS_HEADER_427', 'Heinlein Support Spamschutz Header-427 Spam'),
	('HS_HEADER_428', 'Heinlein Support Spamschutz Header-428'),
	('HS_HEADER_429', 'Heinlein Support Spamschutz Header-429 Spam'),
	('HS_HEADER_42', 'Heinlein Support Spamschutz Header-42 Header-Spamschutzregel 1037'),
	('HS_HEADER_430', 'Heinlein Support Spamschutz Header-430 Spam'),
	('HS_HEADER_431', 'Heinlein Support Spamschutz Header-431'),
	('HS_HEADER_432', 'Heinlein Support Spamschutz Header-432'),
	('HS_HEADER_433', 'Heinlein Support Spamschutz Header-433'),
	('HS_HEADER_434', 'Heinlein Support Spamschutz Header-434 Spam'),
	('HS_HEADER_435', 'Heinlein Support Spamschutz Header-435 Spam'),
	('HS_HEADER_436', 'Heinlein Support Spamschutz Header-436'),
	('HS_HEADER_437', 'Heinlein Support Spamschutz Header-437'),
	('HS_HEADER_438', 'Heinlein Support Spamschutz Header-438 Spam'),
	('HS_HEADER_439', 'Heinlein Support Spamschutz Header-439 Spam'),
	('HS_HEADER_43', 'Heinlein Support Spamschutz Header-43 Header-Spamschutzregel 1037'),
	('HS_HEADER_440', 'Heinlein Support Spamschutz Header-440 Spam'),
	('HS_HEADER_441', 'Heinlein Support Spamschutz Header-441'),
	('HS_HEADER_442', 'Heinlein Support Spamschutz Header-442 Spam'),
	('HS_HEADER_443', 'Heinlein Support Spamschutz Header-443'),
	('HS_HEADER_444', 'Heinlein Support Spamschutz Header-444'),
	('HS_HEADER_447', 'Heinlein Support Spamschutz Header-447 Spam'),
	('HS_HEADER_448', 'Heinlein Support Spamschutz Header-448 Spam'),
	('HS_HEADER_449', 'Heinlein Support Spamschutz Header-449 Phishing'),
	('HS_HEADER_44', 'Heinlein Support Spamschutz Header-44 Header-Spamschutzregel 1036'),
	('HS_HEADER_450', 'Heinlein Support Spamschutz Header-450 Phishing'),
	('HS_HEADER_451', 'Heinlein Support Spamschutz Header-451 Spam'),
	('HS_HEADER_452', 'Heinlein Support Spamschutz Header-452 SPAM'),
	('HS_HEADER_453', 'Heinlein Support Spamschutz Header-453 Spam'),
	('HS_HEADER_454', 'Heinlein Support Spamschutz Header-454 SPAM'),
	('HS_HEADER_455', 'Heinlein Support Spamschutz Header-455 Phishing'),
	('HS_HEADER_456', 'Heinlein Support Spamschutz Header-456 Spam'),
	('HS_HEADER_457', 'Heinlein Support Spamschutz Header-457 Spam'),
	('HS_HEADER_458', 'Heinlein Support Spamschutz Header-458'),
	('HS_HEADER_45', 'Heinlein Support Spamschutz Header-45 Header-Spamschutzregel 1035'),
	('HS_HEADER_460', 'Heinlein Support Spamschutz Header-460'),
	('HS_HEADER_462', 'Heinlein Support Spamschutz Header-462'),
	('HS_HEADER_463', 'Heinlein Support Spamschutz Header-463 Spam'),
	('HS_HEADER_464', 'Heinlein Support Spamschutz Header-464 Spam'),
	('HS_HEADER_465', 'Heinlein Support Spamschutz Header-465 Spam'),
	('HS_HEADER_466', 'Heinlein Support Spamschutz Header-466 Spam'),
	('HS_HEADER_467', 'Heinlein Support Spamschutz Header-467 Phishing'),
	('HS_HEADER_468', 'Heinlein Support Spamschutz Header-468 Spam'),
	('HS_HEADER_469', 'Heinlein Support Spamschutz Header-469 Phishing'),
	('HS_HEADER_46', 'Heinlein Support Spamschutz Header-46 Header-Spamschutzregel 1034'),
	('HS_HEADER_470', 'Heinlein Support Spamschutz Header-470 Spam'),
	('HS_HEADER_471', 'Heinlein Support Spamschutz Header-471 Spam'),
	('HS_HEADER_472', 'Heinlein Support Spamschutz Header-472 Phishing'),
	('HS_HEADER_473', 'Heinlein Support Spamschutz Header-473 Spam'),
	('HS_HEADER_474', 'Heinlein Support Spamschutz Header-474 Phishing'),
	('HS_HEADER_475', 'Heinlein Support Spamschutz Header-475 Bankphishing'),
	('HS_HEADER_476', 'Heinlein Support Spamschutz Header-476 Bankphishing'),
	('HS_HEADER_477', 'Heinlein Support Spamschutz Header-477 Bankphishing'),
	('HS_HEADER_478', 'Heinlein Support Spamschutz Header-478 Malware'),
	('HS_HEADER_479', 'Heinlein Support Spamschutz Header-479 Spam'),
	('HS_HEADER_47', 'Heinlein Support Spamschutz Header-47 Header-Spamschutzregel 1033'),
	('HS_HEADER_481', 'Heinlein Support Spamschutz Header-481 Spam'),
	('HS_HEADER_482', 'Heinlein Support Spamschutz Header-482 Phishing'),
	('HS_HEADER_483', 'Heinlein Support Spamschutz Header-483 Spam'),
	('HS_HEADER_484', 'Heinlein Support Spamschutz Header-484 Spam'),
	('HS_HEADER_485', 'Heinlein Support Spamschutz Header-485 Spam'),
	('HS_HEADER_486', 'Heinlein Support Spamschutz Header-486 Spam'),
	('HS_HEADER_487', 'Heinlein Support Spamschutz Header-487 Spam'),
	('HS_HEADER_488', 'Heinlein Support Spamschutz Header-488 Spam'),
	('HS_HEADER_489', 'Heinlein Support Spamschutz Header-489 Spam'),
	('HS_HEADER_48', 'Heinlein Support Spamschutz Header-48 Header-Spamschutzregel 1032'),
	('HS_HEADER_491', 'Heinlein Support Spamschutz Header-491 Spam'),
	('HS_HEADER_492', 'Heinlein Support Spamschutz Header-492 Spam'),
	('HS_HEADER_494', 'Heinlein Support Spamschutz Header-494 Spam'),
	('HS_HEADER_495', 'Heinlein Support Spamschutz Header-495 Spam'),
	('HS_HEADER_496', 'Heinlein Support Spamschutz Header-496 Phishing'),
	('HS_HEADER_497', 'Heinlein Support Spamschutz Header-497 Spam'),
	('HS_HEADER_498', 'Heinlein Support Spamschutz Header-498 Phishing'),
	('HS_HEADER_499', 'Heinlein Support Spamschutz Header-499 Phishing'),
	('HS_HEADER_4', 'Heinlein Support Spamschutz Header-4 Header-Spamschutzregel 1175'),
	('HS_HEADER_500', 'Heinlein Support Spamschutz Header-500 Spam'),
	('HS_HEADER_501', 'Heinlein Support Spamschutz Header-501 Spam'),
	('HS_HEADER_502', 'Heinlein Support Spamschutz Header-502 Spam'),
	('HS_HEADER_503', 'Heinlein Support Spamschutz Header-503 Spam'),
	('HS_HEADER_504', 'Heinlein Support Spamschutz Header-504 Phishing'),
	('HS_HEADER_505', 'Heinlein Support Spamschutz Header-505 Spam'),
	('HS_HEADER_506', 'Heinlein Support Spamschutz Header-506 Spam'),
	('HS_HEADER_507', 'Heinlein Support Spamschutz Header-507 Spam'),
	('HS_HEADER_508', 'Heinlein Support Spamschutz Header-508 Spam'),
	('HS_HEADER_509', 'Heinlein Support Spamschutz Header-509 Spam'),
	('HS_HEADER_50', 'Heinlein Support Spamschutz Header-50 Header-Spamschutzregel 1030'),
	('HS_HEADER_51', 'Heinlein Support Spamschutz Header-51 Header-Spamschutzregel 1029'),
	('HS_HEADER_52', 'Heinlein Support Spamschutz Header-52 Header-Spamschutzregel 1028'),
	('HS_HEADER_53', 'Heinlein Support Spamschutz Header-53 Header-Spamschutzregel 1027'),
	('HS_HEADER_54', 'Heinlein Support Spamschutz Header-54 Header-Spamschutzregel 1026'),
	('HS_HEADER_55', 'Heinlein Support Spamschutz Header-55 Header-Spamschutzregel 1025'),
	('HS_HEADER_56', 'Heinlein Support Spamschutz Header-56 Header-Spamschutzregel 1024'),
	('HS_HEADER_57', 'Heinlein Support Spamschutz Header-57 Header-Spamschutzregel 1023'),
	('HS_HEADER_58', 'Heinlein Support Spamschutz Header-58 Header-Spamschutzregel 1022'),
	('HS_HEADER_59', 'Heinlein Support Spamschutz Header-59 Header-Spamschutzregel 1021'),
	('HS_HEADER_5', 'Heinlein Support Spamschutz Header-5 Header-Spamschutzregel 1174'),
	('HS_HEADER_60', 'Heinlein Support Spamschutz Header-60 Header-Spamschutzregel 1020'),
	('HS_HEADER_61', 'Heinlein Support Spamschutz Header-61 Header-Spamschutzregel 1019'),
	('HS_HEADER_62', 'Heinlein Support Spamschutz Header-62 Header-Spamschutzregel 1018'),
	('HS_HEADER_63', 'Heinlein Support Spamschutz Header-63 Header-Spamschutzregel 1017'),
	('HS_HEADER_64', 'Heinlein Support Spamschutz Header-64 Header-Spamschutzregel 1016'),
	('HS_HEADER_65', 'Heinlein Support Spamschutz Header-65 Header-Spamschutzregel 1015'),
	('HS_HEADER_66', 'Heinlein Support Spamschutz Header-66'),
	('HS_HEADER_67', 'Heinlein Support Spamschutz Header-67 Header-Spamschutzregel 1013'),
	('HS_HEADER_68', 'Heinlein Support Spamschutz Header-68 Header-Spamschutzregel 1012'),
	('HS_HEADER_69', 'Heinlein Support Spamschutz Header-69 Header-Spamschutzregel 1011'),
	('HS_HEADER_6', 'Heinlein Support Spamschutz Header-6 Header-Spamschutzregel 1173'),
	('HS_HEADER_70', 'Heinlein Support Spamschutz Header-70 Header-Spamschutzregel 1010'),
	('HS_HEADER_71', 'Heinlein Support Spamschutz Header-71 Header-Spamschutzregel 1009'),
	('HS_HEADER_72', 'Heinlein Support Spamschutz Header-72 Header-Spamschutzregel 1008'),
	('HS_HEADER_73', 'Heinlein Support Spamschutz Header-73 Header-Spamschutzregel 1007'),
	('HS_HEADER_74', 'Heinlein Support Spamschutz Header-74 Header-Spamschutzregel 1006'),
	('HS_HEADER_75', 'Heinlein Support Spamschutz Header-75 Header-Spamschutzregel 1005'),
	('HS_HEADER_76', 'Heinlein Support Spamschutz Header-76 Header-Spamschutzregel 1004'),
	('HS_HEADER_77', 'Heinlein Support Spamschutz Header-77 Header-Spamschutzregel 1003'),
	('HS_HEADER_78', 'Heinlein Support Spamschutz Header-78 Header-Spamschutzregel 1002'),
	('HS_HEADER_79', 'Heinlein Support Spamschutz Header-79 Header-Spamschutzregel 1001'),
	('HS_HEADER_7', 'Heinlein Support Spamschutz Header-7 Header-Spamschutzregel 1172'),
	('HS_HEADER_80', 'Heinlein Support Spamschutz Header-80 Header-Spamschutzregel 1000'),
	('HS_HEADER_81', 'Heinlein Support Spamschutz Header-81  Nazi Spam1'),
	('HS_HEADER_82', 'Heinlein Support Spamschutz Header-82  Nazi Spam2'),
	('HS_HEADER_83', 'Heinlein Support Spamschutz Header-83  Nazi Spam3'),
	('HS_HEADER_84', 'Heinlein Support Spamschutz Header-84  Nazi Spam4'),
	('HS_HEADER_85', 'Heinlein Support Spamschutz Header-85  Nazi Spam5'),
	('HS_HEADER_86', 'Heinlein Support Spamschutz Header-86  Nazi Spam6'),
	('HS_HEADER_87', 'Heinlein Support Spamschutz Header-87  Nazi Spam7'),
	('HS_HEADER_88', 'Heinlein Support Spamschutz Header-88  Nazi Spam8'),
	('HS_HEADER_89', 'Heinlein Support Spamschutz Header-89  Nazi Spam9'),
	('HS_HEADER_8', 'Heinlein Support Spamschutz Header-8 Header-Spamschutzregel 1171'),
	('HS_HEADER_90', 'Heinlein Support Spamschutz Header-90  Nazi Spam10'),
	('HS_HEADER_91', 'Heinlein Support Spamschutz Header-91  Nazi Spam11'),
	('HS_HEADER_92', 'Heinlein Support Spamschutz Header-92  JPBERLIN-14'),
	('HS_HEADER_93', 'Heinlein Support Spamschutz Header-93  JPBERLIN-15'),
	('HS_HEADER_94', 'Heinlein Support Spamschutz Header-94  JPBERLIN-16'),
	('HS_HEADER_95', 'Heinlein Support Spamschutz Header-95  JPBERLIN-17'),
	('HS_HEADER_96', 'Heinlein Support Spamschutz Header-96  JPBERLIN-18'),
	('HS_HEADER_97', 'Heinlein Support Spamschutz Header-97  JPBERLIN-19'),
	('HS_HEADER_98', 'Heinlein Support Spamschutz Header-98  JPBERLIN-20'),
	('HS_HEADER_99', 'Heinlein Support Spamschutz Header-99  JPBERLIN-21'),
	('HS_HEADER_9', 'Heinlein Support Spamschutz Header-9 Header-Spamschutzregel 1170'),
	('HS_INDEX_PARAM', 'Link contains a common tracker pattern.'),
	('HS_MEETUP_FOR_SEX', 'Talks about meeting up for sex.'),
	('HS_SUBJ_NEW_SOFTWARE', 'Subject starts with \'New software uploaded by\''),
	('HS_SUBJ_ONLINE_PHARMACEUTICAL', 'Subject contains the phrase \'Online pharmaceutical\''),
	('HS_VPXL', 'Contains VPXL, yet the recommended dose is only 2 tablets.'),
	('HTML_BADTAG_40_50', 'HTML message is 40% to 50% bad tags'),
	('HTML_BADTAG_50_60', 'HTML message is 50% to 60% bad tags'),
	('HTML_BADTAG_60_70', 'HTML message is 60% to 70% bad tags'),
	('HTML_BADTAG_90_100', 'HTML message is 90% to 100% bad tags'),
	('HTML_CHARSET_FARAWAY', 'A foreign language charset used in HTML markup'),
	('HTML_COMMENT_SAVED_URL', 'HTML message is a saved web page'),
	('HTML_COMMENT_SHORT', 'HTML comment is very short'),
	('HTML_EMBEDS', 'HTML with embedded plugin object'),
	('HTML_EXTRA_CLOSE', 'HTML contains far too many close tags'),
	('HTML_FONT_FACE_BAD', 'HTML font face is not a word'),
	('HTML_FONT_LOW_CONTRAST', 'HTML font color similar to background'),
	('HTML_FONT_SIZE_HUGE', 'HTML font size is huge'),
	('HTML_FONT_SIZE_LARGE', 'HTML font size is large'),
	('HTML_FORMACTION_MAILTO', 'HTML includes a form which sends mail'),
	('HTML_IFRAME_SRC', 'Message has HTML IFRAME tag with SRC URI'),
	('HTML_IMAGE_ONLY_04', 'HTML: images with 0-400 bytes of words'),
	('HTML_IMAGE_ONLY_08', 'HTML: images with 400-800 bytes of words'),
	('HTML_IMAGE_ONLY_12', 'HTML: images with 800-1200 bytes of words'),
	('HTML_IMAGE_ONLY_16', 'HTML: images with 1200-1600 bytes of words'),
	('HTML_IMAGE_ONLY_20', 'HTML: images with 1600-2000 bytes of words'),
	('HTML_IMAGE_ONLY_24', 'HTML: images with 2000-2400 bytes of words'),
	('HTML_IMAGE_ONLY_28', 'HTML: images with 2400-2800 bytes of words'),
	('HTML_IMAGE_ONLY_32', 'HTML: images with 2800-3200 bytes of words'),
	('HTML_IMAGE_RATIO_02', 'HTML has a low ratio of text to image area'),
	('HTML_IMAGE_RATIO_04', 'HTML has a low ratio of text to image area'),
	('HTML_IMAGE_RATIO_06', 'HTML has a low ratio of text to image area'),
	('HTML_IMAGE_RATIO_08', 'HTML has a low ratio of text to image area'),
	('HTML_MESSAGE', 'HTML included in message'),
	('HTML_MIME_NO_HTML_TAG', 'HTML-only message, but there is no HTML tag'),
	('HTML_MISSING_CTYPE', 'Message is HTML without HTML Content-Type'),
	('HTML_NONELEMENT_30_40', '30% to 40% of HTML elements are non-standard'),
	('HTML_NONELEMENT_40_50', '40% to 50% of HTML elements are non-standard'),
	('HTML_NONELEMENT_60_70', '60% to 70% of HTML elements are non-standard'),
	('HTML_NONELEMENT_80_90', '80% to 90% of HTML elements are non-standard'),
	('HTML_OBFUSCATE_05_10', 'Message is 5% to 10% HTML obfuscation'),
	('HTML_OBFUSCATE_10_20', 'Message is 10% to 20% HTML obfuscation'),
	('HTML_OBFUSCATE_20_30', 'Message is 20% to 30% HTML obfuscation'),
	('HTML_OBFUSCATE_30_40', 'Message is 30% to 40% HTML obfuscation'),
	('HTML_OBFUSCATE_50_60', 'Message is 50% to 60% HTML obfuscation'),
	('HTML_OBFUSCATE_70_80', 'Message is 70% to 80% HTML obfuscation'),
	('HTML_OBFUSCATE_90_100', 'Message is 90% to 100% HTML obfuscation'),
	('HTML_OFF_PAGE', 'HTML element rendered well off the displayed page'),
	('HTML_SHORT_CENTER', 'HTML is very short with CENTER tag'),
	('HTML_SHORT_LINK_IMG_1', 'HTML is very short with a linked image'),
	('HTML_SHORT_LINK_IMG_2', 'HTML is very short with a linked image'),
	('HTML_SHORT_LINK_IMG_3', 'HTML is very short with a linked image'),
	('HTML_TAG_BALANCE_BODY', 'HTML has unbalanced "body" tags'),
	('HTML_TAG_BALANCE_HEAD', 'HTML has unbalanced "head" tags'),
	('HTML_TAG_EXIST_BGSOUND', 'HTML has "bgsound" tag'),
	('HTTP_77', 'Contains an URL-encoded hostname (HTTP77)'),
	('HTTP_ESCAPED_HOST', 'Uses %-escapes inside a URL\'s hostname'),
	('HTTP_EXCESSIVE_ESCAPES', 'Completely unnecessary %-escapes inside a URL'),
	('HTTPS_IP_MISMATCH', 'IP to HTTPS link found in HTML'),
	('IMPOTENCE', 'Impotence cure'),
	('INVALID_DATE', 'Invalid Date: header (not RFC 2822)'),
	('INVALID_DATE_TZ_ABSURD', 'Invalid Date: header (timezone does not exist)'),
	('INVALID_MSGID', 'Message-Id is not valid, according to RFC 2822'),
	('INVALID_TZ_CST', 'Invalid date in header (wrong CST timezone)'),
	('INVALID_TZ_EST', 'Invalid date in header (wrong EST timezone)'),
	('INVESTMENT_ADVICE', 'Message mentions investment advice'),
	('IP_LINK_PLUS', 'Dotted-decimal IP address followed by CGI'),
	('JAPANESE_UCE_BODY', 'Body contains Japanese UCE tag'),
	('JAPANESE_UCE_SUBJECT', 'Subject contains a Japanese UCE tag'),
	('JM_SOUGHT_1', 'Body contains frequently-spammed text patterns'),
	('JM_SOUGHT_FRAUD_1', 'Body contains frequently-spammed text patterns'),
	('JM_SOUGHT_FRAUD_2', 'Body contains frequently-spammed text patterns'),
	('JM_SOUGHT_FRAUD_3', 'Body contains frequently-spammed text patterns'),
	('JOIN_MILLIONS', 'Join Millions of Americans'),
	('JS_FROMCHARCODE', 'Document is built from a Javascript charcode array'),
	('KAM_1LINE', 'One liner SPAMs'),
	('KAM_6C822ECF', '$6c822ecf@ VERY prevalent message-ID header in SPAMs'),
	('KAM_ABOUT', 'Email Scam Hawking Anti-Spyware'),
	('KAM_ACNE', 'Spammers hawking Acne products'),
	('KAM_ADDRESS', 'Addresses and Companies prevalent in spams'),
	('KAM_ADVANCE', 'Advance Spams'),
	('KAM_ADV_EMAIL', 'Marks adv@<domain.com> Addresses as likely SPAM'),
	('KAM_ADVERT2', 'This is probably an unwanted commercial email...'),
	('KAM_ADVERT3', 'Traffic / Expiring Domain List Spam'),
	('KAM_ADVERT', 'Mailing List Scammers Hawking Their Lists / Services'),
	('KAM_ALARM', 'Security and Alarm Company Spams'),
	('KAM_ALT', 'Requests use of an alternate email which may indicate spam'),
	('KAM_ANA', 'Likely Weight-loss / Medical Spam'),
	('KAM_ANATA', 'Drug Spam'),
	('__KAM_AOL', 'Partial Rule: Marks AOL Addresses'),
	('KAM_AP', 'American Publishing Spam'),
	('KAM_AQUA', 'Spams of yet another product du jour'),
	('KAM_ARREST', 'Arrest Record Scams'),
	('KAM_ASIAN', 'Asian Bride Spams'),
	('KAM_AUGER', 'Spammers hawking Awesome Augers?!?'),
	('KAM_AV', 'Anti-Virus Spams'),
	('KAM_BACK', 'Background Check SPAM'),
	('KAM_BADIPHTTP', 'Due to the Storm Bot Network, IPs in emails is bad'),
	('KAM_BADSWF', 'SWF embedded links in Email Scams'),
	('KAM_BARK', 'Dog Product Scam'),
	('KAM_BBB', 'Better Business Bureau Phishing'),
	('KAM_BILLS', 'Bill Pay Spams'),
	('KAM_BIZ', 'Free Business Card Emails'),
	('KAM_BLOOD', 'Blood Pressure Spams'),
	('KAM_BODY', 'Odd Erectile Dysfunction Messages with Poor Formatting'),
	('KAM_BUS', 'Yet another Nigerian Scam/Phishing Variant'),
	('KAM_CANSPAM', 'SPAM = Lack of Consent (not a Legal Definition)'),
	('KAM_CARDEAL', 'Car Deal Spams'),
	('KAM_CARD', 'Trojan or Virus Payload from fake ecard notice'),
	('KAM_CAREER', 'Spam for Career/Diploma Mills'),
	('KAM_CAR', 'Spammers hawking new car, insurance or warranties'),
	('KAM_CASINO', 'Online Casino Spam'),
	('KAM_CEP', 'CEP Diploma Mill Rule'),
	('KAM_CHANGELOG', 'Phishing Email'),
	('KAM_CHECK', 'Another Nigerian Bank Draft Scam'),
	('KAM_CHOSEN', 'Spam claiming the recipient has been chosen for something'),
	('KAM_CIGAR', 'Cigar Scam Emails'),
	('KAM_CNN', 'CNN Daily Top 10 Link Obfuscation spams'),
	('KAM_COBRA', 'Cobra Insurance Spam'),
	('KAM_COLLECT', 'Spammers hawking debt collection'),
	('KAM_COLLEGE', 'Online Degree/Aid Spams'),
	('KAM_COMBO_BADAOL', 'Invalid AOL Email Address-High probability of spam'),
	('KAM_COMBOJDR', 'Spam Test for Rules Combined with KAM_SPAMJDR'),
	('KAM_COMPANY1', 'Egregious spammers that should also be on RBLs (and might be)'),
	('KAM_COMPANY2', 'Egregious spammers that should also be on RBLs (and might be)'),
	('KAM_COMPROMISED', 'Compromised Accounts Sending Spam'),
	('KAM_COUK', 'Scoring .co.uk emails higher due to poor registry security.'),
	('KAM_CREDIT2', 'Credit Score Spams'),
	('KAM_CREDIT', 'Credit Score Spams'),
	('KAM_DEBT2', 'Likely Debt eradication spams'),
	('KAM_DEBT', 'Debt eradication spams'),
	('KAM_DIET2', 'Diet Scams'),
	('KAM_DIRECT', 'DirectBuy Spam'),
	('KAM_DISCAIR', 'Discount Airfare Spam'),
	('KAM_DISH2', 'Dish Network Spams'),
	('KAM_DISH', 'Dish Network Spams'),
	('KAM_DIV', 'Use of divs to hide Medical Spams'),
	('KAM_DOMAIN', 'Domain Selling Spams'),
	('KAM_DON2', 'Egregious Work at Home Scams'),
	('KAM_DON', 'Work at Home Scams'),
	('KAM_DRILL', 'Oil Drilling SPAM'),
	('KAM_DRUG2_2', 'Higher Certainty of Drug Scam'),
	('KAM_DRUG2', 'More online Drug Scams'),
	('KAM_DRUG', 'More Viagra, Medicine, et al Scams'),
	('KAM_DUCHESS', 'Spammer sending emails using a variety of domains and linked images'),
	('KAM_EBAY', 'SPAMs re: eBay Auction Tips'),
	('KAM_EGG', 'Spams of yet another product du jour'),
	('KAM_EMAILPHISH', 'Email Phishing Scams'),
	('KAM_EMMAPP_WEB_COM', 'Spam from emmapp.web.com'),
	('KAM_EXEURI', 'EXE embedded link'),
	('KAM_FACEBOOKMAIL', 'Fake or Abused Facebook Mail'),
	('KAM_FACE', 'Facebook bogus phishing emails'),
	('KAM_FAKE_ATT', 'Fake AT&T newsletters'),
	('KAM_FAKE_DELIVER', 'Fake delivery notifications'),
	('KAM_FAKEPDF', 'Fake PDF Reader / Writer'),
	('KAM_FARM', 'Farming related Spams'),
	('KAM_FAST', 'Get Rich Quick, Make Money Fast Schemes'),
	('KAM_FDA', 'Carries a not evaluated by the FDA warning or recall warning'),
	('KAM_FLASH', 'Fake Flash Player Phishing Scam'),
	('KAM_FLEXHOSE', 'Product Spam du Jour'),
	('KAM_FOOTBALL', 'Spammy Football Club'),
	('KAM_GALLERY2', 'Higher Likelihood of Exploited Gallery with Porn'),
	('KAM_GALLERY', 'Exploited Gallery with Porn'),
	('KAM_GAS', 'SPAMs re: High Gas Prices'),
	('KAM_GEO_STRING2', 'Use of geocities/yahoo very likely spam as of Dec 2005'),
	('KAM_GERMAN_BUSINESS_CONTACTS', 'Weird German business contact info spam'),
	('KAM_GEVALIA', 'Spams of yet another product du jour'),
	('KAM_GIFT', 'Gift Certificate Spams'),
	('KAM_GINA', 'Employment Poster Marketing Spams'),
	('KAM_GIVE', 'Free stuff "giveaway" scam'),
	('__KAM_GOODAOL', 'Partial Rule: Marks Bad AOL Addresses'),
	('KAM_GOOGLE', 'Google Pyramid Scams'),
	('KAM_GOOGLEPHISH', 'Google Login Phishing Scam'),
	('KAM_GOOGLE_STRING', 'Use of Google redir appearing in spam July 2006'),
	('KAM_GOVT', 'Your tax dollars at work scam...'),
	('KAM_GRANT', 'Government Grant Scams'),
	('KAM_GRASS', 'Spammers hawking lawn products'),
	('KAM_GUN', 'Gun Alert Spams'),
	('KAM_H1QNUM2', 'H1 Qnum higher spamminess indicators'),
	('KAM_H1QNUM', 'H1 Qnum indicator'),
	('KAM_HAIR', 'Hair Loss Spams'),
	('KAM_HAIRTRANS', 'Spam for Hair Restoration'),
	('KAM_HANGOVER', 'Hangover Patch Spams'),
	('KAM_HEALTH2', 'Health Insurance Spam Emails'),
	('KAM_HEALTH', 'Health/Life Insurance Spam Emails'),
	('KAM_HEART', 'Spam for Heart Attack prevention'),
	('KAM_HEX', 'Crazy Empty Hex Messages'),
	('KAM_HIDDEN_URI', 'URI obfuscation techniques'),
	('KAM_HIP', 'Hip Replacement Recall Spam'),
	('KAM_HOLLY', 'Obfuscated address appearing in SPAM Jun 06'),
	('KAM_HOME', 'Mortage & Refinance Spam Rule'),
	('KAM_HOMESALE', 'Home Sale Spams'),
	('KAM_HONEY', 'Spammer sending to a honeypot or known spammer through other means'),
	('KAM_HOODIA', 'Hoodia / Weight Loss Product Promotion Spam'),
	('KAM_HOSE', 'Garden Hose Spams'),
	('KAM_HSR', 'High Speed Rail Spam'),
	('KAM_IDENTNET', 'Identity Network Spams'),
	('KAM_INFOUSMEBIZ', 'Prevalent use of .info|.us|.me|.biz domains in spam/malware'),
	('KAM_INK2', 'Spams for Ink refills'),
	('KAM_INK', 'Spams of yet another product du jour'),
	('KAM_INSURE', 'Life, Health, Auto, etc. Insurance SPAMs'),
	('KAM_INVFROM', 'Invalid From Header containing mismatched <>\'s'),
	('KAM_JOB', 'People let go, work at home, earn billions!'),
	('KAM_JOINT', 'Joint relief Spam'),
	('KAM_JUDGE', 'Email Contains Judicial Judgment Solicitation'),
	('KAM_LAKE', 'Odd spamming engine LAKE signature on URLs'),
	('KAM_LANG', 'Language Method Spams'),
	('KAM_LASIK', 'Lasik Treatment Spams'),
	('KAM_LIKE', 'I like your website link exchange spam'),
	('KAM_LINGERIE', 'Sexually Explicity Lingerie Spam'),
	('KAM_LIST', 'Mailing List Database SPAM'),
	('KAM_LIVE', 'blogspot.com & livejournal.com likely spam (Apr 2010)'),
	('KAM_LIVEURI2', 'More online Scams + Known URI'),
	('KAM_LOTTO1', 'Likely to be an e-Lotto Scam Email'),
	('KAM_LOTTO2', 'Highly Likely to be an e-Lotto Scam Email'),
	('KAM_LOTTO3', 'Almost certain to be an e-Lotto Scam Email'),
	('KAM_MAID', 'Maid Service Spams'),
	('KAM_MAILBOX', 'Mailbox Quota Phishing Scams'),
	('KAM_MAILER', 'Automated Mailer Tag Left in Email'),
	('KAM_MARK', 'Email arrived marked as Spam'),
	('KAM_MASCARA', 'Make-up Spams'),
	('KAM_MASSERROR', 'Error in usage of a mass mailing software'),
	('KAM_MED2', 'More Medical SPAM'),
	('KAM_MED', 'Economizing your meds spam'),
	('KAM_MEDICARE', 'Medicare Scams'),
	('KAM_MEDTOUR', 'Medical Tourism Spam'),
	('KAM_MEMBER', 'Dating Scams'),
	('KAM_MOVIE', 'Spammers hawking Movie Extra positions'),
	('KAM_MSNBR_REDIR', 'Use of MSN Brasil Redirector for Spam seen in 2011'),
	('KAM_MSN_STRING', 'spaces.msn.com likely spam (Mar 2006) + spaces.live.com (Mar 2010)'),
	('KAM_MUSTREAD', 'Subject indicative of a SPAM message'),
	('KAM_MX4', 'MX Record and dot info domains associated with FAKERBL Spammers'),
	('__KAM_MX', 'Odd prevalence of mx records associated with the FAKERBL Spammers'),
	('KAM_MXURI', 'URI begins with a mail exchange prefix, i.e. mx.[...]'),
	('KAM_NEW_CREDITCARD', 'Spam for new credit cards'),
	('KAM_NEWS', 'Forged Emails with NEWS!'),
	('KAM_NIGERIAN2', 'Yet more Nigerian scams. Some even explaining the scam.'),
	('KAM_NIGERIAN', 'Nigerian Scam and Variants'),
	('KAM_NOTIFY2', 'Higher likelihood of fake notification'),
	('KAM_NOTIFY', 'Fake Notifications'),
	('KAM_NURSE', 'Spam for Career/Diploma Mills'),
	('KAM_OBF', 'Obfuscated Porn Spams'),
	('KAM_OBFURI', 'Obfuscated URI trick'),
	('KAM_OBFURL', 'Obfuscated URL'),
	('KAM_OPRAH', 'SPAMs re: Oprah Winfrey Show'),
	('KAM_OVERPAY', 'Common Medicinal Ad Trick'),
	('KAM_OWAPHISH1', 'Rash of OWA setting change emails for phishing'),
	('KAM_OZ', 'Fake Dr. Oz Spam\'s'),
	('KAM_PAGE', 'Page.TL likely spam (Nov 2011)'),
	('KAM_PAINT', 'Paint Spams'),
	('KAM_PAYDAY', 'Payday loan Spams'),
	('KAM_PAYPAL', 'rampant paypal phishing scams'),
	('KAM_PEEL', 'Spams of yet another product du jour'),
	('KAM_PERPARK', 'Obfuscated address appearing in SPAM Feb 06'),
	('KAM_PEST', 'Spam for Pest Control'),
	('KAM_PET', 'Pet Insurance Spam'),
	('KAM_PHISH1', 'Test for PHISH that changes the cursor'),
	('KAM_PHISH2', 'Prevalent Phishing Scam emails'),
	('KAM_PHISH3', 'Phishing emails for account notification'),
	('KAM_PIC', 'Share Pictures and Chat SPAM'),
	('KAM_PILLS2', 'Male enhancement spams'),
	('KAM_PILLS', 'Spam for scam pharmacy'),
	('KAM_POLITICS', 'Unsolicited Political E-Mails'),
	('KAM_POLY', 'Political Spams'),
	('KAM_POWER', 'Motorized Chair Spams'),
	('KAM_PRIV', 'Private Messages using Exploits in attached HTML files'),
	('KAM_PRODUCT', 'Product scams often used with MSN/Live URIs'),
	('KAM_PROPHET', 'Spam for Prophecy'),
	('KAM_PUBLIC', 'Obtained from Public List != to Consent == SPAM!'),
	('KAM_QTJARS', 'QTJars Spams'),
	('KAM_QUOTA', 'Limited Access / Quota Phishing Scam'),
	('KAM_RAT', 'Variable Replacements Indicative of RatWare/Mass Mailing'),
	('KAM_RBL', 'Higher scores for hitting multiple trusted RBLs'),
	('KAM_REAL2', 'Real-estate investment scams'),
	('KAM_REAL', 'Real Estate or Re-Finance Spam'),
	('KAM_REFI', 'Real Estate / Re-Finance Spam'),
	('KAM_REHAB', 'Rehab Spam'),
	('KAM_REPLACE', 'Spams that use obfuscated URLs with instructions'),
	('KAM_RE_PLUS', 'Bad Subject and Image Only rule hit == SPAM!'),
	('KAM_RE', 'Subject of Re[0]: etc prevalent in Spam'),
	('KAM_RICH', 'Get Rich Quick Schemes'),
	('KAM_RXPLAN', 'Rx Plan Spams'),
	('KAM_SANTA', 'Ho Ho Holy smokes Batman another Santa Letter spam...'),
	('KAM_SCHOOL', 'School Spams'),
	('KAM_SCOOTER', 'Blood Pressure Spams'),
	('KAM_SEARCH', 'Spammers hawking SEO'),
	('KAM_SELLPHONE', 'Used Equipment Spam'),
	('KAM_SELL', 'Selling Cards Marketing Scams'),
	('KAM_SENIOR_DATING', 'Senior dating spam'),
	('KAM_SEO', 'Spammers hawking SEO'),
	('KAM_SETTING2', 'Phishing scams w/Setting Files or Webmail + Bad File link'),
	('KAM_SETTING', 'Phishing scams w/Setting Files or Webmail'),
	('KAM_SEX04_2', 'Likely Sexually Explicit SPAM'),
	('KAM_SEX04', 'Sexually Explicit SPAM'),
	('KAM_SEX05', 'Sexually Explicit SPAM'),
	('KAM_SEX06', 'Sexual Stimulant Spam'),
	('KAM_SEX_AFFAIR', 'Subject or body soliciting an affair'),
	('KAM_SEX_EXPLICIT', 'Subject or body indicates Sexually Explicit material'),
	('KAM_SEX', 'Sexually Explicit SPAM / Penis Enlargement Scam'),
	('KAM_SEXSUBJECT', 'Sexually Explicit Subject'),
	('KAM_SHAM', 'More product scams...'),
	('KAM_SHARP', 'Ceramic Blade Spam'),
	('KAM_SHOP', 'Mystery Shopper Scams'),
	('KAM_SILD', 'Simple rule to block one more enhancement message'),
	('KAM_SKIN', 'Spammers hawking skin/medical/foot products'),
	('KAM_SKYPE', 'Skype/Voip scams likely to spread malware'),
	('KAM_SLICEOMATIC', 'Spam for Kitchen Tools'),
	('KAM_SMOKE', 'Smokeless cigarette spam'),
	('KAM_SNORE', 'Snoring Aid Spams'),
	('KAM_SOCKET', 'Product Spam du Jour'),
	('KAM_SOFTWARE', 'Spammers hawking Software products'),
	('KAM_SOLAR', 'Solar Power Spams'),
	('KAM_SPAMJDR', 'Emails seen with SPAM containing this header X-Mailerinfo: OTHR_JDR1173771'),
	('KAM_SPAMKING', 'SPAM using throw-away domains and addresses.  SpamKing\'s Heir!'),
	('KAM_SPAM', 'These are people who spam me.  I\'m tired of it.'),
	('KAM_STAR', 'Stupid Obfuscated Link SPAMs'),
	('KAM_STOCK2', 'Another Round of Pump & Dump Stock Scams'),
	('__KAM_STOCK3', 'Email Looks like it references a 4 character stock symbol'),
	('KAM_STOCKGEN', 'Email Contains Generic Pump & Dump Stock Tip'),
	('KAM_STOCKG', 'Graphical Pump and Dump Scams'),
	('KAM_STOCKTIP', 'Email Contains Pump & Dump Stock Tip'),
	('KAM_STUDENT', 'Student Loan Forgiveness Spams'),
	('KAM_SURVEY', 'Online Survey Spams'),
	('KAM_SWIPE2', 'SwipeBid Spam / Penny Auction Spams'),
	('KAM_SWIPE', 'SwipeBid Spam / Penny Auction Spams'),
	('KAM_TAX', 'Tax Filing Scams'),
	('KAM_TELEWORK', 'Stupid telework scam'),
	('KAM_TESTOSTERONE', 'Product Spam du Jour'),
	('KAM_THEBAT', 'Abused X-Mailer Header for The Bat! MUA'),
	('KAM_TIMEGEO', 'Email references geocities & wrist watch sales'),
	('KAM_TIME', 'Pssss.  Hey Buddy, wanna buy a watch?'),
	('KAM_TIMESHARE', 'Timeshare Scams'),
	('KAM_TINNI', 'Another Medical Scam'),
	('KAM_TIP', 'Beauty Tip Spams'),
	('KAM_TIRES', 'Spam for Tires'),
	('KAM_TK', 'Abuse of .tk domain registrar which offers free domains'),
	('KAM_TRACK', 'Fake Tracking Emails'),
	('KAM_TUB', 'Tub Spams'),
	('KAM_TV2', 'Free TV/Cable/etc. Spams'),
	('KAM_TV', 'Free TV/Cable/etc. Scams'),
	('KAM_TWIT', 'Twitter bogus phishing emails'),
	('KAM_UAH_YAHOOGROUP_SENDER', 'Sender appears to be a legit Yahoo! Group Mail'),
	('KAM_UNIV', 'Diploma Mill Rule'),
	('KAM_UNSUB', 'Completely ridiculous unsubscribe text found'),
	('KAM_UPS', 'UPS doesn\'t send invoices with delivery problem notes'),
	('KAM_URIPARSE', 'Attempted use of URI bug-high probability of fraud'),
	('KAM_URONLINE', 'Chat Scams'),
	('KAM_URUNIT', 'Recent penile and body enhancement spams'),
	('KAM_URZEST', 'Recent penile and body enhancement spams'),
	('KAM_USB', 'USB Promotion Spammer'),
	('KAM_VACATION', 'Vacation Spams'),
	('KAM_VIAGRA1', 'Common Viagra and Medicinal Table Trick'),
	('KAM_VIAGRA2', 'Common Viagra and Medicinal Table Trick'),
	('KAM_VIAGRA3', 'Common Viagra and Medicinal Table Trick'),
	('KAM_VIAGRA4', 'Common Viagra and Medicinal Table Trick'),
	('KAM_VIAGRA5', 'Viagra Obfuscation Technique SPAM'),
	('KAM_VIAGRA6', 'Viagra Obfuscation Technique SPAM'),
	('KAM_VIAGRA7', 'Viagra Obfuscation Technique SPAM'),
	('KAM_VIAGRA8', 'Viagra Obfuscation Technique SPAM'),
	('KAM_VIAGRA9', 'Viagra Obfuscation Technique SPAM'),
	('KAM_WARRANTY2', 'Spammers pushing home warranties'),
	('KAM_WARRANTY', 'Spammers hawking home warranties'),
	('KAM_WEALTH', 'Wealth Scheme Spams'),
	('KAM_WEBS', 'webs.com links used in Spams'),
	('KAM_WEB', 'Web design spams'),
	('KAM_WHATS', 'WhatsApp Spams'),
	('KAM_WHITEN', 'Teeth Whitening Scams'),
	('KAM_WIFE', 'Mail order bride scams'),
	('KAM_WINDOWS', 'Spam for House Windows'),
	('KAM_WORKHOME2', 'Work at Home Spam'),
	('KAM_WORKHOME', 'Work at Home Spam'),
	('KAM_WTA', 'Ridiculous campaign by unapologetic spammers purposefully using throwaway domains'),
	('KHOP_SC_CIDR16', 'Relay listed in SpamCop top 12 IP/16 CIDRs'),
	('KHOP_SC_CIDR24', 'Relay listed in SpamCop top 12 IP/24 CIDRs'),
	('KHOP_SC_TOP200', 'Relay listed in SpamCop top 200 spammer IPs'),
	('KHOP_SC_TOP_CIDR16', 'Relay listed in SpamCop top 6 IP/16 CIDRs'),
	('KHOP_SC_TOP_CIDR8', 'Relay listed in SpamCop top 4 IP/8 CIDRs'),
	('KOREAN_UCE_SUBJECT', 'Subject: contains Korean unsolicited email tag'),
	('LIST_PRTL_PUMPDUMP', 'Incomplete List-* headers and stock pump-and-dump'),
	('LIST_PRTL_SAME_USER', 'Incomplete List-* headers and from+to user the same'),
	('LIVE_PORN', 'Possible porn - Live Porn'),
	('LOCALPART_IN_SUBJECT', 'Local part of To: address appears in Subject'),
	('LOCAL_TEST1', 'This is a unique phrase to trigger a + score'),
	('LONG_HEX_URI', 'Very long purely hexadecimal URI'),
	('LONG_IMG_URI', 'Image URI with very long path component - web bug?'),
	('LONGWORDS', 'Long string of long words'),
	('LOOPHOLE_1', 'A loop hole in the banking laws?'),
	('LOTTO_AGENT', 'Claims Agent'),
	('LOTTO_AGENT_RPLY', 'Claims Agent'),
	('LOTTO_DEPT', 'Claims Department'),
	('LOW_PRICE', 'Lowest Price'),
	('LUCRATIVE', 'Make lots of money!'),
	('MAILER_EQ_ORG', 'X-Mailer: same as Organization:'),
	('MALE_ENHANCE', 'Message talks about enhancing men'),
	('MALFORMED_FREEMAIL', 'Bad headers on message from free email service'),
	('MANY_GOOG_PROXY', 'Many Google feedproxy URIs'),
	('MANY_SPAN_IN_TEXT', 'Many <SPAN> tags embedded within text'),
	('MARKETING_PARTNERS', 'Claims you registered with a partner'),
	('MICROSOFT_EXECUTABLE', 'Message includes Microsoft executable program'),
	('MILLION_USD', 'Talks about millions of dollars'),
	('MIME_BAD_ISO_CHARSET', 'MIME character set is an unknown ISO charset'),
	('MIME_BASE64_BLANKS', 'Extra blank lines in base64 encoding'),
	('__MIME_BASE64', 'Includes a base64 attachment'),
	('MIME_BASE64_TEXT', 'Message text disguised using base64 encoding'),
	('MIME_BOUND_DD_DIGITS', 'Spam tool pattern in MIME boundary'),
	('MIME_BOUND_DIGITS_15', 'Spam tool pattern in MIME boundary'),
	('MIME_BOUND_MANY_HEX', 'Spam tool pattern in MIME boundary'),
	('MIME_CHARSET_FARAWAY', 'MIME character set indicates foreign language'),
	('MIME_HEADER_CTYPE_ONLY', '\'Content-Type\' found without required MIME headers'),
	('MIME_HTML_MOSTLY', 'Multipart message mostly text/html MIME'),
	('MIME_HTML_ONLY', 'Message only has text/html MIME parts'),
	('MIME_HTML_ONLY_MULTI', 'Multipart message only has text/html MIME parts'),
	('__MIME_QP', 'Includes a quoted-printable attachment'),
	('MIME_QP_LONG_LINE', 'Quoted-printable line longer than 76 chars'),
	('MIME_SUSPECT_NAME', 'MIME filename does not match content'),
	('MISSING_DATE', 'Missing Date: header'),
	('MISSING_FROM', 'Missing From: header'),
	('MISSING_HB_SEP', 'Missing blank line between message header and body'),
	('MISSING_HEADERS', 'Missing To: header'),
	('MISSING_MID', 'Missing Message-Id: header'),
	('MISSING_MIME_HB_SEP', 'Missing blank line between MIME header and body'),
	('MISSING_MIMEOLE', 'Message has X-MSMail-Priority, but no X-MimeOLE'),
	('MISSING_SUBJECT', 'Missing Subject: header'),
	('__ML_TURNS_SP_TO_TAB', 'A mailing list changing a space to a TAB'),
	('MONEY_ATM_CARD', 'Lots of money on an ATM card'),
	('MONEY_BACK', 'Money back guarantee'),
	('MONEY_BARRISTER', 'Lots of money from a UK lawyer'),
	('MONEY_FORM', 'Lots of money if you fill out a form'),
	('MONEY_FORM_SHORT', 'Lots of money if you fill out a short form'),
	('MONEY_FRAUD_3', 'Lots of money and several fraud phrases'),
	('MONEY_FRAUD_5', 'Lots of money and many fraud phrases'),
	('MONEY_FRAUD_8', 'Lots of money and very many fraud phrases'),
	('MONEY_FROM_41', 'Lots of money from Africa'),
	('MONEY_FROM_MISSP', 'Lots of money and misspaced From'),
	('MONEY_LOTTERY', 'Lots of money from a lottery'),
	('MORE_SEX', 'Talks about a bigger drive for sex'),
	('MPART_ALT_DIFF_COUNT', 'HTML and text parts are different'),
	('MPART_ALT_DIFF', 'HTML and text parts are different'),
	('MSGID_FROM_MTA_HEADER', 'Message-Id was added by a relay'),
	('MSGID_MULTIPLE_AT', 'Message-ID contains multiple \'@\' characters'),
	('MSGID_OUTLOOK_INVALID', 'Message-Id is fake (in Outlook Express format)'),
	('MSGID_RANDY', 'Message-Id has pattern used in spam'),
	('MSGID_SHORT', 'Message-ID is unusually short'),
	('MSGID_SPAM_CAPS', 'Spam tool Message-Id: (caps variant)'),
	('MSGID_SPAM_LETTERS', 'Spam tool Message-Id: (letters variant)'),
	('MSGID_YAHOO_CAPS', 'Message-ID has ALLCAPS@yahoo.com'),
	('MULTI_FORGED', 'Received headers indicate multiple forgeries'),
	('NA_DOLLARS', 'Talks about a million North American dollars'),
	('NAME_EMAIL_DIFF', 'Sender NAME is an unrelated email address'),
	('NML_ADSP_CUSTOM_HIGH', 'ADSP custom_high hit, and not from a mailing list'),
	('NML_ADSP_CUSTOM_LOW', 'ADSP custom_low hit, and not from a mailing list'),
	('NML_ADSP_CUSTOM_MED', 'ADSP custom_med hit, and not from a mailing list'),
	('NO_DNS_FOR_FROM', 'Envelope sender has no MX or A DNS records'),
	('NO_HEADERS_MESSAGE', 'Message appears to be missing most RFC-822 headers'),
	('NO_MEDICAL', 'No Medical Exams'),
	('NONEXISTENT_CHARSET', 'Character set doesn\'t exist'),
	('NO_PRESCRIPTION', 'No prescription needed'),
	('NO_RDNS_DOTCOM_HELO', 'Host HELO\'d as a big ISP, but had no rDNS'),
	('NO_RECEIVED', 'Informational: message has no Received headers'),
	('NO_RELAYS', 'Informational: message was not relayed via SMTP'),
	('NORMAL_HTTP_TO_IP', 'Uses a dotted-decimal IP address in URL'),
	('NOT_ADVISOR', 'Not registered investment advisor'),
	('__NSL_ORIG_FROM_41', 'Originates from 41.0.0.0/8'),
	('NSL_ORIG_FROM_41', 'Originates from 41.0.0.0/8'),
	('__NSL_RCVD_FROM_41', 'Received from 41.0.0.0/8'),
	('NSL_RCVD_FROM_41', 'Received from 41.0.0.0/8'),
	('NSL_RCVD_FROM_USER', 'Received from User'),
	('NSL_RCVD_HELO_USER', 'Received from HELO User'),
	('NULL_IN_BODY', 'Message has NUL (ASCII 0) byte in message'),
	('NUMERIC_HTTP_ADDR', 'Uses a numeric IP address in URL'),
	('OBFU_JVSCR_ESC', 'Injects content using obfuscated javascript'),
	('OBFUSCATING_COMMENT', 'HTML comments which obfuscate text'),
	('OBSCURED_EMAIL', 'Message seems to contain rot13ed address'),
	('ONE_TIME', 'One Time Rip Off'),
	('ONLINE_PHARMACY', 'Online Pharmacy'),
	('PART_CID_STOCK', 'Has a spammy image attachment (by Content-ID)'),
	('PART_CID_STOCK_LESS', 'Has a spammy image attachment (by Content-ID, more specific)'),
	('PERCENT_RANDOM', 'Message has a random macro in it'),
	('PHP_NOVER_MUA', 'Mail from PHP with no version number'),
	('PLING_QUERY', 'Subject has exclamation mark and question mark'),
	('PREST_NON_ACCREDITED', '\'Prestigious Non-Accredited Universities\''),
	('PREVENT_NONDELIVERY', 'Message has Prevent-NonDelivery-Report header'),
	('PRICES_ARE_AFFORDABLE', 'Message says that prices aren\'t too expensive'),
	('PUMPDUMP_MULTI', 'Pump-and-dump stock scam phrases'),
	('PUMPDUMP', 'Pump-and-dump stock scam phrase'),
	('PUMPDUMP_TIP', 'Pump-and-dump stock tip'),
	('PYZOR_CHECK', 'Listed in Pyzor (http://pyzor.sf.net/)'),
	('RAND_HEADER_MANY', 'Many random gibberish message headers'),
	('RATWARE_EFROM', 'Bulk email fingerprint (envfrom) found'),
	('RATWARE_EGROUPS', 'Bulk email fingerprint (eGroups) found'),
	('RATWARE_HASH_DASH', 'Contains a hashbuster in Send-Safe format'),
	('RATWARE_MOZ_MALFORMED', 'Bulk email fingerprint (Mozilla malformed) found'),
	('RATWARE_MPOP_WEBMAIL', 'Bulk email fingerprint (mPOP Web-Mail)'),
	('RATWARE_MS_HASH', 'Bulk email fingerprint (msgid ms hash) found'),
	('RATWARE_NAME_ID', 'Bulk email fingerprint (msgid from) found'),
	('RATWARE_OE_MALFORMED', 'X-Mailer has malformed Outlook Express version'),
	('RATWARE_OUTLOOK_NONAME', 'Bulk email fingerprint (Outlook no name) found'),
	('RATWARE_RCVD_AT', 'Bulk email fingerprint (Received @) found'),
	('RATWARE_RCVD_PF', 'Bulk email fingerprint (Received PF) found'),
	('RATWARE_ZERO_TZ', 'Bulk email fingerprint (+0000) found'),
	('RAZOR2_CF_RANGE_51_100', 'Razor2 gives confidence level above 50%'),
	('RAZOR2_CF_RANGE_E4_51_100', 'Razor2 gives engine 4 confidence level above 50%'),
	('RAZOR2_CF_RANGE_E8_51_100', 'Razor2 gives engine 8 confidence level above 50%'),
	('RAZOR2_CHECK', 'Listed in Razor2 (http://razor.sf.net/)'),
	('RCVD_AM_PM', 'Received headers forged (AM/PM)'),
	('RCVD_BAD_ID', 'Received header contains id field with bad characters'),
	('RCVD_DBL_DQ', 'Malformatted message header'),
	('RCVD_DOUBLE_IP_LOOSE', 'Received: by and from look like IP addresses'),
	('RCVD_DOUBLE_IP_SPAM', 'Bulk email fingerprint (double IP) found'),
	('RCVD_FAKE_HELO_DOTCOM', 'Received contains a faked HELO hostname'),
	('RCVD_FORGED_WROTE', 'Forged \'Received\' header found (\'wrote:\' spam)'),
	('RCVD_HELO_IP_MISMATCH', 'Received: HELO and IP do not match, but should'),
	('RCVD_ILLEGAL_IP', 'Received: contains illegal IP address'),
	('__RCVD_IN_2WEEKS', 'Received: within the past 2 weeks'),
	('RCVD_IN_BL_SPAMCOP_NET', 'Received via a relay in bl.spamcop.net'),
	('RCVD_IN_CSS', 'Received via a relay in Spamhaus CSS'),
	('RCVD_IN_DNSWL_BLOCKED', 'ADMINISTRATOR NOTICE: The query to DNSWL was blocked.  See http://wiki.apache.org/spamassassin/DnsBlocklists\\#dnsbl-block for more information.'),
	('RCVD_IN_DNSWL_HI', 'Sender listed at http://www.dnswl.org/, high trust'),
	('RCVD_IN_DNSWL_LOW', 'Sender listed at http://www.dnswl.org/, low trust'),
	('RCVD_IN_DNSWL_MED', 'Sender listed at http://www.dnswl.org/, medium trust'),
	('RCVD_IN_DNSWL_NONE', 'Sender listed at http://www.dnswl.org/, no trust'),
	('RCVD_IN_IADB_DK', 'IADB: Sender publishes Domain Keys record'),
	('RCVD_IN_IADB_DOPTIN_GT50', 'IADB: Confirmed opt-in used more than 50% of the time'),
	('RCVD_IN_IADB_DOPTIN', 'IADB: All mailing list mail is confirmed opt-in'),
	('RCVD_IN_IADB_DOPTIN_LT50', 'IADB: Confirmed opt-in used less than 50% of the time'),
	('RCVD_IN_IADB_EDDB', 'IADB: Participates in Email Deliverability Database'),
	('RCVD_IN_IADB_EPIA', 'IADB: Member of Email Processing Industry Alliance'),
	('RCVD_IN_IADB_GOODMAIL', 'IADB: Sender has been certified by GoodMail'),
	('RCVD_IN_IADB_LISTED', 'Participates in the IADB system'),
	('RCVD_IN_IADB_LOOSE', 'IADB: Adds relationship addrs w/out opt-in'),
	('RCVD_IN_IADB_MI_CPEAR', 'IADB: Complies with Michigan\'s CPEAR law'),
	('RCVD_IN_IADB_MI_CPR_30', 'IADB: Checked lists against Michigan\'s CPR within 30 days'),
	('RCVD_IN_IADB_MI_CPR_MAT', 'IADB: Sends no material under Michigan\'s CPR'),
	('RCVD_IN_IADB_ML_DOPTIN', 'IADB: Mailing list email only, confirmed opt-in'),
	('RCVD_IN_IADB_NOCONTROL', 'IADB: Has absolutely no mailing controls in place'),
	('RCVD_IN_IADB_OOO', 'IADB: One-to-one/transactional email only'),
	('RCVD_IN_IADB_OPTIN_GT50', 'IADB: Opt-in used more than 50% of the time'),
	('RCVD_IN_IADB_OPTIN', 'IADB: All mailing list mail is opt-in'),
	('RCVD_IN_IADB_OPTIN_LT50', 'IADB: Opt-in used less than 50% of the time'),
	('RCVD_IN_IADB_OPTOUTONLY', 'IADB: Scrapes addresses, pure opt-out only'),
	('RCVD_IN_IADB_RDNS', 'IADB: Sender has reverse DNS record'),
	('RCVD_IN_IADB_SENDERID', 'IADB: Sender publishes Sender ID record'),
	('RCVD_IN_IADB_SPF', 'IADB: Sender publishes SPF record'),
	('RCVD_IN_IADB_UNVERIFIED_1', 'IADB: Accepts unverified sign-ups'),
	('RCVD_IN_IADB_UNVERIFIED_2', 'IADB: Accepts unverified sign-ups, gives chance to opt out'),
	('RCVD_IN_IADB_UT_CPEAR', 'IADB: Complies with Utah\'s CPEAR law'),
	('RCVD_IN_IADB_UT_CPR_30', 'IADB: Checked lists against Utah\'s CPR within 30 days'),
	('RCVD_IN_IADB_UT_CPR_MAT', 'IADB: Sends no material under Utah\'s CPR'),
	('RCVD_IN_IADB_VOUCHED', 'ISIPP IADB lists as vouched-for sender'),
	('RCVD_IN_MAPS_DUL', 'Relay in DUL, http://www.mail-abuse.com/enduserinfo_dul.html'),
	('RCVD_IN_MAPS_NML', 'Relay in NML, http://www.mail-abuse.com/enduserinfo_nml.html'),
	('RCVD_IN_MAPS_OPS', 'Relay in OPS, http://www.mail-abuse.com/enduserinfo_ops.html'),
	('RCVD_IN_MAPS_RBL', 'Relay in RBL, http://www.mail-abuse.com/enduserinfo_rbl.html'),
	('RCVD_IN_MAPS_RSS', 'Relay in RSS, http://www.mail-abuse.com/enduserinfo_rss.html'),
	('RCVD_IN_MSPIKE_BL', 'Mailspike blacklisted'),
	('RCVD_IN_MSPIKE_H2', 'Average reputation (+2)'),
	('RCVD_IN_MSPIKE_H3', 'Good reputation (+3)'),
	('RCVD_IN_MSPIKE_H4', 'Very Good reputation (+4)'),
	('RCVD_IN_MSPIKE_H5', 'Excellent reputation (+5)'),
	('RCVD_IN_MSPIKE_L2', 'Suspicious reputation (-2)'),
	('RCVD_IN_MSPIKE_L3', 'Low reputation (-3)'),
	('RCVD_IN_MSPIKE_L4', 'Bad reputation (-4)'),
	('RCVD_IN_MSPIKE_L5', 'Very bad reputation (-5)'),
	('RCVD_IN_MSPIKE_WL', 'Mailspike good senders'),
	('__RCVD_IN_MSPIKE_Z', 'Spam wave participant'),
	('RCVD_IN_NJABL_CGI', 'NJABL: sender is an open formmail'),
	('RCVD_IN_NJABL_MULTI', 'NJABL: sent through multi-stage open relay'),
	('RCVD_IN_NJABL_PROXY', 'NJABL: sender is an open proxy'),
	('__RCVD_IN_NJABL', 'Received via a relay in combined.njabl.org'),
	('RCVD_IN_NJABL_RELAY', 'NJABL: sender is confirmed open relay'),
	('RCVD_IN_NJABL_SPAM', 'NJABL: sender is confirmed spam source'),
	('RCVD_IN_PBL', 'Received via a relay in Spamhaus PBL'),
	('RCVD_IN_PSBL', 'Received via a relay in PSBL'),
	('RCVD_IN_RP_CERTIFIED', 'Sender is in Return Path Certified (trusted relay)'),
	('RCVD_IN_RP_RNBL', 'Relay in RNBL, https://senderscore.org/blacklistlookup/'),
	('RCVD_IN_RP_SAFE', 'Sender is in Return Path Safe (trusted relay)'),
	('RCVD_IN_SBL_CSS', 'Received via a relay in Spamhaus SBL-CSS'),
	('RCVD_IN_SBL', 'Received via a relay in Spamhaus SBL'),
	('RCVD_IN_SORBS_BLOCK', 'SORBS: sender demands to never be tested'),
	('RCVD_IN_SORBS_DUL', 'SORBS: sent directly from dynamic IP address'),
	('RCVD_IN_SORBS_HTTP', 'SORBS: sender is open HTTP proxy server'),
	('RCVD_IN_SORBS_MISC', 'SORBS: sender is open proxy server'),
	('RCVD_IN_SORBS_SMTP', 'SORBS: sender is open SMTP relay'),
	('RCVD_IN_SORBS_SOCKS', 'SORBS: sender is open SOCKS proxy server'),
	('__RCVD_IN_SORBS', 'SORBS: sender is listed in SORBS'),
	('RCVD_IN_SORBS_WEB', 'SORBS: sender is an abusable web server'),
	('RCVD_IN_SORBS_ZOMBIE', 'SORBS: sender is on a hijacked network'),
	('RCVD_IN_XBL', 'Received via a relay in Spamhaus XBL'),
	('__RCVD_IN_ZEN', 'Received via a relay in Spamhaus Zen'),
	('RCVD_MAIL_COM', 'Forged Received header (contains post.com or mail.com)'),
	('RCVD_NUMERIC_HELO', 'Received: contains an IP address used for HELO'),
	('__RDNS_DYNAMIC_ADELPHIA', 'Relay HELO\'d using suspicious hostname (Adelphia)'),
	('__RDNS_DYNAMIC_ATTBI', 'Relay HELO\'d using suspicious hostname (ATTBI.com)'),
	('__RDNS_DYNAMIC_CHELLO_NL', 'Relay HELO\'d using suspicious hostname (Chello.nl)'),
	('__RDNS_DYNAMIC_CHELLO_NO', 'Relay HELO\'d using suspicious hostname (Chello.no)'),
	('__RDNS_DYNAMIC_COMCAST', 'Relay HELO\'d using suspicious hostname (Comcast)'),
	('RDNS_DYNAMIC', 'Delivered to internal network by host with dynamic-looking rDNS'),
	('__RDNS_DYNAMIC_DHCP', 'Relay HELO\'d using suspicious hostname (DHCP)'),
	('__RDNS_DYNAMIC_DIALIN', 'Relay HELO\'d using suspicious hostname (T-Dialin)'),
	('__RDNS_DYNAMIC_HCC', 'Relay HELO\'d using suspicious hostname (HCC)'),
	('__RDNS_DYNAMIC_HEXIP', 'Relay HELO\'d using suspicious hostname (Hex IP)'),
	('__RDNS_DYNAMIC_HOME_NL', 'Relay HELO\'d using suspicious hostname (Home.nl)'),
	('__RDNS_DYNAMIC_IPADDR', 'Relay HELO\'d using suspicious hostname (IP addr 1)'),
	('__RDNS_DYNAMIC_NTL', 'Relay HELO\'d using suspicious hostname (NTL)'),
	('__RDNS_DYNAMIC_OOL', 'Relay HELO\'d using suspicious hostname (OptOnline)'),
	('__RDNS_DYNAMIC_ROGERS', 'Relay HELO\'d using suspicious hostname (Rogers)'),
	('__RDNS_DYNAMIC_RR2', 'Relay HELO\'d using suspicious hostname (RR 2)'),
	('__RDNS_DYNAMIC_SPLIT_IP', 'Relay HELO\'d using suspicious hostname (Split IP)'),
	('__RDNS_DYNAMIC_TELIA', 'Relay HELO\'d using suspicious hostname (Telia)'),
	('__RDNS_DYNAMIC_VELOX', 'Relay HELO\'d using suspicious hostname (Veloxzone)'),
	('__RDNS_DYNAMIC_VTR', 'Relay HELO\'d using suspicious hostname (VTR)'),
	('__RDNS_DYNAMIC_YAHOOBB', 'Relay HELO\'d using suspicious hostname (YahooBB)'),
	('RDNS_LOCALHOST', 'Sender\'s public rDNS is "localhost"'),
	('RDNS_NONE', 'Delivered to internal network by a host with no rDNS'),
	('REFINANCE_NOW', 'Home refinancing'),
	('REFINANCE_YOUR_HOME', 'Home refinancing'),
	('REMOVE_BEFORE_LINK', 'Removal phrase right before a link'),
	('REPLICA_WATCH', 'Message talks about a replica watch'),
	('REPTO_QUOTE_AOL', 'AOL doesn\'t do quoting like this'),
	('REPTO_QUOTE_IMS', 'IMS doesn\'t do quoting like this'),
	('REPTO_QUOTE_MSN', 'MSN doesn\'t do quoting like this'),
	('REPTO_QUOTE_QUALCOMM', 'Qualcomm/Eudora doesn\'t do quoting like this'),
	('REPTO_QUOTE_YAHOO', 'Yahoo! doesn\'t do quoting like this'),
	('RISK_FREE', 'No risk!'),
	('RUDE_HTML', 'Spammer message says you need an HTML mailer'),
	('S25R_1', 'S25R: Bottom of rDNS has num, non-num, num'),
	('S25R_6', 'S25R: rDNS looks dynamic or customer-facing'),
	('SANE_04e8bf28eb445199a7f11b943c44d209', 'Email.Spam.Gen3177.Sanesecurity.08051611'),
	('SANE_1c4f3286fa4aed6424ced88bfaf8b09c', 'Email.Spam.Gen3234.Sanesecurity.08052309'),
	('SANE_2b173a7fb7518c75ac8a2d294d773fd8', 'Email.Spam.Sanesecurity.Url_2496'),
	('SANE_3b92eda751c992f230f215fb7eb36844', 'Email.Spam.Gen158.Sanesecurity.07012700'),
	('SANE_4ef8302546bf270a19baf98508afacc4', 'Email.Spam.Gen1941.Sanesecurity.07112519'),
	('SANE_7429530a7398f43f1f1b795f9420714e', 'Email.Spam.Gen2507.Sanesecurity.08021303'),
	('SANE_75491b468ec2e117e50f9842d32abfab', 'Email.Spam.Gen1361.Sanesecurity.07100500'),
	('SANE_91eb43f705d25c804374a746d7519660', 'Email.Malware.Sanesecurity.07011300'),
	('SANE_d0d2b0f6373bf91253d66dd74c594b87', 'Email.Spam.Sanesecurity.Url_2499'),
	('SERGIO_SUBJECT_PORN014', 'F\\*\\*\\* garbled subject'),
	('SERGIO_SUBJECT_VIAGRA01', 'Viagra garbled subject'),
	('SHARE_50_50', 'Share the money 50/50'),
	('SHORTCIRCUIT', 'Not all rules were run, due to a shortcircuited rule'),
	('SHORT_HELO_AND_INLINE_IMAGE', 'Short HELO string, with inline image'),
	('SHORT_T_CO_200', 'Message contains a t.co URL that has a warning due to abuse'),
	('SHORT_URIBL', 'Message contains shortened URL(s) and also hits a URIDNSBL'),
	('SHORT_URL_404', 'Message has short URL that returns 404'),
	('SHORT_URL_CHAINED', 'Message has shortened URL chained to other shorteners'),
	('SHORT_URL_LOOP', 'Message has short URL that loops back to itself'),
	('SHORT_URL_MAXCHAIN', 'Message has shortened URL that causes more than 10 redirections'),
	('SINGLETS_LOW_CONTRAST', 'Single-letter formatted HTML + hidden text'),
	('SORTED_RECIPS', 'Recipient list is sorted by address'),
	('SPAMMY_XMAILER', 'X-Mailer string is common in spam and not in ham'),
	('SPF_FAIL', 'SPF: sender does not match SPF record (fail)'),
	('SPF_HELO_FAIL', 'SPF: HELO does not match SPF record (fail)'),
	('SPF_HELO_NEUTRAL', 'SPF: HELO does not match SPF record (neutral)'),
	('SPF_HELO_NONE', 'SPF: HELO does not publish an SPF Record'),
	('SPF_HELO_PASS', 'SPF: HELO matches SPF record'),
	('SPF_HELO_SOFTFAIL', 'SPF: HELO does not match SPF record (softfail)'),
	('SPF_NEUTRAL', 'SPF: sender does not match SPF record (neutral)'),
	('SPF_NONE', 'SPF: sender does not publish an SPF Record'),
	('SPF_PASS', 'SPF: sender matches SPF record'),
	('SPF_SOFTFAIL', 'SPF: sender does not match SPF record (softfail)'),
	('SPOOF_COM2COM', 'URI contains ".com" in middle and end'),
	('SPOOF_COM2OTH', 'URI contains ".com" in middle'),
	('SPOOF_NET2COM', 'URI contains ".net" or ".org", then ".com"'),
	('STOCK_ALERT', 'Offers a alert about a stock'),
	('STOCK_IMG_CTYPE', 'Stock spam image part, with distinctive Content-Type header'),
	('STOCK_IMG_HDR_FROM', 'Stock spam image part, with distinctive From line'),
	('STOCK_IMG_HTML', 'Stock spam image part, with distinctive HTML'),
	('STOCK_IMG_OUTLOOK', 'Stock spam image part, with Outlook-like features'),
	('STOCK_LOW_CONTRAST', 'Stocks + hidden text'),
	('STRONG_BUY', 'Tells you about a strong buy'),
	('STYLE_GIBBERISH', 'Nonsense in HTML <STYLE> tag'),
	('SUBJ_ALL_CAPS', 'Subject is all capitals'),
	('SUBJ_AS_SEEN', 'Subject contains "As Seen"'),
	('SUBJ_BUY', 'Subject line starts with Buy or Buying'),
	('SUBJ_DOLLARS', 'Subject starts with dollar amount'),
	('SUBJECT_DIET', 'Subject talks about losing pounds'),
	('SUBJECT_DRUG_GAP_C', 'Subject contains a gappy version of \'cialis\''),
	('SUBJECT_DRUG_GAP_L', 'Subject contains a gappy version of \'levitra\''),
	('SUBJECT_DRUG_GAP_S', 'Subject contains a gappy version of \'soma\''),
	('SUBJECT_DRUG_GAP_VA', 'Subject contains a gappy version of \'valium\''),
	('SUBJECT_DRUG_GAP_X', 'Subject contains a gappy version of \'xanax\''),
	('SUBJECT_FUZZY_CHEAP', 'Attempt to obfuscate words in Subject:'),
	('SUBJECT_FUZZY_MEDS', 'Attempt to obfuscate words in Subject:'),
	('SUBJECT_FUZZY_PENIS', 'Attempt to obfuscate words in Subject:'),
	('SUBJECT_FUZZY_TION', 'Attempt to obfuscate words in Subject:'),
	('SUBJECT_FUZZY_VPILL', 'Attempt to obfuscate words in Subject:'),
	('SUBJECT_IN_BLACKLIST', 'Subject: contains string in the user\'s black-list'),
	('SUBJECT_IN_WHITELIST', 'Subject: contains string in the user\'s white-list'),
	('SUBJECT_NEEDS_ENCODING', 'Subject is encoded but does not specify the encoding'),
	('SUBJECT_SEXUAL', 'Subject indicates sexually-explicit content'),
	('SUBJ_ILLEGAL_CHARS', 'Subject: has too many raw illegal characters'),
	('SUBJ_UNNEEDED_HTML', 'Unneeded HTML formatting in Subject:'),
	('SUBJ_YOUR_DEBT', 'Subject contains "Your Bills" or similar'),
	('SUBJ_YOUR_FAMILY', 'Subject contains "Your Family"'),
	('SUSPICIOUS_RECIPS', 'Similar addresses in recipient list'),
	('SYSADMIN', 'Supposedly from your IT department'),
	('__TAB_IN_FROM', 'From starts with a tab'),
	('TAB_IN_FROM', 'From starts with a tab'),
	('T_AXB_MIME_IMG830', 'Valpolicella Fingerprint'),
	('T_AXB_PDF_FUZZY1_OEM1', 'Fuzzy MD5 Match 2BDA96267AD8ED27FD583669CC76C4F7'),
	('T_AXB_PDF_FUZZY1_OEM2', 'Fuzzy MD5 Match CE46CE748DD314A742F7AEA05575F379'),
	('TBIRD_SUSP_MIME_BDRY', 'Unlikely Thunderbird MIME boundary'),
	('T_DATE_IN_FUTURE_96_Q', 'Date: is 4 days to 4 months after Received: date'),
	('T_DATE_IN_FUTURE_Q_PLUS', 'Date: is over 4 months after Received: date'),
	('T_DOS_OUTLOOK_TO_MX_IMAGE', 'Direct to MX with Outlook headers and an image'),
	('T_EMRCP', '"Excess Maximum Return Capital Profit" Fidelity scam'),
	('T_END_FUTURE_EMAILS', 'Spammy unsubscribe'),
	('TEQF_USR_IMAGE', 'To and from user nearly same + image'),
	('TEQF_USR_MSGID_HEX', 'To and from user nearly same + unusual message ID'),
	('TEQF_USR_MSGID_MALF', 'To and from user nearly same + malformed message ID'),
	('T_FRM_SILVER_GOLD', 'ReplaceTags: Silver & Gold'),
	('T_FROM_12LTRDOM', 'From a 12-letter domain'),
	('T_FROM_MISSPACED', 'From: missing whitespace'),
	('T_FRT_ABSOLUT', 'ReplaceTags: Absolutely'),
	('T_FRT_ADULT2', 'ReplaceTags: Adult'),
	('T_FRT_BEFORE', 'ReplaceTags: Before'),
	('T_FRT_BELOW2', 'ReplaceTags: Below (2)'),
	('T_FRT_CANSPAM', 'ReplaceTags: Can Spam'),
	('T_FRT_CLICK', 'ReplaceTags: Click'),
	('T_FRT_COCK', 'ReplaceTags: Cock'),
	('T_FRT_CONTACT', 'ReplaceTags: Contact'),
	('T_FRT_ERECTION', 'ReplaceTags: Erection'),
	('T_FRT_ESTABLISH', 'ReplaceTags: Establish'),
	('T_FRT_EXPERIENCE', 'ReplaceTags: Experience'),
	('T_FRT_FOLLOW1', 'ReplaceTags: Follow'),
	('T_FRT_FOLLOW2', 'ReplaceTags: Follow (2)'),
	('T_FRT_FREE', 'ReplaceTags: Free'),
	('T_FRT_FRIEND', 'ReplaceTags: Friend'),
	('T_FRT_FUCK1', 'ReplaceTags: Fuck (1)'),
	('T_FRT_HEALTH', 'ReplaceTags: Health'),
	('T_FRT_HOUR', 'ReplaceTags: Hour'),
	('T_FRT_INCOME', 'ReplaceTags: Income'),
	('T_FRT_INTEREST', 'ReplaceTags: Interest'),
	('T_FRT_LITTLE', 'ReplaceTags: Little'),
	('T_FRT_LOLITA1', 'ReplaceTags: Lolita (1)'),
	('T_FRT_OPPORTUN1', 'ReplaceTags: Oppertun (1)'),
	('T_FRT_PACKAGE', 'ReplaceTags: Package'),
	('T_FRT_PAYMENT', 'ReplaceTags: Payment'),
	('T_FRT_PHARMAC', 'ReplaceTags: Pharmac'),
	('T_FRT_POSSIBLE', 'ReplaceTags: Possible'),
	('T_FRT_PROFILE1', 'ReplaceTags: Profile (1)'),
	('T_FRT_PROFILE2', 'ReplaceTags: Profile (2)'),
	('T_FRT_PROFIT1', 'ReplaceTags: Profit (1)'),
	('T_FRT_PROFIT2', 'ReplaceTags: Profit (2)'),
	('T_FRT_PUSSY', 'ReplaceTags: Pussy'),
	('T_FRT_SLUT', 'ReplaceTags: Slut'),
	('T_FRT_STOCK1', 'ReplaceTags: Stock (1)'),
	('T_FRT_STOCK2', 'ReplaceTags: Stock (2)'),
	('T_FRT_VIRGIN1', 'ReplaceTags: Virgin (1)'),
	('T_HDRS_LCASE', 'Odd capitalization of message header'),
	('THIS_AD', '"This ad" and variants'),
	('TINY_FLOAT', 'Has small-font floating HTML elements - text obfuscation?'),
	('T_KAM_HTML_FONT_INVALID', 'Test for Invalidly Named or Formatted Colors in HTML'),
	('T_KHOP_FOREIGN_CLICK', 'Click here link in French or Spanish'),
	('T_LOTTO_AGENT_FM', 'Claims Agent'),
	('T_LOTTO_AGENT_RPLY', 'Claims Agent'),
	('T_LOTTO_DEPT', 'Claims Department'),
	('T_LOTTO_URI', 'Claims Department URL'),
	('T_MANY_HDRS_LCASE', 'Odd capitalization of multiple message headers'),
	('TO_EQ_FM_DIRECT_MX', 'To == From and direct-to-MX'),
	('__TO_EQ_FROM_DOM', 'To: domain same as From: domain'),
	('__TO_EQ_FROM', 'To: same as From:'),
	('__TO_EQ_FROM_USR_NN', 'To: username same as From: username sans trailing nums'),
	('__TO_EQ_FROM_USR', 'To: username same as From: username'),
	('TO_IN_SUBJ', 'To address is in Subject'),
	('TO_MALFORMED', 'To: has a malformed address'),
	('TO_NO_BRKTS_FROM_MSSP', 'Multiple formatting errors'),
	('TO_NO_BRKTS_HTML_IMG', 'To: misformatted and HTML and one image'),
	('TO_NO_BRKTS_HTML_ONLY', 'To: misformatted and HTML only'),
	('TO_NO_BRKTS_MSFT', 'To: misformatted and supposed Microsoft tool'),
	('TO_NO_BRKTS_NORDNS_HTML', 'To: misformatted and no rDNS and HTML only'),
	('TO_NO_BRKTS_PCNT', 'To: misformatted + percentage'),
	('TRACKER_ID', 'Incorporates a tracking ID number'),
	('T_RP_MATCHES_RCVD', 'Envelope sender domain matches handover relay domain'),
	('TT_MSGID_TRUNC', 'Scora: Message-Id ends after left-bracket + digits'),
	('TT_OBSCURED_VALIUM', 'Scora: obscured "VALIUM" in subject'),
	('TT_OBSCURED_VIAGRA', 'Scora: obscured "VIAGRA" in subject'),
	('TVD_ACT_193', 'Message refers to an act passed in the 1930s'),
	('TVD_APPROVED', 'Body states that the recipient has been approved'),
	('TVD_DEAR_HOMEOWNER', 'Spam with generic salutation of "dear homeowner"'),
	('TVD_ENVFROM_APOST', 'Envelope From contains single-quote'),
	('TVD_FLOAT_GENERAL', 'Message uses CSS float style'),
	('TVD_FUZZY_DEGREE', 'Obfuscation of the word "degree"'),
	('TVD_FUZZY_FINANCE', 'Obfuscation of the word "finance"'),
	('TVD_FUZZY_FIXED_RATE', 'Obfuscation of the phrase "fixed rate"'),
	('TVD_FUZZY_MICROCAP', 'Obfuscation of the word "micro-cap"'),
	('TVD_FUZZY_PHARMACEUTICAL', 'Obfuscation of the word "pharmaceutical"'),
	('TVD_FUZZY_SYMBOL', 'Obfuscation of the word "symbol"'),
	('TVD_FW_GRAPHIC_NAME_LONG', 'Long image attachment name'),
	('TVD_FW_GRAPHIC_NAME_MID', 'Medium sized image attachment name'),
	('TVD_INCREASE_SIZE', 'Advertising for penis enlargement'),
	('TVD_LINK_SAVE', 'Spam with the text "link to save"'),
	('TVD_PH_BODY_ACCOUNTS_PRE', 'The body matches phrases such as "accounts suspended", "account credited", "account verification"'),
	('TVD_PH_REC', 'Message includes a phrase commonly used in phishing mails'),
	('TVD_PH_SEC', 'Message includes a phrase commonly used in phishing mails'),
	('TVD_QUAL_MEDS', 'The body matches phrases such as "quality meds" or "quality medication"'),
	('TVD_RATWARE_CB_2', 'Content-Type header that is commonly indicative of ratware'),
	('TVD_RATWARE_CB', 'Content-Type header that is commonly indicative of ratware'),
	('TVD_RATWARE_MSGID_02', 'Ratware with a Message-ID header that is entirely lower-case'),
	('TVD_RCVD_IP4', 'Message was received from an IPv4 address'),
	('TVD_RCVD_IP', 'Message was received from an IP address'),
	('TVD_RCVD_SINGLE', 'Message was received from localhost'),
	('TVD_SECTION', 'References to specific legal codes'),
	('TVD_SILLY_URI_OBFU', 'URI obfuscation that can fool a URIBL or a uri rule'),
	('TVD_SPACED_SUBJECT_WORD3', 'Entire subject is "UPPERlowerUPPER" with no whitespace'),
	('TVD_STOCK1', 'Spam related to stock trading'),
	('TVD_SUBJ_ACC_NUM', 'Subject has spammy looking monetary reference'),
	('TVD_SUBJ_FINGER_03', 'Entire subject is enclosed in asterisks "* like so *"'),
	('TVD_SUBJ_OWE', 'Subject line states that the recipieint is in debt'),
	('TVD_SUBJ_WIPE_DEBT', 'Spam advertising a way to eliminate debt'),
	('TVD_VIS_HIDDEN', 'Invisible textarea HTML tags'),
	('TVD_VISIT_PHARMA', 'Body mentions online pharmacy'),
	('TW_GIBBERISH_MANY', 'Lots of gibberish text to spoof pattern matching filters'),
	('TWO_IPS_RCVD', 'Received: Relay identifies itself as wrong IP'),
	('TXREP', 'Score normalizing based on sender\'s reputation'),
	('UC_GIBBERISH_OBFU', 'Multiple instances of "word VERYLONGGIBBERISH word"'),
	('UNCLAIMED_MONEY', 'People just leave money laying around'),
	('UNCLOSED_BRACKET', 'Headers contain an unclosed bracket'),
	('UNPARSEABLE_RELAY', 'Informational: message has unparseable relay lines'),
	('UNRESOLVED_TEMPLATE', 'Headers contain an unresolved template'),
	('UNWANTED_LANGUAGE_BODY', 'Message written in an undesired language'),
	('UPPERCASE_50_75', 'message body is 50-75% uppercase'),
	('UPPERCASE_75_100', 'message body is 75-100% uppercase'),
	('URG_BIZ', 'Contains urgent matter'),
	('URI_BITLY_BLOCKED', 'Message contains a bit.ly URL that has been disabled due to abuse'),
	('URIBL_AB_SURBL', 'Contains an URL listed in the AB SURBL blocklist'),
	('URIBL_BLACK', 'Contains an URL listed in the URIBL blacklist'),
	('URIBL_BLOCKED', 'ADMINISTRATOR NOTICE: The query to URIBL was blocked.  See http://wiki.apache.org/spamassassin/DnsBlocklists\\#dnsbl-block for more information.'),
	('URIBL_DBL_ABUSE_BOTCC', 'Contains an abused botnet C&C URL listed in the DBL blocklist'),
	('URIBL_DBL_ABUSE_MALW', 'Contains an abused malware URL listed in the DBL blocklist'),
	('URIBL_DBL_ABUSE_PHISH', 'Contains an abused phishing URL listed in the DBL blocklist'),
	('URIBL_DBL_ABUSE_REDIR', 'Contains an abused redirector URL listed in the DBL blocklist'),
	('URIBL_DBL_ABUSE_SPAM', 'Contains an abused spamvertized URL listed in the DBL blocklist'),
	('URIBL_DBL_BOTNETCC', 'Contains a botned C&C URL listed in the DBL blocklist'),
	('URIBL_DBL_ERROR', 'Error: queried the DBL blocklist for an IP'),
	('URIBL_DBL_MALWARE', 'Contains a malware URL listed in the DBL blocklist'),
	('URIBL_DBL_PHISH', 'Contains a Phishing URL listed in the DBL blocklist'),
	('URIBL_DBL_SPAM', 'Contains a spam URL listed in the DBL blocklist'),
	('URIBL_GREY', 'Contains an URL listed in the URIBL greylist'),
	('URIBL_JP_SURBL', 'Contains an URL listed in the JP SURBL blocklist'),
	('URIBL_MW_SURBL', 'Contains a Malware Domain or IP listed in the MW SURBL blocklist'),
	('URIBL_OB_SURBL', 'Contains an URL listed in the OB SURBL blocklist'),
	('URIBL_PH_SURBL', 'Contains an URL listed in the PH SURBL blocklist'),
	('URIBL_RED', 'Contains an URL listed in the URIBL redlist'),
	('URIBL_RHS_DOB', 'Contains an URI of a new domain (Day Old Bread)'),
	('URIBL_SBL', 'Contains an URL\'s NS IP listed in the SBL blocklist'),
	('URIBL_SC_SURBL', 'Contains an URL listed in the SC SURBL blocklist'),
	('URIBL_WS_SURBL', 'Contains an URL listed in the WS SURBL blocklist'),
	('URI_DQ_UNSUB', 'IP-address unsubscribe URI'),
	('URI_GOOGLE_PROXY', 'Accessing a blacklisted URI or obscuring source of phish via Google proxy?'),
	('URI_HEX', 'URI hostname has long hexadecimal sequence'),
	('URI_HOST_IN_BLACKLIST', 'host or domain listed in the URI black-list'),
	('URI_HOST_IN_WHITELIST', 'host or domain listed in the URI white-list'),
	('URI_MIGRE_BLOCKED', 'Message contains a migre.me URL that has been disabled due to abuse'),
	('URI_NOVOWEL', 'URI hostname has long non-vowel sequence'),
	('URI_NO_WWW_BIZ_CGI', 'CGI in .biz TLD other than third-level "www"'),
	('URI_NO_WWW_INFO_CGI', 'CGI in .info TLD other than third-level "www"'),
	('URI_OBFU_WWW', 'Obfuscated URI'),
	('URI_ONLY_MSGID_MALF', 'URI only + malformed message ID'),
	('URI_OPTOUT_3LD', 'Opt-out URI, suspicious hostname'),
	('URI_OPTOUT_USME', 'Opt-out URI, unusual TLD'),
	('URI_PHISH', 'Phishing using web form'),
	('URI_SIMURL_BLOCKED', 'Message contains a simurl URL that has been disabled due to abuse'),
	('URI_TINYURL_BLOCKED', 'Message contains a tinyurl that has been disabled due to abuse'),
	('URI_TRUNCATED', 'Message contained a URI which was truncated'),
	('URI_TRY_3LD', '"Try it" URI, suspicious hostname'),
	('URI_TRY_USME', '"Try it" URI, unusual TLD'),
	('URI_UNSUBSCRIBE', 'URI contains suspicious unsubscribe link'),
	('URI_WPADMIN', 'WordPress login/admin URI, possible phishing'),
	('URI_WP_DIRINDEX', 'URI for compromised WordPress site, possible malware'),
	('URI_WP_HACKED_2', 'URI for compromised WordPress site, possible malware'),
	('URI_WP_HACKED', 'URI for compromised WordPress site, possible malware'),
	('US_DOLLARS_3', 'Mentions millions of $ ($NN,NNN,NNN.NN)'),
	('USER_IN_ALL_SPAM_TO', 'User is listed in \'all_spam_to\''),
	('USER_IN_BLACKLIST', 'From: address is in the user\'s black-list'),
	('USER_IN_BLACKLIST_TO', 'User is listed in \'blacklist_to\''),
	('USER_IN_DEF_DKIM_WL', 'From: address is in the default DKIM white-list'),
	('USER_IN_DEF_SPF_WL', 'From: address is in the default SPF white-list'),
	('USER_IN_DEF_WHITELIST', 'From: address is in the default white-list'),
	('USER_IN_DKIM_WHITELIST', 'From: address is in the user\'s DKIM whitelist'),
	('USER_IN_MORE_SPAM_TO', 'User is listed in \'more_spam_to\''),
	('USER_IN_SPF_WHITELIST', 'From: address is in the user\'s SPF whitelist'),
	('USER_IN_WHITELIST', 'From: address is in the user\'s white-list'),
	('USER_IN_WHITELIST_TO', 'User is listed in \'whitelist_to\''),
	('VANITY', 'Vanity or fake awards'),
	('VBOUNCE_MESSAGE', 'Virus-scanner bounce message'),
	('VIA_GAP_GRA', 'Attempts to disguise the word \'viagra\''),
	('__VIA_ML', 'Mail from a mailing list'),
	('__VIA_RESIGNER', 'Mail through a popular signing remailer'),
	('WEIRD_PORT', 'Uses non-standard port number for HTTP'),
	('WEIRD_QUOTING', 'Weird repeated double-quotation marks'),
	('WITH_LC_SMTP', 'Received line contains spam-sign (lowercase smtp)'),
	('X_IP', 'Message has X-IP header'),
	('X_MESSAGE_INFO', 'Bulk email fingerprint (X-Message-Info) found'),
	('XM_PHPMAILER_FORGED', 'Apparently forged header'),
	('XPRIO', 'Has X-Priority header'),
	('X_PRIORITY_CC', 'Cc: after X-Priority: (bulk email fingerprint)'),
	('YAHOO_DRS_REDIR', 'Has Yahoo Redirect URI'),
	('YAHOO_RD_REDIR', 'Has Yahoo Redirect URI'),
	('YOU_INHERIT', 'Discussing your inheritance'),
	('ZMIde_900SEARCHES', '900 search engines dot com'),
	('ZMIde_ADDRSALE1', 'deutschlandb2b broker SPAM, at least 3 hits'),
	('ZMIde_ADDRSALE2', 'deutschlandb2b broker SPAM, at least 6 hits'),
	('ZMIde_ADDRSALE3', 'deutschlandb2b broker SPAM, at least 8 hits'),
	('ZMIde_AdNet01', 'Mail passed through a known advertising net'),
	('ZMIde_AdNet02', 'Mail passed through a known advertising net'),
	('ZMIde_AdNet03', 'Mail passed through a known advertising net'),
	('ZMIde_AD_URI', 'ad banner network'),
	('ZMIde_AnswerThis01', 'AnswerThis: if you answer, they know your address 3+ hits'),
	('ZMIde_AnswerThis02', 'AnswerThis: if you answer, they know your address 5+ hits'),
	('ZMIde_AnswerThis03', 'AnswerThis: if you answer, they know your address 7+ hits'),
	('ZMIde_AUCTION1', 'privilegeauctions SPAM text, one hit'),
	('ZMIde_AUCTION2', 'privilegeauctions SPAM text, two hits'),
	('ZMIde_BankCash01', 'Some Bank, some peoples account money for you, 2+'),
	('ZMIde_BankCash02', 'Some Bank, some peoples account money for you, 4+'),
	('ZMIde_BankCash03', 'Some Bank, some peoples account money for you, 6+'),
	('ZMIde_BankCash04', 'Some Bank, some peoples account money for you, 8+'),
	('ZMIde_BESTWORLD', 'bestworld-news sending news SPAM'),
	('ZMIde_BIKINI1', 'bikini girl porn spam, at least 3 hits'),
	('ZMIde_BIKINI2', 'bikini girl porn spam, at least 5 hits'),
	('ZMIde_BIOMETDE1', 'biometric book SPAM, at least 2 DE hits'),
	('ZMIde_BORDELL1', 'teen porn, 3+ hits'),
	('ZMIde_BORDELL2', 'teen porn, 6+ hits'),
	('ZMIde_CALENDAR1', 'product ads via spam, one hit'),
	('ZMIde_CALENDAR2', 'product ads via spam, two hits'),
	('ZMIde_CALENDAR3', 'product ads via spam, three or more hits'),
	('ZMIde_CAR01', 'CAR 3+ hits'),
	('ZMIde_CAR02', 'CAR 5+ hits'),
	('ZMIde_CAR03', 'CAR 7+ hits'),
	('ZMIde_Casino01', 'Earn money, Casino, 1+ hits'),
	('ZMIde_Casino02', 'Earn money, Casino, 2+ hits'),
	('ZMIde_Casino03', 'Earn money, Casino, 3+ hits'),
	('ZMIde_Casino04', 'Earn money, Casino, 7+ hits'),
	('ZMIde_CATALLURI', 'catall spam'),
	('ZMIde_CHEAPSMOKEURI', 'sell cigarettes'),
	('ZMIde_Coach1', 'Coaching by millonaire or software, 4+'),
	('ZMIde_Coach2', 'Coaching by millonaire or software, 6+'),
	('ZMIde_Coach3', 'Coaching by millonaire or software, 9+'),
	('ZMIde_COARTURI', 'co-art SPAM URI'),
	('ZMIde_DATING1', 'Dating or partner search site, 2 or more hits'),
	('ZMIde_DATING2', 'Dating or partner search site, 4 or more hits'),
	('ZMIde_DATING3', 'Dating or partner search site, 6 or more hits'),
	('ZMIde_DATINGURI1', 'dating site sending spam'),
	('ZMIde_DCImail01', 'DCImail 2+ hits'),
	('ZMIde_DCImail02', 'DCImail 3+ hits'),
	('ZMIde_DCImail03', 'DCImail 5+ hits'),
	('ZMIde_DRUGS1', 'Medizin, Creme, Lotion, 1+ hits'),
	('ZMIde_DRUGS2', 'Medizin, Creme, Lotion, 1+ hits'),
	('ZMIde_DRUGS3', 'Medizin, Creme, Lotion, 1+ hits'),
	('ZMIde_EBAYJOBSURI', 'guys help ebay transfers'),
	('ZMIde_Effektblock01', 'Effektblock 2+ hits'),
	('ZMIde_Effektblock02', 'Effektblock 3+ hits'),
	('ZMIde_Effektblock03', 'Effektblock 5+ hits'),
	('ZMIde_EMAIL_BULLDER', 'Try to look like careerbuilder.net, but isn\'t'),
	('ZMIde_EMAIL_HGS', 'Holos Global System Programm domain'),
	('ZMIde_Erektion1', 'Erektions Probleme Spam'),
	('ZMIde_Erektion2', 'Erektions Probleme Spam'),
	('ZMIde_EUROSELECTURI', 'euroselect.org harvesting addresses, sending SPAM'),
	('ZMIde_Exchangemoney1', 'Exchangemoney spam'),
	('ZMIde_Exchangemoney2', 'Exchangemoney spam'),
	('ZMIde_Exchangemoney3', 'Exchangemoney spam'),
	('ZMIde_ExclPack1', 'TheExclusivePackage, MailingTonic SPAM, 3+ hits'),
	('ZMIde_ExclPack2', 'TheExclusivePackage, MailingTonic SPAM, 6+ hits'),
	('ZMIde_ExclPack3', 'TheExclusivePackage, MailingTonic SPAM, 8+ hits'),
	('ZMIde_ExclPackSelf', 'TheExclusivePackage is a SPAM site'),
	('ZMIde_ExclPackSure', 'TheExclusivePackage spam, 100% sure'),
	('ZMIde_ExclPromo1', 'ExclPromo spam. Receive welcome, play games...'),
	('ZMIde_ExclPromo2', 'ExclPromo spam. Receive welcome, play games...'),
	('ZMIde_ExclPromo3', 'ExclPromo spam. Receive welcome, play games...'),
	('ZMIde_FASTMAIL', 'Home of FastMailing SPAMsoftware'),
	('ZMIde_Flirt1', 'Ad for flirt site, 2+ hits'),
	('ZMIde_Flirt2', 'Ad for flirt site, 4+ hits'),
	('ZMIde_Flirt3', 'Ad for flirt site, 5+ hits'),
	('ZMIde_FlirtSURE', 'Sure Flirt SPAM'),
	('ZMIde_FREEWORLDCCURI', 'sex stuff'),
	('ZMIde_GENERICPORN1', 'generic porn, at least 3 hits'),
	('ZMIde_GENERICPORN2', 'generic porn, at least 6 hits'),
	('ZMIde_GENERICPORN3', 'generic porn, at least 8 hits'),
	('ZMIde_GRATISLEUDERURI', 'free stuff SPAM'),
	('ZMIde_HDROUTLOOK', 'outlook extensions domain'),
	('ZMIde_Hund1', 'lies about dogs'),
	('ZMIde_Hund2', 'lies about dogs'),
	('ZMIde_ibox1', 'cannot unsubscribe from ibox newsletter, 2+ hits'),
	('ZMIde_ibox2', 'cannot unsubscribe from ibox newsletter, 4+ hits'),
	('ZMIde_ITSINTURI', 'itsystems.de int'),
	('ZMIde_ITSYSDEURI', 'itsystems.de 650k verbraucherkontakte'),
	('ZMIde_JHoeller01', 'JHoeller 2+ hits'),
	('ZMIde_JHoeller02', 'JHoeller 3+ hits'),
	('ZMIde_JHoeller03', 'JHoeller 5+ hits'),
	('ZMIde_JOBEARN1', 'earn more money, 2+ hits'),
	('ZMIde_JOBEARN2', 'earn more money, 3+ hits'),
	('ZMIde_JOBEARN3', 'earn more money, 4+ hits'),
	('ZMIde_JOBHAVE1', 'you must have something, one hit'),
	('ZMIde_JOBHAVE2', 'you must have something, more than one hits'),
	('ZMIde_JOBKNOWLOT', 'several things to know found'),
	('ZMIde_JOBKNOW', 'you must know something or nothing'),
	('ZMIde_JOBLEGAL', 'legal job'),
	('ZMIde_JOBMAKE', 'you must make something'),
	('ZMIde_JOBTIME', 'you must have time to work'),
	('ZMIde_JOBTRANSFER', 'you must make money transfers'),
	('ZMIde_JOINWINURI', 'spam domain'),
	('ZMIde_KINGTELURI', 'kingtel telephone SPAM'),
	('ZMIde_KrankenKassa01', 'KrankenKassa 2+ hits'),
	('ZMIde_KrankenKassa02', 'KrankenKassa 3+ hits'),
	('ZMIde_KrankenKassa03', 'KrankenKassa 5+ hits'),
	('ZMIde_Kredit01', 'Kredit 2+ hits'),
	('ZMIde_Kredit02', 'Kredit 3+ hits'),
	('ZMIde_Kredit03', 'Kredit 4+ hits'),
	('ZMIde_Kreditkarte1', 'Kreditkarte spam'),
	('ZMIde_Kreditkarte2', 'Kreditkarte spam'),
	('ZMIde_Kreditkarte3', 'Kreditkarte spam'),
	('ZMIde_LINKEXCHG1', 'link exchange offer for higher google rank #1'),
	('ZMIde_LINKEXCHG2', 'link exchange offer for higher google rank #2'),
	('ZMIde_LINKEXCHG3', 'link exchange offer for higher google rank #3'),
	('ZMIde_LIVESEX1', 'teen porn, at least 3 hits'),
	('ZMIde_LIVESEX2', 'teen porn, at least 6 hits'),
	('ZMIde_LIVESEX3', 'teen porn, at least 8 hits'),
	('ZMIde_LOGOSHAKURI', 'logoshaker german pages'),
	('ZMIde_LOTSACASHINETURI', '400.000â‚¬ in 7 months promised'),
	('ZMIde_LOTTERY1', 'lottery fake, 1+ hit'),
	('ZMIde_LOTTERY2', 'lottery fake, 2+ hits'),
	('ZMIde_LOTTERY3', 'lottery fake, 4+ hits'),
	('ZMIde_LOTTERY4', 'lottery fake, 6+ hits'),
	('ZMIde_LOTTERYHARD1', 'lottery fake, 1+ hits'),
	('ZMIde_LOTTERYHARD2', 'lottery fake, 2+ hits'),
	('ZMIde_LOTTERYHARD3', 'lottery fake, 3+ hits'),
	('ZMIde_LOTTERYHARD4', 'lottery fake, known subject and attachment'),
	('ZMIde_LOTTERY_OL', 'lottery fake sent via Outlook Express'),
	('ZMIde_Mahnung1', 'Mahnung spam'),
	('ZMIde_Mahnung2', 'Mahnung spam'),
	('ZMIde_Mahnung3', 'Mahnung spam'),
	('ZMIde_Marketing1', 'SPAM addresses for sale, 3+ hits'),
	('ZMIde_Marketing2', 'SPAM addresses for sale, 5+ hits'),
	('ZMIde_Marketing3', 'SPAM addresses for sale, 7+ hits'),
	('ZMIde_MILFHUNTERURI1', 'milfhunter sexual stuff'),
	('ZMIde_NotOrdered1', 'something you did not order'),
	('ZMIde_NotOrdered2', 'something you did not order'),
	('ZMIde_ONLGAME1', 'play and win online games spam, at least 3 hits'),
	('ZMIde_ONLGAME2', 'play and win online games spam, at least 5 hits'),
	('ZMIde_OnlineAds01', 'OnlineAds 2+ hits'),
	('ZMIde_OnlineAds02', 'OnlineAds 3+ hits'),
	('ZMIde_OnlineAds03', 'OnlineAds 5+ hits'),
	('ZMIde_OPENJOB', 'open jobs'),
	('ZMIde_OutlookExpress', 'Outlook Express should not be used anymore'),
	('ZMIde_Paketdienst1', 'Paketdienst spam'),
	('ZMIde_Paketdienst2', 'Paketdienst spam'),
	('ZMIde_Paketdienst3', 'Paketdienst spam'),
	('ZMIde_PayPal_OL', 'Paypal fishing sent via Outlook Express'),
	('ZMIde_PayPal_SPF', 'Paypal SPF Record unsafe'),
	('ZMIde_Pharmacy01', 'Pharmacy Spam, 2+ hits'),
	('ZMIde_Pharmacy02', 'Pharmacy Spam, 4+ hits'),
	('ZMIde_Pharmacy03', 'Pharmacy Spam, 5+ hits'),
	('ZMIde_PharmacyHIGH01', 'Pharmacy Spam mispelled words, 2+ hits'),
	('ZMIde_PharmacyHIGH02', 'Pharmacy Spam mispelled words'),
	('ZMIde_PharmacyPURE', 'Absolutely sure pharmacy Spam'),
	('ZMIde_Pharmacysub01', 'Pharmacy Spam discrete shopping info, 2+ hits'),
	('ZMIde_Pharmacysub02', 'Pharmacy Spam discrete shopping info, 3+ hits'),
	('ZMIde_Pharmacysub03', 'Pharmacy Spam discrete shopping info, 4+ hits'),
	('ZMIde_PLRebell1', 'PLRebell sending UCE'),
	('ZMIde_PORNCRACK1', 'porncrack sex SPAM now in german, 3+ hits'),
	('ZMIde_PORNCRACK2', 'porncrack sex SPAM now in german, 5+ hits'),
	('ZMIde_PORNCRACK3', 'porncrack sex SPAM now in german, 7+ hits'),
	('ZMIde_PORNSITE1', 'sexual SPAM'),
	('ZMIde_PPTREFF1', 'parking lot meeting porn spam, at least 3 hits'),
	('ZMIde_PPTREFF2', 'parking lot meeting porn spam, at least 6 hits'),
	('ZMIde_PPTREFF3', 'parking lot meeting porn spam, at least 8 hits'),
	('ZMIde_PRIVAMATEURURI', 'private amateur sex ad'),
	('ZMIde_PRIVAUCTURI', 'auctions spam'),
	('ZMIde_PRIVGEIL1', 'private amateur porn, 3+ hits'),
	('ZMIde_PRIVGEIL2', 'private amateur porn, 5+ hits'),
	('ZMIde_PRIVGEIL3', 'private amateur porn, 7+ hits'),
	('ZMIde_PRODUCTAD1', 'product ads via spam, one hit'),
	('ZMIde_PRODUCTAD2', 'product ads via spam, two hits'),
	('ZMIde_PRODUCTAD3', 'product ads via spam, three or more hits'),
	('ZMIde_RSSFeed01', 'RSSFeed 2+ hits'),
	('ZMIde_RSSFeed02', 'RSSFeed 3+ hits'),
	('ZMIde_RSSFeed03', 'RSSFeed 5+ hits'),
	('ZMIde_SAFESALEURI', 'spam company'),
	('ZMIde_SALE1', 'sell marketing, at least 3 hits'),
	('ZMIde_SALE2', 'sell marketing, at least 6 hits'),
	('ZMIde_SALE3', 'sell marketing, at least 8 hits'),
	('ZMIde_SAMEWOMEN1', 'findest du die Frauen.. SPAM, at least 3 hits'),
	('ZMIde_SAMEWOMEN2', 'findest du die Frauen.. SPAM, at least 6 hits'),
	('ZMIde_SAMEWOMEN3', 'findest du die Frauen.. SPAM, at least 8 hits'),
	('ZMIde_SatTV1', 'SatTV spam. Better use onlinetvrecorder.com, they don\'t spam'),
	('ZMIde_SatTV2', 'SatTV spam. Better use onlinetvrecorder.com, they don\'t spam'),
	('ZMIde_SatTV3', 'SatTV spam. Better use onlinetvrecorder.com, they don\'t spam'),
	('ZMIde_SOCFARM1', 'social pharming, they want your personal data!'),
	('ZMIde_SpainSpamPack1', 'SPAM addresses for sale, 4+ hits'),
	('ZMIde_SpainSpamPack2', 'SPAM addresses for sale, 6+ hits'),
	('ZMIde_SpainSpamPack3', 'SPAM addresses for sale, 8+ hits'),
	('ZMIde_STEPMGMNTURI', 'we want you'),
	('ZMIde_STOCKBLOCK1', '3+ typical signs of stock spam'),
	('ZMIde_STOCKBLOCK2', '5+ typical signs of stock spam'),
	('ZMIde_STOCKBLOCK3', '7+ typical signs of stock spam'),
	('ZMIde_STOCKHEADER1', 'from stock trading tip company, 1+ hits'),
	('ZMIde_STOCKHEADER2', 'from stock trading tip company, 3+ hits'),
	('ZMIde_STOCKOFFER1', 'stock trading tip, 3+ hits'),
	('ZMIde_STOCKOFFER2', 'stock trading tip, 5+ hits'),
	('ZMIde_STOCKOFFER3', 'stock trading tip, 7+ hits'),
	('ZMIde_STREET1', 'outdoor, on the street, porn spam, at least 3 hits'),
	('ZMIde_STREET2', 'outdoor, on the street, porn spam, at least 6 hits'),
	('ZMIde_STREET3', 'outdoor, on the street, porn spam, at least 8 hits'),
	('ZMIde_SUBBIG', 'subject suggesting business'),
	('ZMIde_SuperShop01', 'SuperShop 2+ hits'),
	('ZMIde_SuperShop02', 'SuperShop 3+ hits'),
	('ZMIde_SuperShop03', 'SuperShop 4+ hits'),
	('ZMIde_SuperShopURI', 'super shop advertising, cheap products'),
	('ZMIde_Taxi1', 'Makler Newsletter ohne Wunsch'),
	('ZMIde_Taxi2', 'Makler Newsletter ohne Wunsch'),
	('ZMIde_TEENPORN1', 'teen porn, at least 3 hits'),
	('ZMIde_TEENPORN2', 'teen porn, at least 6 hits'),
	('ZMIde_TEENPORN3', 'teen porn, at least 8 hits'),
	('ZMIde_Tombola01', 'Tombola 3+ hits'),
	('ZMIde_Tombola02', 'Tombola 4+ hits'),
	('ZMIde_Tombola03', 'Tombola 5+ hits'),
	('ZMIde_TRAUMCHATURI', 'sexual chat'),
	('ZMIde_Travel01', 'Travel 2+ hits'),
	('ZMIde_Travel02', 'Travel 4+ hits'),
	('ZMIde_Travel03', 'Travel 5+ hits'),
	('ZMIde_TRAVELs1', 'singletravels SPAM mail, one hit'),
	('ZMIde_TRAVELs2', 'singletravels SPAM mail, two hits'),
	('ZMIde_TRAVELs3', 'singletravels SPAM mail, three or more hits'),
	('ZMIde_URIAPCASHJOB', 'applecash SPAM job offer'),
	('ZMIde_URIBBCLUB1', 'baron bernando spam club'),
	('ZMIde_URIBBCLUB2', 'baron bernando spam club'),
	('ZMIde_URIBBCLUB3', 'baron bernando spam club'),
	('ZMIde_URIBBCLUB4', 'baron bernando spam club'),
	('ZMIde_URIBBCLUB5', 'baron bernando spam club'),
	('ZMIde_URICASHDAUMEN', 'spammers URI'),
	('ZMIde_URICHEAPROLEX', 'cheap rolex'),
	('ZMIde_URICREDITCHK', 'business-check.com hidden via nmph.net'),
	('ZMIde_URIDOWNLD1', 'download illegal software'),
	('ZMIde_URIDRUG1', 'online drugs advertised in german spam'),
	('ZMIde_URIDRUG2', 'online drugs advertised in german spam'),
	('ZMIde_URIDRUG3', 'online drugs advertised in german spam'),
	('ZMIde_URIEXCLPACK', 'TheExclusivePackage, new domain'),
	('ZMIde_URIFREEHANDY', 'free mobile phone URI'),
	('ZMIde_URIgewinnspiel', 'they send spam half a year later, claim win'),
	('ZMIde_URIHOLD1', 'holidays offer spammer'),
	('ZMIde_URIibox', 'i-box send spam, you cannot unsubscribe'),
	('ZMIde_URIIEMARK', 'marketing spam'),
	('ZMIde_URIimmer', 'Einfach immer Spam Site'),
	('ZMIde_URIjaevip', 'Party Spam Site'),
	('ZMIde_URIJOB1', 'job offerings'),
	('ZMIde_URIJOB2', 'job offerings'),
	('ZMIde_URIJOB3', 'job offerings'),
	('ZMIde_URIJOB4', 'job offerings'),
	('ZMIde_URIJOB5', 'job offerings'),
	('ZMIde_URI_Krankenkassa', 'Krankenkassa privat versichern'),
	('ZMIde_URILAWGR', 'lawyer from greece for germany'),
	('ZMIde_URILINKEXCHG1', 'link exchange offered in SPAM'),
	('ZMIde_URILottoIT', 'claims Italian lottery'),
	('ZMIde_URImediashop', 'started Jan.2009 to not-subscribed addresses'),
	('ZMIde_URINETAD', 'stupid ad platform'),
	('ZMIde_URINETBIZZ', 'netbizz sales site spam'),
	('ZMIde_URINOSMOKE', 'anti smoking law, used for spamming. what a shame.'),
	('ZMIde_URINUMARKET', 'ebay reseller for what?'),
	('ZMIde_URIONLQUIZ', 'onlinequiz.de'),
	('ZMIde_URIOUTLOOKGOOD', 'outlook extensions crap'),
	('ZMIde_URIPILLE', 'SPAM domain pills, with bayes poison'),
	('ZMIde_URIPORN10', 'porn site'),
	('ZMIde_URIPORN11', 'porn site'),
	('ZMIde_URIPORN12', 'hitz24.info porn site'),
	('ZMIde_URIPORN13', 'inter-news porn list spam server'),
	('ZMIde_URIPORN1', 'porn site assivo.com'),
	('ZMIde_URIPORN2', 'benorto.com used for pornos'),
	('ZMIde_URIPORN3', 'porn site'),
	('ZMIde_URIPORN4', 'porn site'),
	('ZMIde_URIPORN5', 'site for flirting and porn films and xmas cards'),
	('ZMIde_URIPORN6', 'porn site'),
	('ZMIde_URIPORN7', 'porn site'),
	('ZMIde_URIPORN8', 'porn site'),
	('ZMIde_URIPORN9', 'porn site'),
	('ZMIde_URIPROTOTO', 'prototo.com advertised by SPAM'),
	('ZMIde_URIROLREPLI', 'rolex replica'),
	('ZMIde_URISA2', 'SA2 better handy network SPAM URI'),
	('ZMIde_URISCHUFAEML1', 'SPAM from Schufa credit, remove me address'),
	('ZMIde_URISOCFARM1', 'social pharming, give them NO personal data!'),
	('ZMIde_URISpainSpamPack', 'spanisch Spam Address Sales'),
	('ZMIde_URIspammer', 'they claim double-opt-in to a spamtrap :-)'),
	('ZMIde_URISPARKAFAKE', 'phishing URI for sparkasse.at'),
	('ZMIde_URIstocks', 'domain listed in stock spam from direktmail24\\.com'),
	('ZMIde_URISTP', 'Sent SPAM with football WM 2006 plan'),
	('ZMIde_URISTUPIDSITE1', 'stupid site sending random text with a link'),
	('ZMIde_URISUPERM', 'supermailer spam software'),
	('ZMIde_URITravel1', 'singletravels.de spammers'),
	('ZMIde_URITravel2', 'you won a trip spam'),
	('ZMIde_URIVALORADE', 'SPAM advertising stock business'),
	('ZMIde_URIvitalis', 'claim win, turkey bus travel with sales promotions'),
	('ZMIde_URIWEBBOX', 'websitebox spam'),
	('ZMIde_URIWITNET', 'call themselves it-network and are SPAMMERS'),
	('ZMIde_URIWM2006FLAG', 'flag seller for football WM 20006'),
	('ZMIde_URIWRLDCAR', 'career network mail address'),
	('ZMIde_URIWRLDJIM', 'career network SPAM for sure'),
	('ZMIde_Versicherung1', 'Makler Newsletter ohne Wunsch'),
	('ZMIde_Versicherung2', 'Makler Newsletter ohne Wunsch'),
	('ZMIde_VIPPACK', 'TheExclusivePackage.com'),
	('ZMIde_WeightRANK2', 'Weight SPAM, they want your KILOs, >=2'),
	('ZMIde_WeightRANK3', 'Weight SPAM, they want your KILOs, >=3'),
	('ZMIde_WeightRANK4', 'Weight SPAM, they want your KILOs, >=4'),
	('ZMIde_Werbung01', 'Werbung 3+ hits'),
	('ZMIde_Werbung02', 'Werbung 5+ hits'),
	('ZMIde_Werbung03', 'Werbung 3+ hits and alcantara.de'),
	('ZMIde_Werbung', 'alcantara.de sends ads'),
	('ZMIde_WISHCOCK', 'cock enlargement site'),
	('ZMIde_XMASSMOKEURI', 'xmas stuff mentioned in smoking SPAM'),
	('ZMIfish_BACA1', 'BA-CA phishing attack'),
	('ZMIfish_BACA2', 'BA-CA phishing attack'),
	('ZMIfish_BANKMAIL1', 'postbank SPAM or PHISHING, they want your money1'),
	('ZMIfish_BANKMAIL2', 'postbank SPAM or PHISHING, they want your money2'),
	('ZMIfish_BANKMAIL3', 'postbank SPAM or PHISHING, they want your money3'),
	('ZMIfish_BANKMAIL4', 'bank SPAM or PHISHING, they want your money4'),
	('ZMIfish_BANKURLinPATH', 'Website of bank contained in the middle of a link'),
	('ZMIfish_ForgedBill', 'Forged bill, not from this company'),
	('ZMIfish_ISP1', 'Gefaelschte Warnungen und Rechnungen'),
	('ZMIfish_ISP2', 'Gefaelschte Warnungen und Rechnungen'),
	('ZMIfish_ISP3', 'Gefaelschte Warnungen und Rechnungen'),
	('ZMIfish_NETBANKING_F2', 'Phishing from nelbanking\\.com'),
	('ZMIfish_NETBANKING_F3', 'Phishing for netbanking.at'),
	('ZMIfish_NETBANKING_F', 'Phishing from nelbanking\\.com'),
	('ZMIfish_NETBANKING_S', 'Phishing from nelbanking\\.com'),
	('ZMIfish_PayPal01', 'PayPal Phishing'),
	('ZMIfish_PayPal02', 'PayPal Phishing'),
	('ZMIfish_PayPal03', 'PayPal Phishing'),
	('ZMIfish_POSTBANKURI1', 'phishing URI for postbank.de, they want your money #1'),
	('ZMIfish_POSTBANKURI2', 'phishing URI for postbank.de, they want your money #2'),
	('ZMIfish_POSTBANKURI3', 'possible new phishing URI for postbank.de'),
	('ZMIRASSISMUS_MAILS_1', 'Rassistische E-Mails, Titel'),
	('ZMIRASSISMUS_MAILS_2', 'Rassistische E-Mails, Inhalt + Titel'),
	('ZMIRASSISMUS_MAILS_3', 'Rassistische E-Mails, Inhalt + Titel'),
	('ZMISOBER_P_SPAM', 'Rassistische Mail Sober-P'),
	('ZMIvirSobY_SUB31', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB32', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB33', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB34', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB35', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB36', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB37', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB38', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB39', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB40', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB43', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB44', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB46', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB47', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB48', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB49', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB50', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB51', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB52', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB53', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB54', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB55', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB56', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB57', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB58', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB59', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB60', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB61', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB62', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB63', 'SPAM from Sober-Y-Virus'),
	('ZMIvirSobY_SUB64', 'SPAM from Sober-Y-Virus');
/*!40000 ALTER TABLE `sa_rules` ENABLE KEYS */;


-- Dumping structure for table mailscanner.sa_users
DROP TABLE IF EXISTS `sa_users`;
CREATE TABLE IF NOT EXISTS `sa_users` (
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` enum('A','D','U','R','H') COLLATE utf8_unicode_ci DEFAULT NULL,
  `quarantine_report` tinyint(1) DEFAULT '0',
  `spamscore` tinyint(4) DEFAULT '0',
  `highspamscore` tinyint(4) DEFAULT '0',
  `noscan` tinyint(1) DEFAULT '0',
  `quarantine_rcpt` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_id` int(12) DEFAULT '9999',
  `clean_store` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `clean_deliver` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `clean_strip_html` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `clean_header` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `clean_header_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT 'X-Spam-Status: No',
  `spam_store` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `spam_deliver` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `spam_modify` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `spam_modify_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT '{Spam?}',
  `spam_notify` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_delete` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_bounce` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_forward` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_forward_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spam_strip_html` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_attachement` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `spam_header` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `spam_header_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT 'X-Spam-Status: Yes',
  `high_spam_store` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `high_spam_deliver` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_notify` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_delete` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_bounce` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_forward` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_forward_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `high_spam_strip_html` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  `high_spam_attachement` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_header` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `high_spam_header_txt` varchar(40) COLLATE utf8_unicode_ci DEFAULT 'X-Spam-Status: Yes',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sa_users
-- ----------------------------
INSERT INTO `sa_users` VALUES ('1', '', 'Admin', '', 'A', '0', '5', '11', '0', null, '9999', 'y', 'y', 'n', 'y', 'X-Spam-Status: No', 'y', 'y', 'y', '{Spam?}', 'n', 'n', 'n', 'n', null, 'n', 'n', 'y', 'X-Spam-Status: Yes', 'y', 'n', 'n', 'n', 'n', '', null, 'n', 'n', 'n', 'X-Spam-Status: Yes');



-- Dumping structure for table mailscanner.sender_bcc_domain
DROP TABLE IF EXISTS `sender_bcc_domain`;
CREATE TABLE IF NOT EXISTS `sender_bcc_domain` (
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sender_bcc_user
DROP TABLE IF EXISTS `sender_bcc_user`;
CREATE TABLE IF NOT EXISTS `sender_bcc_user` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.slog
DROP TABLE IF EXISTS `slog`;
CREATE TABLE IF NOT EXISTS `slog` (
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stype` text COLLATE utf8_unicode_ci,
  `hour` text COLLATE utf8_unicode_ci,
  `sid` text COLLATE utf8_unicode_ci,
  `sender` text COLLATE utf8_unicode_ci,
  `size` text COLLATE utf8_unicode_ci,
  `nrcpts` text COLLATE utf8_unicode_ci,
  `relay` text COLLATE utf8_unicode_ci,
  `rule` text COLLATE utf8_unicode_ci,
  `arg1` text COLLATE utf8_unicode_ci,
  `status` text COLLATE utf8_unicode_ci,
  `sourceid` text COLLATE utf8_unicode_ci,
  `rcpt` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `to_address` text COLLATE utf8_unicode_ci,
  `spam` text COLLATE utf8_unicode_ci,
  `action` text COLLATE utf8_unicode_ci,
  `file` text COLLATE utf8_unicode_ci,
  `virus` text COLLATE utf8_unicode_ci,
  `error` text COLLATE utf8_unicode_ci,
  `mech` text COLLATE utf8_unicode_ci,
  `type` text COLLATE utf8_unicode_ci,
  `score` text COLLATE utf8_unicode_ci,
  `sender_ip` text COLLATE utf8_unicode_ci,
  `cache` text COLLATE utf8_unicode_ci,
  `autolearn` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.slogconfig
DROP TABLE IF EXISTS `slogconfig`;
CREATE TABLE IF NOT EXISTS `slogconfig` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.spamscores
DROP TABLE IF EXISTS `spamscores`;
CREATE TABLE IF NOT EXISTS `spamscores` (
  `user` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lowspamscore` decimal(10,0) NOT NULL DEFAULT '0',
  `highspamscore` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_addon_blacklist
DROP TABLE IF EXISTS `sys_addon_blacklist`;
CREATE TABLE IF NOT EXISTS `sys_addon_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `relay_ip` text COLLATE utf8_unicode_ci,
  `blocklist` text COLLATE utf8_unicode_ci,
  `info` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_addon_config
DROP TABLE IF EXISTS `sys_addon_config`;
CREATE TABLE IF NOT EXISTS `sys_addon_config` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_addon_lookup
DROP TABLE IF EXISTS `sys_addon_lookup`;
CREATE TABLE IF NOT EXISTS `sys_addon_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `relay` text COLLATE utf8_unicode_ci,
  `relay_ip` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `tld` text COLLATE utf8_unicode_ci,
  `created` text COLLATE utf8_unicode_ci,
  `action` text COLLATE utf8_unicode_ci,
  `to_address` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_api_auth_token
DROP TABLE IF EXISTS `sys_api_auth_token`;
CREATE TABLE IF NOT EXISTS `sys_api_auth_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_UNIQUE` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_api_permit
DROP TABLE IF EXISTS `sys_api_permit`;
CREATE TABLE IF NOT EXISTS `sys_api_permit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_blacklist_lookup
DROP TABLE IF EXISTS `sys_blacklist_lookup`;
CREATE TABLE IF NOT EXISTS `sys_blacklist_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_domain` text COLLATE utf8_unicode_ci,
  `to_domain` text COLLATE utf8_unicode_ci,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_blacklist_lookup_country
DROP TABLE IF EXISTS `sys_blacklist_lookup_country`;
CREATE TABLE IF NOT EXISTS `sys_blacklist_lookup_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_country` text COLLATE utf8_unicode_ci,
  `from_domain` text COLLATE utf8_unicode_ci,
  `to_domain` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_box_firewall
DROP TABLE IF EXISTS `sys_box_firewall`;
CREATE TABLE IF NOT EXISTS `sys_box_firewall` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `num` int(11) unsigned NOT NULL DEFAULT '0',
  `traffic` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `pkts` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `bytes` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `target` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `prot` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `opt` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tin` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tout` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `dest_entry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dest_port` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_box_fw_ports
DROP TABLE IF EXISTS `sys_box_fw_ports`;
CREATE TABLE IF NOT EXISTS `sys_box_fw_ports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prot` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `var` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `var2` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_box_fw_rule
DROP TABLE IF EXISTS `sys_box_fw_rule`;
CREATE TABLE IF NOT EXISTS `sys_box_fw_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `traffic` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `ruleset` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE IF NOT EXISTS `sys_config` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('quarantine_report_days', '7');
INSERT INTO `sys_config` VALUES ('quarantine_report_hosturl', 'http://start.defaultsetup.com');
INSERT INTO `sys_config` VALUES ('quarantine_days_to_keep', '90');
INSERT INTO `sys_config` VALUES ('quarantine_report_from_name', 'EMFABox');
INSERT INTO `sys_config` VALUES ('quarantine_from_addr', 'no-reply@defaultsetup.com');
INSERT INTO `sys_config` VALUES ('quarantine_report_subject', 'Message Quarantine Report');
INSERT INTO `sys_config` VALUES ('emfa_home', '/var/www/html/master');
INSERT INTO `sys_config` VALUES ('quarantine_mail_host', '127.0.0.1');
INSERT INTO `sys_config` VALUES ('fromto_maxlen', '50');
INSERT INTO `sys_config` VALUES ('subject_maxlen', '0');
INSERT INTO `sys_config` VALUES ('time_zone', 'America/New_York');
INSERT INTO `sys_config` VALUES ('date_format', '%d/%m/%y');
INSERT INTO `sys_config` VALUES ('time_format', '%H:%i:%s');
INSERT INTO `sys_config` VALUES ('record_days_to_keep', '90');
INSERT INTO `sys_config` VALUES ('audit_days_to_keep', '90');
INSERT INTO `sys_config` VALUES ('hide_high_spam', 'false');
INSERT INTO `sys_config` VALUES ('html_home', '/var/www/html');
INSERT INTO `sys_config` VALUES ('quarantine_subject', 'Message released from quarantine');
INSERT INTO `sys_config` VALUES ('quarantine_msg_body', 'Please find the original message that was quarantined attached to this mail.\n\nRegards,\nPostmaster');
INSERT INTO `sys_config` VALUES ('MailScanner_conf_file', '/etc/MailScanner/MailScanner.conf');
INSERT INTO `sys_config` VALUES ('ms_config_dir', '/etc/MailScanner');
INSERT INTO `sys_config` VALUES ('cbpolicyd', 'YES');
INSERT INTO `sys_config` VALUES ('greylisting', 'YES');   
INSERT INTO `sys_config` VALUES ('opendmarc', 'YES');
INSERT INTO `sys_config` VALUES ('avira_key', '8V37-9DEL');
INSERT INTO `sys_config` VALUES ('cluster_key', 'KE3Q-4U25');
INSERT INTO `sys_config` VALUES ('datafeed_key', 'NQ4C-9YSZ');
INSERT INTO `sys_config` VALUES ('malwarepatrol_receipt_code', 'YOUR-RECEIPT-NUMBER');
INSERT INTO `sys_config` VALUES ('securiteinfo_authorisation_signature', 'YOUR-SIGNATURE-NUMBER');
INSERT INTO `sys_config` VALUES ('blacklist_relay_country_score', '1.0');
INSERT INTO `sys_config` VALUES ('blacklist_relay_country_codes', 'XA,XB');
INSERT INTO `sys_config` VALUES ('whitelist_relay_country_score', '-1.0');
INSERT INTO `sys_config` VALUES ('whitelist_relay_country_codes', 'XA,XB');
INSERT INTO `sys_config` VALUES ('blacklist_source_country_score', '1.0');
INSERT INTO `sys_config` VALUES ('blacklist_source_country_codes', 'XA,XB');
INSERT INTO `sys_config` VALUES ('whitelist_source_country_score', '-1.0');
INSERT INTO `sys_config` VALUES ('whitelist_source_country_codes', 'XA,XB');


-- Dumping structure for table mailscanner.sys_config_global
DROP TABLE IF EXISTS `sys_config_global`;
CREATE TABLE IF NOT EXISTS `sys_config_global` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config_global
-- ----------------------------

INSERT INTO `sys_config_global` (`variable_name`, `value`) VALUES
	('blacklist_relay_country_score', '1.0'),
	('blacklist_relay_country_codes', 'XA,XB'),
	('whitelist_relay_country_score', '-1.0'),
	('whitelist_relay_country_codes', 'XA,XB'),
	('blacklist_source_country_score', '1.0'),
	('blacklist_source_country_codes', 'XA,XB'),
	('whitelist_source_country_score', '-1.0'),
	('whitelist_source_country_codes', 'XA,XB');



-- Dumping structure for table mailscanner.sys_config_policyd_spf
DROP TABLE IF EXISTS `sys_config_policyd_spf`;
CREATE TABLE IF NOT EXISTS `sys_config_policyd_spf` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_postfix
DROP TABLE IF EXISTS `sys_config_postfix`;
CREATE TABLE IF NOT EXISTS `sys_config_postfix` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_postfix_default
DROP TABLE IF EXISTS `sys_config_postfix_default`;
CREATE TABLE IF NOT EXISTS `sys_config_postfix_default` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_postfix_out
DROP TABLE IF EXISTS `sys_config_postfix_out`;
CREATE TABLE IF NOT EXISTS `sys_config_postfix_out` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_config_postfix_select
DROP TABLE IF EXISTS `sys_config_postfix_select`;
CREATE TABLE IF NOT EXISTS `sys_config_postfix_select` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config_postfix_select
-- ----------------------------
INSERT INTO `sys_config_postfix_select` VALUES ('host', 'host');
INSERT INTO `sys_config_postfix_select` VALUES ('subnet', 'subnet');



-- Dumping structure for table mailscanner.sys_config_sqlgrey
DROP TABLE IF EXISTS `sys_config_sqlgrey`;
CREATE TABLE IF NOT EXISTS `sys_config_sqlgrey` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_dns_forwarders
DROP TABLE IF EXISTS `sys_dns_forwarders`;
CREATE TABLE IF NOT EXISTS `sys_dns_forwarders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` text COLLATE utf8_unicode_ci NOT NULL,
  `ip1` text COLLATE utf8_unicode_ci NOT NULL,
  `ip2` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_dob_lookup
DROP TABLE IF EXISTS `sys_dob_lookup`;
CREATE TABLE IF NOT EXISTS `sys_dob_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `sid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `arg1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relaycountry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relay` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `to_address` (`to_address`),
  KEY `relay` (`relay`),
  KEY `arg1` (`arg1`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_emfa_usage
DROP TABLE IF EXISTS `sys_emfa_usage`;
CREATE TABLE IF NOT EXISTS `sys_emfa_usage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_unicode_ci,
  `domain` text COLLATE utf8_unicode_ci,
  `avira_opt` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  `alias` varchar(120) COLLATE utf8_unicode_ci DEFAULT 'NO',
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NO',
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`(50)),
  KEY `email` (`email`(200))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_fail2ban
DROP TABLE IF EXISTS `sys_fail2ban`;
CREATE TABLE IF NOT EXISTS `sys_fail2ban` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `service` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_filename_rules
DROP TABLE IF EXISTS `sys_filename_rules`;
CREATE TABLE IF NOT EXISTS `sys_filename_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_ext` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_filetype_rules
DROP TABLE IF EXISTS `sys_filetype_rules`;
CREATE TABLE IF NOT EXISTS `sys_filetype_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `allow` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_filetype_select
DROP TABLE IF EXISTS `sys_filetype_select`;
CREATE TABLE IF NOT EXISTS `sys_filetype_select` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.sys_filetype_select: 13 rows
DELETE FROM `sys_filetype_select`;
/*!40000 ALTER TABLE `sys_filetype_select` DISABLE KEYS */;
INSERT INTO `sys_filetype_select` (`variable_name`, `value`, `active`) VALUES
	('Archives', 'archive', 'y'),
	('Text', 'text', 'y'),
	('Postscript', 'postscript', 'y'),
	('Self Extract Archives', 'self-extract', 'y'),
	('Executables', 'executable', 'y'),
	('Programs', 'ELF', 'y'),
	('Registry', 'Registry', 'y'),
	('Mpeg Files', 'MPEG', 'y'),
	('AVI Files', 'AVI', 'y'),
	('MNG Movies', 'MNG', 'y'),
	('QuickTime', 'QuickTime', 'y'),
	('Windows Media', 'ASF', 'y'),
	('Autocad DWG', 'Autocad', 'y');
/*!40000 ALTER TABLE `sys_filetype_select` ENABLE KEYS */;


-- Dumping structure for table mailscanner.sys_gateway_usage
DROP TABLE IF EXISTS `sys_gateway_usage`;
CREATE TABLE IF NOT EXISTS `sys_gateway_usage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_unicode_ci,
  `domain` text COLLATE utf8_unicode_ci,
  `virus_opt` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`(50)),
  KEY `email` (`email`(200))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_gateway_warning
DROP TABLE IF EXISTS `sys_gateway_warning`;
CREATE TABLE IF NOT EXISTS `sys_gateway_warning` (
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reason` text COLLATE utf8_unicode_ci,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_hosts_file
DROP TABLE IF EXISTS `sys_hosts_file`;
CREATE TABLE IF NOT EXISTS `sys_hosts_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aliases` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_kernel_routes
DROP TABLE IF EXISTS `sys_kernel_routes`;
CREATE TABLE IF NOT EXISTS `sys_kernel_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gateway` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `genmask` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metric` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iface` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ldap_conf
DROP TABLE IF EXISTS `sys_ldap_conf`;
CREATE TABLE IF NOT EXISTS `sys_ldap_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_email_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc_2_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_base_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_email_base_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_bind_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_bind_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_account_suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_filter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_auth_search` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_attributes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_authrealm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_ad_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_exchange_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_port` int(11) NOT NULL DEFAULT '389',
  `ldap_proxyuser` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_proxypass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_proxy_addresses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_active` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `exchange_lookup` int(11) NOT NULL DEFAULT '0',
  `exchange_sync` int(11) NOT NULL DEFAULT '0',
  `sync_id` int(2) DEFAULT NULL,
  `sync_time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `auth_domain` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cronjob` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `purge_user` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  `ldaps` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ldap_cron_defaults
DROP TABLE IF EXISTS `sys_ldap_cron_defaults`;
CREATE TABLE IF NOT EXISTS `sys_ldap_cron_defaults` (
  `id` int(2) DEFAULT NULL,
  `value` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `option` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.sys_ldap_cron_defaults: 10 rows
DELETE FROM `sys_ldap_cron_defaults`;
/*!40000 ALTER TABLE `sys_ldap_cron_defaults` DISABLE KEYS */;
INSERT INTO `sys_ldap_cron_defaults` (`id`, `value`, `option`, `active`) VALUES
	(2, '*/15 * * * *', 'Every 15 minutes', 'n'),
	(3, '*/30 * * * *', 'Every 30 minutes', 'n'),
	(4, '*/45 * * * *', 'Every 45 minutes', 'n'),
	(5, '0 * * * *', 'Every hour', 'y'),
	(6, '0 */6 * * *', 'Every 6 hours', 'n'),
	(8, '0 0 * * *', 'Every day', 'n'),
	(9, '0 0 * * 0', 'Every week', 'n'),
	(10, '0 0 1 * *', 'Every month', 'n'),
	(1, '0', 'Disabled', 'y'),
	(7, '0 */12 * * *', 'Every 12 hours', 'n');
/*!40000 ALTER TABLE `sys_ldap_cron_defaults` ENABLE KEYS */;

-- Dumping structure for table mailscanner.sys_ldap_tmp
DROP TABLE IF EXISTS `sys_ldap_tmp`;
CREATE TABLE IF NOT EXISTS `sys_ldap_tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_account_suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mailscanner.sys_ldap_imp_dyn
DROP TABLE IF EXISTS `sys_ldap_imp_dyn`;
CREATE TABLE IF NOT EXISTS `sys_ldap_imp_dyn` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ldap_domain_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `real_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_functions` text COLLATE utf8_unicode_ci NOT NULL,
  `ldap_error_msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_error_reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_in_transport` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_mail_db` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_not_longer_in_mail_db` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_ignore` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.






-- Dumping structure for table mailscanner.sys_listing
DROP TABLE IF EXISTS `sys_listing`;
CREATE TABLE IF NOT EXISTS `sys_listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `msgid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_domain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipaddress` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('n','y') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_logs
DROP TABLE IF EXISTS `sys_logs`;
CREATE TABLE IF NOT EXISTS `sys_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `severity` int(11) DEFAULT NULL,
  `date` varchar(32) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `ipaddr` varchar(64) DEFAULT NULL,
  `command` varchar(128) DEFAULT '0',
  `details` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- ----------------------------
-- Table structure for `sys_log_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_log_config`;
CREATE TABLE `sys_log_config` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



-- Dumping structure for table mailscanner.sys_mail_firewall
DROP TABLE IF EXISTS `sys_mail_firewall`;
CREATE TABLE IF NOT EXISTS `sys_mail_firewall` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_perm_user` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_group` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_other` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `server_id` int(11) unsigned NOT NULL DEFAULT '0',
  `block_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `block_ip_mask` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tcp_port` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `udp_port` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_mcp_rules
DROP TABLE IF EXISTS `sys_mcp_rules`;
CREATE TABLE IF NOT EXISTS `sys_mcp_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` int(11) DEFAULT NULL,
  `rulename` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruledesc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruletype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pattern` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ms_rulessets
DROP TABLE IF EXISTS `sys_ms_rulessets`;
CREATE TABLE IF NOT EXISTS `sys_ms_rulessets` (
  `variable_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_ms_rulessets
DROP TABLE IF EXISTS `sys_ms_rulessets`;
CREATE TABLE IF NOT EXISTS `sys_ms_rulessets` (
  `variable_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.sys_ms_rulessets: 25 rows
DELETE FROM `sys_ms_rulessets`;
/*!40000 ALTER TABLE `sys_ms_rulessets` DISABLE KEYS */;
INSERT INTO `sys_ms_rulessets` (`variable_name`, `value`) VALUES
	('clean_store', 'store'),
	('clean_deliver', 'deliver'),
	('clean_strip_html', 'striphtml'),
	('clean_header', 'header'),
	('clean_header_txt', '"X-Spam-Status: No"'),
	('spam_store', 'store'),
	('spam_deliver', 'deliver'),
	('spam_modify', 'yes'),
	('spam_notify', 'notify'),
	('spam_delete', 'delete'),
	('spam_bounce', 'bounce'),
	('spam_forward', 'forward'),
	('spam_strip_html', 'striphtml'),
	('spam_attachement', 'attachment'),
	('spam_header', ' header'),
	('spam_header_txt', '"X-Spam-Status: Yes"'),
	('high_spam_store', 'store'),
	('high_spam_deliver', 'deliver'),
	('high_spam_notify', 'notify'),
	('high_spam_delete', 'delete'),
	('high_spam_bounce', 'bounce'),
	('high_spam_forward', 'forward'),
	('high_spam_attachement', 'attachment'),
	('high_spam_header', 'header'),
	('high_spam_header_txt', '"X-Spam-Status: Yes"');
/*!40000 ALTER TABLE `sys_ms_rulessets` ENABLE KEYS */;


-- Dumping structure for table mailscanner.sys_phishing_bad_sites_conf_master
DROP TABLE IF EXISTS `sys_phishing_bad_sites_conf_master`;
CREATE TABLE IF NOT EXISTS `sys_phishing_bad_sites_conf_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text COLLATE utf8_unicode_ci,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_phishing_sites
DROP TABLE IF EXISTS `sys_phishing_sites`;
CREATE TABLE IF NOT EXISTS `sys_phishing_sites` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `site` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `safe` enum('y','n') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_policyd_config
DROP TABLE IF EXISTS `sys_policyd_config`;
CREATE TABLE IF NOT EXISTS `sys_policyd_config` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_pop3_conf
DROP TABLE IF EXISTS `sys_pop3_conf`;
CREATE TABLE IF NOT EXISTS `sys_pop3_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `server` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port` int(11) DEFAULT '110',
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `domain` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_quarantine_message
DROP TABLE IF EXISTS `sys_quarantine_message`;
CREATE TABLE IF NOT EXISTS `sys_quarantine_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `html` text COLLATE utf8_unicode_ci,
  `domain` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_rbl_blacklist
DROP TABLE IF EXISTS `sys_rbl_blacklist`;
CREATE TABLE IF NOT EXISTS `sys_rbl_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `relay_ip` text CHARACTER SET latin1,
  `blocklist` text CHARACTER SET latin1,
  `info` text CHARACTER SET latin1,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_rbl_config
DROP TABLE IF EXISTS `sys_rbl_config`;
CREATE TABLE IF NOT EXISTS `sys_rbl_config` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.sys_rbl_config: 41 rows
DELETE FROM `sys_rbl_config`;
/*!40000 ALTER TABLE `sys_rbl_config` DISABLE KEYS */;
INSERT INTO `sys_rbl_config` (`variable_name`, `value`) VALUES
	('spamscore', '8'),
	('hamscore', '5'),
	('blocktime', '120'),
	('errtemplate', 'REJECT Client host [ip] blocked using EMFABox'),
	('rbltempfile', '/tmp/mailwatch2rbl.rbl'),
	('rblfile', NULL),
	('rblsoa', '$SOA 300 localhost. postmaster.localhost. 0 300 150 86400 300'),
	('rblns', '$NS 300 localhost.'),
	('rblmode', 'advanced'),
	('rbls1', '$1 You have been blocked for sending more than 4'),
	('rbls2', '$2 spam emails and no non-spam emails to us within the last 23 hours. You will be unblocked shortly after'),
	('rbltemplate', '$1 $2 [expires3]'),
	('customtempfile', '/tmp/mailwatch2rbl.tmp'),
	('customfile', '/etc/postfix/mailwatch2rbl.cidr'),
	('customtemplate', '[ip]	REJECT'),
	('customcmd', NULL),
	('minspamcount', '2'),
	('debug', '0'),
	('dobblocktime', '120'),
	('dobservice', '1'),
	('rblservice', '1'),
	('dobdomainage', '90'),
	('rbl_update_url', 'http://dl.emfabox.org/add2list.php'),
	('rbl_server_key', NULL),
	('server_sync', '0'),
	('block_domains_host_count', '10'),
	('block_age_check_count', '10'),
	('reject_error_msg', 'blocked using EMFABox dl.emfabox.org RBL domain reputation list'),
	('rbl_report', '0'),
	('block_domains_not_found', '0'),
	('smtp_block_array', ''),
	('do_not_block_from_email', NULL),
	('do_not_block_from_domain', NULL),
	('do_not_block_from_ip', NULL),
	('do_not_block_from_ip_range', NULL),
	('smtp_country_array', ''),
	('allow_country_array', ''),
	('block_from_external_blocktime', '240'),
	('server_sync_blocktime', '48'),
	('server_api_key', NULL),
	('server_update_url', NULL);
/*!40000 ALTER TABLE `sys_rbl_config` ENABLE KEYS */;


-- Dumping structure for table mailscanner.sys_rbl_list
DROP TABLE IF EXISTS `sys_rbl_list`;
CREATE TABLE IF NOT EXISTS `sys_rbl_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rbl` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `sys_rbl_list` VALUES ('1', 'zen.spamhaus.org', 'Spamhaus ZEN', 'y');
INSERT INTO `sys_rbl_list` VALUES ('2', 'cbl.abuseat.org', 'CBL Abuseat', 'n');
INSERT INTO `sys_rbl_list` VALUES ('3', 'ix.dnsbl.manitu.net', 'manitu.net', 'y');
INSERT INTO `sys_rbl_list` VALUES ('4', 'b.barracudacentral.org', 'barracudacentral.org', 'y');
INSERT INTO `sys_rbl_list` VALUES ('5', 'bl.spamcop.net', 'SpamCop', 'n');


-- Dumping structure for table mailscanner.sys_rbl_lookup
DROP TABLE IF EXISTS `sys_rbl_lookup`;
CREATE TABLE IF NOT EXISTS `sys_rbl_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `relay` text,
  `relay_ip` text,
  `country` text,
  `from_address` text,
  `tld` text,
  `created` text,
  `action` text,
  `to_address` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_rbl_override
DROP TABLE IF EXISTS `sys_rbl_override`;
CREATE TABLE IF NOT EXISTS `sys_rbl_override` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_relay_country
DROP TABLE IF EXISTS `sys_relay_country`;
CREATE TABLE IF NOT EXISTS `sys_relay_country` (
  `id` int(11) DEFAULT NULL,
  `to_address` text COLLATE utf8_unicode_ci,
  `to_domain` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `from_domain` text COLLATE utf8_unicode_ci,
  `country` text COLLATE utf8_unicode_ci,
  `relay` text COLLATE utf8_unicode_ci,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_sasl_user
DROP TABLE IF EXISTS `sys_sasl_user`;
CREATE TABLE IF NOT EXISTS `sys_sasl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_sa_rules
DROP TABLE IF EXISTS `sys_sa_rules`;
CREATE TABLE IF NOT EXISTS `sys_sa_rules` (
  `id` int(11) DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `header` text COLLATE utf8_unicode_ci,
  `describe` text COLLATE utf8_unicode_ci,
  `score` text COLLATE utf8_unicode_ci,
  `type` enum('bad','good') COLLATE utf8_unicode_ci DEFAULT 'bad',
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table mailscanner.sys_slog_data
DROP TABLE IF EXISTS `sys_slog_data`;
CREATE TABLE IF NOT EXISTS `sys_slog_data` (
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rid` mediumtext COLLATE utf8_unicode_ci,
  `size` bigint(20) DEFAULT '0',
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `clientip` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  `source` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `report` mediumtext COLLATE utf8_unicode_ci,
  `rule` mediumtext COLLATE utf8_unicode_ci,
  `stype` mediumtext COLLATE utf8_unicode_ci,
  KEY `slog_sid_idx` (`sid`),
  KEY `slog_timestamp_idx` (`timestamp`),
  KEY `slog_from_address_idx` (`from_address`(200)),
  KEY `slog_to_address_idx` (`to_address`(200)),
  KEY `slog_clientip_idx` (`clientip`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.



-- Dumping structure for table mailscanner.sys_static_routes
DROP TABLE IF EXISTS `sys_static_routes`;
CREATE TABLE IF NOT EXISTS `sys_static_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `netmask` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interface` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metric` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `network` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `command` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_cpuload
DROP TABLE IF EXISTS `sys_stats_cpuload`;
CREATE TABLE IF NOT EXISTS `sys_stats_cpuload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `load1` decimal(5,2) NOT NULL,
  `load5` decimal(5,2) NOT NULL,
  `load15` decimal(5,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_disk
DROP TABLE IF EXISTS `sys_stats_disk`;
CREATE TABLE IF NOT EXISTS `sys_stats_disk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mount` varchar(255) DEFAULT NULL,
  `used` varchar(255) NOT NULL,
  `free` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `percent_used` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_memload
DROP TABLE IF EXISTS `sys_stats_memload`;
CREATE TABLE IF NOT EXISTS `sys_stats_memload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `used` varchar(255) NOT NULL,
  `free` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `percent_used` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_network
DROP TABLE IF EXISTS `sys_stats_network`;
CREATE TABLE IF NOT EXISTS `sys_stats_network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `interface` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `transmit` varchar(255) DEFAULT NULL,
  `receive` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_stats_swap
DROP TABLE IF EXISTS `sys_stats_swap`;
CREATE TABLE IF NOT EXISTS `sys_stats_swap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `used` varchar(255) NOT NULL,
  `free` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `percent_used` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_time_zones
DROP TABLE IF EXISTS `sys_time_zones`;
CREATE TABLE IF NOT EXISTS `sys_time_zones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `time_zones` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_tls_domains
DROP TABLE IF EXISTS `sys_tls_domains`;
CREATE TABLE IF NOT EXISTS `sys_tls_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `smtp` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_tls_log_level
DROP TABLE IF EXISTS `sys_tls_log_level`;
CREATE TABLE IF NOT EXISTS `sys_tls_log_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`value`),
  UNIQUE KEY `description` (`option`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.sys_tls_log_level: 5 rows
DELETE FROM `sys_tls_log_level`;
/*!40000 ALTER TABLE `sys_tls_log_level` DISABLE KEYS */;
INSERT INTO `sys_tls_log_level` (`id`, `value`, `option`, `active`) VALUES
	(1, '0', '0', 'Y'),
	(2, '1', '1', 'Y'),
	(3, '2', '2', 'N'),
	(4, '3', '3', 'N'),
	(5, '4', '4', 'N');
/*!40000 ALTER TABLE `sys_tls_log_level` ENABLE KEYS */;


-- Dumping structure for table mailscanner.sys_tls_policy_type
DROP TABLE IF EXISTS `sys_tls_policy_type`;
CREATE TABLE IF NOT EXISTS `sys_tls_policy_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`value`),
  UNIQUE KEY `description` (`option`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.sys_tls_policy_type: 8 rows
DELETE FROM `sys_tls_policy_type`;
/*!40000 ALTER TABLE `sys_tls_policy_type` DISABLE KEYS */;
INSERT INTO `sys_tls_policy_type` (`id`, `value`, `option`, `active`) VALUES
	(1, 'none', 'none', 'Y'),
	(2, 'may', 'may', 'Y'),
	(3, 'encrypt', 'encrypt', 'Y'),
	(4, 'dane', 'dane', 'N'),
	(5, 'dane-only', 'dane-only', 'N'),
	(6, 'fingerprint', 'fingerprint', 'Y'),
	(7, 'verify', 'verify', 'Y'),
	(8, 'secure', 'secure', 'Y');
/*!40000 ALTER TABLE `sys_tls_policy_type` ENABLE KEYS */;


-- Dumping structure for table mailscanner.sys_trusted_networks
DROP TABLE IF EXISTS `sys_trusted_networks`;
CREATE TABLE IF NOT EXISTS `sys_trusted_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.sys_urls
DROP TABLE IF EXISTS `sys_urls`;
CREATE TABLE IF NOT EXISTS `sys_urls` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` text COLLATE utf8_unicode_ci,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` enum('A','D','U','R','H','S','X','V') COLLATE utf8_unicode_ci DEFAULT NULL,
  `quarantine_report` tinyint(1) DEFAULT '0',
  `spamscore` tinyint(4) DEFAULT '0',
  `highspamscore` tinyint(4) DEFAULT '0',
  `noscan` tinyint(1) DEFAULT '0',
  `quarantine_rcpt` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation` char(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reg_date` datetime DEFAULT NULL,
  `mod_date` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL,
  `num_logins` int(11) NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `memo` text COLLATE utf8_unicode_ci NOT NULL,
  `ldap_account_suffix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `proxyaddresses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'LOCAL',
  `lang` int(11) NOT NULL DEFAULT '1',
  `portaluser` int(1) NOT NULL,
  `enabled` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '', 'Admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'Admin', 'A', '0', '0', '0', '0', null, 'postmaster@example.com', '', null, '2015-06-29 10:54:17', '2015-05-31 00:00:00', '0', 'example.com', '', '', '', 'LOCAL', '1', '0', '1');


-- Dumping structure for table mailscanner.user_auth_type
DROP TABLE IF EXISTS `user_auth_type`;
CREATE TABLE IF NOT EXISTS `user_auth_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.user_auth_type: 2 rows
DELETE FROM `user_auth_type`;
/*!40000 ALTER TABLE `user_auth_type` DISABLE KEYS */;
INSERT INTO `user_auth_type` (`id`, `type`, `description`, `active`) VALUES
	(1, 'LOCAL', 'Local user', 'y'),
	(2, 'LDAP', 'Domain user', 'y');
/*!40000 ALTER TABLE `user_auth_type` ENABLE KEYS */;


-- Dumping structure for table mailscanner.user_filters
DROP TABLE IF EXISTS `user_filters`;
CREATE TABLE IF NOT EXISTS `user_filters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filter` mediumtext COLLATE utf8_unicode_ci,
  `verify_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `user_filters_username_idx` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.user_type
DROP TABLE IF EXISTS `user_type`;
CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`),
  UNIQUE KEY `description` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table mailscanner.user_type: 4 rows
DELETE FROM `user_type`;
/*!40000 ALTER TABLE `user_type` DISABLE KEYS */;
INSERT INTO `user_type` (`id`, `type`, `description`, `active`) VALUES
	(1, 'A', 'Administrator', 'Y'),
	(2, 'D', 'Domain Administrator', 'Y'),
	(3, 'U', 'User', 'Y'),
	(4, 'R', 'User (Regexp)', 'N');
/*!40000 ALTER TABLE `user_type` ENABLE KEYS */;


-- Dumping structure for table mailscanner.vacation
DROP TABLE IF EXISTS `vacation`;
CREATE TABLE IF NOT EXISTS `vacation` (
  `email` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `cache` text NOT NULL,
  `domain` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`email`),
  KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.vacation_notification
DROP TABLE IF EXISTS `vacation_notification`;
CREATE TABLE IF NOT EXISTS `vacation_notification` (
  `on_vacation` varchar(255) NOT NULL,
  `notified` varchar(255) NOT NULL,
  `notified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`on_vacation`,`notified`),
  CONSTRAINT `vacation_notification_ibfk_1` FOREIGN KEY (`on_vacation`) REFERENCES `vacation` (`email`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.verified_online
DROP TABLE IF EXISTS `verified_online`;
CREATE TABLE IF NOT EXISTS `verified_online` (
  `phish_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phish_detail_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submission_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `online` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.virtual_sender_access
DROP TABLE IF EXISTS `virtual_sender_access`;
CREATE TABLE IF NOT EXISTS `virtual_sender_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `access` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.


-- Dumping structure for table mailscanner.whitelist
DROP TABLE IF EXISTS `whitelist`;
CREATE TABLE IF NOT EXISTS `whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `to_domain` mediumtext COLLATE utf8_unicode_ci,
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `whitelist_uniq` (`to_address`(100),`from_address`(100))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
