/*
MySQL Data Transfer

Source Server         : test
Source Server Version : 50173
Source Host           : localhost:3306
Source Database       : mailscanner

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2015-05-08 18:43:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of admin
-- ----------------------------

-- ----------------------------
-- Table structure for `alias`
-- ----------------------------
DROP TABLE IF EXISTS `alias`;
CREATE TABLE `alias` (
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `goto` text COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`address`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of alias
-- ----------------------------

-- ----------------------------
-- Table structure for `allowed_hosts`
-- ----------------------------
DROP TABLE IF EXISTS `allowed_hosts`;
CREATE TABLE `allowed_hosts` (
  `active` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  `IP` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Client` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Comments` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of allowed_hosts
-- ----------------------------

-- ----------------------------
-- Table structure for `audit_log`
-- ----------------------------
DROP TABLE IF EXISTS `audit_log`;
CREATE TABLE `audit_log` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `action` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of audit_log
-- ----------------------------

-- ----------------------------
-- Table structure for `blacklist`
-- ----------------------------
DROP TABLE IF EXISTS `blacklist`;
CREATE TABLE `blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_address` text COLLATE utf8_unicode_ci,
  `to_domain` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `blacklist_uniq` (`to_address`(100),`from_address`(100))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of blacklist
-- ----------------------------

-- ----------------------------
-- Table structure for `captcha`
-- ----------------------------
DROP TABLE IF EXISTS `captcha`;
CREATE TABLE `captcha` (
  `captcha_id` int(11) NOT NULL AUTO_INCREMENT,
  `captcha_phpsessid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `captcha_time` int(11) NOT NULL,
  `captcha_captcha` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`captcha_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of captcha
-- ----------------------------

-- ----------------------------
-- Table structure for `dkim_config`
-- ----------------------------
DROP TABLE IF EXISTS `dkim_config`;
CREATE TABLE `dkim_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selector` varchar(63) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `private_key` text COLLATE utf8_unicode_ci,
  `public_key` text COLLATE utf8_unicode_ci,
  `SigningTable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KeyTable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of dkim_config
-- ----------------------------

-- ----------------------------
-- Table structure for `dkim_trustedhosts`
-- ----------------------------
DROP TABLE IF EXISTS `dkim_trustedhosts`;
CREATE TABLE `dkim_trustedhosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of dkim_trustedhosts
-- ----------------------------

-- ----------------------------
-- Table structure for `dnsbl_domains`
-- ----------------------------
DROP TABLE IF EXISTS `dnsbl_domains`;
CREATE TABLE `dnsbl_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of dnsbl_domains
-- ----------------------------

-- ----------------------------
-- Table structure for `domain`
-- ----------------------------
DROP TABLE IF EXISTS `domain`;
CREATE TABLE `domain` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `aliases` int(10) NOT NULL DEFAULT '0',
  `mailboxes` int(10) NOT NULL DEFAULT '0',
  `maxquota` bigint(20) NOT NULL DEFAULT '0',
  `quota` bigint(20) NOT NULL DEFAULT '0',
  `transport` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'dovecot',
  `backupmx` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of domain
-- ----------------------------

-- ----------------------------
-- Table structure for `domain_admins`
-- ----------------------------
DROP TABLE IF EXISTS `domain_admins`;
CREATE TABLE `domain_admins` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of domain_admins
-- ----------------------------

-- ----------------------------
-- Table structure for `geoip_country`
-- ----------------------------
DROP TABLE IF EXISTS `geoip_country`;
CREATE TABLE `geoip_country` (
  `begin_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `begin_num` bigint(20) DEFAULT NULL,
  `end_num` bigint(20) DEFAULT NULL,
  `iso_country_code` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` text COLLATE utf8_unicode_ci,
  KEY `geoip_country_begin` (`begin_num`),
  KEY `geoip_country_end` (`end_num`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of geoip_country
-- ----------------------------

-- ----------------------------
-- Table structure for `imp_mailuser`
-- ----------------------------
DROP TABLE IF EXISTS `imp_mailuser`;
CREATE TABLE `imp_mailuser` (
  `imp_userid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_perm_user` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_group` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_other` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ad_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ad_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ad_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ad_username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ad_functions` text COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`imp_userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of imp_mailuser
-- ----------------------------

-- ----------------------------
-- Table structure for `inq`
-- ----------------------------
DROP TABLE IF EXISTS `inq`;
CREATE TABLE `inq` (
  `id` mediumtext COLLATE utf8_unicode_ci,
  `cdate` date DEFAULT NULL,
  `ctime` time DEFAULT NULL,
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `message` mediumtext COLLATE utf8_unicode_ci,
  `size` mediumtext COLLATE utf8_unicode_ci,
  `priority` mediumtext COLLATE utf8_unicode_ci,
  `attempts` mediumtext COLLATE utf8_unicode_ci,
  `lastattempt` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  KEY `inq_hostname` (`hostname`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of inq
-- ----------------------------

-- ----------------------------
-- Table structure for `lil_urls`
-- ----------------------------
DROP TABLE IF EXISTS `lil_urls`;
CREATE TABLE `lil_urls` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` text COLLATE utf8_unicode_ci,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lil_urls
-- ----------------------------

-- ----------------------------
-- Table structure for `log`
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of log
-- ----------------------------

-- ----------------------------
-- Table structure for `lusers`
-- ----------------------------
DROP TABLE IF EXISTS `lusers`;
CREATE TABLE `lusers` (
  `lusername` text COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lusername`(255))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of lusers
-- ----------------------------

-- ----------------------------
-- Table structure for `mail_smtp_spam_blacklists`
-- ----------------------------
DROP TABLE IF EXISTS `mail_smtp_spam_blacklists`;
CREATE TABLE `mail_smtp_spam_blacklists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mail_smtp_spam_blacklists
-- ----------------------------

-- ----------------------------
-- Table structure for `mailbox`
-- ----------------------------
DROP TABLE IF EXISTS `mailbox`;
CREATE TABLE `mailbox` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `storagebasedirectory` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `maildir` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `quota` bigint(20) NOT NULL DEFAULT '0',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rank` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'normal',
  `employeeid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enablesmtp` tinyint(1) NOT NULL DEFAULT '1',
  `enablepop3` tinyint(1) NOT NULL DEFAULT '1',
  `enableimap` tinyint(1) NOT NULL DEFAULT '1',
  `enabledeliver` tinyint(1) NOT NULL DEFAULT '1',
  `enablemanagesieve` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mailbox
-- ----------------------------

-- ----------------------------
-- Table structure for `maillog`
-- ----------------------------
DROP TABLE IF EXISTS `maillog`;
CREATE TABLE `maillog` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id` mediumtext COLLATE utf8_unicode_ci,
  `size` bigint(20) DEFAULT '0',
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `from_domain` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `to_domain` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `clientip` mediumtext COLLATE utf8_unicode_ci,
  `archive` mediumtext COLLATE utf8_unicode_ci,
  `isspam` tinyint(1) DEFAULT '0',
  `ishighspam` tinyint(1) DEFAULT '0',
  `issaspam` tinyint(1) DEFAULT '0',
  `isrblspam` tinyint(1) DEFAULT '0',
  `isfp` tinyint(1) DEFAULT '0',
  `isfn` tinyint(1) DEFAULT '0',
  `spamwhitelisted` tinyint(1) DEFAULT '0',
  `spamblacklisted` tinyint(1) DEFAULT '0',
  `sascore` decimal(7,2) DEFAULT '0.00',
  `spamreport` mediumtext COLLATE utf8_unicode_ci,
  `virusinfected` tinyint(1) DEFAULT '0',
  `nameinfected` tinyint(1) DEFAULT '0',
  `otherinfected` tinyint(1) DEFAULT '0',
  `report` mediumtext COLLATE utf8_unicode_ci,
  `ismcp` tinyint(1) DEFAULT '0',
  `ishighmcp` tinyint(1) DEFAULT '0',
  `issamcp` tinyint(1) DEFAULT '0',
  `mcpwhitelisted` tinyint(1) DEFAULT '0',
  `mcpblacklisted` tinyint(1) DEFAULT '0',
  `mcpsascore` decimal(7,2) DEFAULT '0.00',
  `mcpreport` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `headers` mediumtext COLLATE utf8_unicode_ci,
  `quarantined` tinyint(1) DEFAULT '0',
  KEY `maillog_datetime_idx` (`date`,`time`),
  KEY `maillog_id_idx` (`id`(20)),
  KEY `maillog_clientip_idx` (`clientip`(20)),
  KEY `maillog_from_idx` (`from_address`(200)),
  KEY `maillog_to_idx` (`to_address`(200)),
  KEY `maillog_host` (`hostname`(30)),
  KEY `from_domain_idx` (`from_domain`(50)),
  KEY `to_domain_idx` (`to_domain`(50)),
  KEY `maillog_quarantined` (`quarantined`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of maillog
-- ----------------------------

-- ----------------------------
-- Table structure for `mcp_rules`
-- ----------------------------
DROP TABLE IF EXISTS `mcp_rules`;
CREATE TABLE `mcp_rules` (
  `rule` char(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule_desc` char(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`rule`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mcp_rules
-- ----------------------------

-- ----------------------------
-- Table structure for `ms_archive_fname_rules`
-- ----------------------------
DROP TABLE IF EXISTS `ms_archive_fname_rules`;
CREATE TABLE `ms_archive_fname_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ms_archive_fname_rules
-- ----------------------------

-- ----------------------------
-- Table structure for `ms_archive_ftype_rules`
-- ----------------------------
DROP TABLE IF EXISTS `ms_archive_ftype_rules`;
CREATE TABLE `ms_archive_ftype_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ms_archive_ftype_rules
-- ----------------------------

-- ----------------------------
-- Table structure for `ms_config`
-- ----------------------------
DROP TABLE IF EXISTS `ms_config`;
CREATE TABLE `ms_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `external` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `options` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ms_config
-- ----------------------------

-- ----------------------------
-- Table structure for `ms_filename_rules`
-- ----------------------------
DROP TABLE IF EXISTS `ms_filename_rules`;
CREATE TABLE `ms_filename_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ms_filename_rules
-- ----------------------------

-- ----------------------------
-- Table structure for `ms_filetype_rules`
-- ----------------------------
DROP TABLE IF EXISTS `ms_filetype_rules`;
CREATE TABLE `ms_filetype_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `build` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ftype` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fdesc` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fdefault` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ms_filetype_rules
-- ----------------------------

-- ----------------------------
-- Table structure for `ms_rulesets`
-- ----------------------------
DROP TABLE IF EXISTS `ms_rulesets`;
CREATE TABLE `ms_rulesets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `num` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rule` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ms_rulesets
-- ----------------------------

-- ----------------------------
-- Table structure for `mtalog`
-- ----------------------------
DROP TABLE IF EXISTS `mtalog`;
CREATE TABLE `mtalog` (
  `timestamp` datetime DEFAULT NULL,
  `host` mediumtext COLLATE utf8_unicode_ci,
  `type` mediumtext COLLATE utf8_unicode_ci,
  `msg_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relay` mediumtext COLLATE utf8_unicode_ci,
  `dsn` mediumtext COLLATE utf8_unicode_ci,
  `status` mediumtext COLLATE utf8_unicode_ci,
  `delay` time DEFAULT NULL,
  UNIQUE KEY `mtalog_uniq` (`timestamp`,`host`(10),`type`(10),`msg_id`,`relay`(20)),
  KEY `mtalog_timestamp` (`timestamp`),
  KEY `mtalog_type` (`type`(10)),
  KEY `msg_id` (`msg_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mtalog
-- ----------------------------

-- ----------------------------
-- Table structure for `mtalog_ids`
-- ----------------------------
DROP TABLE IF EXISTS `mtalog_ids`;
CREATE TABLE `mtalog_ids` (
  `smtpd_id` varchar(20) CHARACTER SET ascii DEFAULT NULL,
  `smtp_id` varchar(20) CHARACTER SET ascii DEFAULT NULL,
  UNIQUE KEY `mtalog_ids_idx` (`smtpd_id`,`smtp_id`),
  KEY `smtpd_id` (`smtpd_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mtalog_ids
-- ----------------------------

-- ----------------------------
-- Table structure for `mwrblblock`
-- ----------------------------
DROP TABLE IF EXISTS `mwrblblock`;
CREATE TABLE `mwrblblock` (
  `clientip` text COLLATE utf8_unicode_ci,
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `errormsg` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mwrblblock
-- ----------------------------

-- ----------------------------
-- Table structure for `mwrblclients`
-- ----------------------------
DROP TABLE IF EXISTS `mwrblclients`;
CREATE TABLE `mwrblclients` (
  `clientip` text COLLATE utf8_unicode_ci,
  `hour` smallint(6) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `ham` int(11) NOT NULL DEFAULT '0',
  `spam` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mwrblclients
-- ----------------------------

-- ----------------------------
-- Table structure for `mwrblconfig`
-- ----------------------------
DROP TABLE IF EXISTS `mwrblconfig`;
CREATE TABLE `mwrblconfig` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of mwrblconfig
-- ----------------------------

-- ----------------------------
-- Table structure for `outq`
-- ----------------------------
DROP TABLE IF EXISTS `outq`;
CREATE TABLE `outq` (
  `id` mediumtext COLLATE utf8_unicode_ci,
  `cdate` date DEFAULT NULL,
  `ctime` time DEFAULT NULL,
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `subject` mediumtext COLLATE utf8_unicode_ci,
  `message` mediumtext COLLATE utf8_unicode_ci,
  `size` mediumtext COLLATE utf8_unicode_ci,
  `priority` mediumtext COLLATE utf8_unicode_ci,
  `attempts` mediumtext COLLATE utf8_unicode_ci,
  `lastattempt` mediumtext COLLATE utf8_unicode_ci,
  `hostname` mediumtext COLLATE utf8_unicode_ci,
  KEY `outq_hostname` (`hostname`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of outq
-- ----------------------------

-- ----------------------------
-- Table structure for `recipient_bcc_domain`
-- ----------------------------
DROP TABLE IF EXISTS `recipient_bcc_domain`;
CREATE TABLE `recipient_bcc_domain` (
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of recipient_bcc_domain
-- ----------------------------

-- ----------------------------
-- Table structure for `recipient_bcc_user`
-- ----------------------------
DROP TABLE IF EXISTS `recipient_bcc_user`;
CREATE TABLE `recipient_bcc_user` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of recipient_bcc_user
-- ----------------------------

-- ----------------------------
-- Table structure for `sa_rules`
-- ----------------------------
DROP TABLE IF EXISTS `sa_rules`;
CREATE TABLE `sa_rules` (
  `rule` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `rule_desc` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`rule`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sa_rules
-- ----------------------------

-- ----------------------------
-- Table structure for `saved_filters`
-- ----------------------------
DROP TABLE IF EXISTS `saved_filters`;
CREATE TABLE `saved_filters` (
  `name` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `col` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `operator` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `username` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_filters` (`name`(20),`col`(20),`operator`(20),`value`(20),`username`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of saved_filters
-- ----------------------------

-- ----------------------------
-- Table structure for `sender_bcc_domain`
-- ----------------------------
DROP TABLE IF EXISTS `sender_bcc_domain`;
CREATE TABLE `sender_bcc_domain` (
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sender_bcc_domain
-- ----------------------------

-- ----------------------------
-- Table structure for `sender_bcc_user`
-- ----------------------------
DROP TABLE IF EXISTS `sender_bcc_user`;
CREATE TABLE `sender_bcc_user` (
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `bcc_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expired` datetime NOT NULL DEFAULT '9999-12-31 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sender_bcc_user
-- ----------------------------

-- ----------------------------
-- Table structure for `slog`
-- ----------------------------
DROP TABLE IF EXISTS `slog`;
CREATE TABLE `slog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stype` text COLLATE utf8_unicode_ci,
  `hour` text COLLATE utf8_unicode_ci,
  `sid` text COLLATE utf8_unicode_ci,
  `sender` text COLLATE utf8_unicode_ci,
  `size` text COLLATE utf8_unicode_ci,
  `nrcpts` text COLLATE utf8_unicode_ci,
  `relay` text COLLATE utf8_unicode_ci,
  `rule` text COLLATE utf8_unicode_ci,
  `arg1` text COLLATE utf8_unicode_ci,
  `status` text COLLATE utf8_unicode_ci,
  `sourceid` text COLLATE utf8_unicode_ci,
  `rcpt` text COLLATE utf8_unicode_ci,
  `from_address` text COLLATE utf8_unicode_ci,
  `to_address` text COLLATE utf8_unicode_ci,
  `spam` text COLLATE utf8_unicode_ci,
  `action` text COLLATE utf8_unicode_ci,
  `file` text COLLATE utf8_unicode_ci,
  `virus` text COLLATE utf8_unicode_ci,
  `error` text COLLATE utf8_unicode_ci,
  `mech` text COLLATE utf8_unicode_ci,
  `type` text COLLATE utf8_unicode_ci,
  `score` text COLLATE utf8_unicode_ci,
  `sender_ip` text COLLATE utf8_unicode_ci,
  `cache` text COLLATE utf8_unicode_ci,
  `autolearn` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of slog
-- ----------------------------

-- ----------------------------
-- Table structure for `slogconfig`
-- ----------------------------
DROP TABLE IF EXISTS `slogconfig`;
CREATE TABLE `slogconfig` (
  `setting` text COLLATE utf8_unicode_ci,
  `value` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of slogconfig
-- ----------------------------

-- ----------------------------
-- Table structure for `spamscores`
-- ----------------------------
DROP TABLE IF EXISTS `spamscores`;
CREATE TABLE `spamscores` (
  `user` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lowspamscore` decimal(10,0) NOT NULL DEFAULT '0',
  `highspamscore` decimal(10,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of spamscores
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_box_firewall`
-- ----------------------------
DROP TABLE IF EXISTS `sys_box_firewall`;
CREATE TABLE `sys_box_firewall` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `num` int(11) unsigned NOT NULL DEFAULT '0',
  `traffic` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `pkts` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `bytes` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `target` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `prot` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `opt` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tin` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `tout` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(125) COLLATE utf8_unicode_ci NOT NULL,
  `dest_entry` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dest_port` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_box_firewall
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_box_fw_ports`
-- ----------------------------
DROP TABLE IF EXISTS `sys_box_fw_ports`;
CREATE TABLE `sys_box_fw_ports` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prot` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `var` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `var2` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_box_fw_ports
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_box_fw_rule`
-- ----------------------------
DROP TABLE IF EXISTS `sys_box_fw_rule`;
CREATE TABLE `sys_box_fw_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `traffic` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `ruleset` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_box_fw_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `variable_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_config_policyd_spf`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config_policyd_spf`;
CREATE TABLE `sys_config_policyd_spf` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config_policyd_spf
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_config_postfix`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config_postfix`;
CREATE TABLE `sys_config_postfix` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config_postfix
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_config_postfix_default`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config_postfix_default`;
CREATE TABLE `sys_config_postfix_default` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config_postfix_default
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_config_postfix_out`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config_postfix_out`;
CREATE TABLE `sys_config_postfix_out` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config_postfix_out
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_config_sqlgrey`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config_sqlgrey`;
CREATE TABLE `sys_config_sqlgrey` (
  `variable_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`variable_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_config_sqlgrey
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_dns_forwarders`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dns_forwarders`;
CREATE TABLE `sys_dns_forwarders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `dns_ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_dns_forwarders
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_filename_rules`
-- ----------------------------
DROP TABLE IF EXISTS `sys_filename_rules`;
CREATE TABLE `sys_filename_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_ext` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Allow` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_filename_rules
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_filetype_rules`
-- ----------------------------
DROP TABLE IF EXISTS `sys_filetype_rules`;
CREATE TABLE `sys_filetype_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Allow` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_filetype_rules
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_gateway_usage`
-- ----------------------------
DROP TABLE IF EXISTS `sys_gateway_usage`;
CREATE TABLE `sys_gateway_usage` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_unicode_ci,
  `domain` text COLLATE utf8_unicode_ci,
  `virus_opt` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `domain` (`domain`(50)),
  KEY `email` (`email`(200))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_gateway_usage
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_gateway_warning`
-- ----------------------------
DROP TABLE IF EXISTS `sys_gateway_warning`;
CREATE TABLE `sys_gateway_warning` (
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reason` text COLLATE utf8_unicode_ci,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_gateway_warning
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_kernel_routes`
-- ----------------------------
DROP TABLE IF EXISTS `sys_kernel_routes`;
CREATE TABLE `sys_kernel_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gateway` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `genmask` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flags` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metric` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `use` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iface` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_kernel_routes
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_ldap_conf`
-- ----------------------------
DROP TABLE IF EXISTS `sys_ldap_conf`;
CREATE TABLE `sys_ldap_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ldap_domain_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldap_email_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dc_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_base_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_bind_dn` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_bind_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_filter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_attributes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_authrealm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_ad_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_exchange_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_port` int(11) NOT NULL DEFAULT '389',
  `ldap_proxyuser` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_proxypass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_proxy_addresses` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_active` tinyint(1) DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `exchange_lookup` int(11) NOT NULL DEFAULT '0',
  `exchange_sync` int(11) NOT NULL DEFAULT '0',
  `auth_domain` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ldaps` tinyint(1) DEFAULT '0',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_ldap_conf
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_ldap_domain_settings`
-- ----------------------------
DROP TABLE IF EXISTS `sys_ldap_domain_settings`;
CREATE TABLE `sys_ldap_domain_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ldap_domain_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `server_id` int(11) unsigned NOT NULL DEFAULT '0',
  `ldap_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ignore_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sync_time` int(25) NOT NULL,
  `sync_time_hour` int(25) unsigned NOT NULL DEFAULT '1',
  `sync_time_min` int(25) unsigned NOT NULL DEFAULT '0',
  `send_password` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `keep_ignore_list` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `create_filter_list` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `drop_user` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_ldap_domain_settings
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_ldap_domains`
-- ----------------------------
DROP TABLE IF EXISTS `sys_ldap_domains`;
CREATE TABLE `sys_ldap_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ldap_domain_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_ldap_domains
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_ldap_imp_dyn`
-- ----------------------------
DROP TABLE IF EXISTS `sys_ldap_imp_dyn`;
CREATE TABLE `sys_ldap_imp_dyn` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ldap_domain_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `real_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_functions` text COLLATE utf8_unicode_ci NOT NULL,
  `ldap_error_msg` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_error_reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_in_transport` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_mail_db` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_not_longer_in_mail_db` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `ldap_in_ignore` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_ldap_imp_dyn
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_ldap_imp_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_ldap_imp_log`;
CREATE TABLE `sys_ldap_imp_log` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ldap_email` varchar(125) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ldap_domain` varchar(125) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `real_domain` varchar(125) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` varchar(125) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `action` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_ldap_imp_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_ldap_mail_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_ldap_mail_user`;
CREATE TABLE `sys_ldap_mail_user` (
  `ldap_userid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ldap_domain_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ldap_functions` text COLLATE utf8_unicode_ci NOT NULL,
  `ldap_ignore` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`ldap_userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_ldap_mail_user
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_mail_firewall`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mail_firewall`;
CREATE TABLE `sys_mail_firewall` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_perm_user` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_group` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sys_perm_other` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `server_id` int(11) unsigned NOT NULL DEFAULT '0',
  `block_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `block_ip_mask` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tcp_port` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `udp_port` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_mail_firewall
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_mcp_rules`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mcp_rules`;
CREATE TABLE `sys_mcp_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` int(11) DEFAULT NULL,
  `rulename` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruledesc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruletype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pattern` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_mcp_rules
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_ms_rule_sets`
-- ----------------------------
DROP TABLE IF EXISTS `sys_ms_rule_sets`;
CREATE TABLE `sys_ms_rule_sets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rname` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `0direction` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `1target` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `2and` int(1) NOT NULL DEFAULT '0',
  `3anddirection` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `4andtarget` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `isdefault` int(1) NOT NULL DEFAULT '0',
  `action` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_ms_rule_sets
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_phishing_bad_sites_conf_master`
-- ----------------------------
DROP TABLE IF EXISTS `sys_phishing_bad_sites_conf_master`;
CREATE TABLE `sys_phishing_bad_sites_conf_master` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_phishing_bad_sites_conf_master
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_phishing_sites`
-- ----------------------------
DROP TABLE IF EXISTS `sys_phishing_sites`;
CREATE TABLE `sys_phishing_sites` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `site` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `safe` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_phishing_sites
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_quarantine_message`
-- ----------------------------
DROP TABLE IF EXISTS `sys_quarantine_message`;
CREATE TABLE `sys_quarantine_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `html` text COLLATE utf8_unicode_ci,
  `domain` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_quarantine_message
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_rbl_list`
-- ----------------------------
DROP TABLE IF EXISTS `sys_rbl_list`;
CREATE TABLE `sys_rbl_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rbl` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_rbl_list
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_rbl_override`
-- ----------------------------
DROP TABLE IF EXISTS `sys_rbl_override`;
CREATE TABLE `sys_rbl_override` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_rbl_override
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_relay_domains`
-- ----------------------------
DROP TABLE IF EXISTS `sys_relay_domains`;
CREATE TABLE `sys_relay_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relay_to` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relay_to_port` int(11) DEFAULT '25',
  `relay_use_mx` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'NO',
  `recipient_verification` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `av_scan` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'YES',
  `av_opt` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'NO',
  `antispoofing` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'REJECT',
  `auth_scheme` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'LOCAL',
  `auth_scheme_id` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain_UNIQUE` (`domain`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_relay_domains
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_smtpd_client_restrictions`
-- ----------------------------
DROP TABLE IF EXISTS `sys_smtpd_client_restrictions`;
CREATE TABLE `sys_smtpd_client_restrictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pos` int(12) NOT NULL,
  `part` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_smtpd_client_restrictions
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_static_routes`
-- ----------------------------
DROP TABLE IF EXISTS `sys_static_routes`;
CREATE TABLE `sys_static_routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `netmask` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interface` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipaddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `metric` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `network` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `command` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('n','y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_static_routes
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_time_zones`
-- ----------------------------
DROP TABLE IF EXISTS `sys_time_zones`;
CREATE TABLE `sys_time_zones` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `time_zones` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_time_zones
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_tls_domains`
-- ----------------------------
DROP TABLE IF EXISTS `sys_tls_domains`;
CREATE TABLE `sys_tls_domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `match` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_tls_domains
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_trusted_networks`
-- ----------------------------
DROP TABLE IF EXISTS `sys_trusted_networks`;
CREATE TABLE `sys_trusted_networks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `network` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sys_trusted_networks
-- ----------------------------

-- ----------------------------
-- Table structure for `user_filters`
-- ----------------------------
DROP TABLE IF EXISTS `user_filters`;
CREATE TABLE `user_filters` (
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `filter` mediumtext COLLATE utf8_unicode_ci,
  `verify_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  KEY `user_filters_username_idx` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_filters
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` enum('A','D','U','R','H') COLLATE utf8_unicode_ci DEFAULT NULL,
  `quarantine_report` tinyint(1) DEFAULT '0',
  `spamscore` tinyint(4) DEFAULT '0',
  `highspamscore` tinyint(4) DEFAULT '0',
  `noscan` tinyint(1) DEFAULT '0',
  `quarantine_rcpt` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `confirmation` char(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reg_date` datetime DEFAULT NULL,
  `mod_date` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL,
  `num_logins` int(11) NOT NULL,
  `groupid` int(2) unsigned NOT NULL DEFAULT '1',
  `memo` text COLLATE utf8_unicode_ci NOT NULL,
  `portaluser` int(1) NOT NULL,
  `enabled` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------

-- ----------------------------
-- Table structure for `user_type`
-- ----------------------------
DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` enum('N','Y') COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`),
  UNIQUE KEY `description` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_type
-- ----------------------------
INSERT INTO `user_type` VALUES ('1', 'A', 'Administrator', 'Y');
INSERT INTO `user_type` VALUES ('2', 'D', 'Domain Administrator', 'N');
INSERT INTO `user_type` VALUES ('3', 'U', 'User', 'Y');
INSERT INTO `user_type` VALUES ('4', 'R', 'User (Regexp)', 'N');


-- ----------------------------
-- Table structure for `vacation`
-- ----------------------------
DROP TABLE IF EXISTS `vacation`;
CREATE TABLE `vacation` (
  `email` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `cache` text NOT NULL,
  `domain` varchar(255) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`email`),
  KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vacation
-- ----------------------------

-- ----------------------------
-- Table structure for `vacation_notification`
-- ----------------------------
DROP TABLE IF EXISTS `vacation_notification`;
CREATE TABLE `vacation_notification` (
  `on_vacation` varchar(255) NOT NULL,
  `notified` varchar(255) NOT NULL,
  `notified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`on_vacation`,`notified`),
  CONSTRAINT `vacation_notification_ibfk_1` FOREIGN KEY (`on_vacation`) REFERENCES `vacation` (`email`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vacation_notification
-- ----------------------------

-- ----------------------------
-- Table structure for `verified_online`
-- ----------------------------
DROP TABLE IF EXISTS `verified_online`;
CREATE TABLE `verified_online` (
  `phish_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phish_detail_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `submission_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verification_time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `online` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of verified_online
-- ----------------------------

-- ----------------------------
-- Table structure for `virtual_sender_access`
-- ----------------------------
DROP TABLE IF EXISTS `virtual_sender_access`;
CREATE TABLE `virtual_sender_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `access` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of virtual_sender_access
-- ----------------------------

-- ----------------------------
-- Table structure for `whitelist`
-- ----------------------------
DROP TABLE IF EXISTS `whitelist`;
CREATE TABLE `whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_address` mediumtext COLLATE utf8_unicode_ci,
  `to_domain` mediumtext COLLATE utf8_unicode_ci,
  `from_address` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `whitelist_uniq` (`to_address`(100),`from_address`(100))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of whitelist
-- ----------------------------
