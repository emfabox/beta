
-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(60) DEFAULT '',
  `password` varchar(32) DEFAULT NULL,
  `fullname` varchar(50) NOT NULL DEFAULT '',
  `type` enum('A','D','U','R','H') DEFAULT NULL,
  `quarantine_report` tinyint(1) DEFAULT '0',
  `spamscore` tinyint(4) DEFAULT '0',
  `highspamscore` tinyint(4) DEFAULT '0',
  `noscan` tinyint(1) DEFAULT '0',
  `quarantine_rcpt` varchar(60) DEFAULT NULL,
  `email` varchar(256) NOT NULL,
  `confirmation` char(40) NOT NULL DEFAULT '',
  `regdate` int(11) unsigned NOT NULL,
  `last_login` datetime NOT NULL,
  `num_logins` int(11) NOT NULL,
  `groupid` int(2) unsigned NOT NULL DEFAULT '1',
  `memo` text NOT NULL,
  `enabled` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

