/*
MySQL Data Transfer

Source Server         : demo3-mail.sentinelbox.net
Source Server Version : 50173
Source Host           : localhost:3306
Source Database       : mailscanner

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2015-05-14 09:48:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sa_users`
-- ----------------------------
DROP TABLE IF EXISTS `sa_users`;
CREATE TABLE `sa_users` (
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(60) COLLATE utf8_unicode_ci DEFAULT '',
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type` enum('A','D','U','R','H') COLLATE utf8_unicode_ci DEFAULT NULL,
  `quarantine_report` tinyint(1) DEFAULT '0',
  `spamscore` tinyint(4) DEFAULT '0',
  `highspamscore` tinyint(4) DEFAULT '0',
  `noscan` tinyint(1) DEFAULT '0',
  `quarantine_rcpt` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sa_users
-- ----------------------------

