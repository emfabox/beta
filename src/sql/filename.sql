/*
Date: 2015-03-25 16:56:40
*/

-- ----------------------------
-- Table structure for `filename`
-- ----------------------------
DROP TABLE IF EXISTS `filename`;
CREATE TABLE `filename` (
  `id` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `rule` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `ext` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of filename
-- ----------------------------
INSERT INTO `filename` VALUES ('1', '\\.bak$', 'bak', '.bak', 'Backup files', 'Backup files.', '0');
INSERT INTO `filename` VALUES ('2', '\\.bz2$', 'bz2', '.bz2', 'Bzip Compressed File', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('3', '\\{[a-hA-H0-9-]{25,}\\}', 'CLSID', '{[a-hA-H0-9-]{25,}}', 'CLSID', 'Files containing CLSID\'s are trying to hide their real type.', '0');
INSERT INTO `filename` VALUES ('4', '\\.Z$', 'z', '.z', 'Compressed File', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('5', '\\s{10,}', 'whitespace', '> 10', 'Contiguous White Space', 'Filename contains a lot of white space. A long gap in a name is often used to hide part of it.', '0');
INSERT INTO `filename` VALUES ('6', '\\.fdf$', 'fdf', '.fdf', 'Dangerous Adobe Acrobat', 'Opening this file can cause auto-loading of any file from the internet.', '0');
INSERT INTO `filename` VALUES ('7', '\\.(mon|tue|wed|thu|fri|sat|sun)\\.[a-z0-9]{3}$', 'days', '.mon .tue .wed .thu .fri .sat .sun', 'Days of Week', 'This will allow file names with days of the week embedded in the file name. Example: report.mon.doc. ', '1');
INSERT INTO `filename` VALUES ('8', '\\.x\\d+\\.rel$', 'doc', '.doc, .docx', 'Document', 'Microsoft Word, etc.', '1');
INSERT INTO `filename` VALUES ('9', '\\.shb$', 'shb', '.shb', 'Document Shortcut', 'Possible Document Shortcut attack.', '0');
INSERT INTO `filename` VALUES ('10', '\\.vcf$', 'vcf', '.vcf', 'E-Business Card', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('11', '\\.lnk$', 'lnk', '.lnk', 'Eudora Links', 'Attack that targets Eudora mail client.', '0');
INSERT INTO `filename` VALUES ('12', '\\.mhtml$', 'mhtml', '.mhtml', 'Eudora Meta Refresh', 'Possible Eudora meta-refresh attack. Eudora vulnerability.', '0');
INSERT INTO `filename` VALUES ('13', '\\.xnk$', 'xnk', '.xnk', 'Exchange Shortcut', 'Possible Microsoft Exchange Shortcut attack.', '0');
INSERT INTO `filename` VALUES ('14', '\\.gif$', 'gif', '.gif', 'GIF Image', 'Image file. Typically harmless.', '1');
INSERT INTO `filename` VALUES ('15', '\\.t?gz$', 'targz', '.tar.gz', 'Gzip & Tar Compressed File', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('16', '\\.[a-z][a-z0-9]{2,3}\\s*\\.[a-z0-9]{3}$', 'double-ext', 'doc.exe, doc.zip, etc', 'Hidden Extensions', 'This stops users trying to hide file types by adding a different extension. By default doc.doc or zip.zip (or similar) is allowed. This will stop items like this: exe.doc, doc.zip, exe.txt, etc. ', '1');
INSERT INTO `filename` VALUES ('17', '\\.ins$', 'ins', '.ins', 'Internet Communications Setting', 'Possible Microsoft Internet Communications settings attack string.', '0');
INSERT INTO `filename` VALUES ('18', '\\.its$', 'its', '.its', 'Internet Document Set', 'Dangerous Internet Document Set (according to Microsoft)', '0');
INSERT INTO `filename` VALUES ('19', '\\.jpg$', 'jpg', '.jpg', 'JPEG Image', 'Image file. Typically harmless.', '1');
INSERT INTO `filename` VALUES ('20', '.{150,}', 'long', '> 150', 'Long File Name', 'Very long filenames are good signs of attacks against Microsoft e-mail packages.', '0');
INSERT INTO `filename` VALUES ('21', '\\.hqx$', 'hqx', '.qx', 'Macintosh Archive', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('22', '\\.sea$', 'sea', '.sea', 'Macintosh Archive', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('23', '\\.sit.bin$', 'sitbin', '.sit.bin', 'Macintosh Archive', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('24', '\\.md[az]$', 'md', '.md[az]', 'Microsoft', 'Dangerous attachment type (according to Microsoft)', '0');
INSERT INTO `filename` VALUES ('25', '\\.vs[stw]$', 'vs', '.vs[stw]', 'Microsoft', 'Dangerous attachment type', '0');
INSERT INTO `filename` VALUES ('26', '\\.mau$', 'mau', '.mau', 'Microsoft', 'Dangerous attachment type (according to Microsoft)', '0');
INSERT INTO `filename` VALUES ('27', '\\.ma[dfgmqrsvw]$', 'ma', '.ma[dfgmqrsvw]', 'Microsoft Access', 'Microsoft Access shortcut attack.', '0');
INSERT INTO `filename` VALUES ('28', '\\.cab$', 'cab', '.cab', 'Microsoft Cabinet File', 'Cabinet files may hide viruses. Should not be emailed. ', '0');
INSERT INTO `filename` VALUES ('29', '\\.xml\\d*\\.rel$', 'xls', '.xml, .xls', 'Microsoft Excel', ' Microsoft Office Spread Sheet.', '1');
INSERT INTO `filename` VALUES ('30', '\\.hta$', 'hta', '.hta', 'Microsoft HTML Archive', 'Possible HTML archive attack.', '0');
INSERT INTO `filename` VALUES ('31', '\\.jse?$', 'jse', '.jse', 'Microsoft JScript', 'Possible Microsoft JScript attack.', '0');
INSERT INTO `filename` VALUES ('32', '\\.pst$', 'pst', '.pst', 'Microsoft Office', 'Dangerous Office Data File (according to Microsoft)', '0');
INSERT INTO `filename` VALUES ('33', '\\.prf$', 'prf', '.prf', 'Microsoft Outlook', 'Dangerous Outlook Profile Settings (according to Microsoft)', '0');
INSERT INTO `filename` VALUES ('34', '\\.pif$', 'pif', '.pif', 'Microsoft Shortcut', 'Probable Microsoft MS-Dos type shortcut attack.', '0');
INSERT INTO `filename` VALUES ('35', '\\.job$', 'job', '.job', 'Microsoft Task scheduler', 'Possible Microsoft Task scheduler attack.', '0');
INSERT INTO `filename` VALUES ('36', '\\.tmp$', 'tmp', '.tmp', 'Microsoft Temp File', 'Dangerous Temporary File', '0');
INSERT INTO `filename` VALUES ('37', '\\.vsmacros$', 'vsmacros', '. vsmacros', 'Microsoft Visual Studio', 'Dangerous Visual Studio Macros', '0');
INSERT INTO `filename` VALUES ('38', '\\.(jan|feb|mar|apr|may|jun|june|jul|july|aug|sep|sept|oct|nov|dec)\\.[a-z0-9]{3}$', 'months', '.jan .feb .mar .apr .may etc', 'Months Extension', 'This will allow documents with months in the file names. Example: report.dec.doc. ', '1');
INSERT INTO `filename` VALUES ('39', '\\.gpg$', 'gpg', '.gpg', 'PGP & GPG Signatures', 'Digital signatures attached as a file.', '1');
INSERT INTO `filename` VALUES ('40', '\\.pgp$', 'pgp', '.pgp', 'PGP & GPG Signatures', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('41', '\\.asc$', 'pgpasc', '.asc', 'PGP & GPG Signatures', 'application/pgp-encrypted, application/pgp.', '1');
INSERT INTO `filename` VALUES ('42', '\\.sig$', 'sig', '.sig', 'PGP Signature', 'PKI signature via PGP or GPG. ', '1');
INSERT INTO `filename` VALUES ('43', '\\.rpm$', 'rpm', '.rpm', 'Redhat RPM Files', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('44', '(\\.[a-z0-9]{3})\\1$', 'repeat-ext', 'zip.zip, doc.doc, etc', 'Repeat Extensions', 'This will allow files with repeat extensions. Examples: file.zip.zip or report.doc.doc. ', '1');
INSERT INTO `filename` VALUES ('45', '\\.rtf$', 'rtf', '.rtf', 'Rich Text', 'Rich text document. ', '1');
INSERT INTO `filename` VALUES ('46', '\\.cer$', 'cer', '.cer', 'Security Certificate', 'Security Certificate, which should never be mailed. ', '0');
INSERT INTO `filename` VALUES ('47', '\\.shs$', 'shs', '.shs', 'Shell Scrap', 'Possible Shell Scrap Object attack.', '0');
INSERT INTO `filename` VALUES ('48', '\\.cnf$', 'cnf', '.cnf', 'Speed Dial', 'Possible Speed Dial attack.', '0');
INSERT INTO `filename` VALUES ('49', '\\.sit$', 'sit', '.sit', 'StuffIt Archive', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('50', '\\.tex$', 'tex', '.tex', 'Tex and Latex', 'Tex and Latex', '1');
INSERT INTO `filename` VALUES ('51', '\\.txt$', 'txt', '.txt', 'Text File', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('52', '\\.vb[es]$', 'vb', '.vb[es]', 'Visual Basic', 'Possible Microsoft Visual Basic script attack.', '0');
INSERT INTO `filename` VALUES ('53', '\\.url$', 'url', '.url', 'Web Link', 'Typically harmless.', '1');
INSERT INTO `filename` VALUES ('54', '\\.cmd$', 'cmd', '.cmd', 'Windows Batch File', 'Possible malicious batch file script.', '0');
INSERT INTO `filename` VALUES ('55', '\\.cpl$', 'cpl', '.cpl', 'Windows Control Panel', 'Windows control panel item.', '0');
INSERT INTO `filename` VALUES ('56', '\\.cur$', 'cur', '.cur', 'Windows Cursor', 'Windows cursor file security vulnerability. Possible buffer overflow attempt.', '0');
INSERT INTO `filename` VALUES ('57', '\\.scf$', 'scf', '.scf', 'Windows Explorer', 'Possible Windows Explorer Command attack.', '0');
INSERT INTO `filename` VALUES ('58', '\\.chm$', 'chm', '.chm', 'Windows Help File', 'Possible compiled Help file based virus.', '0');
INSERT INTO `filename` VALUES ('59', '\\.hlp$', 'hlp', '.hlp', 'Windows Help File', 'Windows Help file security vulnerability. Possible buffer overflow attempt.', '0');
INSERT INTO `filename` VALUES ('60', '\\.ani$', 'ani', '.ani', 'Windows icon file', 'Possible buffer overflow in Windows', '0');
INSERT INTO `filename` VALUES ('61', '\\.ico$', 'ico', '.ico', 'Windows Icons', 'Windows animated cursor file security vulnerability. Possible buffer overflow attempt.', '0');
INSERT INTO `filename` VALUES ('62', '\\.reg$', 'reg', '.reg', 'Windows Registry', 'Possible Windows Registry attack.', '0');
INSERT INTO `filename` VALUES ('63', '\\.scr$', 'scr', '.scr', 'Windows Screensaver', 'Possible virus hidden in a screensaver.', '0');
INSERT INTO `filename` VALUES ('64', '\\.ws$', 'ws2', '.ws', 'Windows Script', 'Dangerous Windows Script ', '0');
INSERT INTO `filename` VALUES ('65', '\\.sct$', 'sct', '.sct', 'Windows Script Component', 'Possible Microsoft Windows Script Component attack.', '0');
INSERT INTO `filename` VALUES ('66', '\\.ws[cfh]$', 'ws', '.ws[cfh]', 'Windows Script Host', 'Possible Microsoft Windows Script Host attack.', '0');
INSERT INTO `filename` VALUES ('67', '\\.com$', 'com', '.com', 'Windows/DOS Executable', 'Windows/DOS Executable', '0');
INSERT INTO `filename` VALUES ('68', '\\.exe$', 'exe', '.exe', 'Windows/DOS Executable', 'Windows/DOS Executable', '0');
INSERT INTO `filename` VALUES ('69', '\\.bat$', 'bat', '.bat', 'Windows/MS-Dos Batch File', 'Possible malicious batch file script.', '0');
INSERT INTO `filename` VALUES ('70', '\\.zip$', 'zip', '.zip', 'Zip Compressed File', 'Typically harmless.', '1');
