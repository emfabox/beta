
-- ----------------------------
-- Table structure for `audit_log`
-- ----------------------------
DROP TABLE IF EXISTS `audit_log`;
CREATE TABLE `audit_log` (
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user` varchar(20) NOT NULL DEFAULT '',
  `ip_address` varchar(15) NOT NULL DEFAULT '',
  `action` text NOT NULL,
  `eventid` varchar(255) NOT NULL,
  `event` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

