#!/usr/bin/perl
##############################################################################
# By BumbleBeeWare.com 2006
# check-release.cgi
##############################################################################
#
# Modified for use with EMFABOX
# This script is an modification of release-msg.cgi for a bit more security
# 
# Copyright (C) 2014  http://www.cycomptec.com 
#
############################################################################## 
#
# MAILWATCH PART
use CGI::Carp qw(fatalsToBrowser);
use CGI qw(:standard);
use DBI;


##########################
# configuration
$tempdir = "/var/www/temp";

##########################

# get ip address
$client= $ENV{REMOTE_ADDR};

# parse the input data
&form_parse;

# lets block direct access that is not via the form post
if ($ENV{"REQUEST_METHOD"} ne "POST"){&nopost;}

# use this program to remove all old temp files
# this keeps the director clean without setting up a cron job
opendir TMPDIR, "$tempdir"; 
@alltmpfiles = readdir TMPDIR;

foreach $oldtemp (@alltmpfiles) {

	$age = 0;
	$age = (stat("$tempdir/$oldtemp"))[9];
	# if age is more than 300 seconds or 5 minutes	
	if ((time - $age) > 300){unlink "$tempdir/$oldtemp";}
	
	}


# open the temp datafile for current user based on ip
$tempfile = "$tempdir/$ENV{'REMOTE_ADDR'}";
open (TMPFILE, "<$tempfile")|| ($nofile = 1);
(@tmpfile) = <TMPFILE>;
close TMPFILE;

# if no matching ip file check for a cookie match
# this will compensate for AOL proxy servers accessing images
if ($nofile == 1){
	
$cookieip = $ENV{HTTP_COOKIE};
$cookieip =~ /checkme=([^;]*)/;
$cookieip = $1;

if ($cookieip ne ""){
	
	$tempfile = "$tempdir/$cookieip";
	open (TMPFILE, "<$tempdir/$cookieip")|| &nofile;
	(@tmpfile) = <TMPFILE>;
	close TMPFILE;
}

}

$imagetext = $tmpfile[0];
chomp $imagetext;

# set the form input to lower case
$FORM{'verifytext'} = lc($FORM{'verifytext'});

# compare the form input with the file text
if ($FORM{'verifytext'} ne "$imagetext"){&error;}

# get the other vars from submit form ...
$datenumber = $FORM{'datenumber'};
$id = $FORM{'id'};
$to_address = $FORM{'to_address'};
$from_address = $FORM{'from_address'};
$subject= $FORM{'subject'};
$myhostname= $FORM{'myhostname'};


# CHECK for vars

if ($id eq "" ){&error;}
if ($datenumber eq "" ){&error;}


$to = param("to");

# get mailscanner db config
open(MWCONFIG, '/etc/emfa/mwconfig');
my @mw_config = <MWCONFIG>;
close MWCONFIG;
s{^\s+|\s+$}{}g foreach @mw_config;
my($db_name) = $mw_config[0];
my($db_host) = $mw_config[1];
my($db_user) = $mw_config[2];
my($db_pass) = $mw_config[3];


  # Verify if id is present in db
   
  $dbh = DBI->connect("DBI:mysql:database=$db_name;host=$db_host", $db_user, $db_pass, {PrintError => 0});
  if (!$dbh) {&error;}
  
    $sql = "SELECT id,from_address,to_address,subject from maillog WHERE id=\"$id\"";
  
  $sth = $dbh->prepare($sql);
      $sth->execute;
      @results = $sth->fetchrow;
      
      $from_address = $results[1];
      $to_address = $results[2];
      $subject = $results[3]; 
      
        if (!$results[0]) { 
        $sth->finish();
        $dbh->disconnect();  
 
        # redirect to failure page
        print "<meta http-equiv=\"refresh\" content=\"0;URL=/notreleased.html\">";
        
        exit;
      }

      


# now delete the temp file so it cannot be used again by the same user
unlink "$tempfile";


# if no error continue with the program
print "Content-type: text/html\n\n";
#print "sucessful $test verification";
$query    = new CGI;
$sendmail = "/usr/sbin/sendmail.postfix";
#$id = param("id");
#$datenumber = param("datenumber");
#$to = param("to");
$msgtorelease = "/var/spool/MailScanner/quarantine/$datenumber/spam/$id";
#open(MAIL, "|$sendmail $to <$msgtorelease") or die "Cannot open $sendmail: $!";
#open(MAIL, "|$sendmail $to_address <$msgtorelease") or die "Cannot open $sendmail: $!";
open(MAIL, "|$sendmail -f no-reply $to <$msgtorelease") or die "Cannot open $sendmail: $!";
close(MAIL);




# send info to customer commit it out if you don't like it ...
#&mail_results;

# redirect to success page
print "<meta http-equiv=\"refresh\" content=\"0;URL=/released.html\">";



exit;



sub error {
	
print "Content-type: text/html\n\n";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
print "<center>";
print " :( ... ";
#print "Input verification code does not match the text on the image.";
print "Something went wrong and we can't process your request right now.";
print "<br>";
print "Please try again later.";
print "</center>";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
# now delete the temp file so it cannot be used again by the same user
unlink "$tempdir/$ENV{'REMOTE_ADDR'}";
exit;	
}

sub nofile {
	
print "Content-type: text/html\n\n";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
print "<center>";
print " :( ... ";
#print "No file found for verification.";
print "Something went wrong and we can't process your request right now.";
print "<br>";
print "Please try again later.";
print "</center>";

print "</center>";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";	
exit;	
}


sub nopost {
	
print "Content-type: text/html\n\n";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
print "<center>";
print " :( ... ";
#print "Method not allowed, input must be via a form post.";
print "Something went wrong and we can't process your request right now.";
print "<br>";
print "Please try again later.";
print "<center>";
print "<hr size=\"1\" color=\"#ffa042\" width=\"650\">";
exit;	
}



sub form_parse  {
	read (STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
	@pairs = split(/&/, $buffer);

	foreach $pair (@pairs)
	{
    	($name, $value) = split(/=/, $pair);
    	$value =~ tr/+/ /;
    	$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    	$FORM{$name} = $value;
}}

# e-mail subs

sub mail_results {
	
      open( MAIL, "|$sendmail -t" )or die "Cannot open $sendmail: $!";
      print MAIL "Subject: [INFO] Message has been released from quarantine right now.\n";
      print MAIL "From: no-reply\@$myhostname\n";
      print MAIL "To: $to_address\n";
      print MAIL "Reply-to: $to_address\n\n";
      print MAIL "\n\n";
      print MAIL "Please scroll down for the German translation.\n";
      print MAIL "Die deutsche Uebersetzung finden Sie im Anschluss an den englischen Text.\n\n";
      print MAIL "---- ENGLISH -----------\n\n";
      print MAIL "This message has been released.\n\n";
      print MAIL "From: $from_address\n";
      print MAIL "Subject: $subject\n";
      print MAIL "Message id: $id\n";
      print MAIL "Date code: $datenumber\n\n";
      print MAIL "Your Client IP-Address: $client\n\n\n";
      print MAIL "This is an automatic generated message. Please do not reply.\n\n";
      print MAIL "--\n";
      print MAIL "Thank you.\n\n";
      print MAIL "--- DEUTSCH -----------\n\n";
      print MAIL "Diese Nachricht wurde nun ausgeliefert.\n\n";
      print MAIL "Absender: $from_address\n";
      print MAIL "Betreff: $subject\n";
      print MAIL "Nachrichten ID: $id\n";
      print MAIL "Datums Code: $datenumber\n\n";
      print MAIL "Client Computer IP-Adresse: $client\n\n\n";
      print MAIL "Diese Meldung wurde vom System automatisch erstellt. Beantworten Sie diese bitte nicht.\n\n";
      print MAIL "--\n";
      print MAIL "Danke.\n";
      close MAIL;
}
