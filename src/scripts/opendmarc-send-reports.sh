#!/bin/bash
# Imports data from OpenDMARC's opendmarc.dat file into a local MySQL DB
# and sends DMARC failure reports to domain owners.
# Based on a script from Hamzah Khan (http://blog.hamzahkhan.com/)
 
set -e
 
# Database and History File Info

i=0
while read line; do
  if [[ "$line" =~ ^[^#]*= ]]; then
    name[i]=${line%% =*}
    value[i]=${line#*= }
    ((i++))
  fi
done < /etc/emfa/dmarcconfig

DBHOST=${name[1]}
DBUSER=${name[2]}
DBPASS=${name[3]}
DBNAME=${name[0]}

REPORTEMAIL='noreply@emfabox.org'
REPORTORG='emfabox.org'




HISTDIR='/var/spool/opendmarc'
HISTFILE='opendmarc'


# Make sure history file exists
touch ${HISTDIR}/${HISTFILE}.dat
 
# Move history file temp dir for processing
mv ${HISTDIR}/${HISTFILE}.dat /tmp/${HISTFILE}.$$
 
# Import temp history file data and send reports
#/usr/sbin/opendmarc-import -dbhost=${DBHOST} -dbuser=${DBUSER} -dbpasswd=${DBPASS} -dbname=${DBNAME} -verbose < /tmp/${HISTFILE}.$$
#/usr/sbin/opendmarc-reports -dbhost=${DBHOST} -dbuser=${DBUSER} -dbpasswd=${DBPASS} -dbname=${DBNAME} -verbose -interval=86400 -report-email ${REPORTEMAIL} -report-org ${REPORTORG}
#/usr/sbin/opendmarc-expire -dbhost=${DBHOST} -dbuser=${DBUSER} -dbpasswd=${DBPASS} -dbname=${DBNAME} -verbose
 


 
 
 # Delete temp history file
rm -rf *.$$ 