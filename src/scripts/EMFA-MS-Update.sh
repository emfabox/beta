#!/bin/bash

action="$1"

################################################################
#
# Variables
#
# MAX update delay in seconds
UPDATEMAXDELAY=1800
# Source files:
BADSRC="http://dl.emfabox.org/downloads/phishing.bad.sites.conf"
SAFESRC="http://dl.emfabox.org/downloads/phishing.safe.sites.conf"
BACKUP_DIR='/opt/emfa/backup'

#
# delay so we won't hammer the servers
#
function delay()
{
  sleeptime=$RANDOM
  let "sleeptime %= $UPDATEMAXDELAY"
  sleep $sleeptime
  run_updates
}
#
function safe_updates()
{
  sleeptime=$RANDOM
  let "sleeptime %= $UPDATEMAXDELAY"
  sleep $sleeptime
  update_safe
}
#
function bad_updates()
{
  sleeptime=$RANDOM
  let "sleeptime %= $UPDATEMAXDELAY"
  sleep $sleeptime
  update_bad
}
###

#
# Main function to Run updates
#
function run_updates()
{
  update_bad
  update_safe
}
#

#
# Update bad file
#
function update_bad()
{
  reload=1
  cd /etc/MailScanner
  wget -N $BADSRC
  if [ "$?" = "0" ]; then
        echo It completed okay.
		
        if [ -r /etc/MailScanner/phishing.bad.sites.conf.backup ]; then
                if [ phishing.bad.sites.conf -nt /etc/MailScanner/phishing.bad.sites.conf.backup ]; then
                        if ( tail -10 phishing.bad.sites.conf | grep -q '^#.*EOF' ); then
                                # echo It succeeded, so make a backup
                                cp -f phishing.bad.sites.conf ${BACKUP_DIR}/phishing.bad.sites.conf.backup
                         else
                                echo ERROR: Could not find EOF marker in phishing.bad.sites.conf
                                cp -f ${BACKUP_DIR}/phishing.bad.sites.conf.backup phishing.bad.sites.conf
                        fi
                else
                # Remote file not newer than local copy
                reload=0
                fi
        else
                # No backup file present, so delete file if it is bad
                if ( tail -10 phishing.bad.sites.conf | grep -q '^#.*EOF' ); then
                        #      echo Success, make a backup
                        cp -f phishing.bad.sites.conf ${BACKUP_DIR}/phishing.bad.sites.conf.backup
                 else
                        echo ERROR: Could not find EOF marker in phishing.bad.sites.conf and no backup
                        rm -f phishing.bad.sites.conf
                        reload=0
                fi
        fi
  else
        echo It failed to complete properly
        if [ -r ${BACKUP_DIR}/phishing.bad.sites.conf.backup ]; then
                echo Restored backup of phishing.bad.sites.conf
                cp -f ${BACKUP_DIR}/phishing.bad.sites.conf.backup phishing.bad.sites.conf
         else
                # No backup copy present, so delete bad phishing.bad.sites.conf
                echo ERROR: wget of phishing.bad.sites.conf failed and no backup
                rm -f phishing.bad.sites.conf
                reload=0
        fi
		if [ -f /etc/MailScanner/phishing.bad.sites.conf ]; then
		chmod 0644 /etc/MailScanner/phishing.bad.sites.conf
		fi
  fi
    
  # Reload MailScanner only if we need to.
  if [ "$reload" = "1" ]; then
        /etc/init.d/MailScanner reload #> /dev/null 2>&1
        if [ $? != 0 ] ; then
                echo "MailScanner reload failed - Retrying..."
            /etc/init.d/MailScanner reload
                if [ $? = 0 ] ; then
                        echo "MailScanner reload succeeded."
         else
                        echo "Stopping MailScanner..."
            /etc/init.d/MailScanner stop
            echo "Waiting for a minute..."
            sleep 60
            echo "Attemping to start MailScanner..."
            /etc/init.d/MailScanner start
        fi
        fi
  fi
}
#

#
# Update safe file
#
function update_safe()
{
  reload=1
  cd /etc/MailScanner
  wget -N $SAFESRC
  if [ "$?" = "0" ]; then
        echo It completed okay.
        if [ -r /etc/MailScanner/phishing.safe.sites.conf.backup ]; then
                if [ phishing.safe.sites.conf -nt /etc/MailScanner/phishing.safe.sites.conf.backup ]; then
                        if ( tail -10 phishing.safe.sites.conf | grep -q '^#.*EOF' ); then
                                # echo It succeeded, so make a backup
                                cp -f phishing.safe.sites.conf ${BACKUP_DIR}/phishing.safe.sites.conf.backup
                         else
                                echo ERROR: Could not find EOF marker in phishing.safe.sites.conf
                                cp -f ${BACKUP_DIR}/phishing.safe.sites.conf.backup phishing.safe.sites.conf
                        fi
                else
                # Remote file not newer than local copy
                reload=0
                fi
        else
                # No backup file present, so delete file if it is bad
                if ( tail -10 phishing.safe.sites.conf | grep -q '^#.*EOF' ); then
                        #      echo Success, make a backup
                        cp -f phishing.safe.sites.conf ${BACKUP_DIR}/phishing.safe.sites.conf.backup
                 else
                        echo ERROR: Could not find EOF marker in phishing.safe.sites.conf and no backup
                        rm -f phishing.safe.sites.conf
                        reload=0
                fi
        fi
  else
        echo It failed to complete properly
        if [ -r ${BACKUP_DIR}/phishing.safe.sites.conf.backup ]; then
                echo Restored backup of phishing.safe.sites.conf
                cp -f ${BACKUP_DIR}/phishing.safe.sites.conf.backup phishing.safe.sites.conf
         else
                # No backup copy present, so delete bad phishing.safe.sites.conf
                echo ERROR: wget of phishing.safe.sites.conf failed and no backup
                rm -f phishing.safe.sites.conf
                reload=0
        fi
		if [ -f /etc/MailScanner/phishing.safe.sites.conf ]; then
		chmod 0644 /etc/MailScanner/phishing.safe.sites.conf
		fi
  fi
    
  # Reload MailScanner only if we need to.
  if [ "$reload" = "1" ]; then
        /etc/init.d/MailScanner reload #> /dev/null 2>&1
        if [ $? != 0 ] ; then
                echo "MailScanner reload failed - Retrying..."
            /etc/init.d/MailScanner reload
                if [ $? = 0 ] ; then
                        echo "MailScanner reload succeeded."
         else
                        echo "Stopping MailScanner..."
            /etc/init.d/MailScanner stop
            echo "Waiting for a minute..."
            sleep 60
            echo "Attemping to start MailScanner..."
            /etc/init.d/MailScanner start
        fi
        fi
  fi
}
#

#
# Check if we are root
#
function user_check()
{
  if [ `whoami` != root ]
    then
                echo "[EMFA] Please become root to run this update"
                exit 0
  fi
}
#

# show the usage

function show_usage()
{
  echo "Usage: $0 [option]"
  echo "Where [option] is:"
  echo ""
  echo "-update"
  echo "   Update to the latest version"
  echo ""
  echo "-cron"
  echo "   run from cron"
  echo ""
  echo "-bad"
  echo "   run from bad phishing update cron"
  echo ""
  echo "-safe"
  echo "   run from bad phishing update cron"
  echo ""
  
}



# Parse action

function parse_action()
{
  case $action in
      -update)
        user_check
                run_updates
        ;;
          -cron)
        user_check
                delay
        ;;
        -bad)
        user_check
               bad_updates
        ;;
        -safe)
        user_check
               safe_updates
        ;;
      *)
        show_usage
        ;;
  esac
  exit 0
}
#

#
# Main function
#
function main()
{
  if [ "X${action}" == "X" ]
    then
      show_usage
      exit 0
    else
      parse_action
  fi
}
#

#
# Run main
#
main
# 