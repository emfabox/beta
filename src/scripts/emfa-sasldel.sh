#!/bin/bash
#
#This script will delete a SASL user from your host

if [ "$1" = "" ]; then
	echo "emfa-sasldel usage:"
	echo "emfa-sasldel username"
	echo ""
	exit 0
fi

if [ "$1" = "--help" ]; then
	echo "emfa-sasldel usage:"
	echo "emfa-sasldel username"
	echo ""
	exit 0
fi

saslpasswd2 -p -d -u `postconf -h myhostname` $1

