#!/bin/bash
#
#Syslog

export on=$1
export syslog_server=$2
if [ "$1" = "" ]; then
        echo "usage:"
        echo "emfa-syslog ON|OFF [address]"
        echo ""
        exit 0
fi
if [ "$1" = "ON" ]; then
        sed -i "/^# \*\.\*/ c\*.* @$syslog_server" /etc/rsyslog.conf
fi
if [ "$1" = "OFF" ]; then
        sed -i "/^\*\.\*/ c\# *.* @serveraddress" /etc/rsyslog.conf
fi
/etc/init.d/rsyslog restart