#!/bin/bash
#
#Set TimeZone Script
export tzone=$1
 
rm -f /etc/localtime
ln -s /usr/share/zoneinfo/$tzone /etc/localtime
echo "UTC=false">/etc/sysconfig/clock
echo "ARC=false">>/etc/sysconfig/clock
echo "ZONE=$tzone">>/etc/sysconfig/clock
sed -i "/^date.timezone =/ c\date.timezone = '$tzone'" /etc/php.ini