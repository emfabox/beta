#!/bin/bash
#
# Configure Avira Savapi Service

export action=$1
if [ "$1" = "" ]; then
        echo "usage:"
        echo "savapi-service on|off"
        echo ""
        exit 0
fi

chkconfig --level 345 savapi $action
if [ "$1" = "on" ]; then
  rm -rf /var/log/savapi.log
  rm -rf /var/log/avupdate.log	
fi
