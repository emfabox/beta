#!/bin/bash

# Check if we have messages older than 5 minutes in incoming queue
if [[ -n $(find /var/spool/postfix/hold/ -mmin +10 -not -path "/var/spool/postfix/hold/") ]]; then
    echo "$(date) - The following messages are older than 10 minutes waiting to be analyzed:">>/var/log/queue_age
    find /var/spool/postfix/hold/ -mmin +10 -not -path "/var/spool/postfix/hold/" >>/var/log/queue_age
fi