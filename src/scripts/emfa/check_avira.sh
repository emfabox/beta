#!/bin/bash
# 
hn=`hostname`
source /etc/emfa/variables.conf

# Check Avira
echo "SELECT value FROM ms_config WHERE external='virusscanners' AND hostname='$hn';" | mysql -uroot -p"${MYSQL_ROOTPASS}" -Dmailscanner 2>&1 | grep "savapi"  > /dev/null
if [ "$?" -ne "0" ]; then
	# Avira Config OFF
	echo "" > /dev/null 2>&1
else
	# Avira Config ON
	if [ ! "$(ps ax | grep savapi | grep -v grep)" ]; then
	    echo "SAVAPI Server Stopped - Restarting..." >> /var/log/messages
		/etc/init.d/savapi start > /dev/null 2>&1
	fi
fi