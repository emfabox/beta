#!/bin/bash
export action=$1
if [ "$1" = "" ]; then
        echo "usage:"
        echo "emfa-cluster-lsyncd on|off"
        echo ""
        exit 0
fi

chkconfig --level 345 lsyncd $action
if [ "$1" = "on" ]; then
  rm -rf /var/log/lsyncd.*
	/etc/init.d/lsyncd start
fi
if [ "$1" = "off" ]; then
        /etc/init.d/lsyncd stop
fi
