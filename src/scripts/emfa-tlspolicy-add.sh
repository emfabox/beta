#!/bin/bash
#
#emfa-tlspolicy-add

export domain=$1
export policy=$2
export attribute=$3
export compile=$4

if [ "$domain" = "" ]; then
	echo "emfa-tlspolicy-add usage:"
	echo "emfa-tlspolicy-add domain.tld tlspolicy attribute (- for none)"
	echo ""
	exit 0
fi
if [ "$policy" = "" ]; then
	echo "emfa-tlspolicy-add usage:"
	echo "emfa-tlspolicy-add domain.tld tlspolicy attribute (- for none)"
	echo ""
	exit 0
fi

if [ "$attribute" = "" ]; then
	echo "emfa-tlspolicy-add usage:"
	echo "emfa-tlspolicy-add domain.tld tlspolicy attribute (- for none)"
	echo ""
	exit 0
fi

if [ "$domain" = "--help" ]; then
	echo "emfa-tlspolicy-add usage:"
	echo "emfa-tlspolicy-add domain.tld tlspolicy attribute (- for none)"
	echo ""
	exit 0
fi

#Configure the tlspolicy map
domainescaped=`sed 's/[]\.|$(){}[?+*^]/\\\&/g' <<< $domain`
sed -i "/^$domainescaped/d" /etc/postfix/tls_policy
if [ "$attribute" = "-" ]; then
	echo "$domain $policy">>/etc/postfix/tls_policy
else
	echo "$domain $policy $attribute">>/etc/postfix/tls_policy
fi

if [ "$compile" != "NO" ]; then
	postmap /etc/postfix/tls_policy
	postfix reload
fi
