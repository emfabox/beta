#!/bin/bash
# Enable Extended Globs
shopt -s extglob
export LANG=en_US.UTF-8

# +---------------------------------------------------+
# Recover MySQL
# +---------------------------------------------------+
function func_recover-mysql() {
  echo -e ""
  echo -e "MySQL Daemon and Database Recovery"
  echo -e ""
  echo -e "Description:"
  echo -e "This tool will assist with mysql recovery"
  echo -e "after a system crash or power failure."
  echo -e ""
  echo -e "If recovery fails, it is recommended that"
  echo -e "you restore your mysql database from a"
  echo -e "recent backup."
  echo -e ""
  echo -e -n "$green[EMFA]$clean Do you wish to continue? (y/n): "
  read MYSQLQ
  flag=0
  while [ $flag == "0" ]
    do
      if [[ $MYSQLQ == "y" || $MYSQLQ == "Y" ]]; then
        echo -e ""
        echo -e "Beginning recovery..."

        echo -e ""
        echo -e "Stopping MySQL, MailScanner, and sqlgrey"
        service sqlgrey stop
        service MailScanner stop
        sleep 5
        service mysqld stop
        #killall -9 mysqld

        echo -e "Removing socket if present"
        rm -f /var/lib/mysql/mysql.sock

        echo -e "Performing MyISAM checks"
        myisamchk --force --fast --update-state --key_buffer_size=100M --sort_buffer_size=100M --read_buffer_size=1M --write_buffer_size=1M /var/lib/mysql/*/*.MYI
        echo "Press [enter] key to continue. . ."
        read enterKey

        echo -e "Attempting to start MySQL"
        service mysqld start

        echo -e "Performing additional database checks"
        mysqlcheck --repair --all-databases
        echo "Press [enter] key to continue. . ."
        read enterKey
       
        echo -e "Runing database optimization"
        mysqlcheck --optimize --all-databases
        echo "Press [enter] key to continue. . ."
        read enterKey 

        echo -e "Starting MailScanner and Greylisting if needed"
        service MailScanner start
        grey=`grep 'check_policy_service inet:127.0.0.1:2501' /etc/postfix/main.cf`
		if [ "$grey" != "" ]; then
			service sqlgrey start
		fi
		echo "Press [enter] key to continue. . ."
        read enterKey 

        flag=1
      elif [[ $MYSQLQ == "n" || $MYSQLQ == "N" ]]; then
        echo -e ""
        echo -e "Exiting..."
        sleep 1
        flag=1
      else
        echo -e "Choice $red\"$MYSQLQ\"$clean is not a valid choice."
        echo -e ""
        echo -e -n "$green[EMFA]$clean: "
        read MYSQLQ
      fi
    done
}
clear
echo ""
func_recover-mysql