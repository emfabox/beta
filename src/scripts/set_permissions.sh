#!/bin/bash
#

# mtagroup #
chmod 0640 /etc/postfix/main.cf
chown root:mtagroup /etc/my.cnf
chmod 0640 /etc/my.cnf
chown root:mtagroup /etc/sysconfig/iptables
chmod 0640 /etc/sysconfig/iptables
chown root:mtagroup /etc/httpd/conf/httpd.conf
chmod 0640 /etc/httpd/conf/httpd.conf
chown -R root:mtagroup /etc/MailScanner/reports/
chown postfix:mtagroup /var/spool/MailScanner
chown postfix:mtagroup /var/spool/MailScanner/incoming
chown postfix:mtagroup /var/spool/MailScanner/quarantine
chown postfix.mtagroup /var/spool/MailScanner/spamassassin
chown postfix:mtagroup /var/spool/postfix
chown postfix:mtagroup /var/spool/postfix/incoming
chown -R root:mtagroup /var/www/html
find /var/www/html -type d -exec chmod 0655 {} \;
find /var/www/html -type f -exec chmod 0644 {} \;

find /etc/MailScanner/reports/ -type d -exec chmod 0655 {} \;
find /etc/MailScanner/reports/ -type f -exec chmod 0664 {} \;

chmod g+w /var/spool/MailScanner/*
touch /var/spool/MailScanner/incoming/SpamAssassin.cache.db
chown postfix:mtagroup /var/spool/MailScanner/incoming/SpamAssassin.cache.db
touch /var/spool/MailScanner/incoming/Processing.db
chown postfix:mtagroup /var/spool/MailScanner/incoming/Processing.db

mkdir -p /var/spool/postfix/hold
chown postfix:mtagroup /var/spool/postfix/hold
chmod 0775 /var/spool/postfix/hold

#Postfix Dir
chown root:apache /etc/postfix/main.cf
chmod 664 /etc/postfix/main.cf
chown root:apache /etc/postfix/sasl_passwd
chmod 664 /etc/postfix/sasl_passwd

#Postfix OUT-Dir
chown root:apache /etc/postfix-out/main.cf
chmod 664 /etc/postfix-out/main.cf
#chown root:apache /etc/postfix/sasl_passwd
#chmod 664 /etc/postfix/sasl_passwd

#Policyd
chown root:apache /etc/python-policyd-spf/policyd-spf.conf
chmod 664 /etc/python-policyd-spf/policyd-spf.conf

chmod 755 /var/www/html/master/sbin/emfa-*
chmod 755 /var/www/html/master/sbin/*.php
chmod 755 /var/www/html/master/sbin/*.sh
chmod 755 /var/www/html/master/sbin/*.pl
chmod 664 /var/www/html/master/sbin/index.php

#GEOIP
chmod 777 /var/www/html/master/lib/temp

# CSV upload
chmod 755 /var/www/html/master/plugins/jquery-file-upload/server/upload/email/files
chown apache:apache  /var/www/html/master/plugins/jquery-file-upload/server/upload/email/files

chmod 755 /var/www/html/master/plugins/jquery-file-upload/server/upload/domain/files
chown apache:apache /var/www/html/master/plugins/jquery-file-upload/server/upload/domain/files

chmod 755 /var/www/html/master/plugins/jquery-file-upload/server/upload/relayhosts/files
chown apache:apache /var/www/html/master/plugins/jquery-file-upload/server/upload/relayhosts/files

#web update

chmod 755 /opt/emfa/update
chown apache:apache /opt/emfa/update
chmod 755 /opt/emfa/staging
chown apache:apache /opt/emfa/staging

