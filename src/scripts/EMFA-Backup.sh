#!/bin/bash
#

action="$1"

#
# Variables
#
BACKUPDIR=/opt/emfa/backup
VARDIR=/etc/emfa
MYSQLROOTPWD="`grep MYSQLROOTPWD /etc/DB-Config | sed 's/.*://'`"
TMPDIR="/opt/emfa/tmp"
DAYSTOKEEP=5
#

#
# Begin Backup Purge
#
function start_purge()
{

  echo "Purging Backups older than $DAYSTOKEEP..."
  # Do not purge if no recent backups are present
  if [[ -n `find ${BACKUPDIR} -type f -mtime -$((DAYSTOKEEP+1))` ]]; then
    find ${BACKUPDIR} -type f -mtime +$DAYSTOKEEP -exec rm -f {} \;
    echo "Old backups purged."
  else
    echo "No recent backups are present!  Skipping purge."
  fi
} 
#

#
# Begin Backup
#
function start_backup()
{
  # Get current date and time
  CDATE=`date +%m%d%Y`
  CTIME=`date +%H%M%S`

  echo "Beginning System Backup at $CDATE $CTIME"

  WORKINGDIR=$TMPDIR/$CDATE$CTIME/backup

  mkdir -p ${WORKINGDIR}

  # Perform SQL Dump
  mkdir -p ${WORKINGDIR}/sql
  mysqldump --user=root --password=$MYSQLROOTPWD --all-databases --events > ${WORKINGDIR}/sql/backup.sql
  
  # Backup Postfix Settings
  mkdir -p ${WORKINGDIR}/etc/postfix/ssl
  cp -a /etc/postfix/*cf ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/*access ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/transport ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/virtual ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/sasl_passwd ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/relocated ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/access ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/canonical ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/generic ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/header_checks ${WORKINGDIR}/etc/postfix
  cp -a /etc/postfix/ssl/smtpd.pem ${WORKINGDIR}/etc/postfix/ssl
  
  
   # Backup Postfix-out Settings
  mkdir -p ${WORKINGDIR}/etc/postfix-out/ssl
  cp -a /etc/postfix-out/*cf ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/*access ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/transport ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/virtual ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/sasl_passwd ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/relocated ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/access ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/canonical ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/generic ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/header_checks ${WORKINGDIR}/etc/postfix-out
  cp -a /etc/postfix-out/ssl/smtpd.pem ${WORKINGDIR}/etc/postfix-out/ssl
  
  

  # Backup DB-Config
  cp -a /etc/DB-Config ${WORKINGDIR}/etc
  
  # variables
  cp -a ${VARDIR}/variables.conf ${WORKINGDIR}/
  
  # Backup sysconfig
  mkdir ${WORKINGDIR}/etc/sysconfig/
  cp -ra /etc/sysconfig/* ${WORKINGDIR}/etc/sysconfig 
  
  # Backup portions of /etc
  cp -a /etc/passwd ${WORKINGDIR}/etc
  cp -a /etc/fstab ${WORKINGDIR}/etc
  cp -a /etc/shadow ${WORKINGDIR}/etc
  cp -a /etc/clamd.conf ${WORKINGDIR}/etc
  cp -a /etc/hosts* ${WORKINGDIR}/etc
  cp -a /etc/networks ${WORKINGDIR}/etc
  cp -a /etc/my.cnf ${WORKINGDIR}/etc
  cp -a /etc/php.ini ${WORKINGDIR}/etc
  cp -a /etc/resolv* ${WORKINGDIR}/etc
  cp -a /etc/sudo.conf ${WORKINGDIR}/etc
  if [ -f /etc/webmin.config ] ; then
  cp -a /etc/webmin.config ${WORKINGDIR}/etc
  fi
  
  #OPENDKIM
  cp -a /etc/opendkim.conf ${WORKINGDIR}/etc
  mkdir -p ${WORKINGDIR}/etc/opendkim/
  cp -ra /etc/opendkim/* ${WORKINGDIR}/etc/opendkim/
  
  
  # Backup MailScanner settings
  mkdir -p ${WORKINGDIR}/etc/MailScanner
  cp -ra /etc/MailScanner/* ${WORKINGDIR}/etc/MailScanner
  
  # Backup Cron
  cp -ra /etc/cron* ${WORKINGDIR}/etc

  # Backup SQLGrey
  mkdir -p ${WORKINGDIR}/etc/sqlgrey
  cp -a /etc/sqlgrey/* ${WORKINGDIR}/etc/sqlgrey
  
  # Backup SSH Config
  mkdir -p ${WORKINGDIR}/etc/ssh
  cp -a /etc/ssh/* ${WORKINGDIR}/etc/ssh

  # Backup pki
  mkdir -p ${WORKINGDIR}/etc/pki
  cp -ra /etc/pki/* ${WORKINGDIR}/etc/pki
  
  
  # Backup WEB
  mkdir -p ${WORKINGDIR}/var/www/html/
  cp -a /var/www/html/* ${WORKINGDIR}/var/www/html/
  
  # remove smarty temp files ...
  rm -rf ${WORKINGDIR}/var/www/html/cache/*
  rm -rf ${WORKINGDIR}/var/www/html/templates_c/*  

  # Backup 
  #mkdir -p ${WORKINGDIR}/var/www/html/master/functions/conf.php
  #cp -a /var/www/html/master/functions/conf.php ${WORKINGDIR}/var/www/html/master/functions/conf.php

  # Backup Apache
  mkdir -p ${WORKINGDIR}/etc/httpd
  cp -ra /etc/httpd/* ${WORKINGDIR}/etc/httpd

  # Gzip tarball the collection
  tar -cpzf $TMPDIR/backup-$CDATE-$CTIME.tar.gz -C $TMPDIR/$CDATE$CTIME .
  rm -rf $TMPDIR/$CDATE$CTIME
  mv $TMPDIR/backup-$CDATE-$CTIME.tar.gz ${BACKUPDIR}
  chmod 0600 ${BACKUPDIR}/backup-$CDATE-$CTIME.tar.gz

  echo "Backup Completed at `date +%m%d%Y\ %H%M%S`"
}
#

#
# Check if we are root
#
function user_check()
{
  if [ `whoami` == root ]
    then
      echo "[EMFA] Good you are root"
      if [[ ${action} == "-backup" ]]; then
        start_backup
      elif [[ ${action} == "-purge" ]]; then
        start_purge
      fi
  else
    echo "[EMFA] Please become root to run this backup"
    exit 0
  fi
}
#

#
# show the usage
#
function show_usage()
{
  echo "Usage: $0 [option]"
  echo "Where [option] is:"
  echo ""
  echo "-backup"
  echo "   Initiate backup"
  echo ""
  echo "-purge"
  echo "   Purge oldest backups"
  echo ""
}
#

#
# Parse action
#
function parse_action()
{
  case $action in
      -backup)
        user_check
        ;;
      -purge)
        user_check
        ;;
      *)
        show_usage
        ;;
  esac
  exit 0
}
#

#
# Main function
#
function main()
{
  if [ "X${action}" == "X" ]
    then
      show_usage
      exit 0
    else
      parse_action
  fi
}
#

#
# Run main
#
main
#
 