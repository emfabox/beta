#!/bin/bash
#
action="$1"
################################################################

#
# Variables
#
# MAX update delay in seconds
UPDATEMAXDELAY=1800
# Source files:
KAMSRC="http://www.peregrinehw.com/downloads/SpamAssassin/contrib/KAM.cf"
#
KAM_BACKUP_DIR=/opt/emfa/backup/KAM
#
# delay so we won't hammer the servers
#
function delay()
{
  sleeptime=$RANDOM
  let "sleeptime %= $UPDATEMAXDELAY"
  sleep $sleeptime
  run_updates
}
#

#
# Main function to Run updates
#
function run_updates()
{
  update_kam
}
#

#
# Update KAM file
#
function update_kam()
{
  reload=1
  cd /etc/mail/spamassassin
  wget -N $KAMSRC
  if [ "$?" = "0" ]; then
        echo It completed okay.
        if [ -r ${KAM_BACKUP_DIR}/KAM.cf.backup ]; then
                if [ KAM.cf -nt ${KAM_BACKUP_DIR}/KAM.cf.backup ]; then
                        if ( tail -10 KAM.cf | grep -q '^#.*EOF' ); then
                                # echo It succeeded, so make a backup
                                cp -f KAM.cf ${KAM_BACKUP_DIR}/KAM.cf.backup
                         else
                                echo ERROR: Could not find EOF marker in KAM.cf
                                cp -f ${KAM_BACKUP_DIR}/KAM.cf.backup KAM.cf
                        fi
                else
                # Remote file not newer than local copy
                reload=0
                fi
        else
                # No backup file present, so delete file if it is bad
                if ( tail -10 KAM.cf | grep -q '^#.*EOF' ); then
                        #      echo Success, make a backup
                        cp -f KAM.cf ${KAM_BACKUP_DIR}/KAM.cf.backup
                 else
                        echo ERROR: Could not find EOF marker in KAM.cf and no backup
                        rm -f KAM.cf
                        reload=0
                fi
        fi
  else
        echo It failed to complete properly
        if [ -r ${KAM_BACKUP_DIR}/KAM.cf.backup ]; then
                echo Restored backup of KAM.cf
                cp -f ${KAM_BACKUP_DIR}/KAM.cf.backup KAM.cf
         else
                # No backup copy present, so delete bad KAM.cf
                echo ERROR: wget of KAM.cf failed and no backup
                rm -f KAM.cf
                reload=0
        fi
  fi
    
  # Reload MailScanner only if we need to.
  if [ "$reload" = "1" ]; then
        /etc/init.d/MailScanner reload #> /dev/null 2>&1
        if [ $? != 0 ] ; then
                echo "MailScanner reload failed - Retrying..."
            /etc/init.d/MailScanner reload
                if [ $? = 0 ] ; then
                        echo "MailScanner reload succeeded."
         else
                        echo "Stopping MailScanner..."
            /etc/init.d/MailScanner stop
            echo "Waiting for a minute..."
            sleep 60
            echo "Attemping to start MailScanner..."
            /etc/init.d/MailScanner start
        fi
        fi
  fi
}
#

#
# Check if we are root
#
function user_check()
{
  if [ `whoami` != root ]
    then
                echo "[EMFA] Please become root to run this update"
                exit 0
  fi
}
#

#
# show the usage
#
function show_usage()
{
  echo "Usage: $0 [option]"
  echo "Where [option] is:"
  echo ""
  echo "-update"
  echo "   Update to the latest version"
  echo ""
  echo "-cron"
  echo "   run from cron"
  echo ""
  
}
#

#
# Parse action
#
function parse_action()
{
  case $action in
      -update)
        user_check
                run_updates
        ;;
          -cron)
        user_check
                delay
        ;;
      *)
        show_usage
        ;;
  esac
  exit 0
}
#

#
# Main function
#
function main()
{
  if [ "X${action}" == "X" ]
    then
      show_usage
      exit 0
    else
      parse_action
  fi
}
#

#
# Run main
#
main
#
 