package Mail::SpamAssassin::Plugin::sender_reputation_score;
use Mail::SpamAssassin::Plugin;
use Mail::SpamAssassin::Logger;
use DBI;
use Net::DNS;
our @ISA = qw(Mail::SpamAssassin::Plugin);

sub new {
   my ($class, $mailsa) = @_;

   # the usual perlobj boilerplate to create a subclass object
   $class = ref($class) || $class;
   my $self = $class->SUPER::new($mailsa);
   bless ($self, $class);
 
   # add rule
   $self->register_eval_rule ("check_sender_reputation");
  
   return $self;                               
}

sub check_sender_reputation
{
   my ($self, $pms) = @_;

   # Extract desidered informations from mail header   
   $head = $pms->get('Received-SPF');
   chomp($head);
   # Search for IP in header string
   $ip="0.0.0.0";
   $ip = $1 if $head =~ /(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/;
   dbg ("IP_REPUTATION_CHECK: Received from IP: $ip");
   my ( $a, $b, $c, $d ) = split /\./, $ip;
   my $host = "$d.$c.$b.$a.rf.senderbase.org";
   dbg ("IP_REPUTATION_CHECK: Checking host $host");
   my $res = Net::DNS::Resolver->new;
   #$res->nameservers( '0.0.0.0' );
   $res->udp_timeout(3);
   $res->tcp_timeout(3);
   
   # Important to specify a 'TXT' query
   my $query = $res->search( $host, 'TXT' ) or return 0;

   foreach my $rr ( $query->answer ) {
   next unless $rr->type eq 'TXT';
	   $score = $rr->txtdata;
   }
   if ($score < 0) {
	   $sascore = $score * '-1';
   }
   if ($score >= 0) {
	   $sascore = '0';
   }
   dbg ("IP_REPUTATION_CHECK: Sender IP has reputation ($score)");
   #optional: change the description for report and summary
   #my $description = $pms->{conf}->{descriptions}->{IP_REPUTATION};
   #$description = "Sender IP has reputation ($score)";  
   #$pms->{conf}->{descriptions}->{IP_REPUTATION} = $description;

    #If the score is 0, then you don't need to do anything (obviously)
    if($score) {
        #The magic call
        $pms->got_hit("IP_REPUTATION_CHECK", "HEADER: ", score => $sascore);

        #Yet another magic call
        for my $set (0..3) {
            $pms->{conf}->{scoreset}->[$set]->{"IP_REPUTATION_CHECK"} = sprintf("%0.3f", $sascore);
        }
    }
   
   return 0;
}

1;
