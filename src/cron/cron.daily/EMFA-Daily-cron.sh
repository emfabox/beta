#!/bin/bash
# +--------------------------------------------------------------------+
# EMFA Daily cron
# +--------------------------------------------------------------------+
# Copyright (C) 2015 http://emfabox.com
# +--------- Minute (0-59)                    | Output Dumper: >/dev/null 2>&1
# | +------- Hour (0-23)                      | Multiple Values Use Commas: 3,12,47
# | | +----- Day Of Month (1-31)              | Do every X intervals: */X  -> Example: */15 * * * *  Is every 15 minutes
# | | | +--- Month (1 -12)                    | Aliases: @reboot -> Run once at startup; @hourly -> 0 * * * *;
# | | | | +- Day Of Week (0-6) (Sunday = 0)   | @daily -> 0 0 * * *; @weekly -> 0 0 * * 0; @monthly ->0 0 1 * *;
# | | | | |                                   | @yearly -> 0 0 1 1 *;

/usr/local/sbin/EMFA-SA-Update -cron >> /var/log/emfa/sa-update.log 2>&1
/usr/local/sbin/EMFA-MS-Update -cron >> /var/log/emfa/ms-update.log 2>&1



 