#!/bin/bash
# SMTP-IN

if [[ -n $(find /var/spool/postfix/deferred/ -type f -exec grep -l 'Undelivered Mail Returned to Sender' '{}' \;) ]]; then
   find /var/spool/postfix/deferred/ -type f -exec grep -l 'Undelivered Mail Returned to Sender' '{}' \; | xargs -n1 basename | xargs -n1 postsuper -d >/dev/null 2>&1
fi

# SMTP-OUT
if [[ -n $(find /var/spool/postfix/deferred/ -type f -exec grep -l 'Undelivered Mail Returned to Sender' '{}' \;) ]]; then
   find /var/spool/postfix/deferred/ -type f -exec grep -l 'Undelivered Mail Returned to Sender' '{}' \; | xargs -n1 basename | xargs -n1 postsuper -c /etc/postfix-out/ -d >/dev/null 2>&1
fi

 
 