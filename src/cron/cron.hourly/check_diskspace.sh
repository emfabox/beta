#!/bin/bash
#Alert is at 85%

ADMIN="root"
# set alert-level 85 % standard
ALERT=85
df -P /dev/mapper/vg_00-lv_var | grep -vE '^Filesystem|tmpfs|cdrom' | awk '{ print $5 " " $6 }' | while read output;
do
  usep=$(echo $output | awk '{ print $1}' | cut -d'%' -f1  )
  partition=$(echo $output | awk '{ print $2 }' )
if [ $usep -ge $ALERT ]; then
    echo "Quarantine free space is getting low! Partition is $usep% used, on server $(hostname) at $(date)" |
    mail -s "Quarantine Alert: Quarantine free space low, $usep % used on $partition" $ADMIN
  fi
done 