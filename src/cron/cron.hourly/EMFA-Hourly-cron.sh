SHELL=/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
#
/usr/local/sbin/EMFA-MS-Update -bad >> /var/log/emfa/EMFA-MS-Update.log 2>&1
#
/usr/local/sbin/emfa/emfa_update_relay.sh >/dev/null 2>&1
#
/usr/bin/php /var/www/html/master/sbin/emfa-sa_rules_update.php >/dev/null 2>&1 
#LDAP
/usr/bin/php /var/www/html/master/sbin/emfa-ldap-cron.php >/dev/null 2>&1
#
#httpd
if [ "$(pidof httpd)" ]; then
   exit 0
else
/etc/init.d/MailScanner stop >/dev/null 2>&1
/sbin/fuser -k -n tcp 80 >/dev/null 2>&1
/etc/init.d/httpd start >/dev/null 2>&1
/etc/init.d/MailScanner start >/dev/null 2>&1
fi
#
#check mail_queue
# SMTP-IN
if [[ -n $(find /var/spool/postfix/deferred/ -type f -exec grep -l 'Undelivered Mail Returned to Sender' '{}' \;) ]]; then
   find /var/spool/postfix/deferred/ -type f -exec grep -l 'Undelivered Mail Returned to Sender' '{}' \; | xargs -n1 basename | xargs -n1 postsuper -d >/dev/null 2>&1
fi
# SMTP-OUT
if [[ -n $(find /var/spool/postfix/deferred/ -type f -exec grep -l 'Undelivered Mail Returned to Sender' '{}' \;) ]]; then
   find /var/spool/postfix/deferred/ -type f -exec grep -l 'Undelivered Mail Returned to Sender' '{}' \; | xargs -n1 basename | xargs -n1 postsuper -c /etc/postfix-out/ -d >/dev/null 2>&1
fi
#
#check diskspace
#Alert is at 85%
ADMIN="root"
# set alert-level 85 % standard
ALERT=85
df -P /dev/mapper/vg_var-lv_var | grep -vE '^Filesystem|tmpfs|cdrom' | awk '{ print $5 " " $6 }' | while read output;
do
  usep=$(echo $output | awk '{ print $1}' | cut -d'%' -f1  )
  partition=$(echo $output | awk '{ print $2 }' )
if [ $usep -ge $ALERT ]; then
    echo "Quarantine free space is getting low! Partition is $usep% used, on server $(hostname) at $(date)" |
    mail -s "Quarantine Alert: Quarantine free space low, $usep % used on $partition" $ADMIN
  fi
done 
#
#opendmarc
if [ ! -f /opt/opendmarc.dat ] ; then
/etc/init.d/opendmarc stop >/dev/null 2>&1 
touch  /opt/opendmarc.dat
chown opendmarc:opendmarc  /opt/opendmarc.dat
/etc/init.d/opendmarc start >/dev/null 2>&1
fi

 