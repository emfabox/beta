#!/bin/bash
#
# remove old unused sendmailanalyzer data

/bin/find /usr/local/sendmailanalyzer/data -type d -ctime +30 -exec rm -rf {} \;