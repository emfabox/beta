Comunicazione dal servizio di Protezione della posta dai Virus "EMFABox"
----------------------------------------------------------------------------
L'allegato originale "$filename"
e' considerato non accettabile da questo sito ed e' stato sostituito da questo messaggio di avviso.

Se volete ricevere una copia dell'allegato originale, contattate il vostro
helpdesk, includendo una copia integrale di questo messaggio.

Il giorno $date il sistema antivirus riporta:
$report
Note per l' Help Desk: controllate su $hostname in $quarantinedir/$datenumber (message $id).
-- 
EMFABox 
Enterprise Mail Firewall Appliance
%org-long-name%
%web-site%
