Meddelande från EMFABox E-Mail Virus Protection Service
-----------------------------------------------------------
Bilagan "$filename" 
har verifierats som virussmittad och har ersatts med den
här varningstexten.

Av sekretesskäl har inga kopior av bilagorna sparats.

Den $date genererades följande meddelande:
$report
-- 
EMFABox 
Enterprise Mail Firewall Appliance
%org-long-name%
%web-site%
